//
//  accessLogger.swift
//  Pods
//
//  Created by Victor Barskov on 05.08.16.
//
//

import Foundation
import UIKit
import CocoaLumberjack


//let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
//let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
//let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("failedNetworks.json")


public class AccessLogger: NSObject {
    
    
    var BaseURL = "https://www.wifiasyougo.com/api/" // Prod
    
    public class var sharedInstance: AccessLogger {
        struct Singleton {
            static let instance = AccessLogger()
        }
        return Singleton.instance
    }
    
    let accessSession: URLSession = {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = false
        config.timeoutIntervalForRequest = 90
        config.timeoutIntervalForResource = 90
        return URLSession(configuration: config)
    }()
    
    // Success request
    public func successAccess(pointId: String, latitude: Double, longitude: Double) {
        
        var _: [String: Any] = ["content":["pointid":pointId, "lat": latitude, "lon": longitude]]
        
        let params : [String:AnyObject] = ["pointid": pointId as AnyObject, "latitude":latitude as AnyObject, "longitude":longitude as AnyObject]
        
        let request = NSMutableURLRequest(url: NSURL(string: BaseURL + "wifi/successaccess")! as URL)
        request.addAitaAuth()
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: params, options:JSONSerialization.WritingOptions(rawValue: 0)) as NSData
            request.httpBody = jsonData as Data
        } catch let error as NSError{
            DDLogDebug("Error has been occured with NSJSONSerialization: \(error.localizedDescription)")
        }
        
        accessSession.dataTask(with: request as URLRequest) { data, response, error in
            
            if let data = data {
                let json = JSON(data: data)
                DispatchQueue.main.async {
                    DDLogDebug("Data on success response accessLogger \(json)")
                }
            } else {
                DDLogDebug("Error has been occured")
            }
            }.resume()
    }
    
    public func unSuccessAccess(pointId: String, latitude: Double, longitude: Double) {
        
        let params : [String:AnyObject] = ["pointid": pointId as AnyObject, "latitude":latitude as AnyObject, "longitude":longitude as AnyObject]
        
        let request = NSMutableURLRequest(url: NSURL(string: BaseURL + "wifi/unsuccessaccess")! as URL)
        request.addAitaAuth()
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: params, options:JSONSerialization.WritingOptions(rawValue: 0)) as NSData
            request.httpBody = jsonData as Data
        } catch let error as NSError{
            DDLogDebug("Error has been occured with NSJSONSerialization: \(error.localizedDescription)")
        }
        
        accessSession.dataTask(with: request as URLRequest) { data, response, error in
            
            if let data = data {
                let json = JSON(data: data)
                DispatchQueue.main.async {
                    DDLogDebug("Data on UNNNNNsuccess response accessLogger \(json)")
                }
            } else {
                DDLogDebug("Error has been occured")
            }
            }.resume()
    }
    
}


public class failedHotspot: NSObject, NSCoding {

    var point: String
    var lat: Double
    var lon: Double
    
    init(pointID: String, latitude: Double, longitude: Double) {
        self.point = pointID
        self.lat = latitude
        self.lon = longitude
    }
    
    //MARK: NSCoding
    
    required public init(coder aDecoder: NSCoder) {
        self.point = aDecoder.decodeObject(forKey: "point") as! String
        self.lat = aDecoder.decodeObject(forKey: "lat") as! Double
        self.lon = aDecoder.decodeObject(forKey: "lon") as! Double
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(point, forKey: "point")
        aCoder.encode(lat, forKey: "lat")
        aCoder.encode(lon, forKey: "lon")
    }
    
//    public func encodeWithCoder(aCoder: NSCoder) {
//        aCoder.encode(point, forKey: "point")
//        aCoder.encode(lat, forKey: "lat")
//        aCoder.encode(lon, forKey: "lon")
//    }
    
}




