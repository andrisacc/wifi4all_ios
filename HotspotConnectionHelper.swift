//
//  HotspotConnectionHelper.swift
//  WifiHotspot
//
//  Created by Sergey Pronin on 8/7/15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

import Foundation
import NetworkExtension
import SystemConfiguration.CaptiveNetwork
import Kanna
import CocoaLumberjack

let URLGenerate204 = NSURL(string: BaseURLInsecured + "generate_204")!

let HotspotHelperPresentUINotification = "HotspotHelperPresentUINotification"
public let HotspotHelperPaymentNeededNotification = "HotspotHelperPaymentNeededNotification"
let HotspotHelperConfigurationKey = "HotspotHelperConfigurationKey"

let WISPrUserAgent = "WISPR!Trustive"

let SharedSession: URLSession = {
    let config = URLSessionConfiguration.default
    config.timeoutIntervalForResource = 90
    config.timeoutIntervalForRequest = 90
    return URLSession(configuration: config)
}()


public class HotspotConnectionHelper: NSObject {
    
    //workaround for 8.0 and 9.0 compatibility
    private var _command: AnyObject!
    
    @available(iOS 9.0, *)
    var command: NEHotspotHelperCommand! {
        get { return _command as! NEHotspotHelperCommand }
        set { _command = newValue }
    }
    
    let config: WifiConfiguration
    var username: String
    var password: String
    let userAgent: String
    
    var cindex = 0
    
    lazy var HotspotSession: URLSession = {
        
        let config = URLSessionConfiguration.default
        config.protocolClasses = [CaptiveURLProtocol.self]
        config.allowsCellularAccess = false
        config.timeoutIntervalForResource = 90
        config.timeoutIntervalForRequest = 90
        return URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        
    }()
    
    @available(iOS 9.0, *)
    convenience init(command: NEHotspotHelperCommand, configuration: WifiConfiguration) {
        self.init(configuration: configuration)
        self.command = command
    }
    
    public init(configuration: WifiConfiguration) {
        self.config = configuration
        let credentials = HotspotCredentials.credentialsForSSIDRegex(regex: configuration.SSIDregex)
        self.username = credentials.username
        self.password = credentials.password
        self.userAgent = WISPrUserAgent
    }
    
    var wisprEnabled: Bool {
        return config.wisprEnabled
    }
    
    func checkConnectionReachability(completion: @escaping (Bool) -> Void) {
        let request = NSMutableURLRequest(url: URLGenerate204 as URL)
        if #available(iOS 9.0, *) {
            request.bind(to: command)
        }
        
        HotspotSession.dataTask(with: request as URLRequest) { data, response, error in
            
            if error != nil {
//                DDLogDebug("\(error.description)")
                completion(false)
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(false)
                return
            }
            
            if response.statusCode == 204 {
                completion(true)
            } else {
                completion(false)
            }
            
        }.resume()
    }
    
    public func connect(connectURL: NSURL? = nil) {
        
        let url = connectURL ?? URLGenerate204
        
//        DDLogDebug("connecting to -> " + url.absoluteString)

        let request = NSMutableURLRequest(url: url as URL)
        if #available(iOS 9.0, *) {
            request.bind(to: command)
//            DDLogDebug("connect \(command.network!.SSID)")
        }
//        DDLogDebug("connect w/ useragent \(userAgent)")
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        let task = HotspotSession.dataTask(with: request as URLRequest) { data, response, error in
            
            if error != nil {
                
//                DDLogDebug("connect error \(error.description)")
                self.deliverHotspotStatus(connected: false)
                
                return
            }
            
            guard let response = response as? HTTPURLResponse else { return }
            
            if response.statusCode == 204 {
//                Logger.logMessage("connect internet reachable")
//                DDLogDebug("connect internet reachable")
                self.deliverHotspotStatus(connected: true)
                
            } else {
                guard let data = data, let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else {
                    self.checkConnectionReachability { reachable in
                        self.deliverHotspotStatus(connected: reachable)
                    }
                    return
                }
                
//                Logger.logPage(string as String)
//                DDLogDebug(string as String)
                
                if self.wisprEnabled {
                    if let nextURL = HotspotConnectionHelper.WISPrDetectProxy(html: string as String) {
//                        Logger.logMessage("connect wisper redirect")
                        DDLogDebug("connect wisper redirect")
                        self.connect(connectURL: nextURL)
                        return
                    } else if let _ = HotspotConnectionHelper.WISPrParseLoginURL(html: string as String) {
//                        Logger.logMessage("connect wisper login")
                        DDLogDebug("connect wisper login")
                        self.login(html: string as String)
                        return
                    }
                }
                
                if let redirect = HotspotConnectionHelper.extractRedirectURLFromHTMLResponse(html: string as NSString) {
//                    Logger.logMessage(message: "connect nowisper redirect")
//                    DDLogDebug("connect nowisper redirect")
                    self.connect(connectURL: redirect)
                } else {
                    if self.config.url != nil {
                        Logger.logMessage(message: "connect nowisper login")
//                        DDLogDebug("connect nowisper login")
                        self.login(html: string as String)
                    } else {
                        Logger.logMessage(message: "connect nowisper no url UI")
//                        DDLogDebug("connect nowisper no url UI")
                        self.deliverPresentUIStatus(message: "no config")
                    }
                }
            }
        }
        
        task.resume()
    }
    
    
    func __writeHTMLFile(html: String) {
        let fileManager = FileManager.default
        
        let docsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = docsURL.appendingPathComponent("\(NSDate().timeIntervalSince1970).html")
        
        do {
            try html.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        } catch { }
    }
    
    private func login(html: String? = nil) {
        let loginURL: NSURL?
        var body: String?
        
        if wisprEnabled {
//            DDLogDebug("login wisper \(username) \(password)")
            loginURL = HotspotConnectionHelper.WISPrParseLoginURL(html: html!)
            body = [
                "UserName": username,
                "Password": password,
                "button": "Login",
                "FNAME": "0",
                "OriginatingServer": "http://google.com",
                "WISPrVersion": "2.0"
            ].URLEncodedString
        } else {
            loginURL = NSURL(string: config.url)
//            DDLogDebug("login no wisper \(username) \(password) \(loginURL?.absoluteString)")
            var bodyDict = [String: String]()
            //getting username and password key names
            bodyDict[config.params["username"].string!] = username
            bodyDict[config.params["password"].string!] = password
            
            //just extra params with no meaning :)
            if let extras = config.params["extras"].dictionary {
                for (k, v) in extras {
                    bodyDict[k] = v.string!
                }
            }
            
            //extracting params from page (or meta-http redirect value string)
            if let extract = config.params["extract"].dictionary {
                if let query = extract["query"]?.array {
                    for param in query.map({ $0.string! }) {
                        bodyDict[param] = HotspotConnectionHelper.extractParamFromHTMLResponse(html: html! as NSString, paramName: param)
                    }
                }
                if let form = extract["form"]?.array {
                    guard let doc = Kanna.HTML(html: html!, encoding: String.Encoding.utf8) else { return }
                    
                    for param in form.map({ $0.string! }) {
                        let nodes = doc.css("input[name=\"\(param)\"]")
                        if nodes.count > 0 {
                            if let value = nodes[0]["value"] {
                                bodyDict[param] = value
                            }
                        }
                    }
                }
            }
            body = bodyDict.URLEncodedString
        }
        
        guard let url = loginURL else { return }
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "post"
//        DDLogDebug("body - " + body!)
        request.httpBody = body?.data(using: String.Encoding.utf8)
        
        if #available(iOS 9.0, *) {
            request.bind(to: command)
        }
        
        if wisprEnabled {
            request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
//            DDLogDebug("login w/ useragent \(userAgent)")
        }
        
        HotspotSession.dataTask(with: request as URLRequest) { data, response, error in
            
            if error != nil {
//                DDLogDebug("login request error \(error.description)")
                if self.wisprEnabled {
                    self.deliverHotspotStatus(connected: false)
                } else {
                    self.deliverPresentUIStatus(message: "login request error")
                }
                return
            }
            
            guard let response = response as? HTTPURLResponse else { return }
            
            guard let data = data, let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else {
                if response.statusCode == 200 {
                    self.checkConnectionReachability { reachable in
                        Logger.logMessage(message: "login response 200 reachable \(reachable)")
//                        DDLogDebug("login response 200 reachable \(reachable)")
                        if !reachable {
                            self.deliverHotspotStatus(connected: true)
                        } else {
                            if self.wisprEnabled {
                                self.deliverHotspotStatus(connected: false)
                            } else {
                                self.deliverPresentUIStatus(message: "response 200 false")
                            }
                        }
                    }
                } else {
//                    DDLogDebug("login response \(response.statusCode)")
                    if self.wisprEnabled {
                        self.deliverHotspotStatus(connected: false)
                    } else {
                        self.deliverPresentUIStatus(message: "response \(response.statusCode)")
                    }
                }
                return
            }
//            DDLogDebug("LogPage - " + (string as String))
            
            if self.wisprEnabled {
                let wisprResult = HotspotConnectionHelper.WISPrCheckLoginResult(html: string as NSString)
                if wisprResult.success {
                    Logger.logMessage(message: "login wisper true")
//                    DDLogDebug("Login wisper true + \(wisprResult.message)")
                    
                    self.deliverHotspotStatus(connected: true)
                    self.deliverStatusNotification(message: NSLocalizedString("Connection was successful", comment: ""))
                    return
                    
                } else {
                    
//                    DDLogDebug("In_Login_WISPrCheckLoginResult_fail + \(wisprResult.message)")
                    
                    if wisprResult.message == "Sorry, time is over" {
                        self.sendNeedsPaymentNotification(notice: NSLocalizedString("Sorry, free time is up. To enjoy unlimited usage please purchase monthly subscription!", comment: ""))
                    } else if wisprResult.message == "Sorry, game is over" {
                        self.sendNeedsPaymentNotification(notice: NSLocalizedString("Your free data in commercial networks expired. To enjoy unlimited usage please purchase monthly subscription!", comment: ""))
                    }
                }
            }
            
            if let redirectURL = HotspotConnectionHelper.extractRedirectURLFromHTMLResponse(html: string), !self.wisprEnabled {
//                DDLogDebug("login redirect")
                self.connect(connectURL: redirectURL)
            } else {
                self.checkConnectionReachability { reachable in
//                    Logger.logMessage("login reachable \(reachable)")
//                    DDLogDebug("login reachable \(reachable)")
                    if reachable {
                        self.deliverHotspotStatus(connected: true)
                    } else {
                        if self.wisprEnabled {
                            self.deliverHotspotStatus(connected: false)
                        } else {
                            self.deliverPresentUIStatus(message: "response false")
                        }
                    }
                }
            }
        }.resume()
    }
    
    //MARK: Status notifications
    private func deliverHotspotStatus(connected: Bool) {
        
        Logger.flushMessages()
        
        if connected {
            if #available(iOS 9.0, *) {
                command.createResponse(.success).deliver()
            } else {
                CNMarkPortalOnline("en0" as CFString)
            }
            
            HotspotCredentials.updateCredentials()
            reportCurrentSessionStatus()
            Logger.sendLogs()
            
//            Logger.logEvent("wifi_connect_success")
            
//            DDLogDebug("wifi_connect_success")
            
            
        } else {
            if #available(iOS 9.0, *) {
                command.createResponse(.failure).deliver()
            } else {
                CNMarkPortalOffline("en0" as CFString)
            }
            Logger.sendLogs()
        }
    }
    
    private func deliverStatusNotification(message: String) {
        Logger.flushMessages()
        
        DispatchQueue.main.async {
            let notification = UILocalNotification()
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            notification.alertBody = message
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
    
    private func deliverPresentUIStatus(message: String) {
        
        Logger.flushMessages()
        
        DispatchQueue.main.async {
            let notification = UILocalNotification()
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            var dict = self.config.json.dictionaryObject!
            dict.removeValue(forKey: "url")
            let keys = [String](dict.keys)
            for key in keys {
                if dict[key] is NSNull {
                    dict.removeValue(forKey: key)
                }
            }
            notification.userInfo = [
                HotspotHelperPresentUINotification: true,
                HotspotHelperConfigurationKey: dict
            ]
            notification.alertBody = NSLocalizedString("Open app to continue authorization", comment: "")
            if #available(iOS 8.2, *) {
                notification.alertTitle = NSLocalizedString("Network authorization", comment: "")
            }
            UIApplication.shared.scheduleLocalNotification(notification)
            
            if #available(iOS 9.0, *) {
                self.command.createResponse(.uiRequired).deliver()
            }
        }
    }
    
    
    // MARK: User Notifications
    private func requestNotificationsAccess() {
        
        let settings = UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
    }
    
    private func sendNeedsPaymentNotification(notice: String) {
        
        if let aitaToken = UserDefaults.standard.object(forKey: AitaToken) as? String {
            Logger.logMessage(message: "token \(aitaToken)")
//            DDLogDebug("token \(aitaToken)")
        }
        
        Logger.flushMessages()
        
        DispatchQueue.main.async {
            let notification = UILocalNotification()
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            notification.userInfo = [
                HotspotHelperPaymentNeededNotification: true,
            ]
            notification.alertBody = NSLocalizedString(notice, comment: "")
            if #available(iOS 8.2, *) {
                notification.alertTitle = NSLocalizedString("Subscribe", comment: "")
            }
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }

}


//MARK: - NSURLSessionDelegate
extension HotspotConnectionHelper: URLSessionTaskDelegate {
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        //        Logger.logMessage(message: "helper willPerformHTTPRedirection \(response.allHeaderFields)")
        //        DDLogDebug("helper willPerformHTTPRedirection \(response.allHeaderFields)")
        if config.params["follow_redirects"].bool == true {
            completionHandler(request)
        } else {
            completionHandler(nil)
        }
    }
}


//MARK: - Extraction
extension HotspotConnectionHelper {
    /**
        Extracts parameter from meta-http redirect found in the source code of a webpage.
        
        - parameter html:       html source code of the captive page
        - parameter paramName:  name of the parameter to look for
        
        - returns: param value
    */
    class func extractParamFromHTMLResponse(html: NSString, paramName: String) -> String? {
        guard let url = extractRedirectURLFromHTMLResponse(html: html) else { return nil }
        
        guard let params = url.query?.components(separatedBy: "&") else { return nil }
        
        for param in params {
            if param.hasPrefix("\(paramName)=") {
                
                // get startIndex of rangeOfString("=")
                let starty = param.range(of: "=")!.lowerBound
                // get string from range started from lower bounf of the range
                let newRangeString = param.substring(from: starty)
                // get string after successor '='
                let successorString = newRangeString.index(after: newRangeString.startIndex)
                return param.substring(from: successorString)
//                return param.substringFromIndex(param.rangeOfString("=")!.startIndex.successor())
            }
        }
        
        return nil
    }
    
    /**
        Return redirect URL from `META HTTP-EQUIV="Refresh"` found in a webpage sourcecode
    
        Example:
        ```
        <META HTTP-EQUIV="Refresh" CONTENT="0;url=http://google.com/">
        ```
        
        - parameter html:       html source code of the captive page
        
        - returns: redirect url
    */
    class func extractRedirectURLFromHTMLResponse(html: NSString) -> NSURL? {
        
        let startRange = html.range(of: "<META http-equiv=\"refresh\"", options: .caseInsensitive)
        guard startRange.location != NSNotFound else { return nil }
        
        let endRange = html.range(of: ">", options: .caseInsensitive, range: NSRange(location: startRange.location, length: html.length - startRange.location))
//        let endRange = html.rangeOfString(">", options: .CaseInsensitiveSearch, range: NSRange(location: startRange.location, length: html.length - startRange.location))
        
        let range = NSRange(location: startRange.location, length: endRange.location - startRange.location + 1)
        let meta = html.substring(with: range) as NSString
        
        let urlRange = meta.range(of: "url=", options: .caseInsensitive)
        
        var param = meta.substring(from: urlRange.location + urlRange.length) as NSString
        param = param.substring(to: param.range(of: "\"").location) as NSString
        if param.range(of: "noscript").length > 0 {
            return nil
        } else {
            return NSURL(string: param as String)
        }
    }
    
    
    //MARK: WISPr
    
    /**
        Looking for `WISPAccessGatewayParam` XML (according to WISPr 1.0) inside HTML Source.
        See [wikipedia](https://en.wikipedia.org/wiki/WISPr) for more info.
        
        - parameter html:   html source code of the captive page
        
        - returns: xml string
    */
    
    class func WISPrExtractFromHTMLResponse(html: NSString) -> String? {
        let startRange = html.range(of: "<WISPAccessGatewayParam")
        let endRange = html.range(of: "</WISPAccessGatewayParam>")
        
        guard startRange.location != NSNotFound && endRange.location != NSNotFound else { return nil }
        
        let string = html.substring(with: NSRange(location: startRange.location, length: endRange.location + endRange.length - startRange.location))
        if string.range(of: "&amp;") == nil {
            return string.replacingOccurrences(of: "&", with: "&amp;")
        } else {
            return string
        }
    }
    
    /**
        Looking for `WISPAccessGatewayParam` XML (according to WISPr 1.0) inside HTML Source.
        Validates and checks if `AuthenticationReply` contains successful authorization status.
        
        - parameter html:   html source code of the captive page
    
        - returns: true if authorization was successful, false otherwise
    */
    class func WISPrCheckLoginResult(html: NSString) -> (success: Bool, message: String) {
        
        if let data = WISPrExtractFromHTMLResponse(html: html)?.data(using: String.Encoding.utf8) {
            
            do {
                let xml = try AEXMLDocument(xml: data)
                let reply = xml.root["AuthenticationReply"]
                
                //            DDLogDebug("AuthenticationReply " + reply.stringValue)
                
                if let messageType = reply["MessageType"].value, let responseCode = reply["ResponseCode"].value, messageType == "120" {
                    switch responseCode {
                        
                    case "50":
                        
                        //                    DDLogDebug("WISPrCheckLogin_success_50")
                        return (success: true, message: reply["ReplyMessage"].value ?? "Success")
                        
                    case "100":
                        
                        //                    DDLogDebug("WISPrCheckLogin_fail_100 + \(reply["ReplyMessage"].value) and message type is \(messageType) ")
                        //                    DDLogDebug("login fail")
                        fallthrough
                        
                    default:
                        
                        //                    DDLogDebug("failure")
                        
                        //                    DDLogDebug("WISPrCheckLogin_default_Where_we_need_Our_Notification + \(reply["ReplyMessage"].value)")
                        //NSNotificationCenter.defaultCenter().postNotificationName("CaseWeGoTMessage_WISPrCheckLoginResult", object: nil)
                        return (success: false, message: reply["ReplyMessage"].value ?? "WISPr response code: \(responseCode)")
                        
                    }
                }
                
            }
            catch {
                print("\(error)")
                return (success: false, message: "No WISPr response")
            }
            
//            let xml = AEXMLDocument()
//            print(String(data: data, encoding: String.Encoding.utf8) ?? "")
//            if let xmlData = String(data: data, encoding: String.Encoding.utf8) {
////                DDLogDebug(xmlData)
//            }
//            guard xml.readXMLData(data) == nil else { return (success: false, message: "No XML response from WISPr") }
//            
//            let reply = xml.root["AuthenticationReply"]
//            
////            DDLogDebug("AuthenticationReply " + reply.stringValue)
//            
//            if let messageType = reply["MessageType"].value, let responseCode = reply["ResponseCode"].value, messageType == "120" {
//                switch responseCode {
//                    
//                case "50":
//                    
////                    DDLogDebug("WISPrCheckLogin_success_50")
//                    return (success: true, message: reply["ReplyMessage"].value ?? "Success")
//                    
//                case "100":
//                    
////                    DDLogDebug("WISPrCheckLogin_fail_100 + \(reply["ReplyMessage"].value) and message type is \(messageType) ")
////                    DDLogDebug("login fail")
//                    fallthrough
//                    
//                default:
//                    
////                    DDLogDebug("failure")
//                    
////                    DDLogDebug("WISPrCheckLogin_default_Where_we_need_Our_Notification + \(reply["ReplyMessage"].value)")
//                    //NSNotificationCenter.defaultCenter().postNotificationName("CaseWeGoTMessage_WISPrCheckLoginResult", object: nil)
//                    return (success: false, message: reply["ReplyMessage"].value ?? "WISPr response code: \(responseCode)")
//                    
//                }
//            }
        }
        
        return (success: false, message: "No WISPr response")
        
    }
    
    /**
        Looking for `WISPAccessGatewayParam` XML (according to WISPr 1.0) inside HTML Source.
        Parsing it and extracting login url that is needed for login post request.
        
        - parameter html:   html source code of the captive page
        
        - returns: login url
    */
    class func WISPrParseLoginURL(html: String) -> NSURL? {
        
        if let data = WISPrExtractFromHTMLResponse(html: html as NSString)?.data(using: String.Encoding.utf8) {
            print(String(data: data, encoding: String.Encoding.utf8) ?? "")
            if let dataWISPrParseLoginURL = String(data: data, encoding: String.Encoding.utf8) {
                DDLogDebug(dataWISPrParseLoginURL)
            }
            // reading XML for Swift 3 
            
            do {
                let xml = try AEXMLDocument(xml: data)
                guard let loginURL = xml.root["Redirect"]["LoginURL"].value else { return nil }
                return NSURL(string: loginURL)
            }
            catch {
                print("\(error)")
                return nil
            }
            
//            var xml = AEXMLDocument()
//            guard xml.readXMLData(data) == nil else { return nil }
//            guard let loginURL = xml.root["Redirect"]["LoginURL"].value else { return nil }
//            return NSURL(string: loginURL)
        }
        
        return nil
    }
    
    /**
        Looking for `WISPAccessGatewayParam` XML (according to WISPr 1.0) inside HTML Source.
        Parsing it and extracting next url that is present in `Proxy`
        
        - parameter html:   html source code of the captive page
        
        - returns: next url if Root element is `Proxy`
    */
    class func WISPrDetectProxy(html: String) -> NSURL? {
        
        if let data = WISPrExtractFromHTMLResponse(html: html as NSString)?.data(using: String.Encoding.utf8) {
            
            // reading XML for Swift 3
            
            do {
                let xml = try AEXMLDocument(xml: data)
                guard let nextURL = xml.root["Proxy"]["NextURL"].value else { return nil }
                return NSURL(string: nextURL)
            }
            catch {
                print("\(error)")
                return nil
            }
            
//            let xml = AEXMLDocument()
//            guard xml.readXMLData(data) == nil else { return nil }
//            
//            guard let nextURL = xml.root["Proxy"]["NextURL"].value else { return nil }
//            return NSURL(string: nextURL)
        }
        
        return nil
    }
}
