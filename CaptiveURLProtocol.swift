//
//  CaptiveURLProtocol.swift
//  WifiHotspot
//
//  Created by Sergey Pronin on 8/19/15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

import UIKit
import CocoaLumberjack

class CaptiveURLProtocol: URLProtocol {
    
    override class func canInit(with request: URLRequest) -> Bool {
        if request.url?.absoluteString.contains("stripe.com") == true {
            return false
        }
        return NetworkHelper.instance.shouldHandleWebViewRequests
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override class func requestIsCacheEquivalent(_ a: URLRequest, to b: URLRequest) -> Bool {
        return super.requestIsCacheEquivalent(a as URLRequest, to: b as URLRequest)
    }
    
//    override class func canInitWithRequest(withrequest: NSURLRequest) -> Bool {
//        if request.url?.absoluteString.contains("stripe.com") == true {
//            return false
//        }
//        return NetworkHelper.instance.shouldHandleWebViewRequests
//    }
    
//    override class func canonicalRequestForRequest(request: NSURLRequest) -> NSURLRequest {
//        return request
//    }
//    
//    override class func requestIsCacheEquivalent(aRequest: NSURLRequest, toRequest bRequest: NSURLRequest) -> Bool {
//        return super.requestIsCacheEquivalent(aRequest as URLRequest, to:bRequest as URLRequest)
//    }
    
    lazy var ProtocolSession: URLSession = {
        
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = false
        config.timeoutIntervalForResource = 90
        config.timeoutIntervalForRequest = 90
        return URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
  
    }()
    
    override func startLoading() {
        
        let mutableRequest = request as! NSMutableURLRequest
        
        if #available(iOS 9.0, *) {
            if let command = NetworkHelper.instance.lastCommand, NetworkHelper.instance.shouldHandleWebViewRequests {
                mutableRequest.bind(to: command)
            }
        }
        
//        if let url = mutableRequest.url {
//            Logger.logMessage("\(mutableRequest.HTTPMethod) \(url) \(mutableRequest.allHTTPHeaderFields ?? [:])")
//        }
        
        ProtocolSession.dataTask(with: mutableRequest as URLRequest) { data, response, error in
            if let error = error {
                self.client!.urlProtocol(self, didFailWithError: error)
            } else if let response = response, let data = data {
                self.client!.urlProtocol(self, didReceive: response, cacheStoragePolicy: URLCache.StoragePolicy.allowed)
//                self.client!.URLProtocol(self, didReceiveResponse: response, cacheStoragePolicy: URLCache.StoragePolicy.Allowed)
                self.client!.urlProtocol(self, didLoad: data)
                self.client!.urlProtocolDidFinishLoading(self)
            }
        }.resume()
    }
    
    override func stopLoading() {
        print("stopLoading")
        DDLogDebug("stopLoading")
    }
    
    
}

extension CaptiveURLProtocol: URLSessionTaskDelegate {
    private func URLSession(session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (NSURLRequest?) -> Void) {
//        Logger.logMessage(message: "protocol willPerformHTTPRedirection \(response.allHeaderFields)")
        if let config = NetworkHelper.instance.currentConfiguration, config.params["follow_redirects"].bool == true {
            completionHandler(request as NSURLRequest)
        } else {
            completionHandler(nil)
        }
    }
}
