//
//  ProfilePointsHotspotsTableViewCell.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 02.12.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class ProfilePointsHotspotsTableViewCell: UITableViewCell {


    // MARK: - Properties -
    
    var delegate: PointsHotspotsCellViewDelegate?
    
    // MARK: - IBOutlets -
    
    // All about points
    
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var pointsButtonOutlet: UIButton!
    @IBAction func pointsButtonAction(_ sender: UIButton) {
        
        delegate?.presentPointsDelegateScreen()
        
    }
    
    
    
    
    // All about hotspots
    
    @IBOutlet weak var hotspotsView: UIView!
    @IBOutlet weak var hotspotsLabel: UILabel!
    @IBOutlet weak var hotspotsButtonOutlet: UIButton!
    @IBAction func hotspotsButtonAction(_ sender: UIButton) {
       
        delegate?.presentHotspotsDelegateScreen()
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        drawCircle(view: pointsView, radius: 35, color: UIColor(hexString: "#17D6FF"), lineWidth: 1.0)
        drawCircle(view: hotspotsView, radius: 35, color: UIColor(hexString: "#17D6FF"), lineWidth: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

protocol PointsHotspotsCellViewDelegate {
    
    func presentPointsDelegateScreen()
    func presentHotspotsDelegateScreen()
    
}
