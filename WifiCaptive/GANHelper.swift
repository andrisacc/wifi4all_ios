//
//  GANHelper.swift
//  WifiCaptive
//
//  Created by Sergey Pronin on 11/24/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit

// MARK: - GAN Prod
fileprivate let GANIDProd = "UA-70604214-1"

// MARK: - GAN for Test
fileprivate let GANIDTest = "UA-78162499-1"

typealias GA = GANHelper

@objc class GANHelper: NSObject {
    class var sharedHelper: GANHelper {
        struct Singleton {
            static let instance = GANHelper()
        }
        return Singleton.instance
    }
    
    var tracker: GAITracker
    
    fileprivate override init() {
        
        if Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") != nil {
            // Test
            tracker = GAI.sharedInstance().tracker(withTrackingId: GANIDTest)
            GAI.sharedInstance().defaultTracker = tracker
        } else {
            // App Store (and Apple reviewers too)
            tracker = GAI.sharedInstance().tracker(withTrackingId: GANIDProd)
            GAI.sharedInstance().defaultTracker = tracker
        }
        
    }
    
    class func screen(_ name: String) {
        sharedHelper.tracker.set(kGAIScreenName, value: name)
        sharedHelper.tracker.send(GAIDictionaryBuilder.createScreenView().build() as! [AnyHashable: Any])
    }
    
    class func event(_ name: String) {
        event(name, label: nil)
    }
    
    class func event(_ name: String, label: String?) {
        event(name, label: label, category: "event")
    }
    
    class func event(_ name: String, label: String?, category: String) {
        sharedHelper.tracker.send(GAIDictionaryBuilder.createEvent(withCategory: category,
            action: name, label: label, value: 0).build()! as! [AnyHashable: Any])
    }
    
    class func event(_ name: String, labelTimestamped label: String?) {
        event(name, label: Date().analyticsString + ";" + (label ?? ""))
    }
    
    class func condition(_ condition: Bool, eventS: String, eventF: String, label: String?) {
        event(condition ? eventS : eventF, label: label)
    }
    
    class func condition(_ condition: Bool, eventS: String, eventF: String, labelTimestamped label: String?) {
        event(condition ? eventS : eventF, label: Date().analyticsString + ";" + (label ?? ""))
    }
}

let AnalyticsDateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy/MM/dd"
    formatter.locale = Locale(identifier: "en-US")
    formatter.timeZone = nil
    return formatter
}()

extension Date {
    var analyticsString: String {
        return AnalyticsDateFormatter.string(from: self)
    }
}
