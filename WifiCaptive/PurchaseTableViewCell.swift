//
//  PurchaseTableViewCell.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 06.12.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class PurchaseTableViewCell: UITableViewCell {
    
    // MARK: - Properties - 
    
    // MARK: - IBOutlets - 
    
    @IBOutlet weak var bigOneLabel: UILabel!
    @IBOutlet weak var periodUpperCaseLabel: UILabel!
    @IBOutlet weak var ulimAccessLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var pointsNumberLabel: UILabel!
    @IBOutlet weak var pointsWordLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bigOneLabel.font = CurrentFont(fontSize: 100.0).currentFont
        pointsWordLabel.font = CurrentFont(fontSize: 17.0).currentFont
        periodUpperCaseLabel.font = CurrentFont(fontSize: 20.0).currentFont
        ulimAccessLabel.font = CurrentFont(fontSize: 15.0).currentFont
        priceLabel.font = CurrentFont(fontSize: 14.0).currentFont
        pointsNumberLabel.font = CurrentFont(fontSize: 27.0).currentFont
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
