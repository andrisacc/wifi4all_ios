////
////  WifiConfiguration.swift
////  WifiHotspot
////
////  Created by Sergey Pronin on 8/21/15.
////  Copyright © 2015 App in the Air. All rights reserved.
////
//
//import Foundation
//import SQLite
//import CocoaLumberjack
//import MapKit
//
//let WifiConfigurationUpdatedNotification = "WifiConfigurationUpdatedNotification"
//
//let Configurations = DBHelper.createDatabaseConnection("1_Configs")
//
//let SQLQueue = NSOperationQueue()
//
//var BackgroundTask: UIBackgroundTaskIdentifier?
//
//
//public class WifiConfiguration: NSObject {
//    
//    
//    //    var locationManagerS: CLLocationManager!
//    //    var currentLocation: CLLocation?
//    
//    
//    override public class func initialize() {
//        
//        super.initialize()
//        if #available(iOS 9.0, *) {
//            SQLQueue.maxConcurrentOperationCount = NSProcessInfo.processInfo().lowPowerModeEnabled ? 2 : 10
//        } else {
//            SQLQueue.maxConcurrentOperationCount = 10
//        }
//        SQLQueue.qualityOfService = NSQualityOfService.Utility
//        
//    }
//    
//    let id: String
//    let name: String!
//    let SSIDregex: String
//    let params: JSON
//    let SSIDs: [String]
//    let wisprEnabled: Bool
//    let url: String!
//    
//    // Coords add testing
//    
//    let lat: Double
//    let lon: Double
//    
//    let paid: Bool
//    
//    public init(json: JSON) {
//        self.id = json["id"].string!
//        self.params = json["params"]
//        self.name = json["name"].string
//        self.SSIDregex = json["regex"].string!
//        self.SSIDs = json["ssid"].array!.map { $0.string! }
//        self.wisprEnabled = json["wispr_enabled"].bool ?? true
//        self.url = json["url"].string
//        self.paid = json["paid"].bool!
//        
//        // Coords add testing
//        
//        self.lat = Double(json["latitude"].stringValue) ?? 0.0000
//        self.lon = Double(json["longitude"].stringValue) ?? 0.0000
//    }
//    
//    init(row: [Binding?], paid: Bool) {
//        
//        self.id = row[0] as! String
//        self.params = JSON(data: (row[1] as! String).dataUsingEncoding(NSUTF8StringEncoding)!)
//        self.name = row[2] as? String
//        self.SSIDregex = row[3] as! String
//        self.SSIDs = (row[4] as! String).componentsSeparatedByString(",")
//        self.wisprEnabled = row[5] as! Int64 == 1
//        self.url = row[6] as? String
//        self.paid = paid
//        
//        // Coords add testing
//        
//        self.lat = row[7] as? Double ?? 0.0000
//        self.lon = row[8] as? Double ?? 0.0000
//        
//    }
//
//    
//    public init(id: String, name: String, SSIDregex: String, params: JSON, SSIDs: [String], wisprEnabled:Bool, url: String, paid:Bool, lat: Double, lon: Double) {
//        
//        self.id = id
//        self.name = name
//        self.SSIDregex = SSIDregex
//        self.params = params
//        self.SSIDs = SSIDs
//        self.wisprEnabled = wisprEnabled
//        self.url = url
//        self.paid = paid
//        self.lat = lat
//        self.lon = lon
//        
//    }
//    
//    var sqliteBindings: [Binding?] {
//        return [id, params.rawString(), name, SSIDregex, SSIDs.joinWithSeparator(","), wisprEnabled, url, lat, lon]
//    }
//    
//    var json: JSON {
//        let dict: [String: AnyObject] = [
//            "id": id,
//            "name": name,
//            "params": params.dictionaryObject!,
//            "regex": SSIDregex,
//            "ssid": SSIDs,
//            "wispr_enabled": wisprEnabled,
//            "url": url,
//            
//            // Coords add testing
//            
//            "latitude": lat ?? 0.0000,
//            "longitude": lon ?? 0.0000
//            
//        ]
//        return JSON(dict)
//    }
//    
//    func setIgnored() {
//        do {
//            if let statement = try Configurations?.prepare("INSERT OR REPLACE INTO IgnoredConfiguration VALUES(?, ?)") {
//                try statement.run([id, 1])
//            }
//        } catch let error {
//            DDLogDebug("setAllowed sql error \(error)")
//        }
//    }
//    
//    func setAllowed() {
//        do {
//            if let statement = try Configurations?.prepare("INSERT OR REPLACE INTO IgnoredConfiguration VALUES(?, ?)") {
//                try statement.run([id, 0])
//            }
//        } catch let error {
//            DDLogDebug("setAllowed sql error \(error)")
//        }
//    }
//    
//    public class func getNetworkConfigWithSSID(ssid: String) -> JSON? {
//        if let config = configurationForNetworkWithSSID(ssid, allowIgnored: true) {
//            return config.params
//        } else {
//            return nil
//        }
//    }
//    
//    public class func ingoreNetworkWithSSID(ssid: String) {
//        if let config = configurationForNetworkWithSSID(ssid, allowIgnored: true) {
//            config.setIgnored()
//        }
//    }
//    
//    public class func allowNetworkWithSSID(ssid: String) {
//        if let config = configurationForNetworkWithSSID(ssid, allowIgnored: true) {
//            config.setAllowed()
//        }
//    }
//    
//    
//    public class func configurationForNetworkWithSSIDNoCoords(ssid: String, allowIgnored: Bool = false) -> JSON? {
//        do {
//
//            let ignored = allowIgnored ? "" : "AND id NOT IN (SELECT id FROM IgnoredConfiguration WHERE ignored = 1)"
//            var query = "SELECT * FROM ConfigurationPaid WHERE regexp(?, ssid_regex) \(ignored) LIMIT 1"
//            if let results = try Configurations?.prepare(query, ssid) {
//                for row in results {
//                    return WifiConfiguration(row: row, paid: true).params
//                }
//            }
//            query = "SELECT * FROM Configuration WHERE ssid = ? \(ignored) LIMIT 1"
//            if let results = try Configurations?.prepare(query, ssid) {
//                for row in results {
//                    
//                    return WifiConfiguration(row: row, paid: false).params
//                    
//                }
//            }
//        } catch let error {
//            print("configs error \(error)")
//        }
//        return nil
//    }
//    
//    public class func configurationForNetworkWithSSIDCords(ssid: String, allowIgnored: Bool = false, latutude: Double, longitude: Double, completion: (JSON?) -> Void) {
//        do {
//            //            DDLogDebug("Here is SSID from configurationForNetworkWithSSID - \(ssid)")
//            // DDLogDebug("\(ssid)")
//            let ignored = allowIgnored ? "" : "AND id NOT IN (SELECT id FROM IgnoredConfiguration WHERE ignored = 1)"
//            var query = "SELECT * FROM ConfigurationPaid WHERE regexp(?, ssid_regex) \(ignored) LIMIT 1"
//            if let results = try Configurations?.prepare(query, ssid) {
//                for row in results {
//                    //                    DDLogDebug("Making a connection to commercial hotspot!")
//                    let roww = WifiConfiguration(row: row, paid: true)
//                    //                    DDLogDebug("Here we go with name: \(roww.name), paid: \(roww.paid), id: \(roww.id)")
//                    completion(WifiConfiguration(row: row, paid: true).params)
//                }
//            }
//            
//            query = "SELECT * FROM Configuration WHERE ssid = ? \(ignored) LIMIT 10"
//            if let results = try Configurations?.prepare(query, ssid) {
//                
//                var rowsArray = [WifiConfiguration]()
//                
//                for row in results {
//                    rowsArray.append(WifiConfiguration(row: row, paid: false))
//                }
//                
//                //                DDLogDebug("Rows: \(rowsArray)")
//                
//                if rowsArray.count > 1 {
//                    
//                    //                    DDLogDebug("More than one network")
//                    
//                    for net in rowsArray {
//                        
//                        
//                        let pointLocation = CLLocation(latitude: latutude, longitude: longitude)
//                        let mkLoc = CLLocation(latitude: net.lat, longitude: net.lon)
//                        let distance = pointLocation.distanceFromLocation(mkLoc)
//                        
//                        //                        DDLogDebug("Distance: \(distance)")
//                        
//                        if distance < 150 {
//                            //                            DDLogDebug("Distance OK")
//                            completion(net.params)
//                        }
//                    }
//                    
//                } else if rowsArray.count == 1 {
//                    
//                    // Test for adding coords conditions
//                    //                    DDLogDebug("Only one network")
//                    
//                    if let network = rowsArray.last {
//                        
//                        let pointLocation = CLLocation(latitude: latutude, longitude: longitude)
//                        let mkLoc = CLLocation(latitude: network.lat, longitude: network.lon)
//                        let distance = pointLocation.distanceFromLocation(mkLoc)
//                        
//                        //                        DDLogDebug("Distance: \(distance)")
//                        
//                        if distance < 150 {
//                            
//                            completion(network.params)
//                        }
//                    } else {
//                        completion(nil)
//                    }
//                } else {
//                    //                    DDLogDebug("No network at all")
//                    completion(nil)
//                }
//            }
//        } catch let error {
//            DDLogDebug("configs error \(error)")
//        }
//        completion(nil)
//    }
//    
//    
//    class func configurationForNetworkWithSSID(ssid: String, allowIgnored: Bool = false) -> WifiConfiguration? {
//        do {
////            DDLogDebug("Here is SSID from configurationForNetworkWithSSID - \(ssid)")
//            // DDLogDebug("\(ssid)")
//            let ignored = allowIgnored ? "" : "AND id NOT IN (SELECT id FROM IgnoredConfiguration WHERE ignored = 1)"
//            var query = "SELECT * FROM ConfigurationPaid WHERE regexp(?, ssid_regex) \(ignored) LIMIT 1"
//            if let results = try Configurations?.prepare(query, ssid) {
//                for row in results {
////                    DDLogDebug("Making a connection to commercial hotspot!")
//                    let roww = WifiConfiguration(row: row, paid: true)
////                    DDLogDebug("Here we go with name: \(roww.name), paid: \(roww.paid), id: \(roww.id)")
//                    return WifiConfiguration(row: row, paid: true)
//                }
//            }
//            
//            query = "SELECT * FROM Configuration WHERE ssid = ? \(ignored) LIMIT 10"
//            if let results = try Configurations?.prepare(query, ssid) {
//                
//                var rowsArray = [WifiConfiguration]()
//                
//                for row in results {
//                    rowsArray.append(WifiConfiguration(row: row, paid: false))
//                }
//                
////                DDLogDebug("Rows: \(rowsArray)")
//                
//                if rowsArray.count > 1 {
//                    
////                    DDLogDebug("More than one network")
//                    
//                    for net in rowsArray {
//                        
//                        let currentCoords = shareCurrentLocationInBackGround()
//                        let mkLoc = CLLocation(latitude: net.lat, longitude: net.lon)
//                        let distance = currentCoords.distanceFromLocation(mkLoc)
//                        
////                        DDLogDebug("Distance: \(distance)")
//                        
//                        if distance < 750 {
////                            DDLogDebug("Distance OK")
//                            return net
//                        }
//                    }
//                    
//                } else if rowsArray.count == 1 {
//                    
//                    // Test for adding coords conditions
////                    DDLogDebug("Only one network")
//                    
//                    if let network = rowsArray.last {
//                        
//                        let currentCoords = shareCurrentLocationInBackGround()
//                        let mkLoc = CLLocation(latitude: network.lat, longitude: network.lon)
//                        let distance = currentCoords.distanceFromLocation(mkLoc)
//                        
////                        DDLogDebug("Distance: \(distance)")
//                        
//                        if distance < 750 {
//                            
//                            return network
//                        }
//                    } else {
//                        return nil
//                    }
//                } else {
////                    DDLogDebug("No network at all")
//                    return nil
//                }
//            }
//        } catch let error {
//            DDLogDebug("configs error \(error)")
//        }
//        return nil
//    }
//    
//    // Get the latest id of the Configuration table
//    
//    public class func getLastID() -> Int64? {
//        
//        var ID = Int64()
//        var SSIDs = [String]()
//        
//        do {
//            if let id_here = try Configurations?.prepare("SELECT Count(*) FROM Configuration") {
//                
//                //SELECT * FROM Configuration ORDER BY id DESC LIMIT 1
//                //SELECT Count(*) FROM Configuration
//                
//                var pID: Int64?
//                for row in id_here {
//                    print("Roui", row[0]!)
//                    if let r = row[0] {
//                        pID = r as! Int64
//                    }
//                }
//                return pID
//            }
//            else {
//                return nil
//                Logger.flushMessages()
//            }
//        } catch let error {
//            return nil
//            DDLogDebug("config insert error \(error)")
//        }
//    }
//    
//    class func allSSIDs() -> [String] {
//        
//        var SSIDs = [String]()
//        do {
//            if let results = try Configurations?.prepare("SELECT ssid FROM Configuration WHERE id NOT IN (SELECT id FROM IgnoredConfiguration WHERE ignored = 1)") {
//                for row in results {
//                    guard let ssid = row[0] as? String else { continue }
//                    SSIDs.appendContentsOf(ssid.componentsSeparatedByString(","))
//                }
//            } else {
//                Logger.flushMessages()
//            }
//        } catch { }
//        return SSIDs
//    }
//    
//    public class func fetchWifiConfiguration(offset: Int = 0, completion: (() -> ())? = nil) {
//        
//        let lastKnownOffset = offset == 0 ? (NSUserDefaults.standardUserDefaults().objectForKey("offset_configs") as? Int ?? offset) : offset
//        
//        DDLogDebug("fetching configs \(lastKnownOffset)")
//        
//        let request = NSMutableURLRequest(URL: NSURL(string: BaseURL + "wifi/list?offset=\(lastKnownOffset)&allow_wifimap=1")!)
//        request.addAitaAuth()
//        NetworkHelperSession.dataTaskWithRequest(request) { data, response, error in
//            
//            if let data = data where error == nil {
//                guard let array = JSON(data: data)["networks"].array else { return }
//                var configs = [WifiConfiguration]()
//                for obj in array {
//                    configs.append(WifiConfiguration(json: obj))
//                }
//                
//                self.saveAll(configs)
//                
//                if array.count > 0 {
//                    if BackgroundTask == nil {
//                        BackgroundTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler {
//                            guard let identifier = BackgroundTask else { return }
//                            UIApplication.sharedApplication().endBackgroundTask(identifier)
//                            BackgroundTask = nil
//                        }
//                    }
//                    self.fetchWifiConfiguration(lastKnownOffset + 500)
//                    NSUserDefaults.standardUserDefaults().setObject(lastKnownOffset, forKey: "offset_configs")
//                    NSUserDefaults.standardUserDefaults().synchronize()
//                    
//                } else {
//                    if let identifier = BackgroundTask {
//                        UIApplication.sharedApplication().endBackgroundTask(identifier)
//                        BackgroundTask = nil
//                        
//                        // Fetching config when hotspot added recently from other device
//                        
//                        NSUserDefaults.standardUserDefaults().setObject(true, forKey: "all_congigs_has_been_loaded")
//                        NSUserDefaults.standardUserDefaults().synchronize()
//                    }
//                }
//            } else {
//                DDLogDebug("fetch configs error \(error)")
//            }
//            }.resume()
//    }
//    
//    public class func fetchWifiConfigurationOnRecentNet(offset: Int = 0, completion: (() -> ())? = nil) {
//        
//        let lastKnownOffset = offset == 0 ? (NSUserDefaults.standardUserDefaults().objectForKey("offset_configs") as? Int ?? offset) : offset
//        
////        DDLogDebug("fetching configs \(lastKnownOffset)")
//        
//        let request = NSMutableURLRequest(URL: NSURL(string: BaseURL + "wifi/list?offset=\(lastKnownOffset)&allow_wifimap=1")!)
//        request.addAitaAuth()
//        NetworkHelperSession.dataTaskWithRequest(request) { data, response, error in
//            
//            if let data = data where error == nil {
//                
//                guard let array = JSON(data: data)["networks"].array else { return }
//                var configs = [WifiConfiguration]()
//                for obj in array {
//                    configs.append(WifiConfiguration(json: obj))
//                }
//                
//                self.saveAll(configs)
//                
////                if array.count > 0 {
////                    self.fetchWifiConfiguration(lastKnownOffset + 500)
////                    NSUserDefaults.standardUserDefaults().setObject(lastKnownOffset, forKey: "offset_configs")
////                    NSUserDefaults.standardUserDefaults().synchronize()
////                }
//
//            } else {
//                DDLogDebug("fetch configs error \(error)")
//            }
//            }.resume()
//    }
//    
//    public class func saveAll(array: [WifiConfiguration]) {
//        SQLQueue.addOperationWithBlock {
//            do {
//                for item in array {
//                    let tableName = item.paid ? "ConfigurationPaid" : "Configuration"
//                    
//                    // Coords add test
//                    
//                    let statement = try Configurations?.prepare("INSERT OR REPLACE INTO \(tableName) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)")
//                    try statement?.run(item.sqliteBindings)
//                }
//            } catch let error {
//                DDLogDebug("config insert error \(error)")
//            }
//        }
//    }
//    
//    class func updateConfiguration(configuration: [String: AnyObject], completion: ((Bool) -> ())? = nil) {
//        let request = NSMutableURLRequest(URL: NSURL(string: BaseURL + "wifi/list")!)
//        request.HTTPMethod = "POST"
//        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(configuration, options: [])
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.setValue("\(request.HTTPBody!.length)", forHTTPHeaderField: "Content-Length")
//        request.addAitaAuth()
//        NetworkHelperSession.dataTaskWithRequest(request) { data, response, error in
//            completion?(error == nil)
//            
//            if error == nil {
//                self.fetchWifiConfiguration()
//            }
//            }.resume()
//    }
//    
//}
//
//
//
//
