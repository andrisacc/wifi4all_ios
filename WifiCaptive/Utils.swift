//
//  Utils.swift
//  WifiCaptive
//
//  Created by Sergey Pronin on 8/22/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import Firebase
import ObjectiveC
import MapKit

public struct Random {
    static func within(_ range: ClosedRange<Double>) -> Double {
        return (range.upperBound - range.lowerBound) * Double(Double(arc4random()) / Double(UInt32.max)) + range.lowerBound
    }
}

public func isReleaseVersion() -> Bool {
    let released = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") != nil ?  true : false
    return released
}

public func changePhone(viewController: UIViewController) {
    
    if Auth.auth().currentUser != nil {
        
        // Firebase logout
        do {try Auth.auth().signOut()} catch _ {/* Handle error*/}
        
        let storyboard = UIStoryboard(name: "PhoneAuth", bundle: Bundle.main)
        guard let vc: SMSLoginViewController = storyboard.instantiateViewController(withIdentifier: "SMSLoginViewController") as? SMSLoginViewController else { return }
        vc.changePhone = true
        let navigationController = UINavigationController(rootViewController: vc)
        viewController.present(navigationController, animated: true, completion: nil)
        
    } else {
        
        let storyboard = UIStoryboard(name: "PhoneAuth", bundle: Bundle.main)
        guard let vc: SMSLoginViewController = storyboard.instantiateViewController(withIdentifier: "SMSLoginViewController") as? SMSLoginViewController else { return }
        vc.changePhone = true
        let navigationController = UINavigationController(rootViewController: vc)
        viewController.present(navigationController, animated: true, completion: nil)
        
    }
}

public func showWelcome (vc: UIViewController) {
    
    UserDefaults.standard.set(true, forKey: "launchedBefore")
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    guard let welcomeContainerViewController = storyboard.instantiateViewController(withIdentifier: "WelcomeContainerViewController") as? WelcomeContainerViewController else { return }
    UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: welcomeContainerViewController)
    welcomeContainerViewController.navigationController?.navigationBar.isHidden = true
    
    
}

public func showMainVC() {
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let mainViewController = storyboard.instantiateViewController(withIdentifier: "WifiMapViewController") as! WifiMapViewController
    let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftCollectionViewController") as! LeftCollectionViewController
    
    let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
    leftViewController.mainViewController = nvc
    UINavigationBar.appearance().tintColor = UIColor.brandColor()
    let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
    UIApplication.shared.keyWindow?.rootViewController  = slideMenuController
    
    nvc.navigationBar.tintColor = UIColor.white
    nvc.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
    nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
    nvc.navigationBar.shadowImage = UIImage()
    nvc.navigationBar.isTranslucent = false
}

func showTextAlert (_ title: String?, message: String?) {
    
    // create the alert
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    // add an action (button)
    
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    
    // show the alert
    if title != nil && message != nil {
        if let topVC = UIApplication.topViewController() {
            topVC.present(alert, animated: true, completion: nil)
        }
    }
    
}


func sendLocalNotification(_ userInfo: [AnyHashable: Any]?) {
    
    DispatchQueue.main.async {

        guard let userInfoUNWRAPPED = userInfo else {return}
        let userInfoJSON = JSON(userInfoUNWRAPPED)
        print("userInfoJSON \(userInfoJSON)")
        
        guard let alertMessage = userInfoJSON["aps"]["alert"].string else {return}
        
        showTextAlert(NSLocalizedString("Alert", comment: ""), message: alertMessage)

    }
}

func QQLocalizedString(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}


func isWiFi(_ string: String?) -> Bool {
    
    let regEx = "(WIFI):((S:)([A-Z0-9a-z_%+-.,!@#$^&*()<>/|]+)|(T:)(WPA|WPA2));((T:)(WPA|WPA2)|(S:)([A-Z0-9a-z_%+-.,!@#$^&*()<>/|]+));(P:)([A-Z0-9a-z_%+-.,!@#$^&*()<>/|]+);;"
    
    let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
    return predicate.evaluate(with: string)
}

func canOpenURL(_ string: String?) -> Bool {
    guard let urlString = string else {return false}
    guard let url = URL(string: urlString) else {return false}
    if !UIApplication.shared.canOpenURL(url) {return false}
    
    //
    let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
    let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
    return predicate.evaluate(with: string)
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

func isWYGRealm() -> Bool {
    
    // TODO: - Change for HotspotHelper migration -
    #if AITA
        guard let wifi = UserDefaults.standard.object(forKey:"wifi_settings") else { return false}
    #else
        guard let wifi = UserDefaults.standard.object(forKey: "wifi") else { return false}
    #endif
    
    let json = JSON(wifi)
    
    _ = json["bytes_left"].float!
    let realm: String?
    
    // Checking the realm
    let p = HotspotCredentials.username
    
//    print("Here is current Realm: ", p)
    
    let array: NSArray = p.components(separatedBy: "@") as NSArray
    if array.count > 1 {
        realm = array.lastObject as? String
        if realm! == "wifiasyougo.com" && NSDate().timeIntervalSince1970 < HotspotCredentials.expiration?.timeIntervalSinceNow ?? 0 {
            return true
        } else {
            return false
        }
    } else {
        return false
    }
    
}

func drawCircle(view: UIView,  radius: Int, color: UIColor, lineWidth: CGFloat) {
    
    let circlePath = UIBezierPath(arcCenter: CGPoint(x: radius,y: radius), radius: view.bounds.size.width/2 - lineWidth, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.path = circlePath.cgPath
    
    //change the fill color
    shapeLayer.fillColor = UIColor.clear.cgColor
    //you can change the stroke color
    shapeLayer.strokeColor = color.cgColor
    //you can change the line width
    shapeLayer.lineWidth = lineWidth
    
    
    view.layer.addSublayer(shapeLayer)
    
}

func drawFillCircle(view: UIView, alpha: CGFloat, radius: Int, strokeColor: UIColor, fillColor: UIColor, lineWidth: CGFloat) {
    
    let circlePath = UIBezierPath(arcCenter: CGPoint(x: radius,y: radius), radius: view.bounds.size.width/2 - lineWidth, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.path = circlePath.cgPath
    
    //change the fill color
    shapeLayer.fillColor = fillColor.cgColor
    //you can change the stroke color
    shapeLayer.strokeColor = strokeColor.cgColor
    //you can change the line width
    shapeLayer.lineWidth = lineWidth
    
    view.alpha = alpha
    view.layer.addSublayer(shapeLayer)
    
}

//extension UIPanGestureRecognizer {
//
//    func isLeft(theViewYouArePassing: UIView) -> Bool {
//        let velocity : CGPoint = velocityInView(theViewYouArePassing)
//        if velocity.x > 0 {
//            print("Gesture went right")
//            return false
//        } else {
//            print("Gesture went left")
//            return true
//        }
//    }
//}

//MARK: - Direction


public struct Platform {
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
    }
    
}

func dateTimeForString(_ date: Date) -> String {
    
    let formatter = DateFormatter()
    //        formatter.dateFormat = "yyyy-MMM-dd HH:mm"
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    guard let languageID = Bundle.main.preferredLocalizations.first else {return "en-US"}
    formatter.timeZone = TimeZone.autoupdatingCurrent
    formatter.locale =  Locale(identifier: languageID)
    let utcTimeZoneStr = formatter.string(from: date)
    
    return utcTimeZoneStr
}


//func presentSucces(vc:  UIViewController, message messgae: String) {
//    let alert = UIAlertController(title: QQLocalizedString("Success"), message: messgae, preferredStyle: .alert)
//    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//    vc.present(alert, animated: true, completion: nil)
//}
//
//func presentError(vc:  UIViewController, error: String) {
//    let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
//    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
//    vc.present(alert, animated: true, completion: nil)
//}

func addWiFiNetwork (_ vc: UIViewController) {
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    delay(0.3) {
         // TODO: - Change for HotspotHelper migration -
        if #available(iOS 9.0, *) {
            if let network = currentNetwork {
                if !network.isSecure {
                    
                    let alert = UIAlertController(title: QQLocalizedString("Error"), message: QQLocalizedString("Please connect to password-protected network to add it"), preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: QQLocalizedString("OK"), style: UIAlertActionStyle.default, handler: nil))
                    vc.present(alert, animated: true, completion: nil)
                    
//                    UIAlertView(title: QQLocalizedString("Error"), message: QQLocalizedString("Please connect to password-protected network to add it"), delegate: nil, cancelButtonTitle: QQLocalizedString("OK")).show()
                    return
                }
            }
        }
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways)
        {
            
            guard let addNetworkController =
                storyboard.instantiateViewController(withIdentifier: "AddNetworkViewController") as? AddNetworkViewController else { return }
            addNetworkController.transitioningDelegate = TransitioningDelegate
            
            vc.present(addNetworkController, animated: true, completion: nil)
            
            GA.event("wifiProfile_addNetwork")
            
        } else {
            
            let locationAuthStatus = CLLocationManager.authorizationStatus()
            
            if locationAuthStatus == .denied || locationAuthStatus == .restricted {
                
                let alertController = UIAlertController (title: QQLocalizedString("Access to your current location is required"), message: QQLocalizedString("Go to Settings?"), preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: QQLocalizedString("Settings"), style: .default) { (_) -> Void in
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
                
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default, handler: nil)
                alertController.addAction(cancelAction)
                
                vc.present(alertController, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
}

public func shareCurrentLocationInBackGround () -> /*(lat: Double, lon: Double )*/CLLocation {
    
    var locationShare = CLLocation()
    
    if let location = LocationGetter.instance.locationManager?.location {
        locationShare = location
    }
    //return (locationShare.coordinate.latitude, locationShare.coordinate.longitude)
    return locationShare
    
}


class LocationGetter: NSObject {
    
    var currentLocation : CLLocation!
    var locationManager: CLLocationManager!
    static let instance = LocationGetter()
    
    fileprivate override init() {
        
        super.init()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.startUpdatingLocation()
        }
    }
}

extension LocationGetter: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let _ = locations.last else { return }
        
        //        DDLogDebug("location updated \(location.coordinate.latitude),\(location.coordinate.longitude)")
        locationManager.stopUpdatingLocation()
    }
}






