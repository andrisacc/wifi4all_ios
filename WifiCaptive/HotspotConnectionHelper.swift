////
////  HotspotConnectionHelper.swift
////  WifiHotspot
////
////  Created by Sergey Pronin on 8/7/15.
////  Copyright © 2015 App in the Air. All rights reserved.
////
//
//import Foundation
//import NetworkExtension
//import SystemConfiguration.CaptiveNetwork
//import Kanna
//import CocoaLumberjack
//
//let URLGenerate204 = NSURL(string: BaseURLInsecured + "generate_204")!
//
//let HotspotHelperPresentUINotification = "HotspotHelperPresentUINotification"
//public let HotspotHelperPaymentNeededNotification = "HotspotHelperPaymentNeededNotification"
//let HotspotHelperConfigurationKey = "HotspotHelperConfigurationKey"
//
//let WISPrUserAgent = "WISPR!Trustive"
//
//let SharedSession: NSURLSession = {
//    let config = NSURLSessionConfiguration.defaultSessionConfiguration()
//    config.timeoutIntervalForResource = 90
//    config.timeoutIntervalForRequest = 90
//    return NSURLSession(configuration: config)
//}()
//
//
//public class HotspotConnectionHelper: NSObject {
//    
//    //workaround for 8.0 and 9.0 compatibility
//    private var _command: AnyObject!
//    
//    @available(iOS 9.0, *)
//    var command: NEHotspotHelperCommand! {
//        get { return _command as! NEHotspotHelperCommand }
//        set { _command = newValue }
//    }
//    
//    let config: WifiConfiguration
//    var username: String
//    var password: String
//    let userAgent: String
//    
//    var cindex = 0
//    
//    lazy var HotspotSession: NSURLSession = {
//        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
//        config.protocolClasses = [CaptiveURLProtocol.self]
//        config.allowsCellularAccess = false
//        config.timeoutIntervalForResource = 90
//        config.timeoutIntervalForRequest = 90
//        return NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
//    }()
//    
//    @available(iOS 9.0, *)
//    convenience init(command: NEHotspotHelperCommand, configuration: WifiConfiguration) {
//        self.init(configuration: configuration)
//        self.command = command
//    }
//    
//    public init(configuration: WifiConfiguration) {
//        self.config = configuration
//        let credentials = HotspotCredentials.credentialsForSSIDRegex(configuration.SSIDregex)
//        self.username = credentials.username
//        self.password = credentials.password
//        self.userAgent = WISPrUserAgent
//    }
//    
//    var wisprEnabled: Bool {
//        return config.wisprEnabled
//    }
//    
//    func checkConnectionReachability(completion: Bool -> Void) {
//        let request = NSMutableURLRequest(URL: URLGenerate204)
//        if #available(iOS 9.0, *) {
//            request.bindToHotspotHelperCommand(command)
//        }
//        
//        HotspotSession.dataTaskWithRequest(request) { data, response, error in
//            
//            if let error = error {
////                DDLogDebug("\(error.description)")
//                completion(false)
//                return
//            }
//            
//            guard let response = response as? NSHTTPURLResponse else {
//                completion(false)
//                return
//            }
//            
//            if response.statusCode == 204 {
//                completion(true)
//            } else {
//                completion(false)
//            }
//            
//        }.resume()
//    }
//    
//    public func connect(connectURL: NSURL? = nil) {
//        
//        let url = connectURL ?? URLGenerate204
//        
//        Logger.logMessage("connecting to -> " + url.absoluteString)
////        DDLogDebug("connecting to -> " + url.absoluteString)
//
//        let request = NSMutableURLRequest(URL: url)
//        if #available(iOS 9.0, *) {
//            request.bindToHotspotHelperCommand(command)
//            Logger.logMessage("connect \(command.network!.SSID)")
////            DDLogDebug("connect \(command.network!.SSID)")
//        }
//        Logger.logMessage("connect w/ useragent \(userAgent)")
////        DDLogDebug("connect w/ useragent \(userAgent)")
//        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
//        let task = HotspotSession.dataTaskWithRequest(request) { data, response, error in
//            
//            if let error = error {
//                
//                Logger.logMessage("connect error \(error.description)")
////                DDLogDebug("connect error \(error.description)")
//
//                self.deliverHotspotStatus(false)
//                
//                return
//            }
//            
//            guard let response = response as? NSHTTPURLResponse else { return }
//            
//            if response.statusCode == 204 {
//                Logger.logMessage("connect internet reachable")
////                DDLogDebug("connect internet reachable")
//                self.deliverHotspotStatus(true)
//                
//            } else {
//                guard let data = data, string = NSString(data: data, encoding: NSUTF8StringEncoding) else {
//                    self.checkConnectionReachability { reachable in
//                        self.deliverHotspotStatus(reachable)
//                    }
//                    return
//                }
//                
//                Logger.logPage(string as String)
////                DDLogDebug(string as String)
//                
//                if self.wisprEnabled {
//                    if let nextURL = HotspotConnectionHelper.WISPrDetectProxy(string as String) {
//                        Logger.logMessage("connect wisper redirect")
////                        DDLogDebug("connect wisper redirect")
//                        self.connect(nextURL)
//                        return
//                    } else if let _ = HotspotConnectionHelper.WISPrParseLoginURL(string as String) {
//                        Logger.logMessage("connect wisper login")
////                        DDLogDebug("connect wisper login")
//                        self.login(string as String)
//                        return
//                    }
//                }
//                
//                if let redirect = HotspotConnectionHelper.extractRedirectURLFromHTMLResponse(string as NSString) {
//                    Logger.logMessage("connect nowisper redirect")
////                    DDLogDebug("connect nowisper redirect")
//                    self.connect(redirect)
//                } else {
//                    if self.config.url != nil {
//                        Logger.logMessage("connect nowisper login")
////                        DDLogDebug("connect nowisper login")
//                        self.login(string as String)
//                    } else {
//                        Logger.logMessage("connect nowisper no url UI")
////                        DDLogDebug("connect nowisper no url UI")
//                        self.deliverPresentUIStatus("no config")
//                    }
//                }
//            }
//        }
//        
//        task.resume()
//    }
//    
//    
//    func __writeHTMLFile(html: String) {
//        let fileManager = NSFileManager.defaultManager()
//        
//        let docsURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
//        let fileURL = docsURL.URLByAppendingPathComponent("\(NSDate().timeIntervalSince1970).html")
//        
//        do {
//            try html.writeToURL(fileURL, atomically: true, encoding: NSUTF8StringEncoding)
//        } catch { }
//    }
//    
//    private func login(html: String? = nil) {
//        let loginURL: NSURL?
//        var body: String?
//        
//        if wisprEnabled {
//            Logger.logMessage("login wisper \(username) \(password)")
////            DDLogDebug("login wisper \(username) \(password)")
//            loginURL = HotspotConnectionHelper.WISPrParseLoginURL(html!)
//            body = [
//                "UserName": username,
//                "Password": password,
//                "button": "Login",
//                "FNAME": "0",
//                "OriginatingServer": "http://google.com",
//                "WISPrVersion": "2.0"
//            ].URLEncodedString
//        } else {
//            loginURL = NSURL(string: config.url)
//            Logger.logMessage("login no wisper \(username) \(password) \(loginURL?.absoluteString)")
////            DDLogDebug("login no wisper \(username) \(password) \(loginURL?.absoluteString)")
//            var bodyDict = [String: String]()
//            //getting username and password key names
//            bodyDict[config.params["username"].string!] = username
//            bodyDict[config.params["password"].string!] = password
//            
//            //just extra params with no meaning :)
//            if let extras = config.params["extras"].dictionary {
//                for (k, v) in extras {
//                    bodyDict[k] = v.string!
//                }
//            }
//            
//            //extracting params from page (or meta-http redirect value string)
//            if let extract = config.params["extract"].dictionary {
//                if let query = extract["query"]?.array {
//                    for param in query.map({ $0.string! }) {
//                        bodyDict[param] = HotspotConnectionHelper.extractParamFromHTMLResponse(html! as NSString, paramName: param)
//                    }
//                }
//                if let form = extract["form"]?.array {
//                    guard let doc = Kanna.HTML(html: html!, encoding: NSUTF8StringEncoding) else { return }
//                    
//                    for param in form.map({ $0.string! }) {
//                        let nodes = doc.css("input[name=\"\(param)\"]")
//                        if nodes.count > 0 {
//                            if let value = nodes[0]["value"] {
//                                bodyDict[param] = value
//                            }
//                        }
//                    }
//                }
//            }
//            body = bodyDict.URLEncodedString
//        }
//        
//        guard let url = loginURL else { return }
//        
//        let request = NSMutableURLRequest(URL: url)
//        request.HTTPMethod = "post"
//        
//        Logger.logMessage(body!)
////        DDLogDebug("body - " + body!)
//        request.HTTPBody = body?.dataUsingEncoding(NSUTF8StringEncoding)
//        
//        if #available(iOS 9.0, *) {
//            request.bindToHotspotHelperCommand(command)
//        }
//        
//        if wisprEnabled {
//            request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
//            Logger.logMessage("login w/ useragent \(userAgent)")
////            DDLogDebug("login w/ useragent \(userAgent)")
//        }
//        
//        HotspotSession.dataTaskWithRequest(request) { data, response, error in
//            
//            if let error = error {
//                Logger.logMessage("login request error \(error.description)")
////                DDLogDebug("login request error \(error.description)")
//                if self.wisprEnabled {
//                    self.deliverHotspotStatus(false)
//                } else {
//                    self.deliverPresentUIStatus("login request error")
//                }
//                return
//            }
//            
//            guard let response = response as? NSHTTPURLResponse else { return }
//            
//            guard let data = data, string = NSString(data: data, encoding: NSUTF8StringEncoding) else {
//                if response.statusCode == 200 {
//                    self.checkConnectionReachability { reachable in
//                        Logger.logMessage("login response 200 reachable \(reachable)")
////                        DDLogDebug("login response 200 reachable \(reachable)")
//                        if !reachable {
//                            self.deliverHotspotStatus(true)
//                        } else {
//                            if self.wisprEnabled {
//                                self.deliverHotspotStatus(false)
//                            } else {
//                                self.deliverPresentUIStatus("response 200 false")
//                            }
//                        }
//                    }
//                } else {
//                    Logger.logMessage("login response \(response.statusCode)")
////                    DDLogDebug("login response \(response.statusCode)")
//                    if self.wisprEnabled {
//                        self.deliverHotspotStatus(false)
//                    } else {
//                        self.deliverPresentUIStatus("response \(response.statusCode)")
//                    }
//                }
//                return
//            }
//            
//            Logger.logPage(string as String)
////            DDLogDebug("LogPage - " + (string as String))
//            
//            if self.wisprEnabled {
//                let wisprResult = HotspotConnectionHelper.WISPrCheckLoginResult(string as NSString)
//                if wisprResult.success {
//                    Logger.logMessage("login wisper true")
////                    DDLogDebug("Login wisper true + \(wisprResult.message)")
//                    
//                    self.deliverHotspotStatus(true)
//                    self.deliverStatusNotification(NSLocalizedString("Connection was successful", comment: ""))
//                    return
//                    
//                } else {
//                    
////                    DDLogDebug("In_Login_WISPrCheckLoginResult_fail + \(wisprResult.message)")
//                    
//                    if wisprResult.message == "Sorry, time is over" {
//                        self.sendNeedsPaymentNotification(NSLocalizedString("Sorry, free time is up. To enjoy unlimited usage please purchase monthly subscription!", comment: ""))
//                    } else if wisprResult.message == "Sorry, game is over" {
//                        self.sendNeedsPaymentNotification(NSLocalizedString("Your free data in commercial networks expired. To enjoy unlimited usage please purchase monthly subscription!", comment: ""))
//                    }
//                }
//            }
//            
//            if let redirectURL = HotspotConnectionHelper.extractRedirectURLFromHTMLResponse(string) where !self.wisprEnabled {
//                Logger.logMessage("login redirect")
////                DDLogDebug("login redirect")
//                self.connect(redirectURL)
//            } else {
//                self.checkConnectionReachability { reachable in
//                    Logger.logMessage("login reachable \(reachable)")
////                    DDLogDebug("login reachable \(reachable)")
//                    if reachable {
//                        self.deliverHotspotStatus(true)
//                    } else {
//                        if self.wisprEnabled {
//                            self.deliverHotspotStatus(false)
//                        } else {
//                            self.deliverPresentUIStatus("response false")
//                        }
//                    }
//                }
//            }
//        }.resume()
//    }
//    
//    //MARK: Status notifications
//    private func deliverHotspotStatus(connected: Bool) {
//        
//        Logger.flushMessages()
//        
//        if connected {
//            if #available(iOS 9.0, *) {
//                command.createResponse(.Success).deliver()
//            } else {
//                CNMarkPortalOnline("en0")
//            }
//            
//            HotspotCredentials.updateCredentials()
//            reportCurrentSessionStatus()
//            Logger.sendLogs()
//            
//            Logger.logEvent("wifi_connect_success")
//            
////            DDLogDebug("wifi_connect_success")
//            
//            
//        } else {
//            if #available(iOS 9.0, *) {
//                command.createResponse(.Failure).deliver()
//            } else {
//                CNMarkPortalOffline("en0")
//            }
//            Logger.sendLogs()
//        }
//    }
//    
//    private func deliverStatusNotification(message: String) {
//        Logger.flushMessages()
//        
//        dispatch_async(dispatch_get_main_queue()) {
//            let notification = UILocalNotification()
//            notification.soundName = UILocalNotificationDefaultSoundName
//            notification.fireDate = NSDate(timeIntervalSinceNow: 1)
//            notification.alertBody = message
//            UIApplication.sharedApplication().scheduleLocalNotification(notification)
//        }
//    }
//    
//    private func deliverPresentUIStatus(message: String) {
//        
//        Logger.flushMessages()
//        
//        dispatch_async(dispatch_get_main_queue()) {
//            let notification = UILocalNotification()
//            notification.soundName = UILocalNotificationDefaultSoundName
//            notification.fireDate = NSDate(timeIntervalSinceNow: 1)
//            var dict = self.config.json.dictionaryObject!
//            dict.removeValueForKey("url")
//            let keys = [String](dict.keys)
//            for key in keys {
//                if dict[key] is NSNull {
//                    dict.removeValueForKey(key)
//                }
//            }
//            notification.userInfo = [
//                HotspotHelperPresentUINotification: true,
//                HotspotHelperConfigurationKey: dict
//            ]
//            notification.alertBody = NSLocalizedString("Open app to continue authorization", comment: "")
//            if #available(iOS 8.2, *) {
//                notification.alertTitle = NSLocalizedString("Network authorization", comment: "")
//            }
//            UIApplication.sharedApplication().scheduleLocalNotification(notification)
//            
//            if #available(iOS 9.0, *) {
//                self.command.createResponse(.UIRequired).deliver()
//            }
//        }
//    }
//    
//    
//    // MARK: User Notifications
//    private func requestNotificationsAccess() {
//        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Sound], categories: nil)
//        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
//    }
//    
//    private func sendNeedsPaymentNotification(notice: String) {
//        
//        if let aitaToken = NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String {
//            Logger.logMessage("token \(aitaToken)")
////            DDLogDebug("token \(aitaToken)")
//        }
//        
//        Logger.flushMessages()
//        
//        dispatch_async(dispatch_get_main_queue()) {
//            let notification = UILocalNotification()
//            notification.soundName = UILocalNotificationDefaultSoundName
//            notification.fireDate = NSDate(timeIntervalSinceNow: 1)
//            notification.userInfo = [
//                HotspotHelperPaymentNeededNotification: true,
//            ]
//            notification.alertBody = NSLocalizedString(notice, comment: "")
//            if #available(iOS 8.2, *) {
//                notification.alertTitle = NSLocalizedString("Subscribe", comment: "")
//            }
//            UIApplication.sharedApplication().scheduleLocalNotification(notification)
//        }
//    }
//
//}
//
//
////MARK: - NSURLSessionDelegate
//extension HotspotConnectionHelper: NSURLSessionTaskDelegate {
//    public func URLSession(session: NSURLSession, task: NSURLSessionTask, willPerformHTTPRedirection response: NSHTTPURLResponse, newRequest request: NSURLRequest, completionHandler: (NSURLRequest?) -> Void) {
//        Logger.logMessage("helper willPerformHTTPRedirection \(response.allHeaderFields)")
////        DDLogDebug("helper willPerformHTTPRedirection \(response.allHeaderFields)")
//        if config.params["follow_redirects"].bool == true {
//            completionHandler(request)
//        } else {
//            completionHandler(nil)
//        }
//    }
//}
//
//
////MARK: - Extraction
//extension HotspotConnectionHelper {
//    /**
//        Extracts parameter from meta-http redirect found in the source code of a webpage.
//        
//        - parameter html:       html source code of the captive page
//        - parameter paramName:  name of the parameter to look for
//        
//        - returns: param value
//    */
//    private class func extractParamFromHTMLResponse(html: NSString, paramName: String) -> String? {
//        guard let url = extractRedirectURLFromHTMLResponse(html) else { return nil }
//        
//        guard let params = url.query?.componentsSeparatedByString("&") else { return nil }
//        
//        for param in params {
//            if param.hasPrefix("\(paramName)=") {
//                return param.substringFromIndex(param.rangeOfString("=")!.startIndex.successor())
//            }
//        }
//        
//        return nil
//    }
//    
//    /**
//        Return redirect URL from `META HTTP-EQUIV="Refresh"` found in a webpage sourcecode
//    
//        Example:
//        ```
//        <META HTTP-EQUIV="Refresh" CONTENT="0;url=http://google.com/">
//        ```
//        
//        - parameter html:       html source code of the captive page
//        
//        - returns: redirect url
//    */
//    private class func extractRedirectURLFromHTMLResponse(html: NSString) -> NSURL? {
//        let startRange = html.rangeOfString("<META http-equiv=\"refresh\"", options: .CaseInsensitiveSearch)
//        guard startRange.location != NSNotFound else { return nil }
//        
//        let endRange = html.rangeOfString(">", options: .CaseInsensitiveSearch, range: NSRange(location: startRange.location, length: html.length - startRange.location))
//        
//        let range = NSRange(location: startRange.location, length: endRange.location - startRange.location + 1)
//        let meta = html.substringWithRange(range) as NSString
//        
//        let urlRange = meta.rangeOfString("url=", options: .CaseInsensitiveSearch)
//        
//        var param = meta.substringFromIndex(urlRange.location + urlRange.length) as NSString
//        param = param.substringToIndex(param.rangeOfString("\"").location)
//        if param.rangeOfString("noscript").length > 0 {
//            return nil
//        } else {
//            return NSURL(string: param as String)
//        }
//    }
//    
//    
//    //MARK: WISPr
//    
//    /**
//        Looking for `WISPAccessGatewayParam` XML (according to WISPr 1.0) inside HTML Source.
//        See [wikipedia](https://en.wikipedia.org/wiki/WISPr) for more info.
//        
//        - parameter html:   html source code of the captive page
//        
//        - returns: xml string
//    */
//    
//    private class func WISPrExtractFromHTMLResponse(html: NSString) -> String? {
//        let startRange = html.rangeOfString("<WISPAccessGatewayParam")
//        let endRange = html.rangeOfString("</WISPAccessGatewayParam>")
//        
//        guard startRange.location != NSNotFound && endRange.location != NSNotFound else { return nil }
//        
//        let string = html.substringWithRange(NSRange(location: startRange.location, length: endRange.location + endRange.length - startRange.location))
//        if string.rangeOfString("&amp;") == nil {
//            return string.stringByReplacingOccurrencesOfString("&", withString: "&amp;")
//        } else {
//            return string
//        }
//    }
//    
//    /**
//        Looking for `WISPAccessGatewayParam` XML (according to WISPr 1.0) inside HTML Source.
//        Validates and checks if `AuthenticationReply` contains successful authorization status.
//        
//        - parameter html:   html source code of the captive page
//    
//        - returns: true if authorization was successful, false otherwise
//    */
//    private class func WISPrCheckLoginResult(html: NSString) -> (success: Bool, message: String) {
//        
//        if let data = WISPrExtractFromHTMLResponse(html)?.dataUsingEncoding(NSUTF8StringEncoding) {
//            
//            let xml = AEXMLDocument()
//            print(String(data: data, encoding: NSUTF8StringEncoding))
//            if let xmlData = String(data: data, encoding: NSUTF8StringEncoding) {
////                DDLogDebug(xmlData)
//            }
//            guard xml.readXMLData(data) == nil else { return (success: false, message: "No XML response from WISPr") }
//            
//            let reply = xml.root["AuthenticationReply"]
//            
////            DDLogDebug("AuthenticationReply " + reply.stringValue)
//            
//            if let messageType = reply["MessageType"].value, responseCode = reply["ResponseCode"].value where messageType == "120" {
//                switch responseCode {
//                    
//                case "50":
//                    
////                    DDLogDebug("WISPrCheckLogin_success_50")
//                    return (success: true, message: reply["ReplyMessage"].value ?? "Success")
//                    
//                case "100":
//                    
////                    DDLogDebug("WISPrCheckLogin_fail_100 + \(reply["ReplyMessage"].value) and message type is \(messageType) ")
////                    DDLogDebug("login fail")
//                    fallthrough
//                    
//                default:
//                    
////                    DDLogDebug("failure")
//                    
////                    DDLogDebug("WISPrCheckLogin_default_Where_we_need_Our_Notification + \(reply["ReplyMessage"].value)")
//                    //NSNotificationCenter.defaultCenter().postNotificationName("CaseWeGoTMessage_WISPrCheckLoginResult", object: nil)
//                    return (success: false, message: reply["ReplyMessage"].value ?? "WISPr response code: \(responseCode)")
//                    
//                }
//            }
//        }
//        
//        return (success: false, message: "No WISPr response")
//        
//    }
//    
//    /**
//        Looking for `WISPAccessGatewayParam` XML (according to WISPr 1.0) inside HTML Source.
//        Parsing it and extracting login url that is needed for login post request.
//        
//        - parameter html:   html source code of the captive page
//        
//        - returns: login url
//    */
//    private class func WISPrParseLoginURL(html: String) -> NSURL? {
//        if let data = WISPrExtractFromHTMLResponse(html)?.dataUsingEncoding(NSUTF8StringEncoding) {
//            print(String(data: data, encoding: NSUTF8StringEncoding))
//            if let dataWISPrParseLoginURL = String(data: data, encoding: NSUTF8StringEncoding) {
////                DDLogDebug(dataWISPrParseLoginURL)
//            }
//            let xml = AEXMLDocument()
//            guard xml.readXMLData(data) == nil else { return nil }
//            
//            guard let loginURL = xml.root["Redirect"]["LoginURL"].value else { return nil }
//            return NSURL(string: loginURL)
//        }
//        
//        return nil
//    }
//    
//    /**
//        Looking for `WISPAccessGatewayParam` XML (according to WISPr 1.0) inside HTML Source.
//        Parsing it and extracting next url that is present in `Proxy`
//        
//        - parameter html:   html source code of the captive page
//        
//        - returns: next url if Root element is `Proxy`
//    */
//    private class func WISPrDetectProxy(html: String) -> NSURL? {
//        
//        if let data = WISPrExtractFromHTMLResponse(html)?.dataUsingEncoding(NSUTF8StringEncoding) {
//            let xml = AEXMLDocument()
//            guard xml.readXMLData(data) == nil else { return nil }
//            
//            guard let nextURL = xml.root["Proxy"]["NextURL"].value else { return nil }
//            return NSURL(string: nextURL)
//        }
//        
//        return nil
//    }
//}
