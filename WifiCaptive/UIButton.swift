//
//  UIButton.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

var ActionBlockKey: UInt8 = 0

public typealias BlockButtonActionBlock = (_ sender: UIButton) -> Void

class ActionBlockWrapper : NSObject {
    var block : BlockButtonActionBlock
    init(block: @escaping BlockButtonActionBlock) {
        self.block = block
    }
}

public extension UIButton {
    
    // a type for our action block closure
   
    
    func block_setAction(_ block: @escaping BlockButtonActionBlock) {
        objc_setAssociatedObject(self, &ActionBlockKey, ActionBlockWrapper(block: block), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        addTarget(self, action: #selector(UIButton.block_handleAction(_:)), for: .touchUpInside)
    }
    
    func block_handleAction(_ sender: UIButton) {
        let wrapper = objc_getAssociatedObject(self, &ActionBlockKey) as! ActionBlockWrapper
        wrapper.block(sender)
    }
}
