//
//  PointsTableViewCell.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 25.11.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class PointsTableViewCell: UITableViewCell {

    // MARK: - IBOutlets -
    
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var pointDescription: UILabel!
    @IBOutlet weak var numberPoints: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dateTime.font = CurrentFont(fontSize: 14.0).currentFont
        pointDescription.font = CurrentFont(fontSize: 17.0).currentFont
        numberPoints.font = CurrentFont(fontSize: 15.0).currentFont
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
