//
//  ContainerViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 07.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class ContainerViewController: SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func awakeFromNib() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "WifiMapViewController") {
            self.mainViewController = controller
        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LeftViewController") {
            self.leftViewController = controller
        }
        super.awakeFromNib()
    }

}
