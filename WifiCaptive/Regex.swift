////
////  Regex.swift
////  WifiHotspot
////
////  Created by Sergey Pronin on 8/10/15.
////  Copyright © 2015 App in the Air. All rights reserved.
////
//
//import UIKit
//
//class Regex {
//    
//    fileprivate var internalExpression: NSRegularExpression!
//    let pattern: String
//    
//    init?(_ pattern: String) {
//        self.pattern = pattern
//        do {
//            internalExpression = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
//        } catch {
//            return nil
//        }
//    }
//    
//    func test(_ input: String) -> Bool {
//        let matches = internalExpression.matches(in: input, options: NSRegularExpression.MatchingOptions(rawValue: 0),
//            range: NSRange(location: 0, length: input.characters.count))
//        return matches.count > 0
//    }
//}
//
//infix operator =~
//func =~ (input: String, pattern: String) -> Bool {
//    return Regex(pattern)?.test(input) ?? false 
//}
