//
//  LoginHelper.swift
//  WifiCaptive
//
//  Created by Sergey Pronin on 8/22/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import Alamofire
//import HotspotHelper
//import KeychainAccess
import CocoaLumberjack
import DigitsKit



class LoginHelper: NSObject {
    
    class func storeUserResponse(_ json: JSON) {
        let defaults = UserDefaults.standard
        if let token = json["token"].string {
            
            //            DDLogDebug("Here is token before I try to save on storeUserResponse: \(NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String)")
            
            DDLogDebug("Here I start to save the token on storeUserResponse")
            
            defaults.set(token, forKey: AitaToken)
            //            if let referrer = defaults.objectForKey("pending_invite_token") as? String {
            //                Alamofire.request(.POST, BaseURL + "wifi/referral", parameters: ["user_token": referrer],
            //                headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).response(completionHandler: { request, response, data, error in
            //                    if error == nil {
            //                        defaults.removeObjectForKey("pending_invite_token")
            //                        defaults.synchronize()
            //                    }
            //                })
            //            }
        }
        if let wifi = json["user"]["wifi"].dictionaryObject {
            defaults.set(wifi, forKey: "wifi")
        }
        if let user = json["user"].dictionaryObject {
            let data = NSKeyedArchiver.archivedData(withRootObject: user)
            defaults.set(data, forKey: "user")
        }
        defaults.synchronize()
        
        //        DDLogDebug("Here is saved token on storeUserResponse: \(NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String)")
    }
    
    class func referralAction (_ json: JSON) {
        
        let defaults = UserDefaults.standard
        
        if let token = json["token"].string {
            
            //            defaults.setObject(token, forKey: AitaToken)
            if let referrer = defaults.object(forKey: "pending_invite_token") as? String {
                
                Alamofire.request(BaseURL + "wifi/referral", method: .post, parameters: ["user_token": referrer], encoding: URLEncoding.default, headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).response(completionHandler: { (resp) in
                    if resp.error == nil {
                        defaults.removeObject(forKey: "pending_invite_token")
                        defaults.synchronize()
                    }
                    
                })
                
            }
        }
    }
    
    class func startHotspotHelper(_ token: String? = nil) {
        // TODO: - Change for HotspotHelper migration -
        NetworkHelper.setupHotspotsMonitoring(token: token, helperName: Settings.HotspotHelperName.value)
    }
    
    class func updateUserState() {
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        Alamofire.request(BaseURL + "user",
                          method: .get, parameters:  nil,
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                //                DDLogDebug("Here is JSON on api/user - \(json)")
                                
                                if json["user"].dictionary != nil {
                                    storeUserResponse(json)
                                    
                                }
                                if json["user_hotspots"] != nil {
                                    let user_hotspots = json["user_hotspots"].stringValue
                                    let defaults = UserDefaults.standard
                                    defaults.set(user_hotspots, forKey: "user_hotspots")
                                }
                                if json["user_points"] != nil {
                                    let user_points = json["user_points"].stringValue
                                    let defaults = UserDefaults.standard
                                    defaults.set(user_points, forKey: "user_points")
                                }
                                
                                
                            default:
                                break
                            }
                            
        }
        
        
    }
    
    class func signUp(email: String, password: String, completion: @escaping (Bool, String?) -> Void) {
        
        
        let defaults = UserDefaults.standard
        
        let referrer = defaults.object(forKey: "pending_invite_token") as? String ?? "no referral_token"
        //        DDLogDebug("Refferal token on signup: \(referrer)")
        let params = ["email": email, "password": password, "source": "wifi", "referral_token" : referrer]
        
        Alamofire.request(BaseURL + Settings.SignUpURL.value,
                          method: .post, parameters: params,
                          encoding: URLEncoding.default).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                if let token = json["token"].string {
                                    
                                    //                        DDLogDebug("SignedUp with \(json)")
                                    
                                    storeUserResponse(json)
                                    let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
                                    if launchedBefore  {
                                        startHotspotHelper(token)
                                    }
                                    //referralAction(json)
                                    
                                    if defaults.object(forKey: "pending_invite_token") as? String != nil {
                                        defaults.removeObject(forKey: "pending_invite_token")
                                        defaults.synchronize()
                                    }
                                    
                                    // TODO: - Change for HotspotHelper migration -
                                    HotspotCredentials.updateCredentials(jsonObj: json)
                                    
                                    completion(true, nil)
                                } else {
                                    completion(false, JSON(object)["error_message"].string)
                                }
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(false, message)
                                } else {
                                    completion(false, nil)
                                }
                            }
        }
        
    }
    
    class func login(email: String, password: String, completion: @escaping (Bool, String?) -> Void) {
        
        let defaults = UserDefaults.standard
        
        var deviceToken = "9da19b64f780243bd1cdc9db174ae6aaeaebb1132183aa563b1e19f5552c16ff"
        
        if defaults.object(forKey: DeviceToken) != nil {
            deviceToken = defaults.object(forKey: DeviceToken) as! String
        }
        
        var params = [String : AnyObject]()
        
        if defaults.object(forKey: DeviceToken) != nil {
            deviceToken = defaults.object(forKey: DeviceToken) as! String
            params = [
                "email": email as AnyObject,
                "password": password as AnyObject,
                "device": deviceToken as AnyObject,
                "device_type":"ios" as AnyObject
            ]
            
        } else {
            params = [
                "email": email as AnyObject,
                "password": password as AnyObject
            ]
        }
        
        
        Alamofire.request(BaseURL + Settings.SignInURL.value,
                          method: .post, parameters: params,
                          encoding: URLEncoding.default).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                //                    DDLogDebug("LoggedIn with \(json)")
                                
                                if let token = json["token"].string {
                                    
                                    storeUserResponse(json)
                                    let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
                                    if launchedBefore  {
                                        startHotspotHelper(token)
                                    }
                                    // TODO: - Change for HotspotHelper migration -
                                    HotspotCredentials.updateCredentials(jsonObj: json)
                                    
                                    completion(true, nil)
                                } else {
                                    completion(false, JSON(object)["error_message"].string)
                                }
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(false, message)
                                } else {
                                    completion(false, nil)
                                }
                                
                            }
        }
    }
    
    
    class func firebaseLogin(_ token: String, completion: @escaping (Bool, String?) -> Void) {
        
        let defaults = UserDefaults.standard
        var deviceToken = "9da19b64f780243bd1cdc9db174ae6aaeaebb1132183aa563b1e19f5552c16ff"
        
        let referrer = defaults.object(forKey: "pending_invite_token") as? String ?? "no referral_token"
        //        DDLogDebug("Refferal token on signup: \(referrer)")
        
        var params = [String : AnyObject]()
        
        if defaults.object(forKey: DeviceToken) != nil {
            deviceToken = defaults.object(forKey: DeviceToken) as! String
            params = [
                "referral_token": referrer as AnyObject,
                "device": deviceToken as AnyObject,
                "device_type":"ios" as AnyObject
            ]
            
        } else {
            params = [
                "referral_token" : referrer as AnyObject
            ]
        }
        
        Alamofire.request(BaseURL + "auth/firebaselogin",
                          method: .post, parameters: params,
                          encoding: URLEncoding.default,
                          headers: ["Firebase-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                //                                DDLogDebug("Firebase with \(json)")
                                
                                if let token = json["token"].string {
                                    
                                    //                                    DDLogDebug("Firebase with \(json)")
                                    
                                    storeUserResponse(json)
                                    
                                    let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
                                    if launchedBefore  {
                                        startHotspotHelper(token)
                                    }
                                    
                                    //referralAction(json)
                                    // TODO: - Change for HotspotHelper migration -
                                    HotspotCredentials.updateCredentials(jsonObj: json)
                                    
                                    if defaults.object(forKey: "pending_invite_token") as? String != nil {
                                        defaults.removeObject(forKey: "pending_invite_token")
                                        defaults.synchronize()
                                    }
                                    completion(true, nil)
                                    
                                } else {
                                    
                                    completion(false, JSON(object)["error_message"].string)
                                    
                                }
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(false, message)
                                    
                                } else {
                                    
                                    completion(false, nil)
                                }
                                
                            }
        }
        
    }
    
    
    class func facebookLogin(_ token: String, completion: @escaping (Bool, String?) -> Void) {
        
        let defaults = UserDefaults.standard
        var deviceToken = "9da19b64f780243bd1cdc9db174ae6aaeaebb1132183aa563b1e19f5552c16ff"
        
        let referrer = defaults.object(forKey: "pending_invite_token") as? String ?? "no referral_token"
        //        DDLogDebug("Refferal token on signup: \(referrer)")
        
        var params = [String : AnyObject]()
        
        if defaults.object(forKey: DeviceToken) != nil {
            deviceToken = defaults.object(forKey: DeviceToken) as! String
            params = [
                "referral_token": referrer as AnyObject,
                "device": deviceToken as AnyObject,
                "device_type":"ios" as AnyObject
            ]
            
        } else {
            params = [
                "referral_token" : referrer as AnyObject
            ]
        }
        
        Alamofire.request(BaseURL + "auth/facebooklogin",
                          method: .post, parameters: params,
                          encoding: URLEncoding.default,
                          headers: ["Facebook-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                if let token = json["token"].string {
                                    
                                    //                                    DDLogDebug("FacebookLoggedIn with \(json)")
                                    
                                    storeUserResponse(json)
                                    
                                    let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
                                    if launchedBefore  {
                                        startHotspotHelper(token)
                                    }
                                    
                                    //referralAction(json)
                                    // TODO: - Change for HotspotHelper migration -
                                    HotspotCredentials.updateCredentials(jsonObj: json)
                                    
                                    if defaults.object(forKey: "pending_invite_token") as? String != nil {
                                        defaults.removeObject(forKey: "pending_invite_token")
                                        defaults.synchronize()
                                    }
                                    completion(true, nil)
                                    
                                } else {
                                    
                                    completion(false, JSON(object)["error_message"].string)
                                    
                                }
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(false, message)
                                    
                                } else {
                                    
                                    completion(false, nil)
                                }
                                
                            }
        }
        
    }
    
    class func changePhoneFIR(_ token: String, completion: @escaping (Bool, String?) -> Void) {
        
        guard let myToken = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        Alamofire.request(BaseURL + "user/firebase",
                          method: .post, parameters: nil,
                          encoding: URLEncoding.default,
                          headers: ["Firebase-Token": token, "WiFi-User-Token":myToken]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                print("object", object)
                                
                                let json = JSON(object)
                                
                                if json["user"].dictionary != nil {
                                    LoginHelper.storeUserResponse(json)
                                    completion(true, nil)
                                } else {
                                    completion(false, JSON(object)["error_message"].string)
                                }
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(false, message)
                                    
                                } else {
                                    
                                    completion(false, nil)
                                }
                                
                            }
        }
        
    }
    
    class func logOut() {
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: AitaToken)
        defaults.removeObject(forKey: "user")
        defaults.removeObject(forKey: "wifi")
        defaults.removeObject(forKey: "user_hotspots")
        defaults.synchronize()
        
        //        DDLogDebug("AitaToken on logout: - \(defaults.objectForKey(AitaToken) as? String)")
        NetworkHelper.shutdownHotspotsMonitoring()
        
        URLCache.shared.removeAllCachedResponses()
        
        // Facebook logout
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        // Firebase logout
        do {try Auth.auth().signOut()} catch _ {/* Handle error*/}
        
    }
    
    
    class func changePhoneDigits (_ authHeaders: [String:String], completion: @escaping (Bool, String?) -> Void) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        let request = NSMutableURLRequest(url: URL(string: BaseURL + "user/digits")!)
        request.allHTTPHeaderFields = authHeaders
        request.addValue(token, forHTTPHeaderField: "WiFi-User-Token")
        
        request.httpMethod = "POST"
        
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            if let data = data {
                
                let json = JSON(data: data)
                
                DispatchQueue.main.async{
                    
                    if json["user"].dictionary != nil {
                        LoginHelper.storeUserResponse(json)
                        completion(true, nil)
                    } else {
                        completion(false, JSON(data: data)["error_message"].string)
                    }
                    
                }
                
            } else {
                
                if let data = data, let message = JSON(data: data)["error_message"].string {
                    completion(false, message)
                } else {
                    completion(false, nil)
                }
                
            }
            
        }).resume()
        
    }
}
