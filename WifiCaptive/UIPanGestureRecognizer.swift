//
//  UIPanGestureRecognizer.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

//MARK: - UIPanGestureRecognizer
public extension UIPanGestureRecognizer {
    
    enum Direction {
        case up
        case down
        case left
        case right
    }

    
    var direction: Direction? {
        
        let velocity = self.velocity(in: view)
        let isVertical = fabs(velocity.y) > fabs(velocity.x)
        
        switch (isVertical, velocity.x, velocity.y) {
        case (true, _, let y) where y < 0: return .up
        case (true, _, let y) where y > 0: return .down
        case (false, let x, _) where x > 0: return .right
        case (false, let x, _) where x < 0: return .left
        default: return nil
        }
    }
}
