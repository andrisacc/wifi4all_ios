//
//  HotspotListTableViewCell.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 17.06.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class HotspotListTableViewCell: UITableViewCell {

    @IBOutlet weak var networkName: UILabel!
    @IBOutlet weak var routeButton: UIButton!
    
    var point:WifiHotspot!
    
    @IBAction func directionTapped(_ sender: AnyObject) {
        
        let coordinate = CLLocationCoordinate2DMake(self.point.latitude, self.point.longitude);
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let destionation = MKMapItem(placemark: place)
        destionation.name = point.name
        destionation.openInMaps(launchOptions: nil)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        routeButton.titleLabel?.font = CurrentFont(fontSize: 20.0).currentFont
        networkName.font = CurrentFont(fontSize: 20.0).currentFont
        self.routeButton.setTitle(NSLocalizedString("Get Directions",  comment: ""), for: UIControlState())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
