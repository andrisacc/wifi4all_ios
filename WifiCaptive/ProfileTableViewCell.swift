//
//  ProfileTableViewCell.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 12.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    // MARK: - IBOutlets -
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var menuItemDescription: UILabel!
    
    @IBOutlet weak var accessValid: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
