//
//  UIViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

extension UIViewController {
    func hideKeyboardWhenTapped() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    // MARK: Email validation
    
    func isValidEmail(_ testStr: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
        
    }
    
    func isValidPhoneNumber(_ phoneStr: String) -> Bool {
        
        let phoneRegEx = "^\\d{7,}"
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: phoneStr)
        
    }
    
    func isNumeric(_ numStr: String) -> Bool {
        
        let numericRegEx = "^[0-9]+$"
        let numTest = NSPredicate(format:"SELF MATCHES %@", numericRegEx)
        return numTest.evaluate(with: numStr)
        
    }
    
    func setNavigationBarItem(_ isLeftOff: Bool = true) {
        
        if !isLeftOff {
            self.addLeftBarButtonWithImage(UIImage(named: "burgerWhite")!)
        }
        //        self.addRightBarButtonWithImage(UIImage(named: "list")!)
        self.slideMenuController()?.removeLeftGestures()
        //        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures(isLeftOff)
        //        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
}
