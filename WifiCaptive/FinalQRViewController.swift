//
//  ViewController.swift
//  QRCodeGen
//
//  Created by Gabriel Theodoropoulos on 27/4/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

import UIKit

class FinalQRViewController: UIViewController {

    
    // MARK: - Properties -
    
    var SSID: String?
    var password: String?
    var qrcodeImage: CIImage!
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var qrCodePromo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "backCrossWhiteSmall"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(FinalQRViewController.dismissVC))
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
//        textField.hidden = truel
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
//        print("SSID", SSID)
//        print("password", password)
        
        if qrcodeImage == nil {
            
            // Check if it IS english letters there
            
            let data: Data?
            
            if SSID == nil || SSID == "" || password == nil || password == "" {
                return
            } else {
                
                let stringToData = "WIFI:S:\(SSID!);T:WPA;P:\(password!);;"
                data = stringToData.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
                
                // WIFI:S:Test;T:WPA;P:hereistest;;
            }
            
//            let data = textField.text!.dataUsingEncoding(NSISOLatin1StringEncoding, allowLossyConversion: false)
            
            let filter = CIFilter(name: "CIQRCodeGenerator")
            
            filter!.setValue(data, forKey: "inputMessage")
            filter!.setValue("Q", forKey: "inputCorrectionLevel")
            
            qrcodeImage = filter!.outputImage
            
//            textField.resignFirstResponder()
//            btnAction.setTitle("Clear", forState: UIControlState.Normal)
            
            displayQRCodeImage()
            
        } else {
            
            imgQRCode.image = nil
            qrcodeImage = nil
//            btnAction.setTitle("Generate", forState: UIControlState.Normal)
            
        }
        
//        textField.enabled = !textField.enabled
        slider.isHidden = !slider.isHidden
        
    }
    
    
    @IBAction func performButtonAction(_ sender: AnyObject) {
        
//        if qrcodeImage == nil {
//            
//            if textField.text == "" {
//                return
//            }
//            
//            let data = textField.text!.dataUsingEncoding(NSISOLatin1StringEncoding, allowLossyConversion: false)
//            
//            let filter = CIFilter(name: "CIQRCodeGenerator")
//            
//            filter!.setValue(data, forKey: "inputMessage")
//            filter!.setValue("Q", forKey: "inputCorrectionLevel")
//            
//            qrcodeImage = filter!.outputImage
//
//            textField.resignFirstResponder()
//            btnAction.setTitle("Clear", forState: UIControlState.Normal)
//            
//            displayQRCodeImage()
//            
//        } else {
//            imgQRCode.image = nil
//            qrcodeImage = nil
//            btnAction.setTitle("Generate", forState: UIControlState.Normal)
//        }
//        
//        textField.enabled = !textField.enabled
//        slider.hidden = !slider.hidden
    }
    
    
    @IBAction func changeImageViewScale(_ sender: AnyObject) {
        imgQRCode.transform = CGAffineTransform(scaleX: CGFloat(slider.value), y: CGFloat(slider.value))
    }
    
    @IBAction func createPDF(_ sender: UIButton) {
        
        if qrcodeImage != nil {
            
//            createPdfFromView(self.qrCodePromo, saveToDocumentsWithFileName: "QRFirstPromo")
//            let view : UIView = self.qrCodePromo //Any view can be here!
//            let snapshotImage = view.getSnapshotImage()
            
            // image to share
//            let image = createImageFormView(self.qrCodePromo)
            
            let view : UIView = self.qrCodePromo//Any view can be here!
            let image = view.getSnapshotImage()
            
            // set up activity view controller
//            let imageToShare = [ image! ]
//            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
//            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
//            
//            // exclude some activity types from the list (optional)
//            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
//            
//            // present the view controller
//            self.present(activityViewController, animated: true, completion: nil)
//            
            let imageToShare = [image]
            let shareViewController = UIActivityViewController(activityItems: imageToShare,applicationActivities: nil)
            shareViewController.excludedActivityTypes = [UIActivityType.airDrop]
            shareViewController.completionWithItemsHandler = { activityType, completed, returnedItems, activityError in
                if completed {
                    
                } else {
                    
                }
            }
            
            //        navigationController?.presentViewController(shareViewController, animated: true, completion: nil)
            present(shareViewController, animated: true, completion: nil)
            
        }

    }
    
    // MARK: Custom method implementation
    
    func displayQRCodeImage() {
        
        let scaleX = imgQRCode.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = imgQRCode.frame.size.height / qrcodeImage.extent.size.height
        
        let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        imgQRCode.image = UIImage(ciImage: transformedImage)
    }
    
    
    func createPdfFromView(_ aView: UIView, saveToDocumentsWithFileName fileName: String)
    
    {
        let pdfData = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil)
        UIGraphicsBeginPDFPage()
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return }
        
        aView.layer.render(in: pdfContext)
        
        UIGraphicsEndPDFContext()
        
        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let documentsFileName = documentDirectories + "/" + fileName
            debugPrint(documentsFileName)
            pdfData.write(toFile: documentsFileName, atomically: true)
        }
    }

    func createImageFormView (_ aView: UIView) -> UIImage? {
        let myImage = UIImage(aView: aView)
        return myImage
        
    }

    
    func createImageFromViewAndSaveAsImage(_ aView: UIView, saveToDocumentsWithFileName fileName: String) {
        
        let myImage = UIImage(aView: aView)
        if let data = UIImagePNGRepresentation(myImage) {
            if let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                let documentsFileName = documentDirectories + "/" + fileName
                debugPrint(documentsFileName)
                try? data.write(to: URL(fileURLWithPath: documentsFileName), options: [.atomic])
            }
        }
    }
    
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func dismissVC () {
        self.dismiss(animated: true, completion: nil)
    }
    
}

