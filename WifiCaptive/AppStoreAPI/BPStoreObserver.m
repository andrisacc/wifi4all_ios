//
//  BPStoreObserver.m
//  SqueekAir
//
//  Created by Sergey Pronin on 14/02/2012.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "BPStoreObserver.h"

@interface BPStoreObserver()
@property (nonatomic, copy) void(^productsCallback)(BOOL, id);
@property (nonatomic, copy) void(^paymentCallback)(BOOL, NSError *);
@property (nonatomic, strong) NSMutableDictionary *products;
@end

@implementation BPStoreObserver {
    NSURLSession *session;
}

+ (instancetype)sharedObserver {
    static dispatch_once_t once = 0;
    static BPStoreObserver *instance = nil;
    dispatch_once(&once, ^{ instance = [[self alloc] init]; });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];

        self.products = [NSMutableDictionary dictionary];
        [self preloadProducts];
    }
    return self;
}

#pragma mark - Products

- (void)preloadProducts {
    [self processProductRequests:@[@"mobi.appintheair.wifi.inapp.month",
                                   @"mobi.appintheair.wifi.inapp.year"]
                        callback:^(BOOL success, id result) {
                            if (success) {
                                self.products = result;
                            }
                        }];
}

- (void)processProductRequests:(NSArray *)ids callback:(void(^)(BOOL, id))callback {
    self.productsCallback = callback;
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:ids];
    for (int i=0; i<[array count]; i++) {
        if (self.products[array[i]]) {
            [array removeObjectAtIndex:i];
            i--;
        }
    }
    
    if ([array count] > 0) {
        SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:ids]];
        request.delegate = self;
        [request start];
    } else {
        if (self.productsCallback) {
            self.productsCallback(YES, self.products);
        }
    }
}

- (void)buyProduct:(SKProduct *)product callback:(void(^)(BOOL, NSError *))callback {
    self.paymentCallback = callback;
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)buyProductWithId:(NSString *)ident callback:(void(^)(BOOL, NSError *))callback {
    SKProduct *product = self.products[ident];
    if (product) {
        [self buyProduct:product callback:callback];
    } else {
        [self processProductRequests:@[ident] callback:^(BOOL success, id result){
            if (success && result[ident]) {
                [self buyProduct:result[ident] callback:callback];
            } else {
                callback(NO, nil);
            }
        }];
    }
}

#pragma mark - Properties

- (NSDate *)currentSubscriptionExpirationDate {
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSUbiquitousKeyValueStore *cloudStorage = [NSUbiquitousKeyValueStore defaultStore];
    
    NSDate *datePushOnly = [defs objectForKey:@"year_pushonly"] ?: [cloudStorage objectForKey:@"year_pushonly"];
    NSDate *dateFull = [defs objectForKey:@"inapp_expire"] ?: [cloudStorage objectForKey:@"inapp_expire"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:MAX([datePushOnly timeIntervalSince1970], [dateFull timeIntervalSince1970])];
    return [date timeIntervalSinceNow] > 0 ? date : nil;
}

- (BOOL)isSubscriptionActive {
    return self.currentSubscriptionType > SubscriptionTypePack;
}

- (SubscriptionType)currentSubscriptionType {
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSUbiquitousKeyValueStore *cloudStorage = [NSUbiquitousKeyValueStore defaultStore];
    
    BOOL lifetime = [defs boolForKey:@"lifetime"] || [cloudStorage boolForKey:@"lifetime"];;
    if (lifetime) {
        return SubscriptionTypeLifetime;
    }
    
    NSDate *date = [defs objectForKey:@"year_pushonly"] ?: [cloudStorage objectForKey:@"year_pushonly"];
    if (date && [date timeIntervalSinceNow] > 0) {
        return SubscriptionTypePushOnly;
    } else {
        date = [defs objectForKey:@"inapp_expire"] ?: [cloudStorage objectForKey:@"inapp_expire"];
        if (date && [date timeIntervalSinceNow] > 0) {
            return SubscriptionTypeFull;
        }
    }
    NSNumber *activations = [defs objectForKey:@"inapps"] ?: [cloudStorage objectForKey:@"inapps"];
    if ([activations integerValue] > 0) {
        return SubscriptionTypePack;
    }
    return SubscriptionTypeNone;
}

- (NSInteger)activationsLeft {
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSUbiquitousKeyValueStore *cloudStorage = [NSUbiquitousKeyValueStore defaultStore];
    
    NSNumber *activations = [defs objectForKey:@"inapps"] ?: [cloudStorage objectForKey:@"inapps"];
    return [activations integerValue];
}

#pragma mark - Transactions

- (void)recordTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    //provide actual product according to current configuration
    
    [self.delegate storeObserver:self didBuy:transaction.payment.productIdentifier];
    
    if (self.paymentCallback) {
        self.paymentCallback(YES, nil);
        self.paymentCallback = nil;
    }
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"%@", [transaction.error description]);

    [self.delegate storeObserver:self didFail:transaction.error];
    
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    if (self.paymentCallback) {
        self.paymentCallback(NO, transaction.error);
        self.paymentCallback = nil;
    }
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    [self recordTransaction: transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)completeTransaction: (SKPaymentTransaction *)transaction {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [self validateInAppWithReceiptData:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]]
                          withCallback:^(BOOL success){
        if (success) {
            [self recordTransaction: transaction];
        } else {
            [self.delegate storeObserver:self didCheat:transaction.payment.productIdentifier];
            
            if (self.paymentCallback) {
                self.paymentCallback(NO, nil);
                self.paymentCallback = nil;
            }
        }
                              
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    }];
}

#pragma mark - SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    for (SKPaymentTransaction *transaction in transactions) {
        NSLog(@"%s, %ld", __PRETTY_FUNCTION__, (long)transaction.transactionState);
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

#pragma mark - SKProductRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSArray *myProduct = response.products;

    if ([myProduct count] > 0) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        for (SKProduct *product in myProduct) {
            dict[product.productIdentifier] = product;
            self.products[product.productIdentifier] = product;
        }
        if (self.productsCallback) {
            self.productsCallback(YES, dict);
        }
    } else {
        if (self.productsCallback) {
            self.productsCallback(NO, nil);
        }
    }
    self.productsCallback = nil;
}

#pragma mark - SKRequestDelegate

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    if ([request isKindOfClass:[SKProductsRequest class]] && self.productsCallback != nil) {
        self.productsCallback(NO, nil);
        self.productsCallback = nil;
    } else if (self.paymentCallback) {
        self.paymentCallback(NO, error);
        self.paymentCallback = nil;
    }
}

#pragma mark - Validation

- (void)validateInAppWithReceiptData:(NSData *)receipt withCallback:(void(^)(BOOL))callback {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSDictionary *dict = @{
        @"application_name": @"wifi",
        @"receipt_data": [receipt base64EncodedStringWithOptions:kNilOptions],
    };
    NSMutableArray *params = [NSMutableArray array];
    for (NSString *key in [dict allKeys]) {
        NSString *value = dict[key];
        [params addObject:[NSString stringWithFormat:@"%@=%@", key, [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://inapp-validation.appspot.com/verify_receipt"]];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [[params componentsJoinedByString:@"&"] dataUsingEncoding:NSUTF8StringEncoding];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                callback([json[@"verified"] boolValue]);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                callback(NO);
            });
        }
        
    }] resume];
}

@end
