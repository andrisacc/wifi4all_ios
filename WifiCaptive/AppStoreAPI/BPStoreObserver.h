//
//  BPStoreObserver.h
//  SqueekAir
//
//  Created by Sergey Pronin on 14/02/2012.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

@import Foundation;
@import StoreKit;

typedef NS_ENUM(NSInteger, SubscriptionType) {
    SubscriptionTypeNone,
    SubscriptionTypePack,
    SubscriptionTypePushOnly,
    SubscriptionTypeFull,
    SubscriptionTypeLifetime
};

@class BPStoreObserver;
@protocol BPStoreObserverDelegate <NSObject>
-(void)storeObserver:(BPStoreObserver *)observer didBuy:(NSString *)productIdentifier;
-(void)storeObserver:(BPStoreObserver *)observer didFail:(NSError *)error;
-(void)storeObserver:(BPStoreObserver *)observer didCheat:(NSString *)productIdentifier;
@end

@interface BPStoreObserver : NSObject <SKPaymentTransactionObserver, UIAlertViewDelegate, SKProductsRequestDelegate>

+ (instancetype)sharedObserver;

@property (nonatomic, weak) id<BPStoreObserverDelegate> delegate;

///Date when any subscription expires
@property (nonatomic, readonly) NSDate *currentSubscriptionExpirationDate;

///If there any subscription (for time period) currently available
@property (nonatomic, readonly) BOOL isSubscriptionActive;

///Type of the current subscription or SubscriptionTypeNone if none available (maybe flight pack)
@property (nonatomic, readonly) SubscriptionType currentSubscriptionType;

@property (nonatomic, readonly) NSInteger activationsLeft;

/**
 * Preload standard set of products from AppStore
 */
- (void)preloadProducts;

/**
 * Load specific products info from AppStore
 *
 * @param ids array of product identifiers to load
 * @param callback success + dict with [NSString(product_id): SKProduct]
 */
- (void)processProductRequests:(NSArray *)ids callback:(void(^)(BOOL, id))callback;


/**
 * Initiates purchase process of the given product
 *
 * @param product   product to be purchased
 * @param callback  will be called with success / failure for the transaction
 */
- (void)buyProduct:(SKProduct *)product callback:(void(^)(BOOL, NSError *))callback;

/**
 * Initiates purchase process of the product for the given id
 *
 * @param ident     identifier of the product to be purchased
 * @param callback  will be called with success / failure for the transaction
 */
- (void)buyProductWithId:(NSString *)ident callback:(void(^)(BOOL, NSError *))callback;


@end
