//
// Created by Sergey Pronin on 7/2/13.
// Copyright (c) 2013 Empatika. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface SKProduct (StringPrice)

@property (nonatomic, readonly) NSString *priceString;
@property (nonatomic, readonly) NSString *currencyCode;

@end