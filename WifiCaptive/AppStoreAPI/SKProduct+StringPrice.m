//
// Created by Sergey Pronin on 7/2/13.
// Copyright (c) 2013 Empatika. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "SKProduct+StringPrice.h"


@implementation SKProduct (StringPrice)

- (NSString *)priceString {
    static NSNumberFormatter *formatter = NULL;
    if (!formatter) {
        formatter = [[NSNumberFormatter alloc] init];
        formatter.formatterBehavior = NSNumberFormatterBehavior10_4;
        formatter.numberStyle = NSNumberFormatterCurrencyStyle;
        formatter.locale = self.priceLocale;
        formatter.minimumFractionDigits = 0;
    }
    
    return [formatter stringFromNumber:self.price];
}

- (NSString *)currencyCode {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:self.priceLocale];
    return formatter.currencyCode;
}

@end