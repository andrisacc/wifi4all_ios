//
//  LoginViewController.swift
//  WifiCaptive
//
//  Created by Sergey Pronin on 8/22/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import Alamofire
import CocoaLumberjack

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate, UITextFieldDelegate {
    
    
    // MARK: - IBOutlets+@IBActions -
    
//    @IBOutlet weak var fcbkButton: UIButton!
//    @IBOutlet weak var userNameTextField: UITextField!
//    @IBOutlet weak var passwordTextField: UITextField!
//    @IBOutlet weak var emailView: UIView!
//    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var imageLogo: UIImageView!
    
    @IBAction func fieldEditingChanged(_ sender: AnyObject) {
        
//        guard let email = userNameTextField.text/*, password = passwordTextField.text*/ else {
//            signInButton.isEnabled = false
//            signInButton.backgroundColor = UIColor.gray
//            return
//        }
        
//        signInButton.isEnabled = /*password.characters.count >= 8 &&*/ email.range(of: "@") != nil || email.characters.count > 7
//        signInButton.titleLabel?.textColor = signInButton.isEnabled ? UIColor.brandColor() : UIColor.gray
        
        //        passwordError.hidden = password.characters.count >= 8
        
    }
    
    @IBOutlet weak var signInWithEmailOutlet: RoundedButton!
    @IBAction func signInWithEmail(_ sender: RoundedButton) {
        
        guard let signUpController =
            self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController else { return }
        
        signUpController.transitioningDelegate = TransitioningDelegate
        signUpController.mode = .login
        signUpController.delegate = self
        
        self.present(signUpController, animated: true, completion: nil)
        
    }
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    @IBAction func signUpButtonAction(_ sender: UIButton) {
        
        guard let signUpController =
            self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController else { return }
        
        signUpController.transitioningDelegate = TransitioningDelegate
        signUpController.mode = .signUp
        signUpController.delegate = self
        
        self.present(signUpController, animated: true, completion: nil)
        
    }
    
    
    
    // MARK: - Properties -
    // Create and initialize a DGTAppearance object with default appearance:
    
    //    let digits = Digits.sharedInstance()
    //    let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
    let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if WiFi4all
            
        self.view.backgroundColor = Settings.LoginBackgroundColor.value
        imageLogo.image = #imageLiteral(resourceName: "splash_small")
        signInWithEmailOutlet.backgroundColor = Settings.SignInButtonBackgroundColor.value
        signInWithEmailOutlet.layer.cornerRadius = 5
        signInWithEmailOutlet.clipsToBounds = true
            
        signUpButtonOutlet.setTitleColor(Settings.SignInButtonBackgroundColor.value, for: UIControlState.normal)
        
        //Status bar style and visibility
        UIApplication.shared.isStatusBarHidden = false
            
        // Hiding keyboard
        self.hideKeyboardWhenTapped()
        
        signInWithEmailOutlet.titleLabel!.numberOfLines = 1
        signInWithEmailOutlet.titleLabel!.adjustsFontSizeToFitWidth = true
        signInWithEmailOutlet.titleLabel!.lineBreakMode = NSLineBreakMode.byClipping
        
        #endif
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        
    }
    
    //MARK: - Events -
    
    @IBAction func clickBack(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSignIn(_ sender: AnyObject) {
        
//        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
//
////        guard let email = userNameTextField.text, let password = passwordTextField.text else { return }
//
//        if isValidEmail(email) {
//
//            CustomActivity.sharedInstance.hideActivityIndicator(self.view)
//
//            guard let passwordController =
//                self.storyboard?.instantiateViewController(withIdentifier: "PasswordViewController") as? PasswordViewController else { return }
//
//            passwordController.transitioningDelegate = TransitioningDelegate
//            passwordController.emailUP = email
//            passwordController.delegate = self
//
//            self.present(passwordController, animated: true, completion: nil)
//
//
//        } else  {
//
//            let emailCleared = email.replacingOccurrences(of: "+", with: "")
//            if isValidPhoneNumber(emailCleared) {
//
//            } else {
//                self.presentError(QQLocalizedString("Please enter valid Email or Phone Number - 10 digits"))
//                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
//            }
//        }
        
    }
    
    @IBAction func clickSignUp(_ sender: AnyObject) {
        
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        
        if let currentUser = Auth.auth().currentUser {
            
            currentUser.getIDToken(completion: { (idToken, error) in
                
                if let error = error {
                    // Handle error
                    print(error)
                    self.presentError(error.localizedDescription)
                    CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                    return
                }
                
                LoginHelper.firebaseLogin(idToken ?? "notoken", completion: {[unowned self] (success, error) in
                    if success {
                        print("I successed with firebase...")
                        CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                        showMainVC()
                    } else {
                        CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                        self.presentError(error ?? "error...")
                        print("Unsucceeded with firebase...")
                    }
                })
            })
        } else {
            CustomActivity.sharedInstance.hideActivityIndicator(self.view)
            let storyboard = UIStoryboard(name: "PhoneAuth", bundle: Bundle.main)
            guard let vc: SMSLoginViewController = storyboard.instantiateViewController(withIdentifier: "SMSLoginViewController") as? SMSLoginViewController else { return }
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
        }
        
    }
    
    // MARK: Facebook integration
    
    func configuringFacebook () {
        
        //        fcbkButton.delegate = self
        
    }
    
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        
        let loginManager = FBSDKLoginManager()
        loginManager.defaultAudience = .onlyMe
        loginManager.loginBehavior = .systemAccount
        loginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            
            if (error != nil) {
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                let ns_error = error! as NSError
                switch ns_error.code {
                    
                case -1009:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                    
                case -1200:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                    
                case 4:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                    
                case 306:
                    
                    let alertController = UIAlertController (title: QQLocalizedString("Access to your Facebook information is required"), message: QQLocalizedString("Go to Settings?"), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: QQLocalizedString("Settings"), style: .default) { (_) -> Void in
                        if #available(iOS 10.0, *){
                            UIApplication.shared.openURL(URL(string: "App-Prefs:root=FACEBOOK")!)
                        }
                        else {
                            UIApplication.shared.openURL(URL(string: "prefs:root=FACEBOOK")!)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                default: break
                    
                }
                
            } else if (result?.isCancelled)! {
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                
            } else {
                
                guard let fbToken = FBSDKAccessToken.current() else {return}
                
                LoginHelper.facebookLogin(fbToken.tokenString, completion: {[unowned self] (success, errorString) in
                    
                    
                    CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                    
                    if success {
                        
                        
                        if self.launchedBefore  {
                            
                            showMainVC()
                            
                        } else {
                            showWelcome(vc: self)
                        }
                        
                    } else {
                        print("ErrorString", errorString ?? "")
                        self.presentError(errorString ?? QQLocalizedString("There was an error during your sign up. Try again later"))
                        self.fbLogout()
                    }
                })
            }
        }
        return false
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
    }
    
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!){
        fbLogout()
    }
    
    // MARK: Helpers 
    
    func facebookLogin () {
        
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        
        let loginManager = FBSDKLoginManager()
        loginManager.defaultAudience = .onlyMe
        loginManager.loginBehavior = .systemAccount
        loginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            
            if (error != nil) {
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                
                let ns_error = error! as NSError
                
                switch ns_error.code {
                case -1009:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                    
                case -1200:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                case 4:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                case 306:
                    let alertController = UIAlertController (title: QQLocalizedString("Access to your Facebook information is required"), message: QQLocalizedString("Go to Settings?"), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: QQLocalizedString("Settings"), style: .default) { (_) -> Void in
                        if #available(iOS 10.0, *){
                            UIApplication.shared.openURL(URL(string: "App-Prefs:root=FACEBOOK")!)
                        }
                        else {
                            UIApplication.shared.openURL(URL(string: "prefs:root=FACEBOOK")!)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                default: break
                    
                }
                
            } else if (result?.isCancelled)! {
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                
            } else {
                
                guard let fbToken = FBSDKAccessToken.current() else {return}
                
                LoginHelper.facebookLogin(fbToken.tokenString, completion: {[unowned self] (success, errorString) in
                    
                    
                    CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                    
                    if success {
                        
                        if self.launchedBefore  {
                            
                            showMainVC()
                            
                        } else {
                            showWelcome(vc: self)
                        }
                        
                    } else {
                        print("ErrorString", errorString ?? "")
                        self.presentError(errorString ?? QQLocalizedString("There was an error during your sign up. Try again later"))
                        self.fbLogout()
                    }
                })
            }
        }
        
    }
    
        
    func presentError(_ error: String) {
        
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    
    func fbLogout () {
        
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    
    //    // MARK: - TextField -
    //
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        let scrollPoint = CGPoint(x: 0, y: textField.frame.origin.y + scrollView.contentInset.top)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
//        if passwordError.isHidden == false {
//            passwordError.isHidden = true
//        }
    }
    
    func animateViewMoving (_ up:Bool, moveValue : CGFloat) {
        let movementDuration:TimeInterval = 0.5
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}


//MARK: - SignupViewControllerDelegate -

extension LoginViewController: SignupViewControllerDelegate {
    func signUpViewControllerSucceeded(_ controller: SignupViewController) {
        
        dismiss(animated: true) {
            
            let data = UserDefaults.standard.object(forKey: "user") as? Data
            guard let _data = data else {return}
            let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
            guard let user = userUnwrapped else {return}
            //            print("Useeerrrr", user)
            let _user = JSON(user)
            let userPhone = _user["phone"]
            //            print("Ussrphone", _user["phone"])
            let isNum = self.isNumeric(userPhone.stringValue)
            
            showMainVC ()
            
//            if userPhone != nil && isNum {
//
//                showMainVC ()
//                
//            } else {
//
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                guard let loginViewController =
//                    storyboard.instantiateViewController(withIdentifier: "AddPhoneViewController") as? AddPhoneViewController else { return  }
//                UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: loginViewController)
//
//            }
        }
    }
}

//MARK: - PasswordViewControllerDelegate -

extension LoginViewController: PasswordControllerDelegate {
    
    func passwordViewControllerSucceeded(_ controller: PasswordViewController) {
        dismiss(animated: false) {
            showMainVC ()
        }
    }
}
