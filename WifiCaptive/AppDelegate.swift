//
//  AppDelegate.swift
//  WifiCaptive
//
//  Created by plotkin on 04/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import Fabric
import Crashlytics
import StoreKit
import COSTouchVisualizer
import CocoaLumberjack
import IQKeyboardManagerSwift
import Firebase


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        //Branch services
        setupBranch(launchOptions: launchOptions)
        
        //Fabric services
        Fabric.with([Crashlytics()])
        
        initializeNotificationServices()
        
        // Keyboard manager
        IQKeyboardManager.sharedManager().enable = true
        
        //Facebook integration
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        lumberjackInitializing()

        checkLaunchedBefore()
        
        //HSH w/out token
        startHSH()
        
        droppingKeychainCredentials()
        
        setApplicationAppearence(application: application)
        
        //Base controller when load the app
        setBaseControllerOnLoading()
        
        SKPaymentQueue.default().add(BPStoreObserver.shared())
        
        UserDefaults.standard.register(defaults: ["UserAgent": "2"])
        UserDefaults.standard.synchronize()
        
        forceLogout()

        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        Branch.getInstance().handleDeepLink(url)
        
        // MARK: Facebook integration
        FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        // Fetching config when hotspot added recently from other device
        guard let configsIsLoaded = UserDefaults.standard.object(forKey: "all_congigs_has_been_loaded") as? Bool else {return}
//        DDLogDebug("Configs loaded: \(configsIsLoaded)")
        
        if configsIsLoaded {
            WifiConfiguration.fetchWifiConfigurationOnRecentNet()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }


}


// MARK: - Setting the APP -

extension AppDelegate: COSTouchVisualizerWindowDelegate {
    
    // Showing taps on screen
    
//    lazy var window: UIWindow? = {
//
//        var customWindow = COSTouchVisualizerWindow(frame: UIScreen.mainScreen().bounds)
//
//        customWindow.fillColor = UIColor.redColor()
//        customWindow.strokeColor = UIColor.redColor()
//        customWindow.touchAlpha = 0.7;
//
//        customWindow.rippleFillColor = UIColor.redColor()
//        customWindow.rippleStrokeColor = UIColor.redColor()
//        customWindow.touchAlpha = 0.1;
//
//        customWindow.touchVisualizerWindowDelegate = self
//        return customWindow
//    }()
    
    // COSTouchVisualizerWindowDelegate
    
    func touchVisualizerWindowShouldAlwaysShowFingertip(_ window: COSTouchVisualizerWindow!) -> Bool {
        // Return YES to make the fingertip always display even if there's no any mirrored screen.
        // Return NO or don't implement this method if you want to keep the fingertip display only when
        // the device is connected to a mirrored screen.
        return true
    }
    
    func touchVisualizerWindowShouldShowFingertip(_ window: COSTouchVisualizerWindow!) -> Bool {
        // Return YES or don't implement this method to make this window show fingertip when necessary.
        // Return NO to make this window not to show fingertip.
        return true
    }
    
    func lumberjackInitializing() {
        
        // Creating lumberjack file logger
        
        // Initialize lumberjack instance
        
        let fileLogger: DDFileLogger = DDFileLogger() // File Logger
        
        DDLog.add(DDTTYLogger.sharedInstance) // TTY = Xcode console
        DDLog.add(DDASLLogger.sharedInstance) // ASL = Apple System Logs
        
        fileLogger.rollingFrequency = 60*60*24  // 24 hours
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7
        DDLog.add(fileLogger)
        
    }
    
    func checkLaunchedBefore() {
        // Check if it is the first launch so to fill up new sqlite database
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if !launchedBefore  {
            
            UserDefaults.standard.set(false, forKey: "all_congigs_has_been_loaded")
            UserDefaults.standard.set(0, forKey: "offset_configs")
            //            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
            
            //             Showing welcoming view
            
            //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //            if NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) == nil {
            //                guard let welcomeContainerViewController = storyboard.instantiateViewControllerWithIdentifier("WelcomeContainerViewController") as? WelcomeContainerViewController else { return true }
            //                window?.rootViewController = UINavigationController(rootViewController: welcomeContainerViewController)
            //                welcomeContainerViewController.navigationController?.navigationBar.hidden = true
            //            }
            
        }
    }
    
    func checkFontsInApp () {
        // Check fonts on in the app
        
        for family: String in UIFont.familyNames {
            
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    
    func forceLogout() {
        if !UserDefaults.standard.bool(forKey: ForceLogout) {
            UserDefaults.standard.set(true, forKey: ForceLogout)
            LoginHelper.logOut()
        }
    }
    
    func startHSH() {
        
        if let token = UserDefaults.standard.object(forKey: AitaToken) as? String {
            
            LoginHelper.startHotspotHelper(token)
            LoginHelper.updateUserState()
            
            //            DDLogDebug("TokenFromAita: \(token)")
            // print("HereIsPureAinaToken", AitaToken)
            
        } else {
            LoginHelper.startHotspotHelper()
        }
    }
    
    func setupBranch(launchOptions: [AnyHashable : Any]!) {
        
        Branch.getInstance().initSession(launchOptions: launchOptions) { params, error in
            //            print("deep link data: \(params?.description)")
            if let referrer = params?["aita_wifi_token"] as? String {
                let defaults = UserDefaults.standard
                
                if defaults.object(forKey: AitaToken) != nil {
                    return
                }
                
                defaults.set(referrer, forKey: "pending_invite_token")
                defaults.synchronize()
                
                GA.event("wifiInvited_Succeded")
            }
        }
    }
    
    func setApplicationAppearence(application: UIApplication) {
        
        application.statusBarStyle = .lightContent
        
        UINavigationBar.appearance().titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.white
        ]
        UINavigationBar.appearance().barTintColor = UIColor(white: 0.1, alpha: 1)
    }
    
    func droppingKeychainCredentials() {
        let defaults = UserDefaults.standard
        if !defaults.bool(forKey: "drop_keychain") {
            defaults.set(true, forKey: "drop_keychain")
            defaults.synchronize()
            
            #if DEBUG
            #else
                HotspotCredentials.username = ""
                HotspotCredentials.password = ""
            #endif
        }
    }
    
    func setBaseControllerOnLoading() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil /*&& launchedBefore*/ {
            
            guard let loginViewController =
                storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            window?.rootViewController = UINavigationController(rootViewController: loginViewController)
            
        } else if  UserDefaults.standard.object(forKey: AitaToken) != nil /* && launchedBefore*/ {
            
            //            print("Token: ", NSUserDefaults.standardUserDefaults().objectForKey(AitaToken))
            
            let mainViewController = storyboard.instantiateViewController(withIdentifier: "WifiMapViewController") as! WifiMapViewController
            let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftCollectionViewController") as! LeftCollectionViewController
            
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            
            nvc.navigationBar.tintColor = UIColor.white
            nvc.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
            nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
            nvc.navigationBar.shadowImage = UIImage()
            nvc.navigationBar.isTranslucent = false
            
            //            let navBackgroundImage:UIImage! = UIImage(named: "WorldMap")
            //            nvc.navigationBar.setBackgroundImage(navBackgroundImage, forBarMetrics: .Default)
            
            let slideMenuController = ContainerViewController(mainViewController: nvc, leftMenuViewController: leftViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
            
            //                let mainViewController = storyboard.instantiateViewControllerWithIdentifier("WifiMapViewController") as! WifiMapViewController
            //                let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftViewController") as! LeftViewController
            //
            //                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            //                leftViewController.mainViewController = nvc
            //
            //                nvc.navigationBar.tintColor = UIColor.whiteColor()
            //                nvc.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
            //                nvc.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
            //                nvc.navigationBar.shadowImage = UIImage()
            //                nvc.navigationBar.translucent = false
            //
            //                //            let navBackgroundImage:UIImage! = UIImage(named: "WorldMap")
            //                //            nvc.navigationBar.setBackgroundImage(navBackgroundImage, forBarMetrics: .Default)
            //
            //                let slideMenuController = ContainerViewController(mainViewController: nvc, leftMenuViewController: leftViewController)
            //                self.window?.rootViewController = slideMenuController
            //                self.window?.makeKeyAndVisible()

        }
    }
}

// MARK: - Setting notifications -

extension AppDelegate {
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        
        // TODO: - Change for HotspotHelper migration -
        //        if notification.userInfo?[HotspotHelperPaymentNeededNotification] as? Bool == true {
        ////            (window?.rootViewController?.navigationController?.topViewController as? WifiMapViewController)?.openProfile(false)
        //            NSNotificationCenter.defaultCenter().postNotificationName("showPaymentController", object: nil)
        //            dispatch_async(dispatch_get_main_queue()) {
        //                 NSNotificationCenter.defaultCenter().postNotificationName("showTapActivity", object: nil)
        //            }
        //
        //        } else {
        //            NetworkHelper.processLocalNotification(notification)
        //        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        let defaults = UserDefaults.standard
        if Platform.isSimulator {
            defaults.set("someTokenSomeNamedLFLKSJDKLkLKAJKJDkskdjskjd", forKey: DeviceToken)
        } else {
            print("deviceToken: ", token)
            defaults.set(token, forKey: DeviceToken)
        }
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // TODO: - Change for HotspotHelper migration -
        NetworkHelper.handleRemoteNotification(userInfo: userInfo as [NSObject : AnyObject], fetchCompletionHandler: completionHandler)
        
        print("Remote notification userInfo: ", userInfo)
        
        if (application.applicationState == .active ) {
            
            sendLocalNotification(userInfo)
            
            //            dispatch_async(dispatch_get_main_queue()) {
            //                let notification = UILocalNotification()
            //                notification.soundName = UILocalNotificationDefaultSoundName
            //                notification.fireDate = NSDate(timeIntervalSinceNow: 1)
            //                notification.userInfo = userInfo
            //                notification.alertBody = NSLocalizedString("You need to have active subscription in order to connect.", comment: "")
            //                if #available(iOS 8.2, *) {
            //                    notification.alertTitle = NSLocalizedString("Subscribe", comment: "")
            //                }
            //                UIApplication.sharedApplication().scheduleLocalNotification(notification)
            //            }
        }
        
    }
    
    func initializeNotificationServices() -> Void {
        let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        
        // This is an asynchronous method to retrieve a Device Token
        // Callbacks are in AppDelegate.swift
        // Success = didRegisterForRemoteNotificationsWithDeviceToken
        // Fail = didFailToRegisterForRemoteNotificationsWithError
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    //Add phone notification -
    
    //    func notifyOnAddPhone (firstAppearence: Bool) {
    //
    //        if !self.addNotificationMade {
    //
    //            self.addNotificationMade = firstAppearence
    //
    //            let timeInterval = NSDate().timeIntervalSince1970
    //            NSUserDefaults.standardUserDefaults().setObject(timeInterval, forKey: "addPhoneNotificationTimeStamp")
    //            NSUserDefaults.standardUserDefaults().setObject(true, forKey: "addPhoneNotification")
    //
    //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //            guard let loginViewController =
    //                storyboard.instantiateViewControllerWithIdentifier("AddPhoneViewController") as? AddPhoneViewController else { return }
    //            UIApplication.sharedApplication().keyWindow?.rootViewController = UINavigationController(rootViewController: loginViewController)
    //
    //        } else {
    //
    //            let currentUnixTime = NSDate().timeIntervalSince1970
    //            guard let lastTimeStamp = NSUserDefaults.standardUserDefaults().objectForKey("addPhoneNotificationTimeStamp") as? NSTimeInterval else {return}
    //
    //            let timePasseSinceLastShow = currentUnixTime - lastTimeStamp
    //
    //            if  timePasseSinceLastShow > 10 /*259200*/ {
    //
    //                let timeInterval = NSDate().timeIntervalSince1970
    //
    //                NSUserDefaults.standardUserDefaults().setObject(timeInterval, forKey: "addPhoneNotificationTimeStamp")
    //
    //                let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //                guard let loginViewController =
    //                    storyboard.instantiateViewControllerWithIdentifier("AddPhoneViewController") as? AddPhoneViewController else { return }
    //                UIApplication.sharedApplication().keyWindow?.rootViewController = UINavigationController(rootViewController: loginViewController)
    //
    //            }
    //        }
    //    }
    
}

