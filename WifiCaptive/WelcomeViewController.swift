//
//  WelcomeViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 11.08.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit


class WelcomeViewController: UIPageViewController  {

    // MARK: - Properties  -
    
    weak var pageViewlDelegate: WelcomeViewControllerDelegate?
    var timer: Timer!
    weak var repeatingTimer: Timer!
   
    // MARK: - VC Load -
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.isUserInteractionEnabled = false

        timer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(WelcomeViewController.forward), userInfo: nil, repeats: true)
        self.repeatingTimer = timer
        
        dataSource = self
        delegate = self
                
        if let firstViewController = orderedViewControllers.first {
            
            DispatchQueue.main.async(execute: {
                self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
            })
            
        }
        
        pageViewlDelegate?.welcomeViewController(self,didUpdatePageCount: orderedViewControllers.count)
        
        //Status bar style and visibility
        
//        UIApplication.sharedApplication().statusBarHidden = true
//
//                let button = UIButton(frame: CGRectMake(view.bounds.maxX - 208, 8, 100, 30))
//                button.backgroundColor = UIColor.clearColor()
//                button.setTitle(QQLocalizedString("Forward"), forState: UIControlState.Normal)
//                button.addTarget(self, action: #selector(WelcomeViewController.forward), forControlEvents: UIControlEvents.TouchUpInside)
//                self.view.addSubview(button) // add to view as subview
//        
//                let buttonBack = UIButton(frame: CGRectMake(view.bounds.maxX - 316, 8, 100, 30))
//                buttonBack.backgroundColor = UIColor.clearColor()
//                buttonBack.setTitle(QQLocalizedString("Back"), forState: UIControlState.Normal)
//                buttonBack.addTarget(self, action: #selector(WelcomeViewController.back), forControlEvents: UIControlEvents.TouchUpInside)
//                self.view.addSubview(buttonBack) // add to view as subview
//
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if repeatingTimer != nil {
            repeatingTimer.invalidate()
            repeatingTimer = nil
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("recieved memory warning in WelcomeVC")
    }
    
    fileprivate(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.newPageViewControllerWith("ZeroPageViewController"),self.newPageViewControllerWith("FirstViewController"), self.newPageViewControllerWith("MiddleViewController"), self.newPageViewControllerWith("LastViewController")]
    }()
    
    fileprivate func newPageViewControllerWith(_ name: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(name)")
    }
    

    // MARK: - DataSourceMethods -
    
    override func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    override func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        let orderedViewControllersCount = orderedViewControllers.count
        
//        guard orderedViewControllersCount != nextIndex else {
////            return orderedViewControllers.first
//            
//            return nil
//        }
//        
//        guard orderedViewControllersCount > nextIndex else {
//            return nil
//        }
//        
        
        if nextIndex == orderedViewControllersCount - 3 {
            
            if repeatingTimer != nil {
                repeatingTimer.invalidate()
                repeatingTimer = nil
            }

            timer = Timer.scheduledTimer(timeInterval: 11, target: self, selector: #selector(WelcomeViewController.forward), userInfo: nil, repeats: true)
            self.repeatingTimer = timer

        }
        
        if nextIndex == orderedViewControllersCount - 2 {
            
            if repeatingTimer != nil {
                repeatingTimer.invalidate()
                repeatingTimer = nil
            }
            timer = Timer.scheduledTimer(timeInterval: 11, target: self, selector: #selector(WelcomeViewController.forward), userInfo: nil, repeats: true)
            self.repeatingTimer = timer
            
        }
        
        if nextIndex == orderedViewControllersCount - 1 {
            
            self.view.isUserInteractionEnabled = true
            if repeatingTimer != nil {
                repeatingTimer.invalidate()
                repeatingTimer = nil
            }
            
        }
        
        if nextIndex == orderedViewControllersCount {
            
             self.view.isUserInteractionEnabled = true
            
            if repeatingTimer != nil {
                repeatingTimer.invalidate()
                repeatingTimer = nil
            }
            
//            timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: #selector(WelcomeViewController.forward), userInfo: nil, repeats: true)
//            self.repeatingTimer = timer
            
            if let lastVC = orderedViewControllers.last  {
                
//                lastVC.view.userInteractionEnabled = true
                
                let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(WelcomeViewController.clearAction))
                let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(WelcomeViewController.clearAction))
                let tap = UITapGestureRecognizer(target: self, action: #selector(WelcomeViewController.clearAction))
                let edge = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(WelcomeViewController.clearAction))
                
                edge.edges = .left
                tap.numberOfTapsRequired = 1
                swipeUp.numberOfTouchesRequired = 1
                swipeDown.numberOfTouchesRequired = 1
                swipeDown.direction = .down
                swipeUp.direction = .up
//                self.view.addGestureRecognizer(tap)
                self.view.addGestureRecognizer(swipeUp)
                self.view.addGestureRecognizer(swipeDown)
                
            }
            return nil
        }
        
//        return nil
        return orderedViewControllers[nextIndex]
    }
    
    func  clearAction() {
        
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginViewController.modalTransitionStyle = .flipHorizontal
        let navigationController = UINavigationController(rootViewController: loginViewController)
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
    // MARK: - Helpers -

    func forward() {

        self.goToNextPage()
        if let firstViewController = viewControllers?.first, let index = orderedViewControllers.index(of: firstViewController) {
            pageViewlDelegate?.welcomeViewController(self, didUpdatePageIndex: index)
        }
    }
    
    func back () {

        self.goToPreviousPage()
    }
    
    // MARK: - Delegation - 
    
//    func zeroViewControllerDelegate() {
//        print("I did triggered in WelcomeViewController!")
//        self.goToNextPage()
//    }
    
    // MARK: - Navigation
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if let ZeroViewController = segue.destinationViewController as? ZeroPageViewController {
//            ZeroViewController.zeroViewlDelegate = self
//        }
//    }

}



// MARK: - UIPageViewControllerDataSource -

extension UIPageViewController: UIPageViewControllerDataSource {
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return nil
    }
    
}

extension WelcomeViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if let firstViewController = viewControllers?.first, let index = orderedViewControllers.index(of: firstViewController) {
            pageViewlDelegate?.welcomeViewController(self, didUpdatePageIndex: index)
        }
    }
    
}

extension UIPageViewController {
    
    func goToNextPage(){
        
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController( self, viewControllerAfter: currentViewController ) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
        
    }
    
    
    func goToPreviousPage(){
        
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let previousViewController = dataSource?.pageViewController( self, viewControllerBefore: currentViewController ) else { return }
        setViewControllers([previousViewController], direction: .reverse, animated: true, completion: nil)
        
    }
    
}

//extension WelcomeViewController: ZeroViewControllerDelegate {
//    
//
//}

protocol WelcomeViewControllerDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter count: the total number of pages.
     */
    func welcomeViewController(_ welcomeViewController: WelcomeViewController,
                                    didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func welcomeViewController(_ welcomeViewController: WelcomeViewController,
                                    didUpdatePageIndex index: Int)
    
    
    
    
}
