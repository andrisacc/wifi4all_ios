//
//  PopOverViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 12.12.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
import DigitsKit
import CocoaLumberjack

class PopOverViewController: UIViewController {

    // MARK: - Properites - 
    var mapCacheText = String()
    var logoutText = String()
    
    // MARK: - IBOutlets - 
    
    @IBOutlet weak var clearCacheButtonOutlet: UIButton!
    @IBAction func clearCacheButtonAction(_ sender: UIButton) {
        
        clearMapCache { (success) in
            
            if success {
                
                let alertController = UIAlertController (title: QQLocalizedString("Thank you"), message: QQLocalizedString("Map cache has been successfully deleted"), preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: QQLocalizedString("Ok"), style: .default) { (_) -> Void in
                }
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
        
    }
    
    @IBOutlet weak var logoutButtonOutlet: UIButton!
    
    @IBAction func logoutButtonAction(_ sender: UIButton) {
        
        logout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        
        clearCacheButtonOutlet.titleLabel?.font = CurrentFont(fontSize: 15.0).currentFont
        logoutButtonOutlet.titleLabel?.font = CurrentFont(fontSize: 15.0).currentFont
        
        clearCacheButtonOutlet.setTitle(mapCacheText, for: UIControlState())
        logoutButtonOutlet.setTitle(logoutText, for: UIControlState())
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Hepers - 
    
    func logout () {
        
        let loginManager = FBSDKLoginManager()
        
        loginManager.logOut()
        
        LoginHelper.logOut()
        
        Digits.sharedInstance().logOut()
        //        navigationController?.popToRootViewControllerAnimated(true) // Why here is popToRoot?
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let loginView: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
        UIApplication.shared.keyWindow?.rootViewController = loginView
        
        
        GA.event("wifiProfile_logOut")
    }
    
    func clearMapCache(_ callBack:(Bool) -> ()) {
        
        let fileManager = FileManager.default
        let documentsUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
        let documentsPath = documentsUrl?.path
        
        do {
            if let documentPath = documentsPath
            {
                let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                
                print("all files in cache: \(fileNames)")
                
                let contained = fileNames.contains("tiles")
                if !contained {
                    let alertController = UIAlertController (title: QQLocalizedString("Cache is clear"), message: /*QQLocalizedString("Map cache has been successfully deleted")*/ nil, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: QQLocalizedString("Ok"), style: .default) { (_) -> Void in
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                for fileName in fileNames {
                    
                    if (fileName == "tiles")
                    {
                        let filePathName = "\(documentPath)/\(fileName)"
                        do {
                            try fileManager.removeItem(atPath: filePathName)
                            GA.event("wifiProfile_mapCache_delete_success")
                            
                            callBack(true)
                            
                        } catch {
                            GA.event("wifiProfile_mapCache_delete_error")
                            DDLogDebug("somenting went worng with map cache")
                        }
                    }
                }
            }
            
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }

}
