////
////  LocationTracker.swift
////  Pods
////
////  Created by Victor Barskov on 15.06.16.
////
////
//
//import Foundation
//import MapKit
//
//public protocol LocationUpdateProtocol {
//    func locationDidUpdateToLocation(_ location : CLLocation)
//}
//
///// Notification on update of location. UserInfo contains CLLocation for key "location"
//public let kLocationDidChangeNotification = "LocationDidChangeNotification"
//
//open class UserLocationManager: NSObject, CLLocationManagerDelegate {
//    
//    open static let SharedManager = UserLocationManager()
//    
//    fileprivate var locationManager = CLLocationManager()
//    
//    open var currentLocation : CLLocation?
//    
//    open var delegate : LocationUpdateProtocol!
//    
//    fileprivate override init () {
//        super.init()
//        self.locationManager.delegate = self
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        self.locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
//        locationManager.requestAlwaysAuthorization()
//        self.locationManager.startUpdatingLocation()
//    }
//    
//    // MARK: - CLLocationManagerDelegate
//    
//   open  func locationManager(_ manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
//        currentLocation = newLocation
//        let userInfo : NSDictionary = ["location" : currentLocation!]
//        
//        DispatchQueue.main.async { () -> Void in
//            self.delegate.locationDidUpdateToLocation(self.currentLocation!)
//            NotificationCenter.default.post(name: Notification.Name(rawValue: kLocationDidChangeNotification), object: self, userInfo: userInfo as! [AnyHashable: Any])
//        }
//    }
//    
//}
