//
//  String.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

public extension String {
    
    enum DeviceLocale: String {
        
        case Unknown = "en"
        case RU = "ru_RU"
        case US = "en_US"
        case BR = "pt_BR"
        
    }
    
    var deviceLoc: DeviceLocale {
        let locale = Locale.current.identifier
        guard let loc = DeviceLocale(rawValue: locale) else { return .Unknown }
        return loc
    }
    
    func stringByRemovingAll(characters: [Character]) -> String {
        return String(self.characters.filter({ !characters.contains($0) }))
    }
    
}
