////
////  Logger.swift
////  WifiCaptive
////
////  Created by Sergey Pronin on 9/30/15.
////  Copyright © 2015 plotkin. All rights reserved.
////
//
//import UIKit
//
//open class Logger: NSObject {
//    static let shared = Logger()
//    
//    static let logsSession: URLSession = {
//        let config = URLSessionConfiguration.default
//        config.timeoutIntervalForRequest = 90
//        config.timeoutIntervalForResource = 90
//        return URLSession(configuration: config)
//    }()
//    
//    static let logsDirectoryURL = FileManager.default
//        .urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("logs")
//    
//    fileprivate var messages = [String]()
//    
//    fileprivate override init() {
//        super.init()
//        
//        do {
//            try FileManager.default.createDirectory(at: Logger.logsDirectoryURL, withIntermediateDirectories: true, attributes: nil)
//        } catch let error as NSError {
//            print(error)
//        }
//    }
//    
//    class var timestamp: TimeInterval {
//        return Date().timeIntervalSince1970
//    }
//    
//    var analytics: ((_ event: String, _ label: String?) -> Void)?
//    
//    open class func logEvent(_ event: String, label: String? = nil) {
//        shared.analytics?(event, label)
//    }
//    
//    open class func setLoggerAnalytics(_ analytics: @escaping (_ event: String, _ label: String?) -> Void) {
//        shared.analytics = analytics
//    }
//    
//    open class func logMessage(_ message: String) {
//        shared.messages.append("[\(timestamp)]: \(message)")
//        print(message)
//        shared.analytics?("wifi_log", message)
//    }
//    
//    class func flushMessages() {
//        let fileURL = logsDirectoryURL.appendingPathComponent("\(timestamp).log")
//        
//        if shared.messages.count == 0 {
//            return
//        }
//        
//        do {
//            try shared.messages.joined(separator: "\n").write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
//            shared.messages.removeAll()
//        } catch let error as NSError {
//            print(error.description)
//        }
//    }
//    
//    open class func logPage(_ html: String) {
//        print(html)
//        let fileURL = logsDirectoryURL.appendingPathComponent("\(timestamp)html.log")
//        
//        do { try html.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8) }
//        catch { }
//    }
//    
//    open class func sendLogs(_ completion: ((Bool, String) -> Void)? = nil) {
//        flushMessages()
//        
//        do {
//            let fileURLs = try FileManager.default.contentsOfDirectory(at: logsDirectoryURL,
//                includingPropertiesForKeys: nil, options: []).filter({ $0.pathExtension == "log" })
//            
//            if fileURLs.count == 0 {
//                completion?(false, "no logs")
//                return
//            }
//            
//            let request = NSMutableURLRequest(url: URL(string: "http://iappintheair.appspot.com/report_hotspots")!)
//            request.httpMethod = "POST"
//            let boundary = "appintheair123456"
//            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//            request.addAitaAuth()
//            
//            let body = NSMutableData()
//            body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
//            body.append("Content-Disposition: form-data; name=\"count\"\r\n\r\n\(fileURLs.count)".data(using: String.Encoding.utf8)!)
//            
//            for (i, fileURL) in fileURLs.enumerated() {
//                body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
//                body.append("Content-Disposition: form-data; name=\"html\(i)\"; filename=\"\(i).html\"\r\n"
//                    .data(using: String.Encoding.utf8)!)
//                body.append("Content-Type: text/html\r\n\r\n".data(using: String.Encoding.utf8)!)
//                if let data = try? Data(contentsOf: fileURL) {
//                    body.append(data)
//                }
//            }
//            body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
//            
//            request.httpBody = body as Data
//            
//            logsSession.dataTask(with: request, completionHandler: { data, response, error in
//                
//                if error == nil {
//                    do {
//                        for url in fileURLs {
//                            try FileManager.default.removeItem(at: url)
//                        }
//                    } catch { }
//                }
//                
//                DispatchQueue.main.async {
//                    completion?(error == nil, "\(error?.description)")
//                }
//                
//            }).resume()
//        } catch let error as NSError {
//            completion?(false, error.description)
//        } catch { }
//    }
//
//}
