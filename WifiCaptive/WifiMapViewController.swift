//
//  MapView.swift
//  WifiCaptive
//
//  Created by plotkin on 04/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Crashlytics
import MessageUI
import CocoaLumberjack
//import HotspotHelper
import Darwin


let MapSession = URLSession(configuration: URLSessionConfiguration.default)
let connectShowed = UserDefaults.standard.bool(forKey: "connectShowed")

#if AITA
let BaseURL = "https://www.appintheair.mobi/api/"
#else
//    import HotspotHelper
#endif



class WifiMapViewController: UIViewController, ErrorHandling, SuccessHandling {
    
    // MARK: - Properties -
    
    // Offline maps
    
    let loadConfig = URLSessionConfiguration.background(withIdentifier: "offline-map-download")
    let queue: OperationQueue = OperationQueue()
//    fileprivate var circularProgress: KYCircularProgress!
    var center = CLLocationCoordinate2D()
    var tileOverlay: MKTileOverlay?
//    var overlayType: CustomMapTileOverlayType?
    var reachability: Reachability?
    var stopButton: UIButton?
    
    
    var locationManager: CLLocationManager!
    var annotations = [WifiHotspot]()
    var clusteringController : KPClusteringController!
    var selectedAnnotaion = MKAnnotationView()
    var hotspotsRequested = false
    var currentCoordinate: CLLocation?
    var autoCompleteView: PlaceAutoCompleteView!
    var firstPoint: CLLocationCoordinate2D?
    
    let imageView = UIImageView()
    let image = UIImage(named: "searchSmall")
    var flag = false
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var superViewAutoComplete: UIView!
    @IBOutlet weak var findView: UIView!
    @IBOutlet weak var mapOutlet: MKMapView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet var findPlaceTextField: UITextField!
    @IBOutlet weak var labelCount: UILabel!
    @IBAction func searchButton(_ sender: UIButton) {self.findPlaceTextField.becomeFirstResponder()}
    
    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBOutlet weak var downloadMabButton: UIButton!
    
    @IBAction func downloadMapButtonAction(_ sender: UIButton) {
        
        writeEmailLogs()
        
//        GA.event("WiFiMap_downloadMap_tap")
//        
//        let alertController = UIAlertController (title: QQLocalizedString("Download this area?"), message:  nil, preferredStyle: .Alert)
//        
//        
//        let action = UIAlertAction(title: QQLocalizedString("Start"), style: .Default) { (_) -> Void in
//            
//            GA.event("WiFiMap_downloadMap_started")
//            VBMapsManager.shared.startDownloading(self.center.latitude,
//                                                  lon: self.center.longitude,
//                                                  zoom: .Deep,
//                                                  radius: .TwoMiles,
//                                                  progressfillColor: UIColor.brandColor(),
//                                                  progressGuideColor: UIColor.lightGrayColor(),
//                                                  fillLineWidth: 12.0,
//                                                  textLabelFont: CurrentFont(fontSize: 28.0).currentFont,
//                                                  textLabelColor: UIColor.blackColor(),
//                                                  stopButtonColor: UIColor.brandColor(),
//                                                  stopButtonLabelFont: CurrentFont(fontSize: 17.0).currentFont)
//        }
//        
//        alertController.addAction(action)
//        
//        let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .Default) { (_) -> Void in
//            
//        }
//        alertController.addAction(cancelAction)
//        
//        self.presentViewController(alertController, animated: true, completion: nil)
        
    }

    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.downloadMabButton.isHidden = true
        
//        if let token = NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String {
//            let launchedBefore = NSUserDefaults.standardUserDefaults().boolForKey("launchedBefore")
//            if !launchedBefore  {
//                LoginHelper.startHotspotHelper(token)
//            }
//        }
        
        labelCount.font = CurrentFont(fontSize: 18.0).currentFont
        
        // Hiding find view
        superViewAutoComplete.isHidden = true
        findView.isHidden = true
                
        //Status bar style and visibility
        UIApplication.shared.isStatusBarHidden = false
        
        // Empty back button
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        // Add notification to show the profile controller
                
        NotificationCenter.default.addObserver(self, selector: #selector(WifiMapViewController.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
        
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WifiMapViewController.locationUpdateNotification(_:)), name: kLocationDidChangeNotification, object: nil)
        
        self.title = QQLocalizedString("Hotspots")
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)]
        
        labelCount.isHidden = true
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.requestAlwaysAuthorization()
        }
        
        if let center = currentCoordinate {
            centerMapOnLocation(center, animated: false)
        } else {
            locationManager?.startUpdatingLocation()
        }
        
        setNeedsStatusBarAppearanceUpdate()
        
        if #available(iOS 9.0, *) {
            mapOutlet.showsCompass = false
//            currentLocationButton.layer.cornerRadius = 0.5 * currentLocationButton.bounds.size.width
        } else {
            currentLocationButton.isHidden = true
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(WifiMapViewController.hideKeyboard(_:)))
        tapGesture.numberOfTapsRequired = 1
        mapOutlet.addGestureRecognizer(tapGesture)
        
        mapOutlet.delegate = self
        
        let algorithm : KPGridClusteringAlgorithm = KPGridClusteringAlgorithm()
        algorithm.annotationSize = CGSize(width: 25, height: 50)
        algorithm.clusteringStrategy = KPGridClusteringAlgorithmStrategy.twoPhase;
        clusteringController = KPClusteringController(mapView: self.mapOutlet, clusteringAlgorithm: algorithm)
        
        
        self.findPlaceTextField.attributedPlaceholder = NSAttributedString(string:QQLocalizedString("Search"), attributes:[NSFontAttributeName : CurrentFont(fontSize: 20.0).currentFont, NSForegroundColorAttributeName: UIColor.brandColor()])
        
//        let rightButton = UIBarButtonItem(image: UIImage(named: "qrMedium"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(WifiMapViewController.showQR))
//        self.navigationItem.rightBarButtonItem = rightButton
//        self.navigationItem.rightBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5)
        
        currentLocationButton.isHidden = true
        let rightButton = UIBarButtonItem(image: UIImage(named: "currentLocWhite"), style: UIBarButtonItemStyle.done, target: self, action: #selector(WifiMapViewController.centerCurrentLocation))
        self.navigationItem.rightBarButtonItem = rightButton
//        self.navigationItem.rightBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        reachable()
        
        searchButtonOutlet.isHidden = true
        imageView.image = image
        imageView.frame = CGRect(x: 40, y: findPlaceTextField.frame.height/2 - 10 , width: 20, height: 20)
        findPlaceTextField.addSubview(imageView)
        let leftView = UIView.init(frame: CGRect(x: 40, y: 0, width: 20, height: 20))
        findPlaceTextField.leftView = leftView
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.setNavigationBarItem(false)
        
        GA.screen("wifiMap")
        GA.event("wifiMap_saw")
        
        reachable()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // MARK: - SendLogs Button -
        
        // Put the send logs buuton on the nav bar
//        let qos = Int(QOS_CLASS_USER_INITIATED.rawValue)
//        dispatch_async(dispatch_get_global_queue(qos, 0)) { () -> Void in
//            if LJLogger.checkConnectionReachability() {
//                dispatch_async(dispatch_get_main_queue()) {
//                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Compose, target: self, action: #selector(WifiMapViewController.writeEmailTapped(_:)))
//                }
//            } else {
//                dispatch_async(dispatch_get_main_queue()) {
//                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "list"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(WifiMapViewController.segueToListOfHotspots(_:)))
//                }
//            }
//        }
        
//        print("findView.bounds.width", findView.bounds.width)
//        print("findView.bounds.height",findView.bounds.height)
        
        autoCompleteView = PlaceAutoCompleteView(frame: CGRect(x: 0, y: 0, width: findView.frame.width, height: findView.frame.height))
        autoCompleteView.delegate = self
        findView.addSubview(autoCompleteView)
        findPlaceTextField.delegate = self
        
        checkOnAlwaysLocationAccess()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        reachability!.stopNotifier()
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }
    
    // MARK: - Lumberjack email send helper -
    
    // PerformSegueWithIdentifier to show HotspotListViewController
    
    @IBAction func segueToListOfHotspots (_ sender: AnyObject) {
        GA.event("HotspotList_tap")
        self.performSegue(withIdentifier: "ListHotspots", sender: self)
    }
    
    @IBAction func writeEmailTapped(_ sender: AnyObject) {
        
        writeEmailLogs()
        
//        if LJLogger.checkConnectionReachability() {
//            
//            if MFMailComposeViewController.canSendMail() {
//                let composeVC = MFMailComposeViewController()
//                composeVC.mailComposeDelegate = self
//                
//                // Configure the fields of the interface.
//                composeVC.setToRecipients(["victor.barskov@gmail.com"])
//                composeVC.setSubject("Feedback for TEST WYG")
//                composeVC.setMessageBody("", isHTML: false)
//                
//                let attachmentData = NSMutableData()
//                
//                for logFileData in LJLogger.sharedInstance.logFileDataArray {
//                    attachmentData.appendData(logFileData)
//                }
//                let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
//                composeVC.addAttachmentData(attachmentData, mimeType: "text/plain", fileName: "diagnostic-\(timestamp).log")
//                self.presentViewController(composeVC, animated: true, completion: nil)
//            } else {
//                
//                // Tell user about not able to send email directly.
//                UIAlertView(title: QQLocalizedString("Sorry"), message: QQLocalizedString("Please try again later"), delegate: nil, cancelButtonTitle: "OK").show()
//            }
//            
//        } else {
//            // Tell user about not able to send email directly.
//            UIAlertView(title: QQLocalizedString("Sorry"), message: QQLocalizedString("No connection to the internet"), delegate: nil, cancelButtonTitle: "OK").show()
//        }
        
    }
    
    // MARK: - Methods -
    
    func goToLocationSettings() {
        
        let alertController = UIAlertController (title: QQLocalizedString("App requires access to your current location. Please set location access to 'Always'"), message: QQLocalizedString("Go to Settings?"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: QQLocalizedString("Settings"), style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url)
            }
        }
        
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func checkOnAlwaysLocationAccess() {
        
        switch CLLocationManager.authorizationStatus() {
   
        case .restricted, .denied:
            // Disable location features
            goToLocationSettings()
            break
        case .authorizedWhenInUse:
            goToLocationSettings()
            break
        case .notDetermined:
            break
        case .authorizedAlways:
            break
        }
    }

    
    fileprivate func reachable () {
        
        reachability = Reachability()

        reachability!.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                
                if reachability.isReachableViaWiFi {
                    
                    GA.event("WiFiMap_on_WiFi")
                    
                } else {
                    
                    GA.event("WiFiMap_on_Cellular")
                }
            }
        }
        
        reachability!.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                
                GA.event("WiFiMap_on_offline")
                self.downloadMabButton.isHidden = true

            }
        }
        
        do {try reachability!.startNotifier()} catch {print("Unable to start notifier")}
        
    }
        
    func addWiFiNetwork() {
                
        delay(0.3) {
            
            if #available(iOS 9.0, *) {
                if let network = currentNetwork {
                    if !network.isSecure {
                        self.handleError(error: QQLocalizedString("Please connect to password-protected network to add it"))
                        return
                    }
                }
            }
            
            if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways)
            {
                
                guard let addNetworkController =
                    self.storyboard?.instantiateViewController(withIdentifier: "AddNetworkViewController") as? AddNetworkViewController else { return }
                addNetworkController.transitioningDelegate = TransitioningDelegate
                
                self.present(addNetworkController, animated: true, completion: nil)
                
                GA.event("wifiProfile_addNetwork")
                
            } else {
                
                let locationAuthStatus = CLLocationManager.authorizationStatus()
                
                if locationAuthStatus == .denied || locationAuthStatus == .restricted {
                    
                    self.goToLocationSettings()
                    
                }
                
            }
        }
    }
    
    @IBAction func findPlaceEditingChanged(_ sender: AnyObject) {
        guard let text = findPlaceTextField.text, text.characters.count > 0 else { return }
        
        findView.isHidden = false
        imageView.isHidden = true
        superViewAutoComplete.isHidden = false
        autoCompleteView!.new(text)
    }
    
    @IBAction func findPlaceEditingDidEnd(_ sender: AnyObject) {
        print("ok")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "ListHotspots"){
            let viewController = segue.destination as! HotspotListViewController
            viewController.title = QQLocalizedString("List of Hotspots")
            viewController.annotations = self.annotations
            
        }
        if (segue.identifier == "DetailsHotspot"){
            let viewController = segue.destination as! HotspotDetailsViewController
            viewController.title = QQLocalizedString("Hotspot Details")
            viewController.point = (selectedAnnotaion.annotation as! KPAnnotation).annotations.first as! WifiHotspot
        }
    }
    
    func setPointsOnMap(_ lat: Double, lon: Double, distance: Double) {
        WifiHotspot.fetchHotspotsForLocation(CLLocationCoordinate2D(latitude: lat, longitude: lon), distance: distance) { success in
            if success {
                self.reloadMapData()
            }
            CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
        }
    }
    
    func updateHotspotsCount(_ lat: Double, lon: Double, distance: Double) {
        
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        WifiHotspot.fetchHotspotsCountForLocation(coordinate, distance: distance) { [weak self] success, count in
            if let count = count, success && count > 0 {
                self?.labelCount.isHidden = false
                self?.labelCount.text = String(format: QQLocalizedString("Hotspots in the area: %d"), count)
            } else {
                self?.labelCount.isHidden = true
            }
        }
        
    }
    
    func centerMapOnLocation(_ location: CLLocation, animated: Bool = true) {
        
        let regionRadius: CLLocationDistance = 2000
        var coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        coordinateRegion.span.latitudeDelta = 0.5
        coordinateRegion.span.longitudeDelta = 0.5
        
        mapOutlet.setRegion(coordinateRegion, animated: animated)
        
    }
    
    func reloadMapData() {
        
        let latitude1 = mapOutlet.region.center.latitude - mapOutlet.region.span.latitudeDelta/3
        let latitude2 = mapOutlet.region.center.latitude + mapOutlet.region.span.latitudeDelta/3
        
        let longitude1 = mapOutlet.region.center.longitude - mapOutlet.region.span.longitudeDelta/3
        let longitude2 = mapOutlet.region.center.longitude + mapOutlet.region.span.longitudeDelta/3
        
        WifiHotspot.cd_hotspotsWithin(latitude1: latitude1, latitude2: latitude2, longitude1: longitude1, longitude2: longitude2) { hotspots in
            
            self.annotations = hotspots
            self.clusteringController.setAnnotations(hotspots)
            
        }
    }
    
    
    func hideKeyboard(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    //MARK: - Events -
    
    func writeEmailLogs () {
        
        if LJLogger.checkConnectionReachability() {
            
            if MFMailComposeViewController.canSendMail() {
                let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self
                
                // Configure the fields of the interface.
                composeVC.setToRecipients(["victor.barskov@gmail.com"])
                composeVC.setSubject("\(Date()) Feedback for TEST WYG")
                composeVC.setMessageBody("", isHTML: false)
                
                let attachmentData = NSMutableData()
                
                for logFileData in LJLogger.sharedInstance.logFileDataArray {
                    attachmentData.append(logFileData as Data)
                }
                let timestamp = DateFormatter.localizedString(from: Date(), dateStyle: .medium, timeStyle: .short)
                composeVC.addAttachmentData(attachmentData as Data, mimeType: "text/plain", fileName: "diagnostic-\(timestamp).log")
                self.present(composeVC, animated: true, completion: nil)
            } else {
                
                // Tell user about not able to send email directly.
                self.handleError(error: QQLocalizedString("Please try again later"))
            }
            
        } else {
            // Tell user about not able to send email directly.
            self.handleError(error: QQLocalizedString("No connection to the internet"))
        }

        
    }
    
    @IBAction func sendLogs(_ sender: AnyObject) {
//            MBProgressHUD.showHUDAddedTo(UIWindow.mainWindow, animated: true)
            CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
            // TODO: - Change for HotspotHelper migration -
            
//            Logger.sendLogs { completed, message in
//                dispatch_async(dispatch_get_main_queue()) {
//                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
//                    //MBProgressHUD.hideAllHUDsForView(UIWindow.mainWindow, animated: true)
//                    
//                    UIAlertView(title: "info", message: "sent status: \(completed) - \(message)", delegate: nil, cancelButtonTitle: "OK").show()
//                }
//            }
            
    }
    
    
    func showQR() {
        
        GA.event("WiFiMapViewView_show_qr_tap")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let qrViewController = storyboard.instantiateViewController(withIdentifier: "QRViewController") as? QRViewController else { return }
        let navigationController = UINavigationController(rootViewController: qrViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    
    func openProfileVC () {
                
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork()
        }
    }
    
    func openProfile(_ animated: Bool = true) {

            if UserDefaults.standard.object(forKey: AitaToken) == nil {
                guard let loginViewController =
                    storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
                
                navigationController?.pushViewController(loginViewController, animated: animated)
            } else {
                
                guard let profileController =
                    storyboard?.instantiateViewController(withIdentifier: "WifiProfileViewController") as? WifiProfileViewController else {return }
                
                navigationController?.pushViewController(profileController, animated: animated)
            }
        GA.event("wifiMap_profile")
    }
    
    @IBAction func clickBarProfile(_ sender: AnyObject) {
        openProfile()
    }
    
    var shouldCenter = false
    
    @IBAction func centerOnCurrentLocation(_ sender: AnyObject) {
        
        shouldCenter = true
        locationManager?.startUpdatingLocation()
        
        GA.event("wifiMap_centerMap")
        
    }
    
    func centerCurrentLocation() {
        
        shouldCenter = true
        locationManager?.startUpdatingLocation()
        
        GA.event("wifiMap_centerMap")
    }
    
}



// MARK: - Extensions -

extension WifiMapViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
}

extension WifiMapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last!
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        self.mapOutlet.setRegion(region, animated: true)
        self.currentCoordinate = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        manager.stopUpdatingLocation()
        
        if shouldCenter {
            shouldCenter = false
            if let currentCoordinate = currentCoordinate {
                let regionRadius: CLLocationDistance = 10000
                var coordinateRegion = MKCoordinateRegionMakeWithDistance(currentCoordinate.coordinate,
                                                                          regionRadius * 2.0, regionRadius * 2.0)
                coordinateRegion.span.latitudeDelta = 0.01
                coordinateRegion.span.longitudeDelta = 0.01
                mapOutlet.setRegion(coordinateRegion, animated: true)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
    }
}



extension WifiMapViewController: MKMapViewDelegate {
    
    
    func shareCurrentLocationInBackGround() -> /*(lat: Double, lon: Double )*/CLLocation {
        
        var locationShare = CLLocation()
        
        if let location = self.locationManager.location {
            locationShare = location
        }
        //return (locationShare.coordinate.latitude, locationShare.coordinate.longitude)
        return locationShare
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        guard let tileOverlay = overlay as? MKTileOverlay else {
            return MKOverlayRenderer()
        }
        
        return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }

        var annotationView : MKAnnotationView?
    
            if let annotation = annotation as? KPAnnotation {
                if annotation.isCluster() {
                    annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "cluster") as MKAnnotationView!
                    if annotationView == nil {
                        annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "cluster")
                    }
                    
                    
                    annotationView!.image = UIImage(named:"multiple_marker")
                    if (annotationView!.subviews.count > 0) {
                        let l =  annotationView!.subviews[0] as? UILabel
                        l?.text = String(annotation.annotations.count)
                        
                    } else {
                        
                        let label = UILabel(frame: CGRect(x: 5, y: 7, width: 20, height: 15))
                        label.text = String(annotation.annotations.count)
                        label.textAlignment = NSTextAlignment.center
                        label.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1)
                        label.font = UIFont(name: "Helvetica-Bold", size: 15)
                        label.minimumScaleFactor = 0.5
                        label.adjustsFontSizeToFitWidth = true
                        annotationView?.addSubview(label)
                        
                    }
    
                } else {
                    
                    annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") as? MKPinAnnotationView
    
                    if (annotationView == nil) {
                        annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
                    }
                    let hotspot = annotation.annotations.first as! WifiHotspot
                    annotationView!.image = UIImage(named: hotspot.isUserHotspot ? "user_marker" : "signal_marker")
                    annotationView!.rightCalloutAccessoryView = UIButton(type: UIButtonType.detailDisclosure)
                }
            }
            return annotationView
    
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if  view.annotation as? MKUserLocation != nil {
            return
        }
        
        self.selectedAnnotaion = view
        let v = CalloutView(frame: CGRect(x: 0, y: self.view.frame.height - 131, width: self.view.frame.width, height: 131))
        let point = (view.annotation as! KPAnnotation).annotations.first as! WifiHotspot
        
        
        v.descField.text = point.ssid
        v.point = point
        v.directionsOutlet.tintColor = UIColor.brandColor()
        

        DispatchQueue.global().async {
            
            WifiConfiguration.configurationForNetworkWithSSIDCords(ssid: point.ssid, allowIgnored: false, latutude: point.latitude, longitude: point.longitude) { (params) in
                
                if params != nil {
                    
                    DispatchQueue.main.async {
                        
                        if let password = params?["hotspot_password"].string {
                            v.passwordLabel.text = NSLocalizedString("Password: ", comment: "") + password
                        }
                    }
                    
                } else {
                    // Do something
                }
            }
        }
        
        
        let currentCoords = shareCurrentLocationInBackGround()
        let mkLoc = CLLocation(latitude: point.latitude, longitude: point.longitude)
        let distance = currentCoords.distance(from: mkLoc)
        
        if distance < 250 {
            v.directionsOutlet.setTitle(QQLocalizedString("Connect"), for: UIControlState())
        } else {
            v.directionsOutlet.setTitle(QQLocalizedString("Get Directions"), for: UIControlState())
        }

        v.tag = 1000
        v.delegate = self
        self.view.addSubview(v)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        let v = self.view.viewWithTag(1000)
        v?.removeFromSuperview()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        reloadMapData()
        
        let currentCenter = mapView.region.center
        
        if firstPoint == nil || abs(firstPoint!.latitude - currentCenter.latitude) > 0.001 || abs(firstPoint!.longitude - currentCenter.longitude) > 0.001 {
            firstPoint = currentCenter
            
            let mapRect = mapView.visibleMapRect;
            let eastMapPoint = MKMapPointMake(MKMapRectGetMinX(mapRect), MKMapRectGetMidY(mapRect))
            let westMapPoint = MKMapPointMake(MKMapRectGetMaxX(mapRect), MKMapRectGetMidY(mapRect));
            let distance = MKMetersBetweenMapPoints(eastMapPoint, westMapPoint)
            
            setPointsOnMap(currentCenter.latitude, lon: currentCenter.longitude, distance: distance > 20000 ? 20000 : distance)
            updateHotspotsCount(currentCenter.latitude, lon: currentCenter.longitude, distance: distance > 500_000 ? 500_000 : distance)
            
        }
        center = mapView.centerCoordinate
    }
}

extension WifiMapViewController: PlaceAutoCompleteViewDelegate {
    
    func selectPlace(_ address: Address) {
        
        findView.isHidden = true
        superViewAutoComplete.isHidden = true
        autoCompleteView.clear()
        flag = true
        imageView.isHidden = true
        print("self.imageView.hidden = ", self.imageView.isHidden)
        
        self.findPlaceTextField.resignFirstResponder()
        if address.description.characters.count > 0 {
            self.findPlaceTextField.text = "\(address.title), \(address.description)"
        } else {
            self.findPlaceTextField.text = "\(address.title)"
        }
        
        let query = address.key.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let urlStringQuery = query else { return }
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        MapSession.dataTask(with: URLRequest(url: URL(string: BaseURL + "autocomplete/resolve_key?key=\(urlStringQuery)")!), completionHandler: { data, response, error in
            if let data = data {
                let json = JSON(data: data)
                guard let latitude = json["address"]["location"]["lat"].double else { return }
                guard let longitude = json["address"]["location"]["lng"].double else { return }
                
                let location = CLLocation(latitude: latitude, longitude: longitude)
                DispatchQueue.main.async {
                    self.centerMapOnLocation(location)
                }
            }
            DispatchQueue.main.async {
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
            }
            }).resume()
    }
}


extension WifiMapViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ findPlaceTextField: UITextField) -> Bool {
        
        findView.isHidden = true
        imageView.isHidden = false
        superViewAutoComplete.isHidden = true
        self.view.endEditing(true)
        
        if (findPlaceTextField.text?.characters.count == 0) {
            return true
        }
        
        CLGeocoder().geocodeAddressString(findPlaceTextField.text!, completionHandler: {(placemarks, error) -> Void in
            let placemarksArray = placemarks as [CLPlacemark]?
            if placemarksArray != nil {
                if placemarksArray!.count > 0 {
                    guard let placeLocation = placemarksArray!.first!.location else {
                        return
                    }
                    
                    let regionRadius: CLLocationDistance = 1000
                    var coordinateRegion = MKCoordinateRegionMakeWithDistance(placeLocation.coordinate,
                        regionRadius * 2.0, regionRadius * 2.0)
                    coordinateRegion.span.latitudeDelta = 0.08
                    coordinateRegion.span.longitudeDelta = 0.08
                    DispatchQueue.main.async {
                        self.mapOutlet.setRegion(coordinateRegion, animated: true)
                    }
                }
            } else {
                self.handleError(error: QQLocalizedString("No Results Found"))
            }
        })
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        autoCompleteView.clear()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        findView.isHidden = true
        imageView.isHidden = false

        superViewAutoComplete.isHidden = true
        autoCompleteView.clear()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(Double(NSEC_PER_SEC) * 0.05)) / Double(NSEC_PER_SEC)) {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if imageView.isHidden {
            if flag {
                imageView.isHidden = true
                flag = false
            } else {
                imageView.isHidden = false
            }
        }
    }
}

extension WifiMapViewController: CalloutViewDelegate {
    
    func NavigateToHotspotDetails() {
        #if AITA
            let viewController = HotspotDetailsViewController()
            viewController.title = QQLocalizedString("Hotspot Details")
            viewController.point = (selectedAnnotaion.annotation as! KPAnnotation).annotations.first as! WifiHotspot
            navigationController?.pushViewController(viewController, animated: true)
        #else
            
            self.performSegue(withIdentifier: "DetailsHotspot", sender: self)
            
        #endif
        GA.event("wifiMap_hotspotDetails")
    }
    
    func showPopUpDirections () {
        
        if connectShowed == true {
            
            let point = (selectedAnnotaion.annotation as! KPAnnotation).annotations.first as! WifiHotspot
            let coordinate = CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude)
            let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let destionation = MKMapItem(placemark: place)
            
            destionation.name = point.name
            destionation.openInMaps(launchOptions: nil)
            
            
        } else {
            
            let point = (selectedAnnotaion.annotation as! KPAnnotation).annotations.first as! WifiHotspot
            let coordinate = CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude)
            let _ = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
//            let destionation = MKMapItem(placemark: place)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let popUpController =
                self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as? PopUpViewController else { return }
            popUpController.mode = .getDirections
            popUpController.transitioningDelegate = TransitioningDelegate
            //            passwordController.mode = .Login
            //        popUpController.delegate = self
            popUpController.point = point
            present(popUpController, animated: true, completion: nil)
        }
        
    }
    

    
    func showPopUp() {
        
        if connectShowed == true {
            
            if #available(iOS 10, *) {
                
                guard let popUpController =
                    self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as? PopUpViewController else { return }
                popUpController.transitioningDelegate = TransitioningDelegate
                popUpController.mode = .ok
                self.present(popUpController, animated: true, completion: nil)
                
                
            } else {
                let settingsUrl = URL(string: "prefs:root=WIFI")
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            
        } else {
            
            //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let popUpController =
                self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as? PopUpViewController else { return }
            
            popUpController.transitioningDelegate = TransitioningDelegate
            //            passwordController.mode = .Login
            //        popUpController.delegate = self
            
            present(popUpController, animated: true, completion: nil)
        }
    }
}




