//
//  Settings.swift
//  emop
//
//  Created by Victor Barskov on 20/11/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import UIKit

class Settings: NSObject {
    
    static private let dict = Settings.loadSettings()

    private class func loadSettings() -> NSDictionary {
        guard let url = Bundle.main.url(forResource: "Settings", withExtension: "plist"), let dict = NSDictionary(contentsOf: url) else { fatalError("plist") }
        return dict
    }
    
    public class func settingForKey(key: String) -> AnyObject {
        return dict.object(forKey: key)! as AnyObject
    }
    
    
    // MARK: - Networking -

//    static let API_URL = (Bundle.main.object(forInfoDictionaryKey: "API_WEB_URL") as? String) ?? "https://www.wifiasyougo.com/api/"
    
    // MARK: - Auth & IDs -
    
    static let AitaToken = Setting<String>(key: "HotspotAitaToken")
    static let DeviceToken = Setting<String>(key: "DeviceToken")
    static let ForceLogout = Setting<String>(key: "ForceLogout")

    
    // MARK: - Canary -
    static let AppID = Setting<String>(key: "AppID")
    
    // images
    static let LoginImageName = Setting<String>(key: "LoginImageName")
    // colors
    static let LoginBackgroundColor = Setting<UIColor>(key: "LoginBackgroundColor")
    static let SignInButtonBackgroundColor = Setting<UIColor>(key: "SignInButtonBackgroundColor")
    static let SignInButtonForegroundColor = Setting<UIColor>(key: "SignInButtonForegroundColor")
    static let SignUpButtonForegroundColor = Setting<UIColor>(key: "SignUpButtonForegroundColor")
    static let BrandColor = Setting<UIColor>(key: "BrandColor")
    static let PaymentOptionsBackgroundColor = Setting<UIColor>(key: "PaymentOptionsBackgroundColor")
    static let PaymentOptionsForegroundColor = Setting<UIColor>(key: "PaymentOptionsForegroundColor")
    // urls
    static let baseURL = Setting<String>(key: "BaseURL")
    static let SignUpURL = Setting<String>(key: "SignUpURL")
    static let SignInURL = Setting<String>(key: "SignInURL")
    static let HotspotsURL = Setting<String>(key: "HotspotsURL")
    static let SubscriptionsURL = Setting<String>(key: "SubscriptionsURL")
    static let FreeSubscriptionURL = OptionalSetting<String>(key: "FreeSubscriptionURL")
    // payments
    static let PaymentType = Setting<String>(key: "PaymentType")
    static let PaymentInitURL = Setting<String>(key: "PaymentInitURL")
    // other
    static let CustomConfig = OptionalSetting<String>(key: "CustomConfig")
    static let HotspotHelperName = Setting<String>(key: "HotspotHelperName")
    static let AddNetworksHidden = Setting<Bool>(key: "AddNetworksHidden")
  
}

