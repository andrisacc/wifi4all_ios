//
//  SearchViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 16.08.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {

    var searchController : UISearchController!
    func updateSearchResults(for searchController: UISearchController) {
    }
    
//    func didPresentSearchController(searchController: UISearchController) {
//        self.searchController.searchBar.becomeFirstResponder()
//    }
    func presentSearchController(_ searchController: UISearchController) {
        self.searchController.searchBar.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchController.isActive = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Configure the search bar
        self.searchController = UISearchController(searchResultsController:  nil)
        self.searchController.definesPresentationContext = true
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
//        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
        self.navigationItem.titleView = searchController.searchBar

        
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DispatchQueue.main.async(execute: {
//            self.searchController.active = true
            self.searchController.searchBar.becomeFirstResponder()
        })
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
