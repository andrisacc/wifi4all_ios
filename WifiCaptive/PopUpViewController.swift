
//
//  SignupViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 19/08/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import Alamofire
//import HotspotHelper
import DigitsKit
import CocoaLumberjack

//enum SignupViewControllerMode {
//    case Login, SignUp, Password
//}

protocol PopUpControllerDelegate: NSObjectProtocol {
    func popUpViewControllerSucceeded(_ controller: PopUpViewController)
}

enum PopUpViewControllerMode {
    case connect, getDirections, ok
}

class PopUpViewController: UIViewController, UITextFieldDelegate, UIViewControllerTransitioningDelegate {
    
    // MARK: - IBOutlets -

    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var helperImage: UIImageView!
    @IBOutlet weak var annoLabel: UILabel!
    
    var emailUP: String?
    
    // MARK: - Propeties -
    weak var delegate: PasswordControllerDelegate?
    //    var mode = SignupViewControllerMode.Login
    
    var mode = PopUpViewControllerMode.connect
    
    let deafaults = UserDefaults.standard
    let scale = (UIScreen.main.bounds.height / 667.0 > 1) ? 1 : UIScreen.main.bounds.height / 667.0
    //    var authByPhoneToken = NSUserDefaults.standardUserDefaults()
    var point: WifiHotspot?
    
    @IBAction func clearButtonAction(_ sender: UIButton) {
                
        if mode == .connect {
                        
            if #available(iOS 10, *) {
                UIApplication.shared.open(URL(string:"App-Prefs:root=WIFI")!, options: [:], completionHandler: nil)
                self.dismiss(animated: true, completion: nil)
            } else {
                let settingsUrl = URL(string: "prefs:root=WIFI")
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            
            GA.event("wifiPopUpConnectButton_tap")
            
        } else if mode == .ok {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(URL(string:"App-Prefs:root=WIFI")!, options: [:], completionHandler: nil)
                self.dismiss(animated: true, completion: nil)
            } else {
                let settingsUrl = URL(string: "prefs:root=WIFI")
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        else {
            
            guard let point = point else {return}
            let coordinate = CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude)
            let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let destionation = MKMapItem(placemark: place)
            
            destionation.name = point.name
            destionation.openInMaps(launchOptions: nil)
            
            self.dismiss(animated: true, completion: nil)
            
            GA.event("wifiPopUpDirectionsButton_tap")
        }
        
    }
    
    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        
        self.preferredContentSize = CGSize(width: 200, height: 420)
//        let imagePop = UIImageView(frame: CGRectMake(8, 8, 320 * scale, 320 * scale))
//        let image = UIImage(named: "popUpSettings")
//        imagePop.image = image
//        self.view.addSubview(imagePop)
        
        mode == .connect ? clearButton.setTitle(QQLocalizedString("Start connection"), for: UIControlState()): clearButton.setTitle(QQLocalizedString("Get Directions"), for: UIControlState())
        if mode == .ok {
            clearButton.setTitle(QQLocalizedString("OK"), for: UIControlState())
            annoLabel.text = QQLocalizedString("Now check your settings!\n Connect seamlessly!")
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.screen("wifiPopUpViewController")
        GA.event("wifiPopUpViewController_saw")
        
        
        UserDefaults.standard.set(true, forKey: "connectShowed")
        UserDefaults.standard.synchronize()
        
    }
    
    func presentError(_ error: String) {
        
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }

}
