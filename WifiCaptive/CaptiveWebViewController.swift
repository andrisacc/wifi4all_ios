////
////  CaptiveWebViewController.swift
////  WifiHotspot
////
////  Created by Sergey Pronin on 8/18/15.
////  Copyright © 2015 App in the Air. All rights reserved.
////
//
//import UIKit
//import NetworkExtension
//import PKHUD
//import CocoaLumberjack
//
//protocol CaptiveWebViewControllerDelegate: NSObjectProtocol {
//    
//    func captiveWebViewControllerDidSubmitForm(controller: CaptiveWebViewController,
//        withConfiguration configuration: [String: AnyObject]?, success: Bool)
//    
//    func captiveWebViewControllerDidCancel(controller: CaptiveWebViewController)
//    
//    func captiveWebViewControllerDidFindSuccessPage(controller: CaptiveWebViewController)
//}
//
////FIXME: internal
//public class CaptiveWebViewController: UIViewController {
//    
//    //workaround for 8.0 and 9.0 compatibility
//    private var _command: AnyObject!
//    
//    @available(iOS 9.0, *)
//    var command: NEHotspotHelperCommand! {
//        get { return _command as! NEHotspotHelperCommand }
//        set { _command = newValue }
//    }
//    
//    weak var delegate: CaptiveWebViewControllerDelegate?
//    
//    let webView = UIWebView()
//    
//    var wifiConfiguration: WifiConfiguration!
//    var postInvoked = false
//    var updatedConfig: [String: AnyObject]?
//    var spinner = UIActivityIndicatorView()
//
//    override public func viewDidLoad() {
//        super.viewDidLoad()
//        
//        title = "Authorization"
//        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel,
//            target: self, action: #selector(CaptiveWebViewController.clickCancel(_:)))
//        
//        automaticallyAdjustsScrollViewInsets = false
//        
//        webView.translatesAutoresizingMaskIntoConstraints = false
//        
//        self.view.addSubview(webView)
//        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "V:[webView]-(0)-|", options: .AlignAllBaseline, metrics: nil, views: ["webView": webView]))
//        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "H:|-(0)-[webView]-(0)-|", options: .AlignAllBaseline, metrics: nil, views: ["webView": webView]))
//
//        webView.delegate = self
//        
//        self.view.addSubview(spinner)
//        spinner.activityIndicatorViewStyle = .Gray
//        spinner.center = self.view.center
//        
//        spinner.hidesWhenStopped = true
//        setupCredentialsView()
//    }
//    
//    override public func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        spinner.startAnimating()
//        
//        webView.loadRequest(NSURLRequest(URL: NSURL(string: BaseURLInsecured + "invisible")!))
//    }
//    
//    public override func viewWillDisappear(animated: Bool) {
//        super.viewWillDisappear(animated)
//        
//        Logger.flushMessages()
//    }
//    
//    func setupCredentialsView() {
//        let username: String, password: String
////        if let credentials = wifiConfiguration?["params"]["_credentials"].dictionary {
////            username = credentials["username"]!.string!
////            password = credentials["password"]!.string!
////        } else {
//        if let regex = wifiConfiguration?.SSIDregex {
//            let credentials = HotspotCredentials.credentialsForSSIDRegex(regex)
//            username = credentials.username
//            password = credentials.password
//        } else {
//            (username, password) = ("username", "password")
//        }
////        }
//        let credentialsView = UIView()
//        credentialsView.translatesAutoresizingMaskIntoConstraints = false
//        credentialsView.backgroundColor = UIColor.whiteColor()
//        self.view.addSubview(credentialsView)
//        self.view.addConstraint(NSLayoutConstraint(item: self.view, attribute: .Top, relatedBy: .Equal,
//            toItem: credentialsView, attribute: .Top, multiplier: 1, constant: -60))
//        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "V:[view(60)]", options: .AlignAllBaseline, metrics: nil, views: ["view": credentialsView]))
//        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "H:|-(0)-[view]-(0)-|", options: .AlignAllBaseline, metrics: nil, views: ["view": credentialsView]))
//        let labelUsername = UILabel()
//        if #available(iOS 8.2, *) {
//            labelUsername.font = UIFont.systemFontOfSize(16, weight: UIFontWeightLight)
//        } else {
//            labelUsername.font = UIFont(name: "HelveticaNeue-Light", size: 16)
//        }
//        labelUsername.textAlignment = .Center
//        labelUsername.userInteractionEnabled = true
//        labelUsername.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CaptiveWebViewController.tapCredentials(_:))))
//        labelUsername.text = username
//        labelUsername.translatesAutoresizingMaskIntoConstraints = false
//        credentialsView.addSubview(labelUsername)
//        credentialsView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "V:|-(0)-[view(30)]", options: .AlignAllBaseline, metrics: nil, views: ["view": labelUsername]))
//        credentialsView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "H:|-(0)-[view]-(0)-|", options: .AlignAllBaseline, metrics: nil, views: ["view": labelUsername]))
//        
//        let labelPassword = UILabel()
//        if #available(iOS 8.2, *) {
//            labelPassword.font = UIFont.systemFontOfSize(16, weight: UIFontWeightLight)
//        } else {
//            labelPassword.font = UIFont(name: "HelveticaNeue-Light", size: 16)
//        }
//        labelPassword.textAlignment = .Center
//        labelPassword.userInteractionEnabled = true
//        labelPassword.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CaptiveWebViewController.tapCredentials(_:))))
//        labelPassword.text = password
//        labelPassword.translatesAutoresizingMaskIntoConstraints = false
//        credentialsView.addSubview(labelPassword)
//        credentialsView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "V:[view(30)]-(0)-|", options: .AlignAllBaseline, metrics: nil, views: ["view": labelPassword]))
//        credentialsView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "H:|-(0)-[view]-(0)-|", options: .AlignAllBaseline, metrics: nil, views: ["view": labelPassword]))
//        
//        let separator = UIView()
//        separator.backgroundColor = UIColor.lightGrayColor()
//        separator.translatesAutoresizingMaskIntoConstraints = false
//        credentialsView.addSubview(separator)
//        credentialsView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "V:[view(1)]-(0)-|", options: .AlignAllBaseline, metrics: nil, views: ["view": separator]))
//        credentialsView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
//            "H:|-(0)-[view]-(0)-|", options: .AlignAllBaseline, metrics: nil, views: ["view": separator]))
//        
//        self.view.addConstraint(NSLayoutConstraint(item: webView, attribute: .Top, relatedBy: .Equal,
//            toItem: credentialsView, attribute: .Bottom, multiplier: 1, constant: 0))
//    }
//    
//    func checkConnectionReachability(completion: Bool -> Void) {
//        let request = NSMutableURLRequest(URL: URLGenerate204)
//        if #available(iOS 9.0, *) {
//            request.bindToHotspotHelperCommand(command)
//        }
//        
//        SharedSession.dataTaskWithRequest(request) { data, response, error in
//            if let error = error {
//                print(error.description)
//                DDLogDebug(error.description)
//                completion(false)
//                return
//            }
//            
//            guard let response = response as? NSHTTPURLResponse else {
//                completion(false)
//                return
//            }
//            
//            if response.statusCode == 204 {
//                completion(true)
//            } else {
//                completion(false)
//            }
//            
//        }.resume()
//    }
//    
//    func fixLinks() {
//        let link = "https://service.thecloud.net"
//        webView.stringByEvaluatingJavaScriptFromString(
//            "var links = document.getElementsByTagName('a'); " +
//            "for (var i=0; i < links.length; i++) { " +
//                "var link = links[i]; var href = link.getAttribute('href'); " +
//                "if (href.startsWith('/')) { " +
//                    "link.setAttribute('href', '\(link)' + href); " +
//                "} " +
//            "}")
//        
//        webView.stringByEvaluatingJavaScriptFromString(
//            "var imgs = document.getElementsByTagName('img'); " +
//            "for (var i=0; i < imgs.length; i++) { " +
//                "var img = imgs[i]; var src = img.getAttribute('src'); " +
//                "if (src.startsWith('/')) { " +
//                "img.setAttribute('src', '\(link)' + src); " +
//                "} " +
//            "}")
//    }
//    
//    //MARK: Events
//    func clickCancel(sender: AnyObject?) {
//        delegate?.captiveWebViewControllerDidCancel(self)
//    }
//    
//    func tapCredentials(recognizer: UITapGestureRecognizer) {
//        if recognizer.state == .Ended {
//            UIPasteboard.generalPasteboard().string = (recognizer.view as! UILabel).text!
//            PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Copied!")
//            PKHUD.sharedHUD.dimsBackground = false
//            PKHUD.sharedHUD.show()
//            PKHUD.sharedHUD.hide(afterDelay: 0.5)
//        }
//    }
//}
//
//
////MARK: - UIWebViewDelegate
//extension CaptiveWebViewController: UIWebViewDelegate {
//    
//    public func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//        Logger.logMessage("captive shouldStart \(request.HTTPMethod!) \(request.URL!)")
//        DDLogDebug("captive shouldStart \(request.HTTPMethod!) \(request.URL!)")
//        
//        if request.HTTPMethod == "POST" && request.HTTPBody != nil {
//            updatedConfig = extractConfigurationFromRequest(request)
//            print("config gathered: \(updatedConfig)")
//            DDLogDebug("config gathered: \(updatedConfig)")
//            postInvoked = true
//        }
//        
//        return true
//    }
//    
//    public func webViewDidFinishLoad(webView: UIWebView) {
//        spinner.stopAnimating()
//        if let html = webView.stringByEvaluatingJavaScriptFromString("document.documentElement.outerHTML"),
//           let url = webView.stringByEvaluatingJavaScriptFromString("document.location.href") {
//            Logger.logMessage("captive finish url \(url)")
//            DDLogDebug("captive finish url \(url)")
//            
//            Logger.logPage(html)
//            Logger.flushMessages()
//        }
//        //fixLinks()
//        if postInvoked {
//            postInvoked = false
//            delegate?.captiveWebViewControllerDidSubmitForm(self, withConfiguration: updatedConfig, success: true)
//            Logger.logMessage("captive post \(updatedConfig)")
//            DDLogDebug("captive post \(updatedConfig)")
//            Logger.flushMessages()
//        } else {
//            let text = webView.stringByEvaluatingJavaScriptFromString("document.body.innerText")
//            if text?.rangeOfString("$invisible$") != nil {
//                delegate?.captiveWebViewControllerDidFindSuccessPage(self)
//                Logger.logMessage("captive success")
//                DDLogDebug("captive success")
//                
//                Logger.flushMessages()
//            }
//        }
//    }
//    
//    public func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
//        Logger.logMessage("captive failedToLoad \(error?.description)")
//        DDLogDebug("captive failedToLoad \(error?.description)")
//        
//        spinner.stopAnimating()
//        if postInvoked {
//            postInvoked = false
//            delegate?.captiveWebViewControllerDidSubmitForm(self, withConfiguration: updatedConfig, success: false)
//        }
//    }
//}
//
//
////MARK: - Extraction
//extension CaptiveWebViewController {
//    /**
//    Exctracting wifi configutation from intercepted POST request from captive page.
//    
//    - parameter request:   intercepted HTTP POST request from captive authorization page
//    
//    - returns: wifi configuration with `url` and `params`
//    */
//    private func extractConfigurationFromRequest(request: NSURLRequest) -> [String: AnyObject]? {
//        guard let url = request.URL, data = request.HTTPBody, body = NSString(data: data, encoding: NSUTF8StringEncoding) else { return nil }
//        
//        var formParams = [String]()
//        var queryParams = [String]()
//        var username: String?
//        var password: String?
//        
//        //exctracting param names from form
//        let paramNames = body.componentsSeparatedByString("&").map({ $0.componentsSeparatedByString("=").first! })
//
//        //finding username and password param names
//        for name in paramNames {
//            if name.rangeOfString("username", options: .CaseInsensitiveSearch) != nil {
//                username = name
//            } else if name.rangeOfString("password", options: .CaseInsensitiveSearch) != nil {
//                password = name
//            } else {
//                formParams.append(name)
//            }
//        }
//
//        //exctracting param names from query
//        if let query = url.query {
//            queryParams = query.componentsSeparatedByString("&").map({ $0.componentsSeparatedByString("=").first! })
//        }
//
//        if let username = username, password = password {
//            return [
//                "url": url.absoluteString,
//                "params": [
//                    "username": username,
//                    "password": password,
//                    "extract": [
//                        "form": formParams,
//                        "query": queryParams
//                    ]
//                ]
//            ]
//        } else {
//            return [
//                "url": url.absoluteString,
//                "params": [
//                    "username": "",
//                    "password": "",
//                    "extract": [
//                        "form": formParams,
//                        "query": queryParams
//                    ]
//                ]
//            ]
//
//        }
//    }
//}
