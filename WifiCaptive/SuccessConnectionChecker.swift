//
//  SuccessConnectionChecker.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 05.08.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import Foundation
import Alamofire
//import HotspotHelper
import CocoaLumberjack


class SuccessConnectionChecker {
    
    class var sharedInstance: SuccessConnectionChecker {
        struct Singleton {
            static let instance = SuccessConnectionChecker()
        }
        return Singleton.instance
    }
    
    // Success request
    func successAccess() {
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        Alamofire.request(BaseURL + "wifi/successaccess", method: .get, parameters:  ["pointid": "test", "latitude": "0.00", "longitude": "0.00"], encoding: URLEncoding.default, headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
            
            switch response.result {
                
            case .success(_): break
                

            case .failure(_):
                
                if let data = response.data, let _ = JSON(data: data)["error_message"].string {
                    //                        DDLogDebug("Erorr made wifi/successaccess with \(message)")
                } else {
                    DDLogDebug("Erorr...just error")
                }
                
            }
        }
    }
    
}
