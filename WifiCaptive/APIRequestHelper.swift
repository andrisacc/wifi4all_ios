//
//  APIRequestHalper.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

import UIKit
import Alamofire
import CocoaLumberjack
import DigitsKit


class APIRequestHelper: NSObject {
    
    
    class func myPointMinus (_ completion: @escaping (JSON?, String?) -> Void) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        Alamofire.request(BaseURL + "wifi/mypoints/minus",
                          method: .get, parameters: nil,
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                completion(json, nil)
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(nil, message)
                                } else {
                                    completion(nil, QQLocalizedString("Error"))
                                }
                                
                            }
        }
        
    }
    
    class func myPointPlus (_ completion: @escaping (JSON?, String?) -> Void) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        Alamofire.request(BaseURL + "wifi/mypoints/plus",
                          method: .get, parameters: nil,
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                completion(json, nil)
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(nil, message)
                                } else {
                                    completion(nil, QQLocalizedString("Error"))
                                }
                                
                }
        }
        
    }
    
    class func getHotspotList (_ completion: @escaping (JSON?, String?) -> Void) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        Alamofire.request(BaseURL + "wifi/mylist",
                          method: .get, parameters: nil,
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                let json = JSON(object)
                                
                                completion(json, nil)
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(nil, message)
                                } else {
                                    completion(nil, QQLocalizedString("Error"))
                                }
                                
                            }
        }
        
    }
    
    class func changingProfile (firstName: String?, surName: String?, email: String?, country: String?, completion: @escaping (Bool, String?) -> Void) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        let params = [ "firstname": firstName ?? "" , "surname": surName ?? "", "email": email ?? "", "country": country ?? ""]
        
        Alamofire.request(BaseURL + "user/update", method: .post, parameters: params, encoding: URLEncoding.default, headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
            
            switch response.result {
                
            case .success(let object):
                
                let json = JSON(object)
                
                if json["user"].dictionary != nil {
                    LoginHelper.storeUserResponse(json)
                    GA.event("EditProfile_change_success")
                    
                    completion(true, nil)
                } else {
                    GA.event("EditProfile_change_failure")
                    completion(false, JSON(object)["error_message"].string)
                }
                
            case .failure:
                
                GA.event("EditProfile_change_failure")
                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                    completion(false, message)
                } else {
                    completion(false, QQLocalizedString("Error"))
                }
            }
        }
    }
    
    class func checkPromoCode (code: String, completion: @escaping (Bool, String?) -> Void) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        Alamofire.request(BaseURL + "promo/code",
                          method: .post, parameters:  ["code": code],
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON { response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                if json["user"].dictionary != nil {
                                    LoginHelper.storeUserResponse(json)
                                }
                                
                                HotspotCredentials.updateCredentials(jsonObj: json)
                                
                                completion(true, nil)
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(false, message)
                                } else {
                                    completion(false, QQLocalizedString("Error"))
                                }
                                
                            }
        }
        
    }
    
    
    class func addNetwork (ssid: String?, password: String?, currentLocation: CLLocation?, completion: @escaping (JSON?, String?) -> Void) {
        
        guard let ssid = ssid, let password = password else { return }
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        var dict: [String : Any] = [
            "name": ssid,
            "ssid": ssid,
            "public": true,
            "params": [
                "hotspot_password" : password
            ]
        ]
        
        GA.event("wifiAddNetwork_public", label: dict.description)
        
        if let location = currentLocation {
            
            dict["location"] = [
                "lat": location.coordinate.latitude,
                "lon": location.coordinate.longitude
            ]
        }
        
        Alamofire.request(BaseURL + "wifi/private",
                          method: .post, parameters: dict,
                          encoding: JSONEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON { response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                GA.event("wifiAddNetwork_public_sucess")
                                
                                let json = JSON(object)
                                let config = json["configuration"]
                                if config["id"].string != nil {
                                    WifiConfiguration.saveAll(array: [WifiConfiguration(json: config)])
                                    DDLogDebug("Saved network: \(json)")
                                }
                                
                                completion(json, nil)
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    completion(nil, message)
                                } else {
                                    completion(nil, QQLocalizedString("There was an error adding new network. Try again later"))
                                }

                            }
                            
            }
        
        
        }
    
    
}
