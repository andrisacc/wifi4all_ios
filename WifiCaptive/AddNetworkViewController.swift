//
//  AddNetworkViewController.swift
//  WifiCaptive
//
//  Created by bars on 09.09.15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import Alamofire
//import HotspotHelper
import CocoaLumberjack
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class AddNetworkViewController: UIViewController, UIViewControllerTransitioningDelegate, ErrorHandling, SuccessHandling {
    
    // MARK: - IBOutlets -
    @IBOutlet weak var SSID: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var addNetworkButton: UIButton!
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    
    @IBOutlet var ssidHeight: NSLayoutConstraint!
    @IBOutlet var passwordHeight: NSLayoutConstraint!
    @IBOutlet var buttonHeight: NSLayoutConstraint!
    
    @IBOutlet var ssidTopConstraint: NSLayoutConstraint!
    @IBOutlet var passwordTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var ssidBottomConstraint: NSLayoutConstraint!
    @IBOutlet var passwordBottomConstarint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var labelError: UILabel!
    
    // MARK: - Properties -
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    var realFormWidth : CGFloat = 0
    let scale = (UIScreen.main.bounds.height / 667.0 > 1) ? 1 : UIScreen.main.bounds.height / 667.0
    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        
        let attributesSSID = [
            NSForegroundColorAttributeName: UIColor.lightGray,
            NSFontAttributeName : CurrentFont(fontSize: 15.0).currentFont
        ] as [String : Any]
        
        let attributesPassword = [
            NSForegroundColorAttributeName: UIColor.brandColor(),
            NSFontAttributeName : CurrentFont(fontSize: 15.0).currentFont
        ] as [String : Any]
        
        SSID.font = CurrentFont(fontSize: 15.0).currentFont
        SSID.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("SSID"), attributes:attributesSSID)
        
        password.font = CurrentFont(fontSize: 15.0).currentFont
        password.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("Password"), attributes:attributesPassword)
        
        topLabel.font = CurrentFont(fontSize: 17.0).currentFont
        labelOne.font = CurrentFont(fontSize: 17.0).currentFont
        labelTwo.font = CurrentFont(fontSize: 17.0).currentFont
        labelError.font = CurrentFont(fontSize: 10.0).currentFont
        
        addNetworkButton.titleLabel?.font = CurrentFont(fontSize: 16.0).currentFont
        
        scrollView.showsVerticalScrollIndicator = false
        
        super.viewDidLoad()
        
        SSID.text = currentSSID ?? ""
        
        SSID.isEnabled = !(SSID.text?.characters.count > 0)
        
        locationManager.delegate = self
        
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        
        preferredContentSize = CGSize(width: 300, height: 560 * scale)
        
//        ssidHeight.constant = ssidHeight.constant * scale
//        passwordHeight.constant = passwordHeight.constant * scale
//        buttonHeight.constant = buttonHeight.constant * scale
//        
//        ssidTopConstraint.constant = ssidTopConstraint.constant * scale
//        passwordTopConstraint.constant = passwordTopConstraint.constant * scale
//        
//        ssidBottomConstraint.constant = ssidBottomConstraint.constant * scale
//        passwordBottomConstarint.constant = passwordBottomConstarint.constant * scale
        
        addNetworkButton.isEnabled = false
        addNetworkButton.backgroundColor = UIColor.gray
        
        labelError.isHidden = true
        
        // Settin textfield placeholder color 
        
        password.attributedPlaceholder = NSAttributedString(string:QQLocalizedString("Password"), attributes:[NSForegroundColorAttributeName: UIColor.brandColor()])
        SSID.attributedPlaceholder = NSAttributedString(string:currentSSID ?? QQLocalizedString("No network"), attributes:[NSForegroundColorAttributeName: UIColor.brandColor()])
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let locationAuthStatus = CLLocationManager.authorizationStatus()
        if CLLocationManager.locationServicesEnabled() && (locationAuthStatus == .authorizedWhenInUse || locationAuthStatus == .authorizedAlways) {
            locationManager.startUpdatingLocation()
        }
        
        if let token = UserDefaults.standard.object(forKey: AitaToken) as? String {
            let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
            if !launchedBefore  {
                LoginHelper.startHotspotHelper(token)
            }
        }
    }
    
    // MARK: - Methods -
    
    func checkFields() {
        guard let SSID = SSID.text, let password = password.text else {
            addNetworkButton.isEnabled = false
            addNetworkButton.backgroundColor = UIColor.gray
            return
        }
        
        addNetworkButton.isEnabled = SSID.characters.count > 0 && password.characters.count >= 8
        addNetworkButton.backgroundColor = addNetworkButton.isEnabled ? UIColor.brandColor() : UIColor.gray

        labelError.isHidden = password.characters.count >= 8
    }
    
    @IBAction func fieldEditingChanged(_ sender: AnyObject) {
        checkFields()
    }
    
    @IBAction func addNetworkTapped(_ sender: AnyObject) {
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        APIRequestHelper.addNetwork(ssid: SSID.text, password: password.text, currentLocation: currentLocation) {[unowned self] (json, errorString) in
            
            if json != nil && errorString == nil {
                
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                GA.event("wifiAddNetwork_public_sucess")
                self.dismiss(animated: false) {
                    self.handleSuccess(alert: QQLocalizedString("Your hotspot has been successfully added. We added some points to your account, please check it!"))
                }
                
            } else {
                self.handleError(error: QQLocalizedString(errorString ?? "error..."))
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                GA.event("wifiAddNetwork_public_failure")
            }
            
        }
    }
}


extension AddNetworkViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last
        locationManager.stopUpdatingLocation()
    }
}
