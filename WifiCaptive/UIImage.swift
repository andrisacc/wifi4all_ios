//
//  UIImage.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

// Create image from the view

public extension UIImage {
    convenience init(aView: UIView) {
        UIGraphicsBeginImageContext(aView.frame.size)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage!)!)
    }
}
