//
//  LJLogger.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 14.06.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import Foundation
import CocoaLumberjack
//import HotspotHelper

class LJLogger {
    
    static let sharedInstance = LJLogger()
        
    var ddFileLogger: DDFileLogger = DDFileLogger()
    
    var logFileDataArray: [Data] {
        get {
            let logFilePaths = ddFileLogger.logFileManager.sortedLogFilePaths!
            var logFileDataArray = [Data]()
            for logFilePath in logFilePaths {
                let fileURL = NSURL(fileURLWithPath: logFilePath)
                if let logFileData = try? NSData(contentsOf: fileURL as URL, options: NSData.ReadingOptions.mappedIfSafe) {
                    // Insert at front to reverse the order, so that oldest logs appear first.
                    logFileDataArray.insert(logFileData as Data, at: 0)
                }
            }
            return logFileDataArray
        }
    }
    
    class func checkConnectionReachability() -> Bool {
        
        guard let scriptUrl = URL(string: "http://www.google.com/m") else {return false}
        let data = try? Data(contentsOf: scriptUrl)
        
        if (data != nil) {
            // Device is connected to the Internet
            return true
        } else {
            // Device is not connected to the Internet
            return false
        }
    }

}
