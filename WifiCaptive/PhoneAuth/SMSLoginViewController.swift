//
//  SMSLoginViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 30/10/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import UIKit
import CoreTelephony
import PhoneNumberKit
import Alamofire
import FirebaseAuth
import FirebaseCore
import Firebase


class SMSLoginViewController: UIViewController, UITextFieldDelegate, ErrorHandling, SuccessHandling {
    

    // MARK: - Properties -
    var countryCode = String()
    var textFieldText = String()
    var changePhone = false
    
    // MARK: - IBOutlets -
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let info = CTTelephonyNetworkInfo()
        let carrier = info.subscriberCellularProvider
        if let code = carrier?.isoCountryCode?.uppercased() ?? ((NSLocale.current as NSLocale).object(forKey: .countryCode) as? String), countryCode.contains(code) {
            countryCode = code
        }
        
        self.tableView.tableFooterView = UIView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods -
    
    fileprivate func setPath() -> String {
        
        return Bundle.main.path(forResource: "phoneCodes", ofType: "json")!
        
//        if let bundleID = Bundle.main.bundleIdentifier {
//            if bundleID == "mobi.appintheair.wifi" {
//                if let reviewPath = defaults.object(forKey: "PhonePath") as? String {
//                    return Bundle.main.path(forResource: reviewPath, ofType: "json")!
//                } else {
//                    return Bundle.main.path(forResource: "phoneCodes", ofType: "json")!
//                }
//            } else if  bundleID == "world.cleaner.emop.debug"{
//                return Bundle.main.path(forResource: "phoneCodesDebug", ofType: "json")!
//            } else {
//                return Bundle.main.path(forResource: "phoneCodes", ofType: "json")!
//            }
//        } else {
//            return Bundle.main.path(forResource: "phoneCodes", ofType: "json")!
//        }
    }
    
    func sendPhone() {
        
        self.view.endEditing(true)
        
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        
        var formattedPhoneNumber = String()
         print("textFieldText: \(textFieldText)")
        
        // Preparing the phone for sending
        
        // TODO: - Set the location abbrevistion here also -
        
        let path = setPath()
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        let countryPhones = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:String]
        
        let characters = textFieldText.characters.map { String($0) }
        var newStringArray = [String]()
        
        if textFieldText.hasPrefix("00") {
            for i in characters.dropFirst(2) {
                newStringArray.append(i)
            }
            let nonNullsString = newStringArray.joined(separator: "")
            let chars: [Character] = [" ","-",")","("]
            let phoneNumber = "+" + (countryPhones[countryCode] ?? "") + nonNullsString.stringByRemovingAll(characters: chars)
            formattedPhoneNumber = phoneNumber
        } else if textFieldText.hasPrefix("0") {
            for i in characters.dropFirst(1) {
                newStringArray.append(i)
            }
            let nonNullsString = newStringArray.joined(separator: "")
            let chars: [Character] = [" ","-",")","("]
            let phoneNumber = "+" + (countryPhones[countryCode] ?? "") + nonNullsString.stringByRemovingAll(characters: chars)
            formattedPhoneNumber = phoneNumber
        } else {
            for i in characters {
                newStringArray.append(i)
            }
            let nonNullsString = newStringArray.joined(separator: "")
            let chars: [Character] = [" ","-",")","("]
            let phoneNumber = "+" + (countryPhones[countryCode] ?? "") + nonNullsString.stringByRemovingAll(characters: chars)
            formattedPhoneNumber = phoneNumber
        }
        
//        print("Phone text: \(formattedPhoneNumber)")
        
        // Here we should check if current user is signed in already ? signin : send Auth.auth().currentUser to verify him
        
        if let currentUser = Auth.auth().currentUser {
            
            currentUser.getIDToken(completion: {[unowned self] (idToken, error) in
                
                if let error = error {
                    // Handle error
                    print(error)
                    return
                }
                print("idToken: ", idToken ?? "NoID TOKEN")

                LoginHelper.firebaseLogin(idToken ?? "notoken", completion: { (success, error) in
                    if success {
                        CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                        print("I successed with firebase...")
                    } else {
                        CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                        self.handleError(error: error ?? "error...")
                        print("Unsucceeded with firebase...")
                    }
                })
            })
            
        } else {
            
            PhoneAuthProvider.provider().verifyPhoneNumber(formattedPhoneNumber) {[unowned self] (verificationID, error) in
                if let error = error {
                    CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                    self.handleError(error: error.localizedDescription)
                    print(error)
                    return
                }
                let token = verificationID ?? "No verification ID"
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                
                // Here we neet to show the code innsertion controller and put there verification code
                
                let storyboard = UIStoryboard(name: "PhoneAuth", bundle: Bundle.main)
                guard let vc: SMSCodeViewController = storyboard.instantiateViewController(withIdentifier: "SMSCodeViewController") as? SMSCodeViewController else { return }
                vc.token = token
                vc.changePhone = self.changePhone
                let navigationController = UINavigationController(rootViewController: vc)
                self.present(navigationController, animated: true, completion: nil)
                
            }
        }
        
        // Here we should call the FireBase method to give the phone number

    }
    
    // MARK: - Delegates -
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let text = textField.text {
            self.textFieldText = text
        }
        
    }
}

extension SMSLoginViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let path = setPath()
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        let countryPhones = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:String]
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "headeSMSCell", for: indexPath) as! SMSHeaderViewTableViewCell
            return cell
            
        } else if indexPath.row == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "countryCheckCell", for: indexPath) as! SMSCountryCheckerTableViewCell
            cell.countryLabel.text = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) ?? countryCode
            cell.flagPic.image = UIImage(named: countryCode.lowercased())
            return cell
            
        } else if indexPath.row == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "phoneEnterCell", for: indexPath) as! SMSPhoneInputTableViewCell
            cell.phoneTextField.delegate = self
            
            let toolbar = UIToolbar(frame: CGRect(x:0, y:0, width: self.view.frame.size.width, height:55.0))
            toolbar.barTintColor = UIColor.brandColor()
            let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: #selector(sendPhone))
            let button = UIBarButtonItem(title: "Get a code to proceed", style: UIBarButtonItemStyle.plain, target: self, action: #selector(sendPhone))
            button.tintColor = UIColor.white
            button.setTitleTextAttributes([NSFontAttributeName:CurrentFont(fontSize: 17).currentFont], for: .normal)
            toolbar.items = [flexibleSpace, button, flexibleSpace]
            toolbar.sizeToFit()
            cell.phoneTextField.inputAccessoryView = toolbar
            
            cell.phoneCode.text = "+" + (countryPhones[countryCode] ?? "")
            
            return cell
            
        } else {
            return UITableViewCell()
        }
        
    }
    
}

extension SMSLoginViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
