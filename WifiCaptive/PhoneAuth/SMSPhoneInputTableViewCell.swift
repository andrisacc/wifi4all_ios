//
//  SMSPhoneInputTableViewCell.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 30/10/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import UIKit
import PhoneNumberKit

class SMSPhoneInputTableViewCell: UITableViewCell {

    @IBOutlet weak var phoneCode: UILabel!
    @IBOutlet weak var phoneTextField: PhoneNumberTextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
