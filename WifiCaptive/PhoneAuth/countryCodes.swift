//
//  countryCodes.swift
//  emop
//
//  Created by Victor on 02/07/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation

fileprivate var path = setPath()
fileprivate let data = try! Data(contentsOf: URL(fileURLWithPath: path))


let countryPhoneCodes = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:String]
let countryCodes = countryPhoneCodes.keys
let countryCodesNames = (countryCodes.map({
    ($0, (Locale.current as NSLocale).displayName(forKey: .countryCode, value: $0) ?? $0)
})).sorted(by:{ $0.1 < $1.1 })

fileprivate func setPath() -> String {
    
    return Bundle.main.path(forResource: "phoneCodes", ofType: "json")!
//    if let bundleID = Bundle.main.bundleIdentifier {
//        if bundleID == "world.cleaner.emop.release" {
//            if let reviewPath = defaults.object(forKey: "PhonePath") as? String {
//                return Bundle.main.path(forResource: reviewPath, ofType: "json")!
//            } else {
//                return Bundle.main.path(forResource: "phoneCodes", ofType: "json")!
//            }
//        } else if  bundleID == "world.cleaner.emop.debug"{
//            return Bundle.main.path(forResource: "phoneCodesDebug", ofType: "json")!
//        } else {
//            return Bundle.main.path(forResource: "phoneCodes", ofType: "json")!
//        }
//    } else {
//        return Bundle.main.path(forResource: "phoneCodes", ofType: "json")!
//    }
}

