//
//  SMSCodeViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 30/10/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import UIKit

class SMSCodeViewController: UIViewController, UITextFieldDelegate, ErrorHandling, SuccessHandling {
    
    // MARK: - Properties -
    
    var token: String?
    var textFieldText = String()
    var changePhone = false
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var codeInputTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        codeInputTextField.delegate = self
        
        let toolbar = UIToolbar(frame: CGRect(x:0, y:0, width: self.view.frame.size.width, height:55.0))
        toolbar.barTintColor = UIColor.brandColor()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: #selector(done))
        let button = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(done))
        button.tintColor = UIColor.white
        button.setTitleTextAttributes([NSFontAttributeName:CurrentFont(fontSize: 17).currentFont], for: .normal)
        toolbar.items = [flexibleSpace, button, flexibleSpace]
        toolbar.sizeToFit()
        codeInputTextField.inputAccessoryView = toolbar
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Methods -
    
    func showMainVC() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "WifiMapViewController") as! WifiMapViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftCollectionViewController") as! LeftCollectionViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        UINavigationBar.appearance().tintColor = UIColor.brandColor()
        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        UIApplication.shared.keyWindow?.rootViewController  = slideMenuController
        
        nvc.navigationBar.tintColor = UIColor.white
        nvc.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isTranslucent = false
        
    }
    
    func done () {
        
        self.view.endEditing(true)
        
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        
        guard let authToken = token else {return}
        guard let code = codeInputTextField.text else {return}
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: authToken,
            verificationCode: code)
        
        Auth.auth().signIn(with: credential) {[unowned self] (user, error) in
            if let error = error {
                // ...
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                self.handleError(error: error.localizedDescription)
                print(error)
                return
            }
            
            user?.getIDToken(completion: { (idToken, error) in
                
                if let error = error {
                    // Handle error
                    print(error)
                    return
                }
                
                self.changePhone == false ? self.newPhone(idToken: idToken) : self.changePhone(idToken: idToken)
                
            })
            
        }
    }
    
    func newPhone (idToken: String?) {
        
        LoginHelper.firebaseLogin(idToken ?? "notoken", completion: {[unowned self] (success, error) in
            if success {
                print("I successed with firebase...")
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                self.showMainVC()
            } else {
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                self.handleErrorAndDismiss(error: error ?? "error...")
                print("Unsucceeded with firebase...")
            }
        })
        
    }
    
    func changePhone (idToken: String?) {
        
        LoginHelper.changePhoneFIR(idToken ?? "notoken") { [unowned self] (success, error)  in
            if success {
                print("I successed with firebase changephone!")
                CustomActivity.sharedInstance.hideActivityIndicator((self.view)!)
                self.showMainVC()
            } else {
                CustomActivity.sharedInstance.hideActivityIndicator((self.view)!)
                self.handleErrorAndDismiss(error: error ?? "error...")
                print("Unsucceeded with firebase changephone...")
            }
        }
    }
    
    // MARK: - Delegates -
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let text = textField.text {
            self.textFieldText = text
        }
    }
    
}
