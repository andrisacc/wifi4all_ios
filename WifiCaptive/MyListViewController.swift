//
//  MyListViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 14.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
//import HotspotHelper
import Alamofire
import CocoaLumberjack

class MyListViewController: UIViewController, ErrorHandling, SuccessHandling {

    // MARK: - Properties - 
    
    var hotspots: JSON?
    var points = [WifiHotspot]()

    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var addHotspotOutlet: UIButton!
    @IBOutlet weak var listHotspotsLabel: UILabel!

    
    @IBAction func addHotspotButton(_ sender: UIButton) {
        addHotspot()
    }
    
    
    // MARK: - VC load -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add notification to show the profile controller
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
        
        
        addHotspotOutlet.titleLabel?.font = CurrentFont(fontSize: 16.0).currentFont
        listHotspotsLabel.font = CurrentFont(fontSize: 15.0).currentFont

        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = false
        
        
        
//        let leftButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(MyListViewController.dismissVC))
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "arrow_back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(MyListViewController.dismissVC))
        self.navigationItem.leftBarButtonItem = leftButton
        
//        let rightButton = UIBarButtonItem(image: UIImage(named: "backCrossWhite"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(MyListViewController.dismissVC))
//        self.navigationItem.rightBarButtonItem = rightButton
//        self.navigationItem.rightBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5)
        
        // Empty back button
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        // Table View Delegates
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = true
    
        
        // Do any additional setup after loading the view.
        navigationController?.topViewController?.title = QQLocalizedString("Added hotspots")
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont]
        
        
//        print("Points", data!["networks"])
//        
//        guard let jsonPoints = data else {return}
//        
//        for point in jsonPoints["networks"] {
//            
//            print("SSID", point.1["ssid"][0].stringValue)
//            print("NAME", point.1["name"].stringValue)
//            print("LAT", point.1["latitude"].double)
//            print("LON", point.1["longitude"].double)
//            print("paid", point.1["paid"].bool)
//            
//            if let _point = self.wifiPoint {
//                
//                guard let lat = point.1["latitude"].double else {return}
//                guard let lon = point.1["longitude"].double else {return}
//                guard let ssid = point.1["ssid"][0].string else {return}
//                guard let name = point.1["name"].string else {return}
//                guard let paid = point.1["paid"].bool else {return}
//                
//                _point.ssid = ssid
//                _point.name = name
//                _point.latitude = lat
//                _point.longitude = lon
//                _point.isUserHotspot = paid
//                
//                //
//                print("SSID", point.1["ssid"][0].stringValue)
//                print("NAME", point.1["name"].stringValue)
//                print("LAT", point.1["latitude"].double)
//                print("LON", point.1["longitude"].double)
//                print("paid", point.1["paid"].bool)
//                //
//                self.points.append(_point)
//            }
//        }
//        
//        print("WiFi points",  self.points)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        GA.screen("MyListViewController")
        GA.event("MyListViewController_saw")
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }
    
     // MARK: - Methods -
    
    func openProfileVC () {
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork(self)
        }
    }
    
    func dismissVC () {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addHotspot () {
        
        if #available(iOS 9.0, *) {
            // TODO: - Change for HotspotHelper migration -
            if let network = currentNetwork {
                if !network.isSecure {
                    self.handleError(error: QQLocalizedString("Please connect to password-protected network to add it"))
                    return
                }
            }
        }
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways)
        {
            
            guard let addNetworkController =
                self.storyboard?.instantiateViewController(withIdentifier: "AddNetworkViewController") as? AddNetworkViewController else { return }
            addNetworkController.transitioningDelegate = TransitioningDelegate
            
            self.present(addNetworkController, animated: true, completion: nil)
            
            GA.event("wifiMyList_addNetwork")
            
        } else {
            
            let locationAuthStatus = CLLocationManager.authorizationStatus()
            
            if locationAuthStatus == .denied || locationAuthStatus == .restricted {
                
                let alertController = UIAlertController (title: QQLocalizedString("Access to your current location is required"), message: QQLocalizedString("Go to Settings?"), preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: QQLocalizedString("Settings"), style: .default) { (_) -> Void in
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
                
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default, handler: nil)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
}

extension MyListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let viewController = segue.destination as! MyHotspotDetailsViewController
        viewController.title = QQLocalizedString("My Hotspot")
        guard let _hotspots = hotspots else {return}
        viewController.point = _hotspots["networks"][(self.tableView.indexPathForSelectedRow?.row)!]
        
        GA.event("myWiFiList_chooseHotspot")
    }
    
}

extension MyListViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if hotspots != nil {
            return hotspots!["networks"].count
        } else {return 0}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HotspotDetailsTableViewCell
        guard let hotspots  = hotspots else {return cell}
        cell.value.font = CurrentFont(fontSize: 17.0).currentFont
        cell.value.text = hotspots["networks"][indexPath.row]["name"].stringValue
        
        return cell
        
    }
    
    // Setting up row heights
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //        if (indexPath.row == 0) {
        //            return 55.0
        //        } else if (indexPath.row == 1) {
        //            return 60.0
        //        } else if (indexPath.row == 2) {
        //            return 150.0
        //        } else {
        return 55.0
        //        }
        
    }
    
}
