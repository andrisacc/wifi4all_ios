//
//  ProfileViewController.swift
//  WifiCaptive
//
//  Created by Sergey Pronin on 8/22/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import Alamofire
import DigitsKit


let SubscriptionExpirationFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .none
    return formatter
}()

class WifiProfileViewController: UIViewController, ErrorHandling, SuccessHandling {
    
    
    // MARK: - IBOutlets -
    @IBOutlet weak var labelSubscriptionInfo: UILabel!
    @IBOutlet weak var labelSubscriptionAction: UILabel!
    
    @IBOutlet weak var viewSubscription: UIView!
    @IBOutlet weak var viewInvite: UIView!
    @IBOutlet weak var viewAdd: UIView!
    
    @IBOutlet weak var imagePromoCode: UIImageView!
    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = QQLocalizedString("Profile")
        
        imagePromoCode?.image = UIImage(named: "extend")?.withRenderingMode(.alwaysTemplate)
        imagePromoCode?.tintColor = UIColor(red: 62.0/255.0, green: 158.0/255.0, blue: 229.0/255.0, alpha: 1)
        
        #if AITA
            viewInvite.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(WifiProfileViewController.clickInvite(_:))))
            viewSubscription.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(WifiProfileViewController.clickSubscription(_:))))
            preferredContentSize = CGSize(width: 400, height: 600)
        #else
            LoginHelper.updateUserState()
        #endif
        
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        reloadSubscriptionInfo()
        
        GA.screen("wifiProfile")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(WifiProfileViewController.tapActivityLoadNotification(_:)), name: NSNotification.Name(rawValue: "showTapActivity"), object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        TapActivity.sharedInstance.hideActivityIndicator(self.viewAdd)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func reloadSubscriptionInfo() {
        #if AITA
            guard let wifi = NSUserDefaults.standardUserDefaults().objectForKey("wifi_settings") else { return }
        #else
            guard let wifi = UserDefaults.standard.object(forKey: "wifi") else { return }
        #endif
        
        let json = JSON(wifi)
        
        let expirationTimestamp = TimeInterval(json["expiration"].int!)
        let expirationDate = Date(timeIntervalSince1970: expirationTimestamp)
        let bytesLeft = json["bytes_left"].float!
        let realm: String?
        
        // Checking the realm
                
        let p = HotspotCredentials.username
        
        let array: NSArray = p.components(separatedBy: "@") as NSArray
        if array.count > 1 {
            realm = array.lastObject as? String
            if realm! == "wifiasyougo.com" {
                #if AITA
                    if json["auto_renew"].bool == true {
                        labelSubscriptionAction.text = QQLocalizedString("Cancel")
                        labelSubscriptionInfo.text =
                        "Valid till \(SubscriptionExpirationFormatter.stringFromDate(expirationDate))"
                    } else {
                        labelSubscriptionAction.text = QQLocalizedString("Subscribe")
                        if expirationDate.timeIntervalSinceNow > 0 {
                            labelSubscriptionInfo.text = String(format: QQLocalizedString("Valid till %@"), SubscriptionExpirationFormatter.stringFromDate(expirationDate))
                        } else {
                            labelSubscriptionInfo.text = QQLocalizedString("Expired")
                        }
                    }
                #else
                    labelSubscriptionAction.text = QQLocalizedString("Subscribe")
                    if expirationDate.timeIntervalSinceNow > 0 {
                        labelSubscriptionInfo.text = String(format: QQLocalizedString("Valid till %@"), SubscriptionExpirationFormatter.string(from: expirationDate))
                    } else {
                        labelSubscriptionInfo.text = QQLocalizedString("Expired")
                    }
                    
                #endif
            } else {
                
                labelSubscriptionAction.text = QQLocalizedString("Subscribe")
                labelSubscriptionInfo.text = "\(Float(bytesLeft/(1024*1024))) " + QQLocalizedString("MB left")
            }
        }
    }
    
    
    // MARK: - Events -
    @IBAction func clickSubscription(_ sender: UIGestureRecognizer) {
        
        guard sender.state == .ended else { return }
        
        #if AITA
            guard let wifi = NSUserDefaults.standardUserDefaults().objectForKey("wifi_settings") else { return }
        #else
            guard let wifi = UserDefaults.standard.object(forKey: "wifi") else { return }
        #endif
        
        let json = JSON(wifi)
        
        #if AITA
            if json["auto_renew"].bool == true {
                //cancel
                MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                Subscription.cancelCurrentSubscription { success, errorString in
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    if success {
                        self.reloadSubscriptionInfo()
                    } else {
                        presentError(vc: self, error: errorString ?? QQLocalizedString("You can't cancel subscription at this time"))
                    }
                }
                GA.event("wifiProfile_cancel")
            } else {
                //if needed to purchase
                presentBuyScreen()
            }
        #else
            presentBuyScreen()
        #endif
    }
    
    func presentBuyScreen() {
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        Subscription.fetchSubscriptionOptions { subscriptions in
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
            
            if let subscriptions = subscriptions, subscriptions.count > 0 {
                
                #if AITA
                    let purchaseController = WifiBuyDescriptionViewController()
                    purchaseController.subscriptions = subscriptions
                    purchaseController.analyticsPrefix = "profile"
                    if UIDevice.type.isIPad {
                        AAPopupViewController.showPopupWithViewController(purchaseController, inContainer: self)
                    } else {
                        self.navigationController?.pushViewController(purchaseController, animated: true)
                    }
                #else
                    guard let purchaseController = self.storyboard?
                        .instantiateViewController(withIdentifier: "PurchaseViewController") as? PurchaseViewController else { return }
                    purchaseController.subscriptions = subscriptions
                    self.navigationController?.pushViewController(purchaseController, animated: true)
                #endif
                
            }
        }
        GA.event("wifiProfile_subscribe")
    }
    
    func checkPromoCode(_ code: String) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        APIRequestHelper.checkPromoCode(code: code) {[unowned self] (success, errorString) in
            
            if success {
                self.reloadSubscriptionInfo()
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                self.handleSuccess(alert: QQLocalizedString("Promo-code has been successfully applied to your account"))
            } else {
                self.handleError(error: errorString ?? "error..")
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
            }
            
        }
    }
    
    @IBAction func clickInvite(_ sender: UIGestureRecognizer) {
        guard sender.state == .ended else { return }
        
        let alertController = UIAlertController (title: QQLocalizedString("Invite friends"), message: QQLocalizedString("Attention! You will get your complimentary access to commercial hotspots once your friend will be registered in the app"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: QQLocalizedString("Ok"), style: .default) { (_) -> Void in
            self.inviteFriend ()
        }
        alertController.addAction(okAction)
        
        
        self.present(alertController, animated: true, completion: nil)
        
        GA.event("wifiProfile_invite")
    }
    
    @IBAction func clickAddWifi(_ sender: UIGestureRecognizer) {
        
        TapActivity.sharedInstance.hideActivityIndicator(self.viewAdd)
        
        if #available(iOS 9.0, *) {
            
            if let network = currentNetwork {
                if !network.isSecure {
                    self.handleError(error: QQLocalizedString("Please connect to password-protected network to add it"))
                    return
                }
            }
        }
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways)
        {
            
            guard let addNetworkController =
                storyboard?.instantiateViewController(withIdentifier: "AddNetworkViewController") as? AddNetworkViewController else { return }
            addNetworkController.transitioningDelegate = TransitioningDelegate
            
            present(addNetworkController, animated: true, completion: nil)
            
            GA.event("wifiProfile_addNetwork")
            
        } else {
            
            let locationAuthStatus = CLLocationManager.authorizationStatus()
            
            if locationAuthStatus == .denied || locationAuthStatus == .restricted {
                
                let alertController = UIAlertController (title: QQLocalizedString("Access to your current location is required"), message: QQLocalizedString("Go to Settings?"), preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: QQLocalizedString("Settings"), style: .default) { (_) -> Void in
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
                
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default, handler: nil)
                alertController.addAction(cancelAction)
                
                present(alertController, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    @IBAction func clickPromoCode(_ sender: AnyObject) {
        
        let alert = UIAlertController(title: QQLocalizedString("Promo-code"), message: QQLocalizedString("Enter the promo-code"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: QQLocalizedString("Cancel"), style: .cancel, handler: { _ in }))
        alert.addAction(UIAlertAction(title: QQLocalizedString("OK"), style: .default, handler: { [unowned alert, weak self] _ in
            guard let code = alert.textFields?.first?.text else { return }
            
            self?.checkPromoCode(code)
        }))
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = QQLocalizedString("Promo-code") + ".."
            textField.autocapitalizationType = .allCharacters
        })
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func clickLogout(_ sender: UIGestureRecognizer) {
        guard sender.state == .ended else { return }
        
        LoginHelper.logOut()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let loginView: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
        UIApplication.shared.keyWindow?.rootViewController = loginView
        
        GA.event("wifiProfile_logOut")
    }
    
    func tapActivityLoadNotification(_ notification: Notification) {
        TapActivity.sharedInstance.showTapIndicator(self.viewAdd)
    }
    
    func inviteFriend () {
        
        let shareString = QQLocalizedString("Get free Internet access to million hotspots worldwide!")
        
        // Custom data
        
        let params = [
            "aita_wifi_token": UserDefaults.standard.object(forKey: AitaToken) as! String
        ]
        
        let tags = ["wifi"]
        let feature = "invite"
        let stage = "profile"
        
        guard let itemProvider = Branch.getActivityItem(withParams: params, feature: feature, stage: stage, tags: tags) else {return}
        
        let shareViewController = UIActivityViewController(activityItems: [shareString, itemProvider],
                                                           applicationActivities: nil)
        shareViewController.completionWithItemsHandler = { activityType, completed, returnedItems, activityError in
            if completed {
                GA.event("wifiProfile_invite_success", label: String(describing: activityType?._rawValue))
            } else {
                GA.event("wifiProfile_invite_faliure")
            }
        }
        
        navigationController?.present(shareViewController, animated: true, completion: nil)
    }
    
}

