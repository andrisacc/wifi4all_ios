//
//  Mark.swift
//  WifiCaptive
//
//  Created by plotkin on 05/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import Foundation
import MapKit
import CoreData
import Alamofire
//import HotspotHelper

@objc(WifiHotspot)
class WifiHotspot: NSManagedObject, MKAnnotation {
    
    @NSManaged var desc: String
    @NSManaged var ssid: String
    @NSManaged var id: String
    @NSManaged var name: String
    @NSManaged var latitude: Double
    @NSManaged var longitude: Double
    @NSManaged var isUserHotspot: Bool
    @NSManaged var ignored: Bool
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    class var entity_: NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: "WifiHotspot", in: CoreDataHelper.instance.context)!
    }
    
    convenience init(latitude: Double, longitude: Double) {
        self.init(entity: WifiHotspot.entity_, insertInto: nil)
    }
    
    convenience init() {
        self.init(entity: WifiHotspot.entity_, insertInto: nil)
    }
    
    class func hotspotWithID(_ hotspotID: String) -> WifiHotspot? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "WifiHotspot")
        request.predicate = NSPredicate(format: "id == %@", hotspotID)
        
        return try! CoreDataHelper.instance.backgroundContext.fetch(request).first as? WifiHotspot
    }
    
    convenience init(id: String) {
        self.init(entity: WifiHotspot.entity_, insertInto: CoreDataHelper.instance.backgroundContext)
        
        self.id = id
    }
    
    func ignore() {
        self.ignored = true
        let _ = try? managedObjectContext?.save()
        // TODO: - Change for HotspotHelper migration -
        WifiConfiguration.ingoreNetworkWithSSID(ssid: ssid)
    }
    
    func allow() {
        self.ignored = false
        let _ = try? managedObjectContext?.save()
        WifiConfiguration.allowNetworkWithSSID(ssid: ssid)
    }
    
    class func cd_hotspotsWithin(latitude1: Double, latitude2: Double, longitude1: Double, longitude2: Double, completion: @escaping ([WifiHotspot]) -> Void) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "WifiHotspot")
        request.predicate = NSPredicate(format: "latitude > %f && latitude < %f && longitude > %f && longitude < %f",
            latitude1, latitude2, longitude1, longitude2)
        request.resultType = .managedObjectIDResultType
        if #available(iOS 9.0, *) {
            request.fetchLimit = ProcessInfo.processInfo.isLowPowerModeEnabled ? 100 : 500
        } else {
            request.fetchLimit = 500
        }
        
        CoreDataHelper.instance.fetchContext.perform {
            
            let ids = try! CoreDataHelper.instance.fetchContext.fetch(request) as! [NSManagedObjectID]
            
            CoreDataHelper.instance.context.perform {
                var hotspots = [WifiHotspot]()
                for objectID in ids {
                    hotspots.append(CoreDataHelper.instance.context.object(with: objectID) as! WifiHotspot)
                }
                completion(hotspots)
            }
        }
    }
    
    class func fetchHotspotsCountForLocation(_ location: CLLocationCoordinate2D, distance: Double, completion: @escaping (Bool, Int?) -> Void) {
        
        let params: [String: AnyObject] = [
            "lat": location.latitude as AnyObject,
            "lon": location.longitude as AnyObject,
            "distance": Int(distance) as AnyObject,
            "allow_wifimap": 1 as AnyObject,
            "count_only": 1 as AnyObject
        ]
        
        Alamofire.request(BaseURL + "wifi/hotspots",
                          method: .get, parameters: params,
                          encoding: URLEncoding.default,
                          headers: nil).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                guard let countSystem = json["count_hotspots"].int, let countUser = json["count_user_hotspots"].int else { completion(false, nil); return }
                                completion(true, countSystem + countUser)
                                
                            case .failure(let error):
                                
                                print(error)
                                completion(false, nil)
                                
                            }
                            
        }
    }
    
    class func fetchHotspotsForLocation(_ location: CLLocationCoordinate2D, distance: Double, completion: @escaping (Bool) -> Void) {
        
        let params: [String: AnyObject] = [
            "lat": location.latitude as AnyObject,
            "lon": location.longitude as AnyObject,
            "limit": 500 as AnyObject,
            "distance": Int(distance) as AnyObject,
            "allow_wifimap": 1 as AnyObject
        ]
        
        Alamofire.request(BaseURL + "wifi/hotspots",
                          method: .get, parameters: params,
                          encoding: URLEncoding.default,
                          headers: nil).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let list = ["hotspots", "user_hotspots"]
                                for hotspot_type in list {
                                    if (object as? NSDictionary)?[hotspot_type] != nil {
                                        
                                        CoreDataHelper.instance.backgroundContext.perform {
                                            
                                            var points = [WifiHotspot]()
                                            let json = JSON(object)
                                            
                                            for var hotspot in json[hotspot_type].array! {
                                                
                                                var point: WifiHotspot! = WifiHotspot.hotspotWithID(hotspot["id"].string!)
                                                
                                                if point == nil {
                                                    point = WifiHotspot(id: hotspot["id"].string!)
                                                }
                                                
                                                if let description = hotspot["description"].string {
                                                    point!.desc = description
                                                } else {
                                                    point!.desc = "no description"
                                                }
                                                if let remoteSSID = hotspot["ssid"].string{
                                                    point!.ssid = remoteSSID
                                                } else {
                                                    point!.ssid = "no_ssid"
                                                }
                                                if let remoteName = hotspot["ssid"].string{
                                                    point!.name = remoteName
                                                } else {
                                                    point!.name = "no_name"
                                                }
                                                if let remoteLon = hotspot["lon"].double{
                                                    point!.longitude = remoteLon
                                                } else {
                                                    point!.longitude = 0.00
                                                }
                                                if let remoteLat = hotspot["lat"].double{
                                                    point!.latitude = remoteLat
                                                } else {
                                                    point!.latitude = 0.00
                                                }
                                                //                                    point!.ssid = hotspot["ssid"].string!
                                                //                                    point!.name = hotspot["name"].string!
                                                //                                    point!.longitude = hotspot["lon"].double!
                                                //                                    point!.latitude = hotspot["lat"].double!
                                                
                                                point!.isUserHotspot = hotspot_type == "user_hotspots"
                                                
                                                points.append(point)
                                                
                                            }
                                            
                                            try! CoreDataHelper.instance.backgroundContext.save()
                                            
                                            DispatchQueue.main.async {
                                                completion(points.count > 0)
                                            }
                                        }
                                    } else {
                                        completion(false)
                                    }
                                }
                                
                                break
                                
                            case .failure(let error):
                                print(error)
                                completion(false)
                                break
                            }
                            
        }
        
    }
}
