////
////  CaptiveURLProtocol.swift
////  WifiHotspot
////
////  Created by Sergey Pronin on 8/19/15.
////  Copyright © 2015 App in the Air. All rights reserved.
////
//
//import UIKit
//import CocoaLumberjack
//
//class CaptiveURLProtocol: NSURLProtocol {
//    override class func canInitWithRequest(request: NSURLRequest) -> Bool {
//        if request.URL?.absoluteString.containsString("stripe.com") == true {
//            return false
//        }
//        return NetworkHelper.instance.shouldHandleWebViewRequests
//    }
//    
//    override class func canonicalRequestForRequest(request: NSURLRequest) -> NSURLRequest {
//        return request
//    }
//    
//    override class func requestIsCacheEquivalent(aRequest: NSURLRequest, toRequest bRequest: NSURLRequest) -> Bool {
//        return super.requestIsCacheEquivalent(aRequest, toRequest:bRequest)
//    }
//    
//    lazy var ProtocolSession: NSURLSession = {
//        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
//        config.allowsCellularAccess = false
//        config.timeoutIntervalForResource = 90
//        config.timeoutIntervalForRequest = 90
//        return NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
//    }()
//    
//    override func startLoading() {
//        let mutableRequest = request.mutableCopy() as! NSMutableURLRequest
//        
//        if #available(iOS 9.0, *) {
//            if let command = NetworkHelper.instance.lastCommand where NetworkHelper.instance.shouldHandleWebViewRequests {
//                mutableRequest.bindToHotspotHelperCommand(command)
//            }
//        }
//        
//        if let url = mutableRequest.URL {
//            Logger.logMessage("\(mutableRequest.HTTPMethod) \(url) \(mutableRequest.allHTTPHeaderFields ?? [:])")
//        }
//        
//        ProtocolSession.dataTaskWithRequest(mutableRequest) { data, response, error in
//            if let error = error {
//                self.client!.URLProtocol(self, didFailWithError: error)
//            } else if let response = response, data = data {
//                self.client!.URLProtocol(self, didReceiveResponse: response, cacheStoragePolicy: NSURLCacheStoragePolicy.Allowed)
//                self.client!.URLProtocol(self, didLoadData: data)
//                self.client!.URLProtocolDidFinishLoading(self)
//            }
//        }.resume()
//    }
//    
//    override func stopLoading() {
//        print("stopLoading")
//        DDLogDebug("stopLoading")
//    }
//    
//    
//}
//
//extension CaptiveURLProtocol: NSURLSessionTaskDelegate {
//    func URLSession(session: NSURLSession, task: NSURLSessionTask, willPerformHTTPRedirection response: NSHTTPURLResponse, newRequest request: NSURLRequest, completionHandler: (NSURLRequest?) -> Void) {
//        Logger.logMessage("protocol willPerformHTTPRedirection \(response.allHeaderFields)")
//        if let config = NetworkHelper.instance.currentConfiguration where config.params["follow_redirects"].bool == true {
//            completionHandler(request)
//        } else {
//            completionHandler(nil)
//        }
//    }
//}
