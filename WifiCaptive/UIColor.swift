//
//  UIColor.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

public extension UIColor {
    
    class func brandColor(_ alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: alpha)
    }
    class func clusterPinColor() -> UIColor {
        return UIColor(red: 0.0/255.0, green: 84.0/255.0, blue: 166.0/255.0, alpha: 1)
    }
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
}

extension UIColor {
    class func colorWithHex(hex: Int) -> UIColor {
        let red = CGFloat((hex & 0xff0000) >> 16)
        let green = CGFloat((hex & 0x00ff00) >> 8)
        let blue = CGFloat(hex & 0x0000ff)
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
}


