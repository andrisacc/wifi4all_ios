//
//  WlecomeContainerViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 12.08.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
import Foundation

class WelcomeContainerViewController: UIViewController, FBSDKLoginButtonDelegate {
    

    
    // MARK: - Properties - 
    
    var timer: Timer!
    weak var repeatingTimer: Timer!
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var facebookButton: FBSDKLoginButton!
    @IBOutlet weak var swipeAnimationView: UIView!
    
    @IBOutlet weak var okButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let button = UIButton(frame: CGRectMake(view.bounds.maxX - 88, 8, 100, 30))
//        button.backgroundColor = UIColor.clearColor()
//        button.setTitle(QQLocalizedString("Skip"), forState: UIControlState.Normal)
//        button.addTarget(self, action: #selector(WelcomeContainerViewController.skip), forControlEvents: UIControlEvents.TouchUpInside)
//        self.view.addSubview(button) // add to view as subview
        
        okButton.isHidden = false
        okButton.setTitle(QQLocalizedString("Skip"), for: UIControlState())
        okButton.titleLabel?.font = CurrentFont(fontSize: 18.0).currentFont
        facebookButton.isHidden = true
        configuringFacebook ()
        
//        if UIDevice().userInterfaceIdiom == .Phone {
//            
//            switch UIScreen.mainScreen().nativeBounds.height {
//                
//            case 480: fallthrough
//            //                    print("iPhone Classic")
//            case 960:
//                //                    print("iPhone 4 or 4S")
//                okButton.hidden = true
//                swipeAnimationView.hidden = true
//            case 1136: fallthrough
//            //                  print("iPhone 5 or 5S or 5C")
//            case 1334: fallthrough
//            //                    print("iPhone 6 or 6S")
//            case 2208: fallthrough
//            //                    print("iPhone 6+ or 6S+")
//            default:
//                //                    print("unknown")
//                swipeActivity.sharedInstance.showSwipeActivity(self.swipeAnimationView)
//            }
//        }
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        
//        guard let initial = self.storyboard?.instantiateInitialViewController() else { return }
//        UIApplication.sharedApplication().keyWindow?.rootViewController = initial
        
//        let loginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
//        loginViewController.modalTransitionStyle = .PartialCurl
//        let navigationController = UINavigationController(rootViewController: loginViewController)
//        UIApplication.sharedApplication().keyWindow?.rootViewController = navigationController
//        self.presentViewController(navigationController, animated: true, completion: nil)
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "WifiMapViewController") as! WifiMapViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftCollectionViewController") as! LeftCollectionViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        UINavigationBar.appearance().tintColor = UIColor.brandColor()
        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        UIApplication.shared.keyWindow?.rootViewController  = slideMenuController
        
        nvc.navigationBar.tintColor = UIColor.white
        nvc.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isTranslucent = false
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore  {
            LoginHelper.startHotspotHelper()
        }
        
        GA.event("wifiWelcomView_Skip")
            
    }
    
    
    // MARK: Facebook integration
    
    func configuringFacebook () {
        facebookButton.delegate = self
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        
        //         MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        
        let loginManager = FBSDKLoginManager()
        loginManager.defaultAudience = .onlyMe
        loginManager.loginBehavior = .systemAccount
        loginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            
            if (error != nil) {
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                let ns_error = error! as NSError
                switch ns_error.code {
                case -1009:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                    
                case -1200:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                case 4:
                    self.presentError(QQLocalizedString("Sorry, no internet conection. Please try again later"))
                case 306:
                    
                    let alertController = UIAlertController (title: QQLocalizedString("Access to your Facebook information is required"), message: QQLocalizedString("Go to Settings?"), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: QQLocalizedString("Settings"), style: .default) { (_) -> Void in
                        if #available(iOS 10.0, *){
                            UIApplication.shared.openURL(URL(string: "App-Prefs:root=FACEBOOK")!)
                        }
                        else {
                            UIApplication.shared.openURL(URL(string: "prefs:root=FACEBOOK")!)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                default: break
                    
                }
                
            } else if (result?.isCancelled)! {
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                
            } else {
                
                guard let fbToken = FBSDKAccessToken.current() else {return}
                
                
                LoginHelper.facebookLogin(fbToken.tokenString, completion: { (success, errorString) in
                    
                    //                     MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    //                    CustomActivity().hideActivityIndicator(self.view)
                    CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                    
                    if success {
                        
                        CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                        guard let initial = self.storyboard?.instantiateInitialViewController() else { return }
                        UIApplication.shared.keyWindow?.rootViewController = initial
                        //
                    } else {
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        //                        CustomActivity().hideActivityIndicator(self.view)
                        self.presentError(errorString ?? QQLocalizedString("There was an error during your sign up. Try again later"))
                        self.fbLogout()
                    }
                })
            }
        }
        return false
    }
    //
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!){
        fbLogout()
    }
    
    func fbLogout () {
        
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    func presentError(_ error: String) {
        
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - Helpers -
    
    func skip () {
        
        //        guard let initial = self.storyboard?.instantiateInitialViewController() else { return }
        //        UIApplication.sharedApplication().keyWindow?.rootViewController = initial
        
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginViewController.modalTransitionStyle = .partialCurl
        let navigationController = UINavigationController(rootViewController: loginViewController)
        UIApplication.shared.keyWindow?.rootViewController = navigationController
        //        self.presentViewController(navigationController, animated: true, completion: nil)
        
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let WelcomeViewController = segue.destination as? WelcomeViewController {
            WelcomeViewController.pageViewlDelegate = self
        }
    }

}

extension WelcomeContainerViewController: WelcomeViewControllerDelegate {
    
    func welcomeViewController(_ welcomeViewController: WelcomeViewController, didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func welcomeViewController(_ welcomeViewController: WelcomeViewController, didUpdatePageIndex index: Int) {
        
        pageControl.currentPage = index
        
        if  index == pageControl.numberOfPages - 1 {
            
            if UIDevice().userInterfaceIdiom == .phone {
                
                switch UIScreen.main.nativeBounds.height {
                    
                case 480: fallthrough
//                    print("iPhone Classic")
                case 960:
//                    print("iPhone 4 or 4S")
//                    okButton.hidden = true
                    fallthrough
                case 1136: fallthrough
//                  print("iPhone 5 or 5S or 5C")
                case 1334: fallthrough
//                    print("iPhone 6 or 6S")
                case 2208: fallthrough
//                    print("iPhone 6+ or 6S+")
                default:
//                    print("unknown")
                    facebookButton.isHidden = true
                    okButton.isHidden = false
                    
                }
            }
            
        }
//        else if index > 0 {
//            swipeActivity.sharedInstance.hideSwipeActivity(self.swipeAnimationView)
//        }

    }
    
}
