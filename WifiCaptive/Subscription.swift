//
//  Subscription.swift
//  WifiCaptive
//
//  Created by Sergey Pronin on 8/22/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
//import HotspotHelper
import Alamofire
import Stripe
import StoreKit
import CocoaLumberjack

class Subscription: NSObject {
    
    let price: String
    let item: String
    let number: String
    let plan: String
    let planDescription: String
    let decimalPrice: Double
    let currency: String
    let productId: String
    let pointsPrice: Int
    
    var product: SKProduct?
    
    init(json: JSON) {
        price = json["price"].string!
        productId = json["itunes_id"].string!
        plan = json["plan"].string!
        item = json["item"].string!
        number = json["number"].string!
        currency = json["currency"].string!
        decimalPrice = json["itunes_price"].double!
        planDescription = json["description"].string!
        pointsPrice = json["points_price"].int!
    }
    
    class func cancelCurrentSubscription(_ completion: @escaping (Bool, String?) -> Void) {
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { completion(false, nil); return }
        
        Alamofire.request(BaseURL + "wifi/cancel",
                          method: .post, parameters: ["app": "wifi"],
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                if json["user"].dictionary != nil {
                                    LoginHelper.storeUserResponse(json)
                                    
                                    completion(true, nil)
                                    
                                } else {
                                    completion(false, JSON(object)["error_message"].string)
                                }
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    
                                    completion(false, message)
                                    
                                } else {
                                    
                                    completion(false, nil)
                                    
                                }
                            }
                            
        }
        
    }
    
    class func setUpStripe(_ production: Bool) {
        let stripeProductionKey = "pk_live_i2pDeZaY0Fby8pIHrH3fDGkq"
        let stripeTestKey = "pk_test_CkKgdnANnwUPTdfmoqH8n50U"
        Stripe.setDefaultPublishableKey(production ? stripeProductionKey : stripeTestKey)
    }
    
    fileprivate class func loadProducts(_ subscriptions: [Subscription], completion: @escaping (Bool, [Subscription]?) -> Void ) {
        
        BPStoreObserver.shared().processProductRequests(subscriptions.map({ $0.productId })) { success, products in
            
            if let products = products as? [AnyHashable: Any], success {
                for subscription in subscriptions {
                    subscription.product = products[subscription.productId] as? SKProduct
                }
                completion(true, subscriptions)
            } else {
                completion(false, nil)
            }
            
        }
    }
    
    class func fetchSubscriptionOptions(_ completion: @escaping ([Subscription]?) -> Void) {
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { completion(nil); return }
        
        Alamofire.request(BaseURL + "wifi/subscriptions",
                          method: .get, parameters: ["app": "wifi"],
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                guard let subscriptions = json["subscriptions"].array?.map({ Subscription(json: $0) }) else { completion(nil); return }
                                //self.setUpStripe(json["live_mode"].bool == true)
                                
                                self.loadProducts(subscriptions) { success, result in
                                    if success {
                                        completion(result)
                                    } else {
                                        completion(nil)
                                    }
                                }
                                
                            case .failure(_):
                                completion(nil)
                            }
                            
        }
        
    }
    
    func subscribe(_ stripeToken: STPToken? = nil, completion: @escaping (Bool, String?) -> Void) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { completion(false, nil); return }
        
        //        DDLogDebug("Purchasing \(self.plan) for \(token) ")
        
        var params = [
            "plan": self.plan,
            "app": "wifi"
        ]
        if let token = stripeToken {
            params["source"] = token.tokenId
        }
        
        Alamofire.request(BaseURL + "wifi/subscribe",
                          method: .post, parameters: params,
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                let json = JSON(object)
                                
                                if json["user"].dictionary != nil {
                                    
                                    //                        DDLogDebug("\(json)to be subscribed with credentials /n \(object)")
                                    
                                    LoginHelper.storeUserResponse(json)
                                    // TODO: - Change for HotspotHelper migration -
                                    HotspotCredentials.updateCredentials(jsonObj: json)
                                    
                                    completion(true, nil)
                                } else {
                                    //                        DDLogDebug("Error - \(JSON(object)["error_message"].string)")
                                    completion(false, JSON(object)["error_message"].string)
                                }
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    //                        DDLogDebug("Fail - \(message)")
                                    completion(false, message)
                                } else {
                                    DDLogDebug("Fail - unknown error")
                                    completion(false, "unknown error")
                                }
                                
                            }
                            
        }
    }
    
    func subscribeWithPoints(_ completion: @escaping (Bool, String?) -> Void) {
        
        guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { completion(false, nil); return }
        
        //        DDLogDebug("Purchasing \(self.plan) for \(token) ")
        
        let params = [
            "plan": self.plan,
            "app": "wifi"
        ]
        
        Alamofire.request(BaseURL + "wifi/points/subscribe",
                          method: .post, parameters: params,
                          encoding: URLEncoding.default,
                          headers: ["WiFi-User-Token": token]).validate(statusCode: 200..<300).responseJSON {response in
                            
                            switch response.result {
                                
                            case .success(let object):
                                
                                let json = JSON(object)
                                
                                //                    DDLogDebug("Json on points.subscribe: \(json)")
                                
                                if json["user_hotspots"] != nil {
                                    let user_hotspots = json["user_hotspots"].stringValue
                                    let defaults = UserDefaults.standard
                                    defaults.set(user_hotspots, forKey: "user_hotspots")
                                }
                                if json["user_points"] != nil {
                                    let user_points = json["user_points"].stringValue
                                    let defaults = UserDefaults.standard
                                    defaults.set(user_points, forKey: "user_points")
                                }
                                
                                if json["user"].dictionary != nil {
                                    
                                    //                        DDLogDebug("\(json)to be subscribed with credentials /n \(object)")
                                    
                                    LoginHelper.storeUserResponse(json)
                                    // TODO: - Change for HotspotHelper migration -
                                    HotspotCredentials.updateCredentials(jsonObj: json)
                                    
                                    completion(true, nil)
                                    
                                } else {
                                    //                        DDLogDebug("Error - \(JSON(object)["error_message"].string)")
                                    completion(false, JSON(object)["error_message"].string)
                                }
                                
                            case .failure(_):
                                
                                if let data = response.data, let message = JSON(data: data)["error_message"].string {
                                    //                        DDLogDebug("Fail - \(message)")
                                    completion(false, message)
                                } else {
                                    DDLogDebug("Fail - unknown error")
                                    completion(false, "unknown error")
                                }
                                
                            }
                            
        }
        
    }
}
