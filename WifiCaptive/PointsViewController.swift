//
//  PointsViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 24.11.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
//import HotspotHelper
import Alamofire
import CocoaLumberjack

class PointsViewController: UIViewController, ErrorHandling, SuccessHandling {

    // MARK: - Properties -
    var pointsDescriptionArray = [JSON]()
    var extra = [JSON]()
    var segmentView: SMSegmentView!
    var noLeftBarItem = false
    
//    weak var delegate: LeftMenuCollectionProtocol?
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var yourPointsLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segemntViewView: UIView!
    
    @IBOutlet weak var introLabel: UILabel!
    
    @IBOutlet weak var storeLinkOutlet: UIButton!
    @IBAction func storeLink(_ sender: UIButton) {
//        delegate?.changeViewController(.Subscribe)
        presentBuyScreen()
    }
    
    // MARK: - VC Load -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Add notification to show the profile controller
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
        
        yourPointsLabel.font = CurrentFont(fontSize: 24.0).currentFont
        introLabel.font = CurrentFont(fontSize: 17.0).currentFont
        storeLinkOutlet.titleLabel?.font = CurrentFont(fontSize: 30.0).currentFont
        pointsLabel.font = CurrentFont(fontSize: 90.0).currentFont
        
        storeLinkOutlet.setTitle(QQLocalizedString("SHOP"), for: UIControlState())
        
        // Do any additional setup after loading the view.
        
        introLabel.text = QQLocalizedString("Earn points by inviting friends or adding the hotspots and get FREE Internet access all over the World!")
        
//        if isWYGRealm() {
            storeLinkOutlet.isHidden = true
//        }
        
        // Segmente View SetUp
        let appearance = SMSegmentAppearance()
        appearance.segmentOnSelectionColour = UIColor.white
        appearance.segmentOffSelectionColour = UIColor(hexString: "#2D81C1")
        appearance.titleOnSelectionColour = UIColor(hexString: "#399DE9")
        appearance.titleOffSelectionColour = UIColor.white
        
        appearance.titleOnSelectionFont = CurrentFont(fontSize: 16.0).currentFont
        appearance.titleOffSelectionFont = CurrentFont(fontSize: 16.0).currentFont
        appearance.contentVerticalMargin = 10.0
        
        let segmentFrame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50.0)
        
        self.segmentView = SMSegmentView(frame: segmentFrame, dividerColour: UIColor.brandColor(), dividerWidth: 0.0, segmentAppearance: appearance)
        segmentView.addSegmentWithTitle(QQLocalizedString("Current"), onSelectionImage: nil, offSelectionImage: nil)
        segmentView.addSegmentWithTitle(QQLocalizedString("Earned"), onSelectionImage: nil, offSelectionImage: nil)
        segmentView.addSegmentWithTitle(QQLocalizedString("Spent"), onSelectionImage: nil, offSelectionImage: nil)
        
        segmentView.addTarget(self, action: #selector(PointsViewController.selectSegmentInSegmentView(_:)), for: .valueChanged)
        
        // Set segment with index 0 as selected by default
        segmentView.selectedSegmentIndex = 0
        
        view.addSubview(segmentView)
        
        // Table View Setup
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.allowsSelectionDuringEditing = false
        
        navigationController?.topViewController?.title = QQLocalizedString("Points")
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)]
        
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = false
        
        
        if noLeftBarItem {
            
//            let rightButton = UIBarButtonItem(image: UIImage(named: "backCrossWhite"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(PointsViewController.dismissVC))
//            self.navigationItem.rightBarButtonItem = rightButton
//            self.navigationItem.rightBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5)
            
            let leftButton = UIBarButtonItem(image: UIImage(named: "arrow_back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PointsViewController.dismissVC))
            self.navigationItem.leftBarButtonItem = leftButton
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if noLeftBarItem {
            self.setNavigationBarItem(true)
        } else {
            self.setNavigationBarItem(false)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }

    
    // MARK: - Methods -
    
    func openProfileVC () {
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork(self)
        }
    }
    
    func dismissVC () {
        self.dismiss(animated: true, completion: nil)
    }
    
    func presentBuyScreen() {
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        Subscription.fetchSubscriptionOptions { subscriptions in
            
            MBProgressHUD.hideAllHUDs(for: UIWindow.mainWindow, animated: true)
            CustomActivity.sharedInstance.hideActivityIndicator(self.view)
            
            if let subscriptions = subscriptions, subscriptions.count > 0 {
        
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                guard let purchaseViewController = storyboard.instantiateViewController(withIdentifier: "PurchaseViewController") as? PurchaseViewController else { return }
                purchaseViewController.subscriptions = subscriptions
                
                purchaseViewController.noLeftBarItem = true
                
                let navigationController = UINavigationController(rootViewController: purchaseViewController)
                self.present(navigationController, animated: true, completion: nil)
                
            }
        }
        GA.event("wifiPoints_store_tap")
    }
    
    func selectSegmentInSegmentView(_ segmentView: SMSegmentView) {

        switch segmentView.selectedSegmentIndex {
            
        case 0:
            
            tableView.isHidden = true
            introLabel.isHidden = false
            
            yourPointsLabel.text = QQLocalizedString("Your current points")
            
            if let userCurrentPointsCount = UserDefaults.standard.object(forKey: "user_points") as? String {
                pointsLabel.text = userCurrentPointsCount
            } else {
                pointsLabel.text = QQLocalizedString("No points yet")
            }
            
            GA.event("wifiPoints_current_points_tap")
            
            break
            
        case 1:
            
            introLabel.isHidden = true
            
            yourPointsLabel.text = QQLocalizedString("Your earned points")
            
            CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
            
            APIRequestHelper.myPointPlus({[unowned self] (json, errorString) in
                
                if let json = json {
                    
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    self.tableView.isHidden = false
                    
                    if json["count"] != nil {
                        if let plus_count = json["count"].int {
                            self.pointsLabel.text = "\(plus_count)"
                        } else {
                            self.pointsLabel.text = QQLocalizedString("No points yet")
                        }
                    }
                    if json["points"] != nil {
                        
                        if let pointsArray = json["points"].array {
                            //                                print("Points", pointsArray)
                            self.pointsDescriptionArray = pointsArray
                        }
                        self.tableView.reloadData()
                    }
                    
                } else {
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    self.handleError(error: errorString ?? "Error...")
                }
                
            })
            
            GA.event("wifiPoints_earned_points_tap")
            
            break
            
        case 2:
            
            introLabel.isHidden = true
            
            yourPointsLabel.text = QQLocalizedString("Your spent points")
            
            CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
            
            guard let token = UserDefaults.standard.object(forKey: AitaToken) as? String else { return }
            
            APIRequestHelper.myPointMinus({[unowned self] (json, errorString) in
                
                if let json = json {
                    
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    
                    self.tableView.isHidden = false
                    
                    if json["count"] != nil {
                        if let minus_count = json["count"].int {
                            if minus_count > 0 {
                                
                                self.pointsLabel.text = "-\(minus_count)"
                            } else {
                                self.pointsLabel.text = "\(minus_count)"
                            }
                            
                        } else {
                            self.pointsLabel.text = QQLocalizedString("No points yet")
                        }
                    }
                    
                    if json["points"] != nil {
                        
                        if let pointsArray = json["points"].array {
                            //                                print("Points", pointsArray)
                            self.pointsDescriptionArray = pointsArray
                        }
                        self.tableView.reloadData()
                    }
                    
                } else {
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    self.handleError(error: errorString ?? "Error...")
                }
            })
            
            GA.event("wifiPoints_spent_points_tap")
            
            break
            
        default: break
            
        }
        
    }

}

extension PointsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return pointsDescriptionArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PointsTableViewCell
        
        cell.numberPoints.font = CurrentFont(fontSize: 15.0).currentFont
        cell.pointDescription.font = CurrentFont(fontSize: 17.0).currentFont
        
        if self.pointsDescriptionArray.count > 0 {
            
            if segmentView.selectedSegmentIndex == 1 {
                
                if self.pointsDescriptionArray[indexPath.row]["type"].stringValue == "HOTSPOT" {
                    cell.pointDescription.text = QQLocalizedString("Addition of a hotspot")
                } else if self.pointsDescriptionArray[indexPath.row]["type"].stringValue == "INVITE" {
                    cell.pointDescription.text = QQLocalizedString("Invite of a friend")
                }
                
                if let numberPoints = self.pointsDescriptionArray[indexPath.row]["amount"].int {
                    cell.numberPoints.text = "+\(numberPoints)" + QQLocalizedString("pt.")
                }
                if let dateTime = self.pointsDescriptionArray[indexPath.row]["date"].int {
                    let timePassed = TimeInterval(Double(dateTime))
                    let time = Date(timeIntervalSince1970: timePassed)
                    
                    let timeString = dateTimeForString(time)
                    
                    cell.dateTime.text = timeString
                }
                
                
            } else if segmentView.selectedSegmentIndex == 2 {
                
                if self.pointsDescriptionArray[indexPath.row]["type"].stringValue == "SUBSCRIB" {
                    //                    cell.pointDescription.text = self.pointsDescriptionArray[indexPath.row]["description"].stringValue
                    
                    if let extra = self.pointsDescriptionArray[indexPath.row]["extra"].dictionaryObject {
                        if let extraItem = extra["item"]  {
                            cell.pointDescription.text = QQLocalizedString("Unlimited access") + " 1 " + QQLocalizedString("\(extraItem)")
                        }
                    }
                }
                
                if let numberPoints = self.pointsDescriptionArray[indexPath.row]["amount"].int {
                    cell.numberPoints.text = "-" + "\(numberPoints)" + QQLocalizedString("pt.")
                }
                if let dateTime = self.pointsDescriptionArray[indexPath.row]["date"].int {
                    let timePassed = TimeInterval(Double(dateTime))
                    let time = Date(timeIntervalSince1970: timePassed)
                    
                    let timeString = dateTimeForString(time)
                    
                    cell.dateTime.text = timeString
                }
            }
        }
        
        return cell
    }
}
