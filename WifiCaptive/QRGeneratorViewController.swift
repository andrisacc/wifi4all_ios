//
//  QRGeneratorViewController.swift
//  QRCodeGen
//
//  Created by Victor Barskov on 24.10.16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

import UIKit

class QRGeneratorViewController: UIViewController {

    // MARK: - Properties - 
    
    
    
    // MARK: - IBOutlets - 
    
    @IBOutlet weak var SSID: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    @IBAction func generAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let finalViewController = storyboard.instantiateViewController(withIdentifier: "FinalQRViewController") as? FinalQRViewController else {return}
        
        finalViewController.SSID = SSID.text
        finalViewController.password = password.text
        
        let navigationController = UINavigationController(rootViewController: finalViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
