import UIKit
import CoreData

class CoreDataHelper: NSObject {
    
    class var instance: CoreDataHelper {
        struct Singleton {
            static var instance = CoreDataHelper()
        }
        return Singleton.instance
    }
    
    let coordinator: NSPersistentStoreCoordinator
    let context: NSManagedObjectContext
    let backgroundContext: NSManagedObjectContext
    let fetchContext: NSManagedObjectContext
    
    fileprivate override init() {
        
        let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd")!
        
        let model = NSManagedObjectModel(contentsOf: modelURL)!
        
        coordinator = NSPersistentStoreCoordinator(
            managedObjectModel: model)
        
        let fileManager = FileManager.default
        
        let libURL = fileManager.urls(for: .libraryDirectory, in: .userDomainMask).first!
        let storeURL = libURL.appendingPathComponent("store.sqlite")
        
        try! coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil,
            at: storeURL,
            options: [
                NSMigratePersistentStoresAutomaticallyOption: true,
                NSInferMappingModelAutomaticallyOption: true,
            ])
        
        context = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
        
        backgroundContext = NSManagedObjectContext(concurrencyType:
            NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)        
        backgroundContext.persistentStoreCoordinator = coordinator
        
        fetchContext = NSManagedObjectContext(concurrencyType:
            NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        fetchContext.persistentStoreCoordinator = coordinator
    }
    
    func save() {
        do {
            try context.save()
        } catch { }
    }
}





