//
//  SettingGenerics.swift
//  emop
//
//  Created by Victor Barskov on 21/11/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation
import UIKit

struct Setting<V> {
    
    let key: String
    let value: V
    
    init(key: String) {
        self.key = key
        let val = Settings.settingForKey(key: key)
        if V.self == UIColor.self {
            value = UIColor.colorWithHex(hex: val as! Int) as! V
        } else if V.self == URL.self {
            value = URL(string: val as! String) as! V
        } else {
            value = val as! V
        }
    }
}

struct OptionalSetting<V> {
    
    let key: String
    let value: V?
    
    init(key: String) {
        
        self.key = key
        let val = Settings.settingForKey(key: key)
        if let s = val as? String, s == "NULL" {
            value = nil
        } else {
            value = val as? V
        }
    }
}
