//
//  LastViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 12.08.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
import SwiftyGif

class LastPageViewController: UIViewController {

    
    @IBOutlet weak var topLabel: UILabel!
    
    @IBOutlet weak var animatedImage: UIImageView!
    @IBAction func startAnimation(_ sender: UITapGestureRecognizer) {
        self.animatedImage.startAnimatingGif()
    }
    
    let gifmanager = SwiftyGifManager(memoryLimit:20)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gif = UIImage(gifName: "Slide4", levelOfIntegrity: 0.5)
        self.animatedImage.setGifImage(gif, manager: gifmanager, loopCount:1)
        
//        let imageview = UIImageView(gifImage: gif, manager: gifmanager, loopCount:1)
//        imageview.frame = CGRect(x: 0.0, y: 5.0, width: view.frame.width, height: view.frame.height)
//        view.addSubview(imageview)

        let label = UILabel(frame: CGRect(x: 8, y: 20, width: view.frame.width - 16, height: 120))
        label.text = QQLocalizedString("Want more?")
        label.font = CurrentFont(fontSize: 28.0).currentFont
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        view.addSubview(label)
        
        let label1 = UILabel(frame: CGRect(x: 8, y: view.bounds.maxY - 148, width: view.frame.width - 16, height: 120))
        label1.text = QQLocalizedString("Subscribe and enjoy 1 000 000+ commercial hotspots all over the World!")
        label1.font = CurrentFont(fontSize: 18.0).currentFont
        label1.textColor = UIColor.white
        label1.textAlignment = .center
        label1.lineBreakMode = NSLineBreakMode.byWordWrapping
        label1.numberOfLines = 0
        label1.adjustsFontSizeToFitWidth = true
        view.addSubview(label1)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        animatedImage.stopAnimatingGIF()
        self.animatedImage.stopAnimatingGif()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
