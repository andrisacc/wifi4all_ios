//
//  ProfileCollectionViewCell.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 24.11.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var menuItemDescription: UILabel!
    
    
}
