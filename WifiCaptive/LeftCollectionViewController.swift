//
//  LeftViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 07.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
import DigitsKit
//import HotspotHelper
import Alamofire

enum LeftMenuCollection: Int {
    case main = 0
    case invite
    case addWifi
    case promo
    case subscribe
    case profile
    case points
    //    case Logout
    
}


struct CollectionViewItems {
    
//    let menuDictUnsubscribed =  [["imageName":"hotspotMap", "menuItemDescription":"Hotspots", "GA.event":"wifiProfile_main_map"],
//                                 ["imageName":"subscribe", "menuItemDescription":"Shop", "GA.event":"wifiProfile_subscribe_tap"],
//                                 ["imageName":"inviteFriend", "menuItemDescription":"Invite friends", "GA.event":"wifiProfile_invite"],
//                                 ["imageName":"addHotspot", "menuItemDescription":"Add my own WiFi", "GA.event":"wifiProfile_add_WiFi_tap"],
//                                 /*["imageName":"promo", "menuItemDescription":"Apply promo-code", "GA.event":"wifiProfile_promo_tap"],*/
//        /*["imageName":"o", "menuItemDescription":"", "GA.event":"wifiProfile_main_map"]*/
//    ]
//
    let menuDictUnsubscribed = [["imageName":"hotspotMap", "menuItemDescription":"Hotspots", "GA.event":"wifiProfile_main_map"],
                                ["imageName":"inviteFriend", "menuItemDescription":"Invite friends", "GA.event":"wifiProfile_invite"],
                                ["imageName":"addHotspot", "menuItemDescription":"Add my own WiFi", "GA.event":"wifiProfile_add_WiFi_tap"],
                                ["imageName":"o", "menuItemDescription":"", "GA.event":"wifiProfile_main_map"]
                                ]
    
    let menuDictSubscribed =    [["imageName":"hotspotMap", "menuItemDescription":"Hotspots", "GA.event":"wifiProfile_main_map"],
                                 ["imageName":"inviteFriend", "menuItemDescription":"Invite friends", "GA.event":"wifiProfile_invite"],
                                 ["imageName":"addHotspot", "menuItemDescription":"Add my own WiFi", "GA.event":"wifiProfile_add_WiFi_tap"],
                                 ["imageName":"o", "menuItemDescription":"", "GA.event":"wifiProfile_main_map"]
        /*["imageName":"promo", "menuItemDescription":"Apply promo-code", "GA.event":"wifiProfile_promo_tap"]*/
        //                                 ["imageName":"o", "menuItemDescription":"", "GA.event":"wifiProfile_main_map"],
        //                                 ["imageName":"o", "menuItemDescription":"", "GA.event":"wifiProfile_main_map"]
    ]
    
//    let actionsUnsubcribed: [LeftMenuCollection] = [.main, .subscribe, .invite, .addWifi,/* .Promo,*/ .main]
    let actionsUnsubcribed: [LeftMenuCollection] = [.main, .invite, .addWifi, .main/* .Main, .Main*/]
    let actionsSubcribed: [LeftMenuCollection] = [.main, .invite, .addWifi, .main/*, .Main, .Main*/]
    
}


class LeftCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, ErrorHandling, SuccessHandling {
    
    // MARK: - Properties -
    
    var mainViewController: UIViewController!
    var purchaseController: UIViewController!
    var addWiFiController: UIViewController!
    var profileViewController: UIViewController!
    var pointsViewController: UIViewController!
    
    
    //configure the collection view
    
    let leftAndRightPaddings: CGFloat = 0.5
    let numberOfItemsPerRow: CGFloat = 2.0
    
    // MARK: - VC Load -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawFillCircle(view: circleOne, alpha: 0.65, radius: 35, strokeColor: UIColor.clear, fillColor: UIColor.white, lineWidth: 0.0)
        drawFillCircle(view: circleTwo, alpha: 0.35, radius: 41, strokeColor: UIColor.clear, fillColor: UIColor.white, lineWidth: 0.0)
        
        firstName.font = CurrentFont(fontSize: 20.0).currentFont
        lastName.font = CurrentFont(fontSize: 20.0).currentFont
        
        
        // Table View Delegates
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        //        tableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
        
        // Make a picture in a round
        self.picture.layer.cornerRadius = self.picture.frame.size.width / 2
        self.picture.clipsToBounds = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else { return }
        self.profileViewController = UINavigationController(rootViewController: profileViewController)
        
        guard let pointsViewController = storyboard.instantiateViewController(withIdentifier: "PointsViewController") as? PointsViewController else { return }
        self.pointsViewController = UINavigationController(rootViewController: pointsViewController)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        // Remove logout button
        //        logoutButtonOutlet.hidden = true
        
        drawCircle(view: picture, radius: 30, color: UIColor(hexString: "#FFFFFF"), lineWidth: 2.5)
        
        reloadSubscriptionInfo()
        
        showCurrentPoints()
        
        let data = UserDefaults.standard.object(forKey: "user") as? Data
        guard let _data = data else {return}
        let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
        guard let user = userUnwrapped else {return}
        let _user = JSON(user)
        
        if _user["firstname"] == nil && _user["firstname"] == nil {
            firstName.text = QQLocalizedString("Update")
            lastName.text = QQLocalizedString("Profile")
        } else {
            firstName.text = _user["firstname"].stringValue
            lastName.text = _user["surname"].stringValue
        }
        
        LoginHelper.updateUserState()
        
        GA.screen("LeftViewController")
        GA.event("LeftViewController_saw")
        
        collectionView.reloadData()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 1
        
        layout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 1, right: 0)
        layout.itemSize = CGSize(width: collectionView!.bounds.width/2 - 0.3, height: collectionView!.bounds.width/2)
        
        // Get number of culumns 
        
        var items = Float()
        
        //        if isWYGRealm() {
        //            items = Float(CollectionViewItems().menuDictSubscribed.count)
        //        } else {
        items = Float(CollectionViewItems().menuDictUnsubscribed.count)
        //        }
        
        // Lets get number of rows
        let rows = ceil(CGFloat(items / 2.0))
        let heightOfRows = layout.itemSize.height * rows
        if heightOfRows > collectionView.frame.size.height {
        } else {
            collectionView.frame.size.height = layout.itemSize.height * rows
        }
    }
    
    // MARK: - IBOutlets and other -
    
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var labelSubscriptionInfo: UILabel!
    @IBOutlet weak var labelpointsInfo: UILabel!
    
    @IBOutlet weak var circleOne: UIView!
    @IBOutlet weak var circleTwo: UIView!

    @IBAction func tapOnAccessInfoCell(_ sender: UITapGestureRecognizer) {
        //        if isWYGRealm() {
        //        } else {
        changeViewController(.subscribe)
        //        }
        
    }
    
    @IBAction func tapOnMBLeft(_ sender: UITapGestureRecognizer) {
        
        //        if isWYGRealm() {
        GA.event("wifiMenuController_unsucceded_show_store_tap_as_subscribed")
        //        } else {
        changeViewController(.subscribe)
        slideMenuController()?.removeLeftGestures()
        GA.event("wifiMenuController_show_store_tap_on_mb_left")
        //        }
        
    }
    
    @IBAction func tapOnCurrentPoints(_ sender: UITapGestureRecognizer) {
        
        changeViewController(.points)
        slideMenuController()?.removeLeftGestures()
        GA.event("wifiMenuController_show_points_tap")
        
    }
    
    
    @IBAction func profileTap(_ sender: UITapGestureRecognizer) {
        showProfile()
    }
    
    // MARK: - Helpers -
    
    func logOut() {
        
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        LoginHelper.logOut()
        
        Digits.sharedInstance().logOut()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let loginView: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
        UIApplication.shared.keyWindow?.rootViewController = loginView
        
        
        GA.event("wifiProfile_logOut")
        
    }
    
    func presentBuyScreen() {
        
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        
        Subscription.fetchSubscriptionOptions { subscriptions in
            
            CustomActivity.sharedInstance.hideActivityIndicator(self.view)
            
            if let subscriptions = subscriptions, subscriptions.count > 0 {
                
                #if AITA
                    let purchaseController = WifiBuyDescriptionViewController()
                    purchaseController.subscriptions = subscriptions
                    purchaseController.analyticsPrefix = "profile"
                    if UIDevice.type.isIPad {
                        AAPopupViewController.showPopupWithViewController(purchaseController, inContainer: self)
                    } else {
                        self.navigationController?.pushViewController(purchaseController, animated: true)
                    }
                #else
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    guard let purchaseViewController = storyboard.instantiateViewController(withIdentifier: "PurchaseViewController") as? PurchaseViewController else { return }
                    purchaseViewController.subscriptions = subscriptions
                    self.purchaseController = UINavigationController(rootViewController: purchaseViewController)
                    self.slideMenuController()?.changeMainViewController(self.purchaseController, close: true)
                    
                #endif
                
            }
        }
        GA.event("wifiProfile_subscribe")
    }
    
    func addWiFiNetwork () {
        
        self.slideMenuController()?.closeLeft()
        
        delay(0.4) {
            
            if #available(iOS 9.0, *) {
                if let network = currentNetwork {
                    if !network.isSecure {
                        self.handleError(error: QQLocalizedString("Please connect to password-protected network to add it"))
                        return
                    }
                }
            }
            
            if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways)
            {
                
                guard let addNetworkController =
                    self.storyboard?.instantiateViewController(withIdentifier: "AddNetworkViewController") as? AddNetworkViewController else { return }
                addNetworkController.transitioningDelegate = TransitioningDelegate
                
                self.present(addNetworkController, animated: true, completion: nil)
                
                GA.event("wifiProfile_addNetwork")
                
            } else {
                
                let locationAuthStatus = CLLocationManager.authorizationStatus()
                
                if locationAuthStatus == .denied || locationAuthStatus == .restricted {
                    
                    let alertController = UIAlertController (title: QQLocalizedString("Access to your current location is required"), message: QQLocalizedString("Go to Settings?"), preferredStyle: .alert)
                    
                    let settingsAction = UIAlertAction(title: QQLocalizedString("Settings"), style: .default) { (_) -> Void in
                        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                        
                        if let url = settingsUrl {
                            UIApplication.shared.openURL(url)
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func showInvite() {
        
        self.slideMenuController()?.closeLeft()
        
        let alertController = UIAlertController (title: QQLocalizedString("Invite friends"), message: QQLocalizedString("You will get your points once your friend will be registered in the app"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: QQLocalizedString("Ok"), style: .default) { (_) -> Void in
            self.inviteFriend ()
        }
        alertController.addAction(okAction)
        //        let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .Default, handler: nil)
        //        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        GA.event("wifiProfile_invite")
    }
    
    func showInsertPromoCode() {
        
        self.slideMenuController()?.closeLeft()
        
        let alert = UIAlertController(title: QQLocalizedString("Promo-code"), message: QQLocalizedString("Enter the promo-code"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: QQLocalizedString("Cancel"), style: .cancel, handler: { _ in }))
        alert.addAction(UIAlertAction(title: QQLocalizedString("OK"), style: .default, handler: { [unowned alert, weak self] _ in
            guard let code = alert.textFields?.first?.text else { return }
            
            self?.checkPromoCode(code)
            
        }))
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = QQLocalizedString("Promo-code") + ".."
            textField.autocapitalizationType = .allCharacters
        })
        
        present(alert, animated: true, completion: nil)
    }
    
    func checkPromoCode(_ code: String) {
        
        APIRequestHelper.checkPromoCode(code: code) {[unowned self] (succes, errorString) in
            if succes {
                self.reloadSubscriptionInfo()
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                self.handleSuccess(alert: QQLocalizedString("Promo-code has been successfully applied to your account"))
            } else {
                self.handleError(error: errorString ?? "error...")
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
            }
        }
    }
    
    
    func reloadSubscriptionInfo() {
        
        guard let wifi = UserDefaults.standard.object(forKey: "wifi") else { return }
        
        let json = JSON(wifi)
        
        let expirationTimestamp = TimeInterval(json["expiration"].int!)
        let expirationDate = Date(timeIntervalSince1970: expirationTimestamp)
        let bytesLeft = json["bytes_left"].float!
        
        if isWYGRealm() {
            
            if expirationDate.timeIntervalSinceNow > 0 {
                
                let attrs1 = [NSFontAttributeName : CurrentFont(fontSize: 20.0).currentFont, NSForegroundColorAttributeName : UIColor.brandColor()] as [String : Any]
                let attributedString1 = NSMutableAttributedString(string:String(format: QQLocalizedString("Valid till %@"), SubscriptionExpirationFormatter.string(from: expirationDate)), attributes:attrs1)
                labelSubscriptionInfo.attributedText = attributedString1
                
                
            } else {
                
                let attrs1 = [NSFontAttributeName : CurrentFont(fontSize: 20.0).currentFont, NSForegroundColorAttributeName : UIColor.brandColor()] as [String : Any]
                let attributedString1 = NSMutableAttributedString(string:QQLocalizedString("Expired"), attributes:attrs1)
                labelSubscriptionInfo.attributedText = attributedString1
            }
            
        } else {
            
            let attrs1 = [NSFontAttributeName : CurrentFont(fontSize: 25.0).currentFont, NSForegroundColorAttributeName : UIColor.brandColor()] as [String : Any]
            let attrs2 = [NSFontAttributeName : CurrentFont(fontSize: 17.0).currentFont, NSForegroundColorAttributeName : UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 1)] as [String : Any]
            let attributedString1 = NSMutableAttributedString(string:"\(Float(bytesLeft/(1024*1024))) ", attributes:attrs1)
            let attributedString2 = NSMutableAttributedString(string:QQLocalizedString("MB left"), attributes:attrs2)
            
            attributedString1.append(attributedString2)
            
            labelSubscriptionInfo.attributedText = attributedString1
            
        }
    }
    
    func inviteFriend () {
        
        let shareString = QQLocalizedString("Get free Internet access to million hotspots worldwide!")
        
        // Custom data
        
        let params = [
            "aita_wifi_token": UserDefaults.standard.object(forKey: AitaToken) as! String
        ]
        
        let tags = ["wifi"]
        let feature = "invite"
        let stage = "profile"
        
        guard let itemProvider = Branch.getActivityItem(withParams: params, feature: feature, stage: stage, tags: tags) else {return}
        
        let shareViewController = UIActivityViewController(activityItems: [shareString, itemProvider],
                                                           applicationActivities: nil)
        shareViewController.completionWithItemsHandler = { activityType, completed, returnedItems, activityError in
            if completed {
                GA.event("wifiProfile_invite_success", label: String(describing: activityType?._rawValue))
            } else {
                GA.event("wifiProfile_invite_faliure")
            }
        }
        
        present(shareViewController, animated: true, completion: nil)
    }
    
    func showProfile () {
        
        changeViewController(.profile)
    }
    
    func showCurrentPoints() {
        
        if let userCurrentPointsCount = UserDefaults.standard.object(forKey: "user_points") as? String {
            
            let attrs1 = [NSFontAttributeName : CurrentFont(fontSize: 25.0).currentFont, NSForegroundColorAttributeName : UIColor.brandColor()] as [String : Any]
            let attrs2 = [NSFontAttributeName : CurrentFont(fontSize: 17.0).currentFont, NSForegroundColorAttributeName : UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 1)] as [String : Any]
            let attributedString1 = NSMutableAttributedString(string:"\(userCurrentPointsCount) ", attributes:attrs1)
            let attributedString2 = NSMutableAttributedString(string:QQLocalizedString("points"), attributes:attrs2)
            
            attributedString1.append(attributedString2)
            
            labelpointsInfo.attributedText = attributedString1
            
        } else {
            
            let attrs1 = [NSFontAttributeName : CurrentFont(fontSize: 25.0).currentFont, NSForegroundColorAttributeName : UIColor.brandColor()] as [String : Any]
            let attrs2 = [NSFontAttributeName : CurrentFont(fontSize: 17.0).currentFont, NSForegroundColorAttributeName : UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 1)] as [String : Any]
            let attributedString1 = NSMutableAttributedString(string:"", attributes:attrs1)
            let attributedString2 = NSMutableAttributedString(string:QQLocalizedString("No points yet"), attributes:attrs2)
            
            attributedString1.append(attributedString2)
            
            labelpointsInfo.attributedText = attributedString1
            
        }
        
    }
    
    func drawCircle(view: UIView, radius: Int, color: UIColor, lineWidth: CGFloat) {
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: radius,y: radius), radius: view.bounds.size.width/2 - lineWidth, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = color.cgColor
        //you can change the line width
        shapeLayer.lineWidth = lineWidth
        
        view.layer.addSublayer(shapeLayer)
        
    }
    
    
    // MARK: - Protocols implementation -
    
    func changeViewController(_ menu: LeftMenuCollection) {
        switch menu {
        case .main:
            DispatchQueue.main.async(execute: {
                self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
            })
        case .invite:
            DispatchQueue.main.async(execute: {
                self.showInvite()
            })
            
        case .addWifi:
            addWiFiNetwork()
        case .promo:
            DispatchQueue.main.async(execute: {
                self.showInsertPromoCode()
            })
            
        case .subscribe:
            DispatchQueue.main.async(execute: {
                self.presentBuyScreen()
            })
            
        case .profile:
            
            self.slideMenuController()?.changeMainViewController(self.profileViewController, close: true)
            
        case .points:
            
            self.slideMenuController()?.changeMainViewController(self.pointsViewController, close: true)
            
        }
        
    }
    
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // Here is the deselection of the row
        collectionView.deselectItem(at: indexPath, animated: true)
        
        changeViewController(CollectionViewItems().actionsUnsubcribed[indexPath.row])
        slideMenuController()?.removeLeftGestures()
        GA.event(CollectionViewItems().menuDictUnsubscribed[indexPath.row]["GA.event"]!)
        collectionView.deselectItem(at: indexPath, animated: true)
        
        
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items

        return CollectionViewItems().menuDictUnsubscribed.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Here is the deselection of the row
        collectionView.deselectItem(at: indexPath, animated: true)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Item", for: indexPath) as! ProfileCollectionViewCell
        cell.menuItemDescription.font = CurrentFont(fontSize: 14.0).currentFont
        

        cell.itemImage.image =  UIImage(named: CollectionViewItems().menuDictUnsubscribed[indexPath.row]["imageName"]!)
        cell.menuItemDescription.text = QQLocalizedString(CollectionViewItems().menuDictUnsubscribed[indexPath.row]["menuItemDescription"]!)
        
        return cell
        
    }
}
