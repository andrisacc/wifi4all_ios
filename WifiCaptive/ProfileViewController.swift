//
//  ProfileViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 13.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
import Alamofire
//import HotspotHelper
import CocoaLumberjack

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SuccessHandling, ErrorHandling {
    
    // MARK: - Properties -
    
    var textFields = ["other", "name", "surname", "email", "phone"]
    
    // MARK: - IBOutlets -
    
    @IBAction func burgerAction(_ sender: UIButton) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func edittAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showEdit", sender: nil)
    }
    
    @IBOutlet weak var worldMapImage: UIImageView!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var privateInfoTableView: UITableView!
    
    
    
    @IBOutlet weak var circleOne: UIView!
    @IBOutlet weak var circleTwo: UIView!
    
    
    @IBAction func pointsButtonAction(_ sender: UIButton) {
        presentPointsScreen()
    }
    
    @IBAction func hotspotsButtonAction(_ sender: UIButton) {
        
        //        GA.event("ProfileViewController_showMyList")
        showList()
    }
    
    @IBAction func tapPointsCircle(_ sender: UITapGestureRecognizer) {
        presentPointsScreen()
    }
    
    @IBAction func tapHotspotsCircle(_ sender: UITapGestureRecognizer) {
        
        GA.event("ProfileViewController_showMyList")
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        showList()
    }
    
    
    
    // MARK: - VC load -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add notification to show the profile controller
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
        
        privateInfoTableView.tableFooterView = UIView()
        
        // Empty back button
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        // Table View Delegates
        privateInfoTableView.delegate = self
        privateInfoTableView.dataSource = self
        privateInfoTableView.showsVerticalScrollIndicator = false
        privateInfoTableView.allowsSelectionDuringEditing = false
        
        //        navigationController?.setNavigationBarHidden(true, animated: true)
        
        // Make a picture in a round
        self.picture.layer.cornerRadius = self.picture.frame.size.width / 2
        self.picture.clipsToBounds = true
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "threeDots"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ProfileViewController.presentLogout(_:)))
        self.navigationItem.rightBarButtonItem = rightButton
        
        navigationController?.topViewController?.title = QQLocalizedString("Profile")
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        // Lets draw the circled in views
        
        drawCircle(view: picture, radius: 50, color: UIColor(hexString: "#FFFFFF"), lineWidth: 2.5)
        
        
        drawFillCircle(view: circleOne, alpha: 0.45, radius: 65, strokeColor: UIColor.clear, fillColor: UIColor.white, lineWidth: 0.0)
        drawFillCircle(view: circleTwo, alpha: 0.15, radius: 80, strokeColor: UIColor.clear, fillColor: UIColor.white, lineWidth: 0.0)
        
        //        navigationController?.navigationBar.hidden = true
        
        DispatchQueue.main.async(execute: {
            self.worldMapImage.image = UIImage(named: "WorldMap")
        })
        
        self.setNavigationBarItem(false)
        
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        
        privateInfoTableView.reloadData()
        
        GA.screen("ProfileViewController")
        GA.event("ProfileViewController_saw")
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }
    
    
    // MARK: TEXTFIELDS
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        addRightBarButton()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let currentTag = textField.tag
        if let text = textField.text {
            self.textFields[currentTag] = text
        }
    }
    
    // MARK: - TABLE VIEW 2DELEGATES REALIZATION -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        var numebrOfSections: Int!
        numebrOfSections = 1
        
        return numebrOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        var numebrOfRows: Int!
        numebrOfRows = 5
        
        return numebrOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let data = UserDefaults.standard.object(forKey: "user") as? Data
        
        //        let bgColorView = UIView()
        if (indexPath.row == 0) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "pointsInfo", for: indexPath) as! ProfilePointsHotspotsTableViewCell
            
            cell.delegate = self
            
            cell.pointsButtonOutlet.titleLabel?.font = CurrentFont(fontSize: 15.0).currentFont
            cell.hotspotsButtonOutlet.titleLabel?.font = CurrentFont(fontSize: 15.0).currentFont
            
            // Localization of objects
            cell.hotspotsButtonOutlet.setTitle(QQLocalizedString("Added hotspots"), for: UIControlState())
            cell.pointsButtonOutlet.setTitle(QQLocalizedString("Points"), for: UIControlState())
            
            cell.pointsLabel.font = CurrentFont(fontSize: 42.0).currentFont
            cell.hotspotsLabel.font = CurrentFont(fontSize: 42.0).currentFont
            
            let tapOnPoints = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.presentPointsScreen))
            let tapOnHotspots = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.showList))
            
            tapOnPoints.numberOfTapsRequired = 1
            tapOnHotspots.numberOfTapsRequired = 1
            
            cell.pointsView.addGestureRecognizer(tapOnPoints)
            cell.hotspotsView.addGestureRecognizer(tapOnHotspots)
            
            if let userHotspotsCount = UserDefaults.standard.object(forKey: "user_hotspots") as? String {
                cell.hotspotsLabel.text = userHotspotsCount
            }
            
            if let userCurrentPointsCount = UserDefaults.standard.object(forKey: "user_points") as? String {
                cell.pointsLabel.text = userCurrentPointsCount
            }
            
            return cell
            
        } else if (indexPath.row == 1) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "name", for: indexPath) as! HotspotDetailsTableViewCell
            
            guard let _data = data else {return cell}
            let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
            guard let user = userUnwrapped else {return cell}
            let _user = JSON(user)
            
            cell.title.font = CurrentFont(fontSize: 17.0).currentFont
            
            cell.nameTextField.delegate = self
            cell.nameTextField.tag = 1
            cell.imageHere.image = UIImage(named: "UserName")
            cell.title.text = QQLocalizedString("NAME")
            
            if _user["firstname"].stringValue == "" && self.textFields[indexPath.row] == "name" {
                
                cell.nameTextField.text = ""
                self.textFields[indexPath.row] = cell.nameTextField.text!
                
                let attributes = [
                    NSForegroundColorAttributeName: UIColor.lightGray,
                    NSFontAttributeName : CurrentFont(fontSize: 17.0).currentFont
                    ] as [String : Any]
                cell.nameTextField.font = CurrentFont(fontSize: 17.0).currentFont
                cell.nameTextField.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("enter your name"), attributes:attributes)
                
            } else if _user["firstname"].stringValue == "" && self.textFields[indexPath.row] != "name"{
                
                cell.nameTextField.text = self.textFields[indexPath.row]
                self.textFields[indexPath.row] = cell.nameTextField.text!
                cell.nameTextField.font = CurrentFont(fontSize: 17.0).currentFont
                cell.nameTextField.textColor = UIColor.brandColor()
                
            } else if self.textFields[indexPath.row] != "name" {
                
                cell.nameTextField.text = self.textFields[indexPath.row]
                self.textFields[indexPath.row] = cell.nameTextField.text!
                cell.nameTextField.font = CurrentFont(fontSize: 17.0).currentFont
                cell.nameTextField.textColor = UIColor.brandColor()
                
            }
                
            else {
                
                cell.nameTextField.text = _user["firstname"].stringValue
                self.textFields[indexPath.row] = cell.nameTextField.text!
                cell.nameTextField.font = CurrentFont(fontSize: 17.0).currentFont
                cell.nameTextField.textColor = UIColor.brandColor()
                
            }
            
            return cell
            
        }
            
        else if (indexPath.row == 2) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "surname", for: indexPath) as! HotspotDetailsTableViewCell
            
            cell.surNameTextfield.tag = 2
            
            guard let _data = data else {return cell}
            let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
            guard let user = userUnwrapped else {return cell}
            let _user = JSON(user)
            
            cell.surNameTextfield.delegate = self
            cell.title.font = CurrentFont(fontSize: 17.0).currentFont
            
            
            cell.imageHere.image = UIImage(named: "UserName")
            cell.title.text = QQLocalizedString("SURNAME")
            
            if _user["surname"].stringValue == "" && self.textFields[indexPath.row] == "surname" {
                
                cell.surNameTextfield.text = ""
                self.textFields[indexPath.row] = cell.surNameTextfield.text!
                
                let attributes = [
                    NSForegroundColorAttributeName: UIColor.lightGray,
                    NSFontAttributeName : CurrentFont(fontSize: 17.0).currentFont
                    ] as [String : Any]
                cell.surNameTextfield.font = CurrentFont(fontSize: 17.0).currentFont
                cell.surNameTextfield.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("enter your surname"), attributes:attributes)
                
            } else if _user["surname"].stringValue == "" && self.textFields[indexPath.row] != "surname"{
                
                cell.surNameTextfield.text = self.textFields[indexPath.row]
                self.textFields[indexPath.row] = cell.surNameTextfield.text!
                cell.surNameTextfield.font = CurrentFont(fontSize: 17.0).currentFont
                cell.surNameTextfield.textColor = UIColor.brandColor()
                
            } else if self.textFields[indexPath.row] != "surname" {
                
                cell.surNameTextfield.text = self.textFields[indexPath.row]
                self.textFields[indexPath.row] = cell.surNameTextfield.text!
                cell.surNameTextfield.font = CurrentFont(fontSize: 17.0).currentFont
                cell.surNameTextfield.textColor = UIColor.brandColor()
                
            }
            else {
                
                cell.surNameTextfield.text = _user["surname"].stringValue
                self.textFields[indexPath.row] = cell.surNameTextfield.text!
                cell.surNameTextfield.font = CurrentFont(fontSize: 17.0).currentFont
                cell.surNameTextfield.textColor = UIColor.brandColor()
                
            }
            
            return cell
            
            
        } else if (indexPath.row == 3)  {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "email", for: indexPath) as! HotspotDetailsTableViewCell
            
            cell.emailTextField.tag = 3
            
            guard let _data = data else {return cell}
            let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
            guard let user = userUnwrapped else {return cell}
            let _user = JSON(user)
            
            cell.title.font = CurrentFont(fontSize: 17.0).currentFont
            
            cell.emailTextField.delegate = self
            cell.imageHere.image = UIImage(named: "Email")
            cell.title.text = QQLocalizedString("EMAIL")
            
            if _user["email"].stringValue == "" && self.textFields[indexPath.row] == "email" {
                
                cell.emailTextField.text = ""
                self.textFields[indexPath.row] = cell.emailTextField.text!
                
                let attributes = [
                    NSForegroundColorAttributeName: UIColor.lightGray,
                    NSFontAttributeName : CurrentFont(fontSize: 17.0).currentFont
                    ] as [String : Any]
                cell.emailTextField.font = CurrentFont(fontSize: 17.0).currentFont
                cell.emailTextField.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("enter your email"), attributes:attributes)
                
            } else if _user["email"].stringValue == "" && self.textFields[indexPath.row] != "email" {
                
                cell.emailTextField.text = self.textFields[indexPath.row]
                self.textFields[indexPath.row] = cell.emailTextField.text!
                cell.emailTextField.font = CurrentFont(fontSize: 17.0).currentFont
                cell.emailTextField.textColor = UIColor.brandColor()
                
            } else if self.textFields[indexPath.row] != "email" {
                
                cell.emailTextField.text = self.textFields[indexPath.row]
                self.textFields[indexPath.row] = cell.emailTextField.text!
                cell.emailTextField.font = CurrentFont(fontSize: 17.0).currentFont
                cell.emailTextField.textColor = UIColor.brandColor()
                
            }
            else {
                
                cell.emailTextField.text = _user["email"].stringValue
                self.textFields[indexPath.row] = cell.emailTextField.text!
                cell.emailTextField.font = CurrentFont(fontSize: 17.0).currentFont
                cell.emailTextField.textColor = UIColor.brandColor()
            }
            
            return cell
            
        } else if (indexPath.row == 4)  {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "phone", for: indexPath) as! HotspotDetailsTableViewCell
            
            guard let _data = data else {return cell}
            let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
            guard let user = userUnwrapped else {return cell}
            let _user = JSON(user)
            
            cell.title.font = CurrentFont(fontSize: 17.0).currentFont
            cell.value.font = CurrentFont(fontSize: 17.0).currentFont
            cell.value.textColor = UIColor.brandColor()
            cell.imageHere.image = UIImage(named: "Phone")
            cell.title.text = QQLocalizedString("PHONE NUMBER")
            
            if _user["phone"].stringValue == ""{
                
                cell.value.textColor = UIColor.lightGray
                cell.value.font = CurrentFont(fontSize: 17.0).currentFont
                cell.value.text = QQLocalizedString("enter your phone")
                
            } else {
                
                cell.value.text = _user["phone"].stringValue
                cell.value.font = CurrentFont(fontSize: 17.0).currentFont
                cell.value.textColor = UIColor.brandColor()
                
            }
            
            return cell
        }
            
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "name", for: indexPath)
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width, 0, 0)
            return cell
            
        }
        
    }
    
    
    // Setting up row heights
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height: CGFloat!
        
        if indexPath.row == 0 {
            height = 136.0
        } else {
            height = 80.0
        }
        
        return height
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
            
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            GA.event("profile_changePhone")
            changePhoneNumber()
        default: break
            
        }
        
    }
    
    // MARK: - Helpers -
    
    func openProfileVC () {
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork(self)
        }
    }
    
    func addRightBarButton () {
        
        let rightButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(ProfileViewController.changeProfile))
        self.navigationItem.rightBarButtonItem = rightButton
        
    }
    
    func changeRightBarButton () {
        
        //        self.navigationItem.rightBarButtonItem = nil
        self.privateInfoTableView.reloadData()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "threeDots"), style: .plain, target: self, action: #selector(ProfileViewController.presentLogout(_:)))
        self.navigationItem.rightBarButtonItem?.title = nil
    }
    
    
    func changeProfile () {
        
        let data = UserDefaults.standard.object(forKey: "user") as? Data
        guard let _data = data else {return }
        let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
        guard let user = userUnwrapped else {return}
        let _user = JSON(user)
        
        GA.event("Profile_save_changes_tap")
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        var name = _user["firstname"].stringValue
        var surname = _user["surname"].stringValue
        var email = _user["email"].stringValue
        
        if textFields[1] != "name" {
            name = textFields[1]
        }
        if textFields[2] != "surname" {
            surname = textFields[2]
        }
        if textFields[3] != "email" {
            email = textFields[3]
        }
        
        if isValidEmail(email) || email == "" {
            
            APIRequestHelper.changingProfile(firstName: name, surName: surname, email: email, country: nil, completion: {[unowned self] (success, errorString) in
                
                if success {
                    
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    self.privateInfoTableView.reloadData()
                    self.changeRightBarButton()
                    
                } else {
                    self.privateInfoTableView.reloadData()
                    self.changeRightBarButton()
                    guard let error = errorString else {return}
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    self.handleError(error: QQLocalizedString(error))
                }
            })
        } else {
            self.handleError(error: QQLocalizedString("Please enter valid email"))
            CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
        }
        
    }
    
    func presentPointsScreen() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let pointsViewController = storyboard.instantiateViewController(withIdentifier: "PointsViewController") as? PointsViewController else { return }
        pointsViewController.noLeftBarItem = true
        
        let navigationController = UINavigationController(rootViewController: pointsViewController)
        self.present(navigationController, animated: true, completion: nil)
        
        GA.event("wifiProfile_points_show_tap")
    }
    
    
    func showList() {
        
        GA.event("ProfileViewController_showMyList")
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        APIRequestHelper.getHotspotList {[unowned self] (data, errorString) in
            
            if data != nil {
                
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                guard let myListViewController: MyListViewController  = mainStoryboard.instantiateViewController(withIdentifier: "MyListViewController") as? MyListViewController else {return}
                myListViewController.hotspots = data
                let navigationController = UINavigationController(rootViewController: myListViewController)
                self.present(navigationController, animated: true, completion: nil)
                
                GA.event("ProfileViewController_showMyList_success")
                
            } else {
                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                GA.event("ProfileViewController_showMyList_failure")
                self.handleError(error: errorString ?? "error...")
            }
        }
    }
    
    func presentALertOnClearing() {
        
        let alertController = UIAlertController (title: QQLocalizedString("Thank you"), message: QQLocalizedString("Map cache has been successfully deleted"), preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: QQLocalizedString("Ok"), style: .default) { (_) -> Void in
        }
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentLogout(_ sender: UIBarButtonItem) {
        
        // ****** ---- ActionSheet ***** ------
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: QQLocalizedString("Logout"), style: .destructive , handler:{ (UIAlertAction)in
            self.logout()
        }))
        
        alert.addAction(UIAlertAction(title: QQLocalizedString("Dismiss"), style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            
            
        }))
        self.present(alert, animated: true, completion: {
            //            print("completion block")
        })
        
        
        // ****** ---- PopOver ***** ------
        
        
        //        guard let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil) else {return}
        //        guard let popViewController = storyboard.instantiateViewControllerWithIdentifier("PopOverViewController") as? PopOverViewController else { return }
        //
        //        //                purchaseViewController.navigationItem.leftBarButtonItem = nil
        //
        //        popViewController.preferredContentSize = CGSize(width: 200.0, height: 50.0)
        //        popViewController.mapCacheText = QQLocalizedString("Clear map cache")
        //        popViewController.logoutText = QQLocalizedString("Logout")
        //
        //
        //
        //
        //        let navigationController = UINavigationController(rootViewController: popViewController)
        //        navigationController.modalPresentationStyle = .Popover
        //
        //        let popOver = navigationController.popoverPresentationController
        //
        //        popOver?.delegate = self
        //        popOver!.sourceView = self.view
        //        popOver!.sourceRect = CGRectMake(self.view.bounds.size.width - 8.0, 0, 0,0)
        //        popOver!.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        //
        //
        //        self.presentViewController(navigationController, animated: true, completion: nil)
        
        
        // **** --- Simple alert *** -----*****
        //        let alertController = UIAlertController (title: QQLocalizedString("Are you sure you want to logout?"), message: /*QQLocalizedString("Once you are logged out you will lose opprotunity to seamlessy connect to our WiFi networks")*/nil, preferredStyle: .Alert)
        //
        //        let moneyAction = UIAlertAction(title: QQLocalizedString("Logout"), style: .Default) { (_) -> Void in
        //            self.logout()
        //        }
        //        alertController.addAction(moneyAction)
        //
        //        let pointsAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .Default) { (_) -> Void in
        //
        //        }
        //        alertController.addAction(pointsAction)
        //
        //        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func changePhoneNumber() {
        
        GA.event("EditProfile_changePhone")
        
        changePhone(viewController: self)

    }
    
    func logout () {
        
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        LoginHelper.logOut()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let loginView: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
        UIApplication.shared.keyWindow?.rootViewController = loginView
        
        
        GA.event("wifiProfile_logOut")
    }
    
    
    func showEditing(_ sender: UIBarButtonItem)
    {
        
        self.performSegue(withIdentifier: "showEdit", sender: nil)
        
        //        if(self.privateInfoTableView.editing == true)
        //        {
        ////            self.privateInfoTableView.editing = false
        //            self.privateInfoTableView.setEditing(false, animated: true)
        ////            self.navigationItem.rightBarButtonItem?.title = "Done"
        //            self.navigationItem.rightBarButtonItem?.image = UIImage(named: "edit")
        //            self.navigationItem.rightBarButtonItem?.title = nil
        //        }
        //        else
        //        {
        //            self.privateInfoTableView.setEditing(true, animated: true)
        ////            self.navigationItem.rightBarButtonItem?.title = "Edit"
        //            self.navigationItem.rightBarButtonItem?.image = nil
        //            self.navigationItem.rightBarButtonItem?.title = QQLocalizedString("Done")
        //        }
    }
    
    func closeEditing() {
        
        self.privateInfoTableView.setEditing(false, animated: true)
        //            self.navigationItem.rightBarButtonItem?.title = "Done"
        self.navigationItem.rightBarButtonItem?.image = UIImage(named: "threeDots")
        self.navigationItem.rightBarButtonItem?.title = nil
        
    }

}

extension ProfileViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

extension ProfileViewController: PointsHotspotsCellViewDelegate {
    
    
    func presentPointsDelegateScreen() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let pointsViewController = storyboard.instantiateViewController(withIdentifier: "PointsViewController") as? PointsViewController else { return }
                
        pointsViewController.noLeftBarItem = true
        
        let navigationController = UINavigationController(rootViewController: pointsViewController)
        self.present(navigationController, animated: true, completion: nil)
        
        GA.event("wifiProfile_points_show_tap")
        
    }
    
    func presentHotspotsDelegateScreen() {
        
        showList()
    }
}

