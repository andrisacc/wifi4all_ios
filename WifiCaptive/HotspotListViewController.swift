//
//  HotspotListViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 12/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import UIKit
//import HotspotHelper

class HotspotListViewController: UITableViewController, CalloutViewDelegate, UIGestureRecognizerDelegate {
    
    var annotations = [WifiHotspot]()
    var selectedPoint = IndexPath()

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return annotations.count
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add notification to show the profile controller
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
        
        
        // Empty back button
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        GA.screen("wifiList")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }
    
    func openProfileVC () {
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork(self)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HotspotCell", for: indexPath) as! HotspotListTableViewCell
//        let v = CalloutView(frame: CGRectMake(0, 0, self.view.frame.width, 100))
//        let point = annotations[indexPath.row]
//        v.descField.text = point.name
//        v.point = point
//        v.removeGestureRecognizer((v.gestureRecognizers?.first)!)
//        cell.contentView.addSubview(v)
        let point = annotations[indexPath.row]
        cell.point = point
        cell.networkName.text = point.name
        
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        // Here is the deselection of the row
        tableView.deselectRow(at: indexPath, animated: true)
        
        // Here put you code...
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let viewController = segue.destination as! HotspotDetailsViewController
        viewController.title = QQLocalizedString("Hotspot Details")
        viewController.point = annotations[(self.tableView.indexPathForSelectedRow?.row)!]
        
        GA.event("wifiList_chooseHotspot")
    }
    
    
    func NavigateToHotspotDetails() {
        self.performSegue(withIdentifier: "DetailsHotspot", sender: self)
    }
    func showPopUp() {
    }
    func showPopUpDirections() {
    }
    func showAddPhone() {
    }

}
