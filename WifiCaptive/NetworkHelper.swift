////
////  NetworkHelper.swift
////  WifiHotspot
////
////  Created by Sergey Pronin on 8/4/15.
////  Copyright © 2015 App in the Air. All rights reserved.
////
//
//import Foundation
//import UIKit
//import NetworkExtension
//import CoreLocation
//import SystemConfiguration.CaptiveNetwork
//import CocoaLumberjack
//
//extension NSMutableURLRequest {
//    func addAitaAuth() {
//        if let aitaToken = NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String {
//            self.setValue(aitaToken, forHTTPHeaderField: "WiFi-User-Token")
//        } else if let userID = NSUserDefaults.standardUserDefaults().objectForKey(DeviceID) as? String {
//            self.setValue(userID, forHTTPHeaderField: "Aita-User")
//        }
//    }
//}
//
//// Send current connection status to server
//
//func reportCurrentSessionStatus(completion: (Bool -> Void)? = nil) {
//    
//    let defaults = NSUserDefaults.standardUserDefaults()
//    
//    guard defaults.objectForKey(AitaToken) != nil || defaults.objectForKey(DeviceID) != nil else { completion?(false); return }
//    guard let ssid = NetworkHelper.instance.currentSSID else { completion?(false); return }
//    
//    let request = NSMutableURLRequest(URL: NSURL(string: BaseURL + "wifi/sessions")!)
//    request.addAitaAuth()
//    request.HTTPMethod = "POST"
//    var params = [
//        "data_used": DataUsageMonitoring.currentWifiDataUsage().description,
//        "ssid": ssid
//    ]
//    if let location = NetworkHelper.instance.locationManager?.location {
//        params["lat"] = location.coordinate.latitude.description
//        params["lon"] = location.coordinate.longitude.description
//    }
//    
//    request.HTTPBody = params.URLEncodedString!.dataUsingEncoding(NSUTF8StringEncoding)
//    NetworkHelperSession.dataTaskWithRequest(request) { data, response, error in
//        completion?(error == nil)
//        }.resume()
//}
//
//public func shareCurrentLocationInBackGround () -> /*(lat: Double, lon: Double )*/CLLocation {
//    
//    var locationShare = CLLocation()
//    
//    if let location = NetworkHelper.instance.locationManager?.location {
//        locationShare = location
//    }
//    //return (locationShare.coordinate.latitude, locationShare.coordinate.longitude)
//    return locationShare
//    
//}
//
//public var currentSSID: String? {
//get {
//    return NetworkHelper.instance.currentSSID
//}
//}
//
//@available(iOS 9.0, *)
//public var currentNetwork: NEHotspotNetwork? {
//    return (NEHotspotHelper.supportedNetworkInterfaces() as? [NEHotspotNetwork])?.first
//}
//
//public func provideRemoteNotificationsToken(token: NSData) {
//    //TODO: is it needed?
//    //    var pushToken = token.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
//    //    pushToken = pushToken.stringByReplacingOccurrencesOfString(" ", withString: "")
//    //
//    //    let defaults = NSUserDefaults.standardUserDefaults()
//    //    defaults.setObject(pushToken, forKey: PushToken)
//    //    defaults.synchronize()
//    //
//    //    guard let aitaToken = NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String else { return }
//    //    guard let deviceID = NSUserDefaults.standardUserDefaults().objectForKey(DeviceID) as? String else { return }
//    //    let request = NSMutableURLRequest(URL: NSURL(string: BaseURL + "user/device")!)
//    //    request.HTTPMethod = "POST"
//    //    request.setValue(pushToken, forHTTPHeaderField: "Aita-Apns-Token")
//    //    request.setValue(aitaToken, forHTTPHeaderField: "Aita-Token")
//    //    request.setValue(deviceID, forHTTPHeaderField: "Aita-User")
//    //    request.setValue("wifi", forHTTPHeaderField: "Aita-App-Name")
//    //    request.setValue(NSLocale.preferredLanguages().first ?? "en", forHTTPHeaderField: "Aita-Device-Language")
//    //    request.setValue(DataUsageMonitoring.deviceName(), forHTTPHeaderField: "Aita-Device-Name")
//    //
//    //    NetworkHelperSession.dataTaskWithRequest(request).resume()
//}
//
//public let AitaToken = "HotspotAitaToken"
//public let PushToken = "HotspotPushToken"
//public let DeviceID = "HotspotDeviceID"
//
//let NetworkHelperSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
//
////FIXME: internal
//public class NetworkHelper: NSObject {
//    
//    //Notification for plushki on adding of free hotspot
//    
//    var plushkiNotificationMade = NSUserDefaults.standardUserDefaults().boolForKey("plushkiNotification")
//    
//    
//    var currentLocation : CLLocation!
//    var testCurrentLocation: CLLocation!
//    
//    //FIXME: internal
//    public static let instance = NetworkHelper()
//    var helperName: String?
//    var captiveController: CaptiveWebViewController?
//    
//    //FIXME: internal
//    public var shouldHandleWebViewRequests = false
//    
//    //workaround for 8.0 and 9.0 compatibility
//    private var _command: AnyObject?
//    
//    @available(iOS 9.0, *)
//    var lastCommand: NEHotspotHelperCommand? {
//        get { return _command as? NEHotspotHelperCommand }
//        set { _command = newValue }
//    }
//    
//    var active = false
//    var currentConfiguration: WifiConfiguration?
//    
//    //iOS 8
//    private var reachabilityConnection: Reachability!
//    private var reachabilityWifi: Reachability!
//    private var locationManager: CLLocationManager!
//    private var timer: NSTimer!
//    
//    deinit {
//        NSNotificationCenter.defaultCenter().removeObserver(self)
//    }
//    
//    private override init() {
//        
//        super.init()
//        
//        if #available(iOS 9.0, *) {
//            //            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NetworkHelper.powerModeChangedNotification), name: NSProcessInfoPowerStateDidChangeNotification, object: nil)
//            //        } else {
//            //            NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(NetworkHelper.reachabilityChangedNotification(_:)),
//            //                name: kReachabilityChangedNotification, object: nil)
//            
//            //            //TODO:
//            //            reachabilityConnection = Reachability.reachabilityForInternetConnection()
//            //            reachabilityConnection.startNotifier()
//            //
//            //            reachabilityWifi = Reachability.reachabilityForLocalWiFi()
//            //            reachabilityWifi.startNotifier()
//            
//            locationManager = CLLocationManager()
//            locationManager.delegate = self
//            locationManager.distanceFilter = 300
//            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
//            locationManager.pausesLocationUpdatesAutomatically = false
//            if #available(iOS 9.0, *) {
//                locationManager.allowsBackgroundLocationUpdates = true
//            }
//            if CLLocationManager.authorizationStatus() == .NotDetermined {
//                locationManager.requestAlwaysAuthorization()
//            }
//            
//            //            timer = NSTimer(timeInterval: 20, target: self, selector: #selector(NetworkHelper.updateBackgroundTask), userInfo: nil, repeats: true)
//            //            timer.tolerance = 5
//            
//            //            prepareCaptiveConfiguration()
//        }
//        
//        requestNotificationsAccess()
//    }
//    
//    private var connectionHelper: HotspotConnectionHelper?
//    
//    @available(iOS 9.0, *)
//    public var currentNetwork: NEHotspotNetwork? {
//        return (NEHotspotHelper.supportedNetworkInterfaces() as? [NEHotspotNetwork])?.first
//    }
//    
//    // Secured or not
//    
//    var secured: Bool {
//        get {
//            if #available(iOS 9.0, *) {
//                let interfaces = NEHotspotHelper.supportedNetworkInterfaces() as! [NEHotspotNetwork]
//                if let wifiNetwork = interfaces.first {
//                    return wifiNetwork.secure
//                } else {
//                    return false
//                }
//            }
//            else {
//                return false
//            }
//        }
//    }
//    
//    // Here we got SSID of the Network connected
//    var currentSSID: String? {
//        get {
//            if #available(iOS 9.0, *) {
//                let interfaces = NEHotspotHelper.supportedNetworkInterfaces() as! [NEHotspotNetwork]
//                if let wifiNetwork = interfaces.first {
//                    return wifiNetwork.SSID
//                } else {
//                    return nil
//                }
//            } else {
//                guard CNCopySupportedInterfaces() != nil else { return nil }
//                
//                if let currentWiFi = CNCopyCurrentNetworkInfo("en0" as CFString) {
//                    return (currentWiFi as NSDictionary)["SSID"] as? String
//                } else {
//                    return nil
//                }
//            }
//        }
//    }
//    
//    func shutdown() {
//        if #available(iOS 9.0, *) {
//            NEHotspotHelper.registerWithOptions(nil, queue: dispatch_get_main_queue()) { cmd in
//                switch cmd.commandType {
//                case .FilterScanList:
//                    let response = cmd.createResponse(.Success)
//                    response.setNetworkList([])
//                    response.deliver()
//                default:
//                    break
//                }
//            }
//        } else {
//            CNSetSupportedSSIDs([])
//        }
//    }
//    
//    
//    
//    func prepareCaptiveConfiguration() {
//        let captiveNetworks = WifiConfiguration.allSSIDs().map({ $0 as CFTypeRef })
//        let res = CNSetSupportedSSIDs(captiveNetworks)
//        //        DDLogDebug("\(captiveNetworks)")
//        DDLogDebug("\(res)")
//    }
//    
//    @available(iOS 9.0, *)
//    
//    func setupNetworkExtension() {
//        
//        //        DDLogDebug("NEHotspotHelper.supportedNetworkInterfaces(): \(NEHotspotHelper.supportedNetworkInterfaces())")
//        
//        // Here we realize that name of the heleper will be displayed properly
//        var options: [String: String]?
//        if let helperName = helperName {
//            options = [kNEHotspotHelperOptionDisplayName: helperName]
//        }
//        
//        // Here we call registerWithOptions to register the application as a HotspotHelper
//        
//        let qos = Int(QOS_CLASS_USER_INITIATED.rawValue)
//        
//        let res = NEHotspotHelper.registerWithOptions(options, queue:dispatch_get_global_queue(qos, 0)) { cmd in
//            
//            guard self.active else {
//                let response = cmd.createResponse(.Failure)
//                response.deliver()
//                return
//                
//            }
//            
//            switch cmd.commandType {
//                
//            case .FilterScanList:
//                
//                guard let list = cmd.networkList else {
//                    let response = cmd.createResponse(.Failure)
//                    response.deliver()
//                    break
//                }
//                
//                var matchedNetworks = [NEHotspotNetwork]()
//                
//                DDLogDebug("Has valid credentials: \(HotspotCredentials.hasValidCredentials())")
//                
//                for network in list {
//                    
//                    if let config = WifiConfiguration.configurationForNetworkWithSSID(network.SSID) {
//                        
//                        if !config.paid /*|| HotspotCredentials.hasValidCredentials()*/ {
//                        
//                            network.setConfidence(.High)
//                            dispatch_async(dispatch_get_main_queue()) {
//                                if let password = config.params["hotspot_password"].string {
//                                    network.setPassword(password)
//                                    //                                    DDLogDebug("Config: \(config.params)")
//                                }
//                            }
//                            matchedNetworks.append(network)
//                        }
//                        else if config.paid {
//                            network.setConfidence(.High)
//                            matchedNetworks.append(network)
//                        }
//                    } else {
//                        network.setConfidence(.None) // It is better to set something here
//                    }
//                }
//                
//                // Notification for motivation to add the hotspot which user connected to
//                
//                let currentNetSSID = self.currentSSID ?? ""
//                if self.secured {
//                    self.notifyOnConnectionWOAdd(true, currentSSID: currentNetSSID)
//                }
//                
////                Logger.logMessage("FilterScanList \(matchedNetworks.count) out of \(list.count)")
////                for network in matchedNetworks {
////                    DDLogDebug("Matched Networks: \(network.SSID)")
////                }
//                DDLogDebug("FilterScanList \(matchedNetworks.count) out of \(list.count)")
//                
//                let response = cmd.createResponse(.Success)
//                response.setNetworkList(matchedNetworks)
//                response.deliver()
//                
//                DDLogDebug("HereWeOnFilterScanList")
//                
//            case .Evaluate:
//                
//                Logger.logMessage("evaluate \(cmd.network)")
//                DDLogDebug("evaluate \(cmd.network)")
//                // TODO: - TO BE CHECKED AFTER SWIFT 3 MIGRATION -
//                guard let network = cmd.network, WifiConfiguration.configurationForNetworkWithSSID(network.SSID) != nil
//                    else {cmd.createResponse(.Failure).deliver(); break }
//                
//                self.currentConfiguration = nil
//                
//                if let config = WifiConfiguration.configurationForNetworkWithSSID(network.SSID) {
//                    if !config.paid /*|| HotspotCredentials.hasValidCredentials()*/ {
//                        network.setConfidence(.High)
//                        dispatch_async(dispatch_get_main_queue()) {
//                            if let password = config.params["hotspot_password"].string {
//                                network.setPassword(password)
//                            }
//                        } 
//                    }
//                    else if config.paid {
//                        network.setConfidence(.High)
//                    }
//                } else {
//                    network.setConfidence(.None)
//                }
//                
//                let response = cmd.createResponse(.Success)
//                response.setNetwork(network)
//                response.deliver()
//                
//                DDLogDebug("HereWeOnEvaluate")
//                
//            case .Authenticate:
//                
//                Logger.logMessage("authenticate \(cmd)")
//                DDLogDebug("authenticate \(cmd)")
//                
//                guard let network = cmd.network, let config = WifiConfiguration.configurationForNetworkWithSSID(network.SSID)
//                    else {cmd.createResponse(.Failure).deliver(); break }
//                
//                if config.paid && !HotspotCredentials.hasValidCredentials() {
//                    cmd.createResponse(.Failure).deliver()
//                    return
//                }
//                
//                self.lastCommand = nil
//                self.dismissCaptiveUI()
//                
//                self.currentConfiguration = config
//                
//                if config.params["hotspot_password"].string != nil {
//                    
//                    Logger.logMessage("hotspot_password network, deliver success")
//                    DDLogDebug("hotspot_password network, deliver success")
//                    cmd.createResponse(.Success).deliver()
//                    
////                    DDLogDebug("Config on Auth: \(config.params, config.id)")
//                    
//                    
////                    let currentCoords = shareCurrentLocationInBackGround()
////                    self.connectionHelper = HotspotConnectionHelper(command: cmd, configuration: config)
////                    self.connectionHelper!.checkConnectionReachability { success in
////                        
////                        if success {
////                            
////                            DDLogDebug("Authenticate Success")
////                            AccessLogger.sharedInstance.successAccess(pointId: config.id, latitude: currentCoords.coordinate.latitude, longitude: currentCoords.coordinate.longitude)
////                            
////                        } else {
////                            
////                            DDLogDebug("Authenticate NO connection reachablity...")
////                            
////                            let point = failedHotspot(pointID: config.id, latitude: currentCoords.coordinate.latitude, longitude: currentCoords.coordinate.longitude)
////                            
////                            DDLogDebug("Point: \(point.point)")
////                            
////                            if let points = NSUserDefaults.standardUserDefaults().objectForKey("failedHotspots") as? NSData {
////                                let pointz = NSKeyedUnarchiver.unarchiveObjectWithData(points) as! NSMutableArray
////                                pointz.addObject(point)
////                                let data = NSKeyedArchiver.archivedDataWithRootObject(pointz)
////                                NSUserDefaults.standardUserDefaults().setObject(data, forKey: "failedHotspots")
////                            } else {
////                                let points = NSMutableArray()
////                                points.addObject(point)
////                                let data = NSKeyedArchiver.archivedDataWithRootObject(points)
////                                NSUserDefaults.standardUserDefaults().setObject(data, forKey: "failedHotspots")
////                            }
////                        }
////                    }
//                    
//                } else if HotspotCredentials.credentialsAcquired {
//                    
//                    Logger.logMessage("gonna connect")
//                    DDLogDebug("gonna connect")
//                    self.connectionHelper = HotspotConnectionHelper(command: cmd, configuration: config)
//                    self.connectionHelper!.connect()
//                    
//                } else {
//                    Logger.logMessage("no credentials")
//                    DDLogDebug("no credentials")
//                    self.sendNeedsPaymentNotification()
//                    cmd.createResponse(.Failure).deliver()
//                }
//                
//                DDLogDebug("HereWeOnAuthenticate")
//                
//            case .PresentUI:
//                
//                guard let network = cmd.network, let config = WifiConfiguration.configurationForNetworkWithSSID(network.SSID)
//                    else { cmd.createResponse(.Failure).deliver(); break }
//                
//                Logger.logMessage("present ui for config \(config.params.rawValue)")
//                //                    DDLogDebug("present ui for config \(config.params.rawValue)")
//                
//                self.lastCommand = cmd
//                self.presentCaptiveUI(config)
//                
//                DDLogDebug("HereWeOnPresentUI")
//                
//            case .Maintain:
//                
//                guard let network = cmd.network, let config = WifiConfiguration.configurationForNetworkWithSSID(network.SSID)
//                    else { cmd.createResponse(.Failure).deliver(); break }
//                self.connectionHelper = HotspotConnectionHelper(command: cmd, configuration: config)
//                self.connectionHelper!.checkConnectionReachability { success in
//                    if success {
//                        cmd.createResponse(.Success).deliver()
//                    } else {
//                        cmd.createResponse(.AuthenticationRequired).deliver()
//                    }
//                }
//                DDLogDebug("HereWeOnMaintain")
//                
//            default:
//                self.currentConfiguration = nil
//                DDLogDebug("default \(cmd)")
//                break
//            }
//        }
//        NSLog("res \(res)")
//        DDLogDebug("res \(res)")
//    }
//    
//    
//    func checkConnectionStatus() {
//        
//        guard let ssid = currentSSID, let config = WifiConfiguration.configurationForNetworkWithSSID(ssid) else { return }
//        
//        connectionHelper = HotspotConnectionHelper(configuration: config)
//        connectionHelper!.connect()
//        
//    }
//    
//    func presentCaptiveUI(configuration: WifiConfiguration) {
//        guard let window = UIApplication.sharedApplication().windows.first else { return }
//        
//        shouldHandleWebViewRequests = true
//        captiveController = CaptiveWebViewController()
//        if #available(iOS 9, *) {
//            captiveController!.command = lastCommand
//        }
//        captiveController!.wifiConfiguration = configuration
//        captiveController!.delegate = self
//        
//        window.rootViewController = UINavigationController(rootViewController: self.captiveController!)
//        
//        //        window.rootViewController?.presentViewController(,
//        //            animated: false, completion: nil)
//    }
//    
//    func dismissCaptiveUI() {
//        shouldHandleWebViewRequests = false
//        captiveController?.navigationController?.presentingViewController?.dismissViewControllerAnimated(false, completion: nil)
//        captiveController = nil
//    }
//    
//    func updateBackgroundTask() {
//        var task: UIBackgroundTaskIdentifier?
//        task = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler {
//            if let task = task {
//                UIApplication.sharedApplication().endBackgroundTask(task)
//            }
//        }
//    }
//    
//    //MARK: User Notifications
//    private func requestNotificationsAccess() {
//        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Sound], categories: nil)
//        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
//    }
//    
//    private func sendNeedsPaymentNotification() {
//        
//        if let aitaToken = NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String {
//            Logger.logMessage("token \(aitaToken)")
//        }
//        
//        Logger.flushMessages()
//        
//        dispatch_async(dispatch_get_main_queue()) {
//            let notification = UILocalNotification()
//            notification.soundName = UILocalNotificationDefaultSoundName
//            notification.fireDate = NSDate(timeIntervalSinceNow: 1)
//            notification.userInfo = [
//                HotspotHelperPaymentNeededNotification: true,
//            ]
//            notification.alertBody = NSLocalizedString("You need to have active subscription in order to connect.", comment: "")
//            if #available(iOS 8.2, *) {
//                notification.alertTitle = NSLocalizedString("Subscribe", comment: "")
//            }
//            UIApplication.sharedApplication().scheduleLocalNotification(notification)
//        }
//    }
//    
//    private func sendLocalNotification(title: String, body: String) {
//        
//        dispatch_async(dispatch_get_main_queue()) {
//            let notification = UILocalNotification()
//            notification.soundName = UILocalNotificationDefaultSoundName
//            notification.fireDate = NSDate(timeIntervalSinceNow: 1)
//            notification.userInfo = [
//                HotspotHelperPaymentNeededNotification: true,
//            ]
//            notification.alertBody = body
//            if #available(iOS 8.2, *) {
//                notification.alertTitle = title
//            }
//            UIApplication.sharedApplication().scheduleLocalNotification(notification)
//        }
//    }
//    
//    // MARK: NSNotificationCenter
//    func reachabilityChangedNotification(notification: NSNotification) {
//        
//        let networkReachability = notification.object as! Reachability;
//        let remoteHostStatus = networkReachability.currentReachabilityStatus
//        
//        switch remoteHostStatus {
//        case .NotReachable:
//            DDLogDebug("Not Reachable")
//            dismissCaptiveUI()
//        case .ReachableViaWiFi:
//            DDLogDebug("Reachable Wifi")
//            checkConnectionStatus()
//        default:
//            DDLogDebug("Reachable")
//        }
//    }
//    
//    @available(iOS 9.0, *)
//    func powerModeChangedNotification() {
//        SQLQueue.maxConcurrentOperationCount = NSProcessInfo.processInfo().lowPowerModeEnabled ? 2 : 10
//    }
//    
//    //MARK: Setup
//    public class func sendLogsIfAny() {
//        Logger.sendLogs()
//    }
//    
//    public class func setServerURL(url: String) {
//        BaseURL = url
//        BaseURLInsecured = url.stringByReplacingOccurrencesOfString("https", withString: "http")
//    }
//    
//    public class func setupHotspotsMonitoring(token token: String? = nil, userID: String? = nil, helperName: String? = nil) {
//        
////        DDLogDebug("Here is token before I try to save it: \(NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String)")
//        
//        let defaults = NSUserDefaults.standardUserDefaults()
//        if let token = token {
//            DDLogDebug("I'm here now starting to save the token")
//            defaults.setObject(token, forKey: AitaToken)
//        } else if let userID = userID {
//            defaults.setObject(userID, forKey: DeviceID)
//        }
////        else {
////            return
////        }
//        
//        defaults.synchronize()
//        
////        DDLogDebug("Here is saved token after login: \(NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String)")
//        
//        NetworkHelper.instance.active = true
//        NetworkHelper.instance.helperName = helperName
//        if #available(iOS 9.0, *) {
//            NetworkHelper.instance.setupNetworkExtension()
//        } else {
//            NetworkHelper.instance.checkConnectionStatus()
//        }
//        
//        // HotspotCredentials.updateCredentials()
//        HotspotCredentials.updateRulesDB()
//        
//        WifiConfiguration.fetchWifiConfiguration()
//        
//        NSURLProtocol.registerClass(CaptiveURLProtocol.self)
//        
//        UIApplication.sharedApplication().registerForRemoteNotifications()
//    }
//    
//    public class func shutdownHotspotsMonitoring(helperName: String? = nil) {
//        
//        let defaults = NSUserDefaults.standardUserDefaults()
//        defaults.removeObjectForKey(AitaToken)
//        defaults.removeObjectForKey(DeviceID)
//        
//        defaults.synchronize()
//        
//        HotspotCredentials.username = ""
//        HotspotCredentials.password = ""
//        
//        // Clean up keychain
//        
//        HotspotKeychain["username"] = nil
//        HotspotKeychain["password"] = nil
//        HotspotKeychain["expiration"] = nil
//        
//        //        DDLogDebug("HotspotKeychain[username]: \(HotspotKeychain["username"])")
//        //        DDLogDebug("HotspotKeychain[password]: \(HotspotKeychain["password"])")
//        //        DDLogDebug("HotspotKeychain[expiration]: \(HotspotKeychain["password"])")
//        
//        
//        NetworkHelper.instance.active = false
//        NetworkHelper.instance.helperName = helperName
//        if #available(iOS 9.0, *) {
//            NetworkHelper.instance.setupNetworkExtension()
//        } else {
//            NetworkHelper.instance.checkConnectionStatus()
//        }
//    }
//    
//    public class func processLocalNotification(notification: UILocalNotification) -> Bool {
//        
//        guard let userInfo = notification.userInfo else { return false }
//        
//        if userInfo[HotspotHelperPresentUINotification] as? Bool == true {
//            if #available(iOS 9.0, *) { }
//            else {
//                NetworkHelper.instance.presentCaptiveUI(WifiConfiguration(json: JSON(userInfo[HotspotHelperConfigurationKey]!)))
//            }
//            return true
//        } else {
//            return false
//        }
//    }
//    
//    public class func handleRemoteNotification(userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
//        reportCurrentSessionStatus { completionHandler( $0 ? .NewData : .Failed ) }
//    }
//    
//    
//    private func notifyOnConnectionWOAdd (firstAppearemce: Bool, currentSSID: String) {
//        
//        if !self.plushkiNotificationMade {
//        
//            self.plushkiNotificationMade = firstAppearemce
//            
//            let timeInterval = NSDate().timeIntervalSince1970
//            NSUserDefaults.standardUserDefaults().setObject(timeInterval, forKey: "PlushkaNotificationStamp")
//            NSUserDefaults.standardUserDefaults().setObject(true, forKey: "plushkiNotification")
//            
//            if let configForSSID = WifiConfiguration.configurationForNetworkWithSSID(currentSSID) {
//                // Do nothing here
//            } else {
//                if self.currentSSID == "" {
//                    self.sendLocalNotification(NSLocalizedString("Hey!", comment: ""), body: NSLocalizedString("Connect to the secured WiFi, add it and get access to > 500 000 hotspots in the world", comment: ""))
//                } else  {
//                    self.sendLocalNotification(NSLocalizedString("Please add the network", comment: ""), body: NSLocalizedString("Please share your network with others to make the world a bit better", comment: ""))
//                }
//            }
//            
//        } else {
//            
//            let currentUnixTime = NSDate().timeIntervalSince1970
//            guard let lastTimeStamp = NSUserDefaults.standardUserDefaults().objectForKey("PlushkaNotificationStamp") as? NSTimeInterval else {return}
//            let timePasseSinceLastNPlushka = currentUnixTime - lastTimeStamp
//            
//            if  timePasseSinceLastNPlushka > 604800 {
//                
//                let timeInterval = NSDate().timeIntervalSince1970
//                
//                if let configForSSID = WifiConfiguration.configurationForNetworkWithSSID(currentSSID) {
//                    // Do nothing here
//                } else {
//                    if self.currentSSID == "" {
//                        // Do nothing here
//                        self.sendLocalNotification(NSLocalizedString("Hey!", comment: ""), body: NSLocalizedString("Connect to the secured WiFi, add it and get access to > 500 000 hotspots in the world", comment: ""))
//                        NSUserDefaults.standardUserDefaults().setObject(timeInterval, forKey: "PlushkaNotificationStamp")
//                    } else  {
//                        self.sendLocalNotification(NSLocalizedString("Please add the network", comment: ""), body: NSLocalizedString("Please share your network with others to make the world a bit better", comment: ""))
//                        NSUserDefaults.standardUserDefaults().setObject(timeInterval, forKey: "PlushkaNotificationStamp")
//                    }
//                }
//            }
//        }
//    }
//}
//
//
////MARK: - CLLocationManagerDelegate
//extension NetworkHelper: CLLocationManagerDelegate {
//    public func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
//        if status == .AuthorizedAlways {
//            locationManager.startUpdatingLocation()
//        }
//    }
//    
//    public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let location = locations.last else { return }
//        
//        //        DDLogDebug("location updated \(location.coordinate.latitude),\(location.coordinate.longitude)")
//        locationManager.stopUpdatingLocation()
//    }
//}
//
//
////MARK: - CaptiveWebViewControllerDelegate
//extension NetworkHelper: CaptiveWebViewControllerDelegate {
//    
//    func captiveWebViewControllerDidSubmitForm(controller: CaptiveWebViewController, withConfiguration configuration: [String : AnyObject]?, success: Bool) {
//        
//        var config = configuration
//        
//        shouldHandleWebViewRequests = false
//        if #available(iOS 9.0, *) {
//            lastCommand = nil
//        }
//        
//        connectionHelper!.checkConnectionReachability { reachable in
//            guard let window = UIApplication.sharedApplication().windows.first else { return }
//            let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//            window.rootViewController = storyboard.instantiateInitialViewController()
//            
//            let message: String
//            
//            if reachable {
//                message = NSLocalizedString("You were successfully authenticated in the network", comment: "")
//                if #available(iOS 9.0, *) {
//                    controller.command.createResponse(.Success).deliver()
//                } else {
//                    CNMarkPortalOnline("en0")
//                }
//                if config != nil {
//                    config!["id"] = self.connectionHelper!.config.id
//                    WifiConfiguration.updateConfiguration(config!)
//                }
//                
//                reportCurrentSessionStatus()
//                
//            } else {
//                message = NSLocalizedString("It was impossible to authorize you in the network", comment: "")
//                if #available(iOS 9.0, *) {
//                    controller.command.createResponse(.Failure).deliver()
//                } else {
//                    CNMarkPortalOffline("en0")
//                }
//            }
//            
//            let alertController = UIAlertController(title: NSLocalizedString("Information", comment: ""), message: message, preferredStyle: .Alert)
//            alertController.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
//            window.rootViewController?.presentViewController(
//                alertController, animated: true, completion: nil)
//        }
//    }
//    
//    func captiveWebViewControllerDidCancel(controller: CaptiveWebViewController) {
//        shouldHandleWebViewRequests = false
//        if #available(iOS 9.0, *) {
//            lastCommand = nil
//        }
//        
//        if #available(iOS 9.0, *) {
//            controller.command.createResponse(.Failure).deliver()
//        } else {
//            CNMarkPortalOffline("en0")
//        }
//        
//        guard let window = UIApplication.sharedApplication().windows.first else { return }
//        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//        window.rootViewController = storyboard.instantiateInitialViewController()
//    }
//    
//    func captiveWebViewControllerDidFindSuccessPage(controller: CaptiveWebViewController) {
//        captiveWebViewControllerDidSubmitForm(controller, withConfiguration: nil, success: true)
//    }
//    
//    
//}
////
////extension NetworkHelper: LocationUpdateProtocol {
////
////    // MARK: - LocationUpdateProtocol
////    public func locationDidUpdateToLocation(location: CLLocation) {
////        testCurrentLocation = location
////        print("Latitude : \(self.currentLocation.coordinate.latitude)")
////        print("Longitude : \(self.currentLocation.coordinate.longitude)")
////    }
//
////}
//
//extension NetworkHelper {
//    
//    // MARK: - Notifications
//    
//    func deliverStatusNotification(message: String) {
//        
//        dispatch_async(dispatch_get_main_queue()) {
//            let notification = UILocalNotification()
//            notification.soundName = UILocalNotificationDefaultSoundName
//            notification.fireDate = NSDate(timeIntervalSinceNow: 1)
//            notification.alertBody = message
//            UIApplication.sharedApplication().scheduleLocalNotification(notification)
//        }
//    }
//}
