//
//  CalloutView.swift
//  WifiCaptive
//
//  Created by plotkin on 22/08/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
//import HotspotHelper

class CalloutView: UIView, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var descField: UILabel!
    var desc:String!
    var view: UIView!
    var point:WifiHotspot!
    var delegate: CalloutViewDelegate?
    
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var buttonRoute: UIButton!
    
    @IBOutlet var viewItem: UIView!
    
    override init(frame: CGRect) {

        super.init(frame: frame)
        xibSetup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
        
    }
    
    func xibSetup() {
        
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        descField.font = CurrentFont(fontSize: 20.0).currentFont
        buttonRoute.titleLabel?.font = CurrentFont(fontSize: 20.0).currentFont
        buttonRoute.titleLabel?.minimumScaleFactor = 0.5
        buttonRoute.titleLabel?.adjustsFontSizeToFitWidth = true
        passwordLabel.font = CurrentFont(fontSize: 15.0).currentFont
        
        addSubview(view)
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CalloutView.detailsTapped(_:))))
        
    }
    
    func loadViewFromNib() -> UIView {

        let nib = UINib(nibName: "CalloutView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBOutlet weak var directionsOutlet: UIButton!
    @IBAction func directionTapped(_ sender: AnyObject) {
                
        let currentCoords = shareCurrentLocationInBackGround()
        
        let coordinate = CLLocationCoordinate2DMake(self.point.latitude, self.point.longitude);
        let mkLoc = CLLocation(latitude: self.point.latitude, longitude: self.point.longitude)
        let distance = currentCoords.distance(from: mkLoc)
        
        if distance < 250 {
            
            delegate?.showPopUp()
            
        } else {
            
            delegate?.showPopUpDirections()
            
//            let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
//            let destionation = MKMapItem(placemark: place)
//            destionation.name = point.name
//            destionation.openInMapsWithLaunchOptions(nil)
        }
    }
    
    @IBAction func detailsTapped(_ sender: AnyObject) {
        delegate?.NavigateToHotspotDetails()
    }
    
}

protocol CalloutViewDelegate{
    func NavigateToHotspotDetails()
    func showPopUp()
    func showPopUpDirections()
}

