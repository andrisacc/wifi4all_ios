//
//  SignupViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 19/08/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import Alamofire
//import HotspotHelper
import DigitsKit
import CocoaLumberjack

enum SignupViewControllerMode {
    case login, signUp, password
}

protocol SignupViewControllerDelegate: NSObjectProtocol {
    func signUpViewControllerSucceeded(_ controller: SignupViewController)
}

class SignupViewController: UIViewController, UITextFieldDelegate, UIViewControllerTransitioningDelegate {

    // MARK: - IBOutlets -
        
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var labelError: UILabel!
    
    var emailUP = String()
    
    // MARK: - Propeties -
    weak var delegate: SignupViewControllerDelegate?
    var mode = SignupViewControllerMode.login
    
    let deafaults = UserDefaults.standard
    
//    var authByPhoneToken = NSUserDefaults.standardUserDefaults()
    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor.lightGray,
            NSFontAttributeName : CurrentFont(fontSize: 19.0).currentFont
        ] as [String : Any]
        emailField.font = CurrentFont(fontSize: 19.0).currentFont
        emailField.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("email"), attributes:attributes)
        passField.font = CurrentFont(fontSize: 19.0).currentFont
        passField.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("password"), attributes:attributes)
        loginButton.titleLabel?.font = CurrentFont(fontSize: 16.0).currentFont
        labelError.font = CurrentFont(fontSize: 10.0).currentFont
        
        emailField.delegate = self
        passField.delegate = self
        
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        
        self.preferredContentSize = CGSize(width: 300, height: 200)
        
        labelError.isHidden = true
        
        loginButton.backgroundColor = UIColor.gray
        loginButton.setTitle((mode == .login ? QQLocalizedString("Sign In") : QQLocalizedString("Sign Up")), for: UIControlState())
        
        
    }
    
    func presentError(_ error: String) {
        
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }

    @IBAction func fieldEditingChanged(_ sender: AnyObject) {
        
        guard let email = emailField.text, let password = passField.text else {
            loginButton.isEnabled = false
            loginButton.backgroundColor = UIColor.gray
            return
        }
        
        loginButton.isEnabled = password.characters.count >= 8 && email.range(of: "@") != nil
        loginButton.backgroundColor = loginButton.isEnabled ? UIColor.brandColor() : UIColor.gray
        
        labelError.isHidden = password.characters.count >= 8
        
    }
    

    @IBAction func registrationTapped(_ sender: AnyObject) {
        
        guard let email = emailField.text, let password = passField.text else { return }
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        if isValidEmail(email) {
            
            if mode == .login {
                
                LoginHelper.login(email: email, password: password) { success, errorString in
                    
                    if success {
                        CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                        self.delegate?.signUpViewControllerSucceeded(self)
                        
                        UserDefaults.standard.set(true, forKey: "launchedBefore")
                        
                    } else {
                        CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                        self.presentError(errorString ?? QQLocalizedString("There was an error during your login. Try again later"))
                    }
                }
            } else {
                
                LoginHelper.signUp(email: email, password: password) { success, errorString in
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    
                    if success {
                        self.delegate?.signUpViewControllerSucceeded(self)
                    } else {
                        self.presentError(errorString ?? QQLocalizedString("There was an error during your sign up. Try again later"))
                    }
                }
                
            }
            
        } else {
            self.presentError(QQLocalizedString("Please enter valid email"))
            CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
        }
        
    }

}
