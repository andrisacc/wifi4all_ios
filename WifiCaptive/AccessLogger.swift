////
////  accessLogger.swift
////  Pods
////
////  Created by Victor Barskov on 05.08.16.
////
////
//
//import Foundation
//import UIKit
//import CocoaLumberjack
//
//
////let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
////let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
////let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("failedNetworks.json")
//
//
//public class AccessLogger: NSObject {
//    
//    
//    var BaseURL = "https://www.wifiasyougo.com/api/" // Prod
//    
//    public class var sharedInstance: AccessLogger {
//        struct Singleton {
//            static let instance = AccessLogger()
//        }
//        return Singleton.instance
//    }
//    
//    let accessSession: NSURLSession = {
//        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
//        config.allowsCellularAccess = false
//        config.timeoutIntervalForRequest = 90
//        config.timeoutIntervalForResource = 90
//        return NSURLSession(configuration: config)
//    }()
//    
//    // Success request
//    public func successAccess(pointId pointId: String, latitude: Double, longitude: Double) {
//        
//        var dict: [String: AnyObject] = ["content":["pointid":pointId, "lat": latitude, "lon": longitude]]
//        
//        var params : [String:AnyObject] = ["pointid": pointId, "latitude":latitude, "longitude":longitude]
//        
//        let request = NSMutableURLRequest(URL: NSURL(string: BaseURL + "wifi/successaccess")!)
//        request.addAitaAuth()
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.HTTPMethod = "POST"
//        do {
//            let jsonData = try NSJSONSerialization.dataWithJSONObject(params, options:NSJSONWritingOptions(rawValue: 0)) as! NSData
//            request.HTTPBody = jsonData
//        } catch let error as NSError{
//            DDLogDebug("Error has been occured with NSJSONSerialization: \(error.localizedDescription)")
//        }
//        
//        accessSession.dataTaskWithRequest(request) { data, response, error in
//            
//            if let data = data {
//                let json = JSON(data: data)
//                dispatch_async(dispatch_get_main_queue()){
//                    DDLogDebug("Data on success response accessLogger \(json)")
//                }
//            } else {
//                DDLogDebug("Error has been occured")
//            }
//            }.resume()
//    }
//    
//    public func unSuccessAccess(pointId pointId: String, latitude: Double, longitude: Double) {
//        
//        var params : [String:AnyObject] = ["pointid": pointId, "latitude":latitude, "longitude":longitude]
//        
//        let request = NSMutableURLRequest(URL: NSURL(string: BaseURL + "wifi/unsuccessaccess")!)
//        request.addAitaAuth()
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.HTTPMethod = "POST"
//        do {
//            let jsonData = try NSJSONSerialization.dataWithJSONObject(params, options:NSJSONWritingOptions(rawValue: 0)) as! NSData
//            request.HTTPBody = jsonData
//        } catch let error as NSError{
//            DDLogDebug("Error has been occured with NSJSONSerialization: \(error.localizedDescription)")
//        }
//        
//        accessSession.dataTaskWithRequest(request) { data, response, error in
//            
//            if let data = data {
//                let json = JSON(data: data)
//                dispatch_async(dispatch_get_main_queue()){
//                    DDLogDebug("Data on UNNNNNsuccess response accessLogger \(json)")
//                }
//            } else {
//                DDLogDebug("Error has been occured")
//            }
//            }.resume()
//    }
//    
//}
//
//
//public class failedHotspot: NSObject, NSCoding {
//    
//    var point: String
//    var lat: Double
//    var lon: Double
//    
//    init(pointID: String, latitude: Double, longitude: Double) {
//        self.point = pointID
//        self.lat = latitude
//        self.lon = longitude
//    }
//    
//    //MARK: NSCoding
//    
//    required public init(coder aDecoder: NSCoder) {
//        self.point = aDecoder.decodeObjectForKey("point") as! String
//        self.lat = aDecoder.decodeObjectForKey("lat") as! Double
//        self.lon = aDecoder.decodeObjectForKey("lon") as! Double
//        
//    }
//    
//    public func encodeWithCoder(aCoder: NSCoder) {
//        aCoder.encodeObject(point, forKey: "point")
//        aCoder.encodeObject(lat, forKey: "lat")
//        aCoder.encodeObject(lon, forKey: "lon")
//    }
//    
//}
//
//
////public class JsonNing: NSObject {
////    
////    public class var sharedInstance: JsonNing {
////        struct Singleton {
////            static let instance = JsonNing()
////        }
////        return Singleton.instance
////    }
////    
////    public func createWriteJsonFile (pointId pointId: String, latitude: Double, longitude: Double) {
////        
////        let fileManager = NSFileManager.defaultManager()
////        var isDirectory: ObjCBool = false
////        
////        // creating a .json file in the Documents folder
////        
////        if !fileManager.fileExistsAtPath(jsonFilePath.absoluteString, isDirectory: &isDirectory) {
////            let created = fileManager.createFileAtPath(jsonFilePath.absoluteString, contents: nil, attributes: nil)
////            if created {
////                DDLogDebug("File created")
////            } else {
////                DDLogDebug("Couldn't create file for some reason")
////            }
////        } else {
////            DDLogDebug("File already exists")
////        }
////        
////        var jsonData: NSData!
////        var params : [String:AnyObject] = ["pointid": pointId, "latitude":latitude, "longitude":longitude]
////        
////        do {
////            jsonData = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions())
////            guard let jsonString = String(data: jsonData, encoding: NSUTF8StringEncoding) else {return}
////            DDLogDebug(jsonString)
////        } catch let error as NSError{
////            DDLogDebug("Array to JSON conversion failed: \(error.localizedDescription)")
////        }
////        
////        // Write that JSON to the file created earlier
////        
////        do {
////            let file = try NSFileHandle(forWritingToURL: jsonFilePath)
////            
////            file.seekToEndOfFile()
////            file.writeData(jsonData)
////            file.closeFile()
////            
////            DDLogDebug("JSON data was written to the file successfully!")
////        } catch let error as NSError {
////            DDLogDebug("Couldn't write to file: \(error.localizedDescription)")
////        }
////    }
//
////    public func createSaveJSON (pointId pointId: String, latitude: Double, longitude: Double) {
////        
////        var jsonData: NSData!
////        var params : [String:AnyObject] = ["pointid": pointId, "latitude":latitude, "longitude":longitude]
////        
////        do {
////            jsonData = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions())
////            guard let jsonString = String(data: jsonData, encoding: NSUTF8StringEncoding) else {return}
////            DDLogDebug(jsonString)
////        } catch let error as NSError{
////            DDLogDebug("Array to JSON conversion failed: \(error.localizedDescription)")
////        }
////        
////        // Write that JSON to the file created earlier
////        
////        //        let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("failedNetworks.json")
////        do {
////            let file = try NSFileHandle(forWritingToURL: jsonFilePath)
////            file.writeData(jsonData)
////            DDLogDebug("JSON data was written to the file successfully!")
////        } catch let error as NSError {
////            DDLogDebug("Couldn't write to file: \(error.localizedDescription)")
////        }
////        
////    }
//    
////   public func readingJSONFromFile (completion: JSON -> Void) {
////        
////        let fileManager = NSFileManager.defaultManager()
////        var isDirectory: ObjCBool = false
////        
////        if fileManager.fileExistsAtPath(jsonFilePath.absoluteString, isDirectory: &isDirectory) {
////            
////            do {
////                let jsonData = try NSData(contentsOfFile: jsonFilePath.absoluteString, options: NSDataReadingOptions.DataReadingMappedIfSafe)
////                
////                do {
////                    
////                    let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
////                    
////                    DDLogDebug("jsonResult: \(jsonResult)")
//////                    if let people : [NSDictionary] = jsonResult["person"] as? [NSDictionary] {
//////                        for person: NSDictionary in people {
//////                            for (name,value) in person {
//////                                print("\(name) , \(value)")
//////                            }
//////                        }
//////                    }
////                } catch {
////                     DDLogDebug("jsonResult is invalid JSON")
////                }
////            } catch {
////                DDLogDebug("Bad things in readingJSONFromFile")
////            }
////            
//////            do {
//////                
//////                let data = try NSData(contentsOfURL: NSURL(fileURLWithPath: jsonFilePath.absoluteString), options: NSDataReadingOptions.DataReadingMappedIfSafe)
//////                let jsonObj = JSON(data)
//////                DDLogDebug("jsonData:\(jsonObj)")
////////                if jsonObj != JSON.null {
////////                    completion(jsonObj)
////////                    DDLogDebug("jsonData:\(jsonObj)")
////////                } else {
////////                    DDLogDebug("could not get json from file, make sure that file contains valid json.")
////////                }
//////            } catch let error as NSError {
//////                print(error.localizedDescription)
//////            }
//////            
//////        } else {
//////            DDLogDebug("Sorry. No file.")
//////        }
////        }
////    
////    }
////    
////    
////   public func eraseJSON () {
////        
////        // Create a FileManager instance
////        let fileManager = NSFileManager.defaultManager()
////        var isDirectory: ObjCBool = false
////        
////        // Delete 'failedNetworks.json' file
////        if fileManager.fileExistsAtPath(jsonFilePath.absoluteString, isDirectory: &isDirectory) {
////            
////            do {
////                try fileManager.removeItemAtPath(jsonFilePath.absoluteString)
////                DDLogDebug("File erased")
////            }
////            catch let error as NSError {
////                print("Ooops! Something went wrong: \(error.description)")
////            }
////            
////        } else {
////            DDLogDebug("Sorry. No file to erase")
////        }
////    }
////}
//
//
//
//
//
