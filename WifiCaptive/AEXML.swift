import Foundation

/**
 This is base class for holding XML structure.
 
 You can access its structure by using subscript like this: `element["foo"]["bar"]` which would
 return `<bar></bar>` element from `<element><foo><bar></bar></foo></element>` XML as an `AEXMLElement` object.
 */

// MARK: - Element

open class AEXMLElement {
    
    // MARK: - Properties
    
    /// Every `AEXMLElement` should have its parent element instead of `AEXMLDocument` which parent is `nil`.
    open internal(set) weak var parent: AEXMLElement?
    
    /// Child XML elements.
    open internal(set) var children = [AEXMLElement]()
    
    /// XML Element name.
    open var name: String
    
    /// XML Element value.
    open var value: String?
    
    /// XML Element attributes.
    open var attributes: [String : String]
    
    /// Error value (`nil` if there is no error).
    open var error: AEXMLError?
    
    /// String representation of `value` property (if `value` is `nil` this is empty String).
    open var string: String { return value ?? String() }
    
    /// Boolean representation of `value` property (`nil` if `value` can't be represented as Bool).
    open var bool: Bool? { return Bool(string) }
    
    /// Integer representation of `value` property (`nil` if `value` can't be represented as Integer).
    open var int: Int? { return Int(string) }
    
    /// Double representation of `value` property (`nil` if `value` can't be represented as Double).
    open var double: Double? { return Double(string) }
    
    // MARK: - Lifecycle
    
    /**
     Designated initializer - all parameters are optional.
     
     - parameter name: XML element name.
     - parameter value: XML element value (defaults to `nil`).
     - parameter attributes: XML element attributes (defaults to empty dictionary).
     
     - returns: An initialized `AEXMLElement` object.
     */
    public init(name: String, value: String? = nil, attributes: [String : String] = [String : String]()) {
        self.name = name
        self.value = value
        self.attributes = attributes
    }
    
    // MARK: - XML Read
    
    /// The first element with given name **(Empty element with error if not exists)**.
    open subscript(key: String) -> AEXMLElement {
        guard let
            first = children.first(where: { $0.name == key })
            else {
                let errorElement = AEXMLElement(name: key)
                errorElement.error = AEXMLError.elementNotFound
                return errorElement
        }
        return first
    }
    
    /// Returns all of the elements with equal name as `self` **(nil if not exists)**.
    open var all: [AEXMLElement]? { return parent?.children.filter { $0.name == self.name } }
    
    /// Returns the first element with equal name as `self` **(nil if not exists)**.
    open var first: AEXMLElement? { return all?.first }
    
    /// Returns the last element with equal name as `self` **(nil if not exists)**.
    open var last: AEXMLElement? { return all?.last }
    
    /// Returns number of all elements with equal name as `self`.
    open var count: Int { return all?.count ?? 0 }
    
    /**
     Returns all elements with given value.
     
     - parameter value: XML element value.
     
     - returns: Optional Array of found XML elements.
     */
    open func all(withValue value: String) -> [AEXMLElement]? {
        let found = all?.flatMap {
            $0.value == value ? $0 : nil
        }
        return found
    }
    
    /**
     Returns all elements containing given attributes.
     
     - parameter attributes: Array of attribute names.
     
     - returns: Optional Array of found XML elements.
     */
    open func all(containingAttributeKeys keys: [String]) -> [AEXMLElement]? {
        let found = all?.flatMap { element in
            keys.reduce(true) { (result, key) in
                result && Array(element.attributes.keys).contains(key)
                } ? element : nil
        }
        return found
    }
    
    /**
     Returns all elements with given attributes.
     
     - parameter attributes: Dictionary of Keys and Values of attributes.
     
     - returns: Optional Array of found XML elements.
     */
    open func all(withAttributes attributes: [String : String]) -> [AEXMLElement]? {
        let keys = Array(attributes.keys)
        let found = all(containingAttributeKeys: keys)?.flatMap { element in
            attributes.reduce(true) { (result, attribute) in
                result && element.attributes[attribute.key] == attribute.value
                } ? element : nil
        }
        return found
    }
    
    // MARK: - XML Write
    
    /**
     Adds child XML element to `self`.
     
     - parameter child: Child XML element to add.
     
     - returns: Child XML element with `self` as `parent`.
     */
    @discardableResult open func addChild(_ child: AEXMLElement) -> AEXMLElement {
        child.parent = self
        children.append(child)
        return child
    }
    
    /**
     Adds child XML element to `self`.
     
     - parameter name: Child XML element name.
     - parameter value: Child XML element value (defaults to `nil`).
     - parameter attributes: Child XML element attributes (defaults to empty dictionary).
     
     - returns: Child XML element with `self` as `parent`.
     */
    @discardableResult open func addChild(name: String,
                                          value: String? = nil,
                                          attributes: [String : String] = [String : String]()) -> AEXMLElement
    {
        let child = AEXMLElement(name: name, value: value, attributes: attributes)
        return addChild(child)
    }
    
    /// Removes `self` from `parent` XML element.
    open func removeFromParent() {
        parent?.removeChild(self)
    }
    
    fileprivate func removeChild(_ child: AEXMLElement) {
        if let childIndex = children.index(where: { $0 === child }) {
            children.remove(at: childIndex)
        }
    }
    
    fileprivate var parentsCount: Int {
        var count = 0
        var element = self
        
        while let parent = element.parent {
            count += 1
            element = parent
        }
        
        return count
    }
    
    fileprivate func indent(withDepth depth: Int) -> String {
        var count = depth
        var indent = String()
        
        while count > 0 {
            indent += "\t"
            count -= 1
        }
        
        return indent
    }
    
    /// Complete hierarchy of `self` and `children` in **XML** escaped and formatted String
    open var xml: String {
        var xml = String()
        
        // open element
        xml += indent(withDepth: parentsCount - 1)
        xml += "<\(name)"
        
        if attributes.count > 0 {
            // insert attributes
            for (key, value) in attributes {
                xml += " \(key)=\"\(value.xmlEscaped)\""
            }
        }
        
        if value == nil && children.count == 0 {
            // close element
            xml += " />"
        } else {
            if children.count > 0 {
                // add children
                xml += ">\n"
                for child in children {
                    xml += "\(child.xml)\n"
                }
                // add indentation
                xml += indent(withDepth: parentsCount - 1)
                xml += "</\(name)>"
            } else {
                // insert string value and close element
                xml += ">\(string.xmlEscaped)</\(name)>"
            }
        }
        
        return xml
    }
    
    /// Same as `xmlString` but without `\n` and `\t` characters
    open var xmlCompact: String {
        let chars = CharacterSet(charactersIn: "\n\t")
        return xml.components(separatedBy: chars).joined(separator: "")
    }
    
}

public extension String {
    
    /// String representation of self with XML special characters escaped.
    public var xmlEscaped: String {
        // we need to make sure "&" is escaped first. Not doing this may break escaping the other characters
        var escaped = replacingOccurrences(of: "&", with: "&amp;", options: .literal)
        
        // replace the other four special characters
        let escapeChars = ["<" : "&lt;", ">" : "&gt;", "'" : "&apos;", "\"" : "&quot;"]
        for (char, echar) in escapeChars {
            escaped = escaped.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        return escaped
    }
    
}

// MARK: - Document

/**
 This class is inherited from `AEXMLElement` and has a few addons to represent **XML Document**.
 
 XML Parsing is also done with this object.
 */
open class AEXMLDocument: AEXMLElement {
    
    // MARK: - Properties
    
    /// Root (the first child element) element of XML Document **(Empty element with error if not exists)**.
    open var root: AEXMLElement {
        guard let rootElement = children.first else {
            let errorElement = AEXMLElement(name: "Error")
            errorElement.error = AEXMLError.rootElementMissing
            return errorElement
        }
        return rootElement
    }
    
    open let options: AEXMLOptions
    
    // MARK: - Lifecycle
    
    /**
     Designated initializer - Creates and returns new XML Document object.
     
     - parameter root: Root XML element for XML Document (defaults to `nil`).
     - parameter options: Options for XML Document header and parser settings (defaults to `AEXMLOptions()`).
     
     - returns: Initialized XML Document object.
     */
    public init(root: AEXMLElement? = nil, options: AEXMLOptions = AEXMLOptions()) {
        self.options = options
        
        let documentName = String(describing: AEXMLDocument.self)
        super.init(name: documentName)
        
        // document has no parent element
        parent = nil
        
        // add root element to document (if any)
        if let rootElement = root {
            _ = addChild(rootElement)
        }
    }
    
    /**
     Convenience initializer - used for parsing XML data (by calling `loadXMLData:` internally).
     
     - parameter xmlData: XML data to parse.
     - parameter options: Options for XML Document header and parser settings (defaults to `AEXMLOptions()`).
     
     - returns: Initialized XML Document object containing parsed data. Throws error if data could not be parsed.
     */
    public convenience init(xml: Data, options: AEXMLOptions = AEXMLOptions()) throws {
        self.init(options: options)
        try loadXML(xml)
    }
    
    /**
     Convenience initializer - used for parsing XML string (by calling `init(xmlData:options:)` internally).
     
     - parameter xmlString: XML string to parse.
     - parameter encoding: String encoding for creating `Data` from `xmlString` (defaults to `String.Encoding.utf8`)
     - parameter options: Options for XML Document header and parser settings (defaults to `AEXMLOptions()`).
     
     - returns: Initialized XML Document object containing parsed data. Throws error if data could not be parsed.
     */
    public convenience init(xml: String,
                            encoding: String.Encoding = String.Encoding.utf8,
                            options: AEXMLOptions = AEXMLOptions()) throws
    {
        guard let data = xml.data(using: encoding) else { throw AEXMLError.parsingFailed }
        try self.init(xml: data, options: options)
    }
    
    // MARK: - Parse XML
    
    /**
     Creates instance of `AEXMLParser` (private class which is simple wrapper around `XMLParser`)
     and starts parsing the given XML data. Throws error if data could not be parsed.
     
     - parameter data: XML which should be parsed.
     */
    open func loadXML(_ data: Data) throws {
        children.removeAll(keepingCapacity: false)
        let xmlParser = AEXMLParser(document: self, data: data)
        try xmlParser.parse()
    }
    
    // MARK: - Override
    
    /// Override of `xml` property of `AEXMLElement` - it just inserts XML Document header at the beginning.
    open override var xml: String {
        var xml =  "\(options.documentHeader.xmlString)\n"
        xml += root.xml
        return xml
    }
    
}

// MARK: - Parser 

/// Simple wrapper around `Foundation.XMLParser`.
internal class AEXMLParser: NSObject, XMLParserDelegate {
    
    // MARK: - Properties
    
    let document: AEXMLDocument
    let data: Data
    
    var currentParent: AEXMLElement?
    var currentElement: AEXMLElement?
    var currentValue = String()
    
    var parseError: Error?
    
    // MARK: - Lifecycle
    
    init(document: AEXMLDocument, data: Data) {
        self.document = document
        self.data = data
        currentParent = document
        
        super.init()
    }
    
    // MARK: - API
    
    func parse() throws {
        let parser = XMLParser(data: data)
        parser.delegate = self
        
        parser.shouldProcessNamespaces = document.options.parserSettings.shouldProcessNamespaces
        parser.shouldReportNamespacePrefixes = document.options.parserSettings.shouldReportNamespacePrefixes
        parser.shouldResolveExternalEntities = document.options.parserSettings.shouldResolveExternalEntities
        
        let success = parser.parse()
        
        if !success {
            guard let error = parseError else { throw AEXMLError.parsingFailed }
            throw error
        }
    }
    
    // MARK: - XMLParserDelegate
    
    func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?,
                attributes attributeDict: [String : String])
    {
        currentValue = String()
        currentElement = currentParent?.addChild(name: elementName, attributes: attributeDict)
        currentParent = currentElement
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        currentValue += string
        let newValue = currentValue.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        currentElement?.value = newValue == String() ? nil : newValue
    }
    
    func parser(_ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?)
    {
        currentParent = currentParent?.parent
        currentElement = nil
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        self.parseError = parseError
    }
    
}

// MARK: - Options 

/// Options used in `AEXMLDocument`
public struct AEXMLOptions {
    
    /// Values used in XML Document header
    public struct DocumentHeader {
        /// Version value for XML Document header (defaults to 1.0).
        public var version = 1.0
        
        /// Encoding value for XML Document header (defaults to "utf-8").
        public var encoding = "utf-8"
        
        /// Standalone value for XML Document header (defaults to "no").
        public var standalone = "no"
        
        /// XML Document header
        public var xmlString: String {
            return "<?xml version=\"\(version)\" encoding=\"\(encoding)\" standalone=\"\(standalone)\"?>"
        }
    }
    
    /// Settings used by `Foundation.XMLParser`
    public struct ParserSettings {
        /// Parser reports the namespaces and qualified names of elements. (defaults to `false`)
        public var shouldProcessNamespaces = false
        
        /// Parser reports the prefixes indicating the scope of namespace declarations. (defaults to `false`)
        public var shouldReportNamespacePrefixes = false
        
        /// Parser reports declarations of external entities. (defaults to `false`)
        public var shouldResolveExternalEntities = false
    }
    
    /// Values used in XML Document header (defaults to `DocumentHeader()`)
    public var documentHeader = DocumentHeader()
    
    /// Settings used by `Foundation.XMLParser` (defaults to `ParserSettings()`)
    public var parserSettings = ParserSettings()
    
    /// Designated initializer - Creates and returns default `AEXMLOptions`.
    public init() {}
    
}

// MARK: - Error 

/// A type representing error value that can be thrown or inside `error` property of `AEXMLElement`.
public enum AEXMLError: Error {
    /// This will be inside `error` property of `AEXMLElement` when subscript is used for not-existing element.
    case elementNotFound
    
    /// This will be inside `error` property of `AEXMLDocument` when there is no root element.
    case rootElementMissing
    
    /// `AEXMLDocument` can throw this error on `init` or `loadXMLData` if parsing with `XMLParser` was not successful.
    case parsingFailed
}



////
//// AEXML.swift
////
//// Copyright (c) 2014 Marko Tadić <tadija@me.com> http://tadija.net
////
//// Permission is hereby granted, free of charge, to any person obtaining a copy
//// of this software and associated documentation files (the "Software"), to deal
//// in the Software without restriction, including without limitation the rights
//// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//// copies of the Software, and to permit persons to whom the Software is
//// furnished to do so, subject to the following conditions:
////
//// The above copyright notice and this permission notice shall be included in all
//// copies or substantial portions of the Software.
////
//// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//// SOFTWARE.
////
//
//import Foundation
//
//
///**
//    This is base class for holding XML structure.
//
//    You can access its structure by using subscript like this: `element["foo"]["bar"]` would return `<bar></bar>` element from `<element><foo><bar></bar></foo></element>` XML as an `AEXMLElement` object.
//*/
//public class AEXMLElement: Equatable {
//    
//    // MARK: Properties
//    
//    /// Every `AEXMLElement` should have its parent element instead of `AEXMLDocument` which parent is `nil`.
//    public private(set) weak var parent: AEXMLElement?
//    
//    /// Child XML elements.
//    public private(set) var children: [AEXMLElement] = [AEXMLElement]()
//    
//    /// XML Element name.
//    public let name: String
//    
//    /// XML Element value.
//    public var value: String?
//    
//    /// XML Element attributes.
//    public private(set) var attributes: [NSObject : AnyObject]
//    
//    /// String representation of `value` property (if `value` is `nil` this is empty String).
//    public var stringValue: String { return value ?? String() }
//    
//    /// Boolean representation of `value` property (if `value` is "true" or 1 this is `True`, otherwise `False`).
//    public var boolValue: Bool { return stringValue.lowercaseString == "true" || Int(stringValue) == 1 ? true : false }
//    
//    /// Integer representation of `value` property (this is **0** if `value` can't be represented as Integer).
//    public var intValue: Int { return Int(stringValue) ?? 0 }
//    
//    /// Double representation of `value` property (this is **0.00** if `value` can't be represented as Double).
//    public var doubleValue: Double { return (stringValue as NSString).doubleValue }
//    
//    // MARK: Lifecycle
//    
//    /**
//        Designated initializer - `name` is required, others are optional.
//    
//        :param: name XML element name.
//        :param: value XML element value
//        :param: attributes XML element attributes
//    
//        :returns: An initialized `AEXMLElement` object.
//    */
//    public init(_ name: String, value: String? = nil, attributes: [NSObject : AnyObject] = [NSObject : AnyObject]()) {
//        self.name = name
//        self.value = value
//        self.attributes = attributes
//    }
//    
//    // MARK: XML Read
//    
//    /// This element name is used when unable to find element.
//    public class var errorElementName: String { return "AEXMLError" }
//    
//    // The first element with given name **(AEXMLError element if not exists)**.
//    public subscript(key: String) -> AEXMLElement {
//        if name == AEXMLElement.errorElementName {
//            return self
//        } else {
//            let filtered = children.filter { $0.name == key }
//            return filtered.count > 0 ? filtered.first! : AEXMLElement(AEXMLElement.errorElementName, value: "element <\(key)> not found")
//        }
//    }
//    
//    /// Returns all of the elements with equal name as `self` **(nil if not exists)**.
//    public var all: [AEXMLElement]? { return parent?.children.filter { $0.name == self.name } }
//    
//    /// Returns the first element with equal name as `self` **(nil if not exists)**.
//    public var first: AEXMLElement? { return all?.first }
//    
//    /// Returns the last element with equal name as `self` **(nil if not exists)**.
//    public var last: AEXMLElement? { return all?.last }
//    
//    /// Returns number of all elements with equal name as `self`.
//    public var count: Int { return all?.count ?? 0 }
//    
//    /**
//        Returns all element with given attributes.
//    
//        :param: attributes Array of Keys (`NSObject`) and Value (`AnyObject`) - both must conform to `Equatable` protocol.
//    
//        :returns: Optional Array of found XML elements.
//    */
//    public func allWithAttributes <K: NSObject, V: AnyObject where K: Equatable, V: Equatable> (attributes: [K : V]) -> [AEXMLElement]? {
//        var found = [AEXMLElement]()
//        if let elements = all {
//            for element in elements {
//                var countAttributes = 0
//                for (key, value) in attributes {
//                    if element.attributes[key] as? V == value {
//                        countAttributes += 1
//                    }
//                }
//                if countAttributes == attributes.count {
//                    found.append(element)
//                }
//            }
//            return found.count > 0 ? found : nil
//        } else {
//            return nil
//        }
//    }
//    
//    /**
//        Counts elements with given attributes.
//    
//        :param: attributes Array of Keys (`NSObject`) and Value (`AnyObject`) - both must conform to `Equatable` protocol.
//    
//        :returns: Number of elements.
//    */
//    public func countWithAttributes <K: NSObject, V: AnyObject where K: Equatable, V: Equatable> (attributes: [K : V]) -> Int {
//        return allWithAttributes(attributes)?.count ?? 0
//    }
//    
//    // MARK: XML Write
//    
//    /**
//        Adds child XML element to `self`.
//    
//        :param: child Child XML element to add.
//    
//        :returns: Child XML element with `self` as `parent`.
//    */
//    public func addChild(child: AEXMLElement) -> AEXMLElement {
//        child.parent = self
//        children.append(child)
//        return child
//    }
//    
//    /**
//        Adds child XML element to `self`.
//        
//        :param: name Child XML element name.
//        :param: value Child XML element value.
//        :param: attributes Child XML element attributes.
//        
//        :returns: Child XML element with `self` as `parent`.
//    */
//    public func addChild(name name: String, value: String? = nil, attributes: [NSObject : AnyObject] = [NSObject : AnyObject]()) -> AEXMLElement {
//        let child = AEXMLElement(name, value: value, attributes: attributes)
//        return addChild(child)
//    }
//    
//    /**
//        Adds given attribute to `self`.
//    
//        :param: name Attribute name.
//        :param: value Attribute value.
//    */
//    public func addAttribute(name: NSObject, value: AnyObject) {
//        attributes[name] = value
//    }
//    
//    /**
//        Adds given attributes to `self`.
//        
//        :param: attributes Dictionary of Attribute names and values.
//    */
//    public func addAttributes(attributes: [NSObject : AnyObject]) {
//        for (attributeName, attributeValue) in attributes {
//            addAttribute(attributeName, value: attributeValue)
//        }
//    }
//    
//    /// Removes `self` from `parent` XML element.
//    public func removeFromParent() {
//        parent?.removeChild(self)
//    }
//    
//    private func removeChild(child: AEXMLElement) {
//        if let childIndex = children.indexOf(child) {
//            children.removeAtIndex(childIndex)
//        }
//    }
//    
//    private var parentsCount: Int {
//        var count = 0
//        var element = self
//        while let parent = element.parent {
//            count += 1
//            element = parent
//        }
//        return count
//    }
//    
//    private func indentation(count: Int) -> String {
//        var indent = String()
//        if count > 0 {
//            for _ in 0..<count {
//                indent += "\t"
//            }
//        }
//        return indent
//    }
//    
//    /// Complete hierarchy of `self` and `children` in **XML** formatted String
//    public var xmlString: String {
//        var xml = String()
//        
//        // open element
//        xml += indentation(parentsCount - 1)
//        xml += "<\(name)"
//        
//        if attributes.count > 0 {
//            // insert attributes
//            for (key, value) in attributes {
//                xml += " \(key)=\"\(value)\""
//            }
//        }
//        
//        if value == nil && children.count == 0 {
//            // close element
//            xml += " />"
//        } else {
//            if children.count > 0 {
//                // add children
//                xml += ">\n"
//                for child in children {
//                    xml += "\(child.xmlString)\n"
//                }
//                // add indentation
//                xml += indentation(parentsCount - 1)
//                xml += "</\(name)>"
//            } else {
//                // insert string value and close element
//                xml += ">\(stringValue)</\(name)>"
//            }
//        }
//        
//        return xml
//    }
//    
//    /// Same as `xmlString` but without `\n` and `\t` characters
//    public var xmlStringCompact: String {
//        let chars = NSCharacterSet(charactersInString: "\n\t")
//        return xmlString.componentsSeparatedByCharactersInSet(chars).joinWithSeparator("")
//    }
//}
//
//// MARK: -
//
///**
//    This class is inherited from `AEXMLElement` and has a few addons to represent **XML Document**.
//
//    XML Parsing is also done with this object.
//*/
//public class AEXMLDocument: AEXMLElement {
//    
//    // MARK: Properties
//    
//    /// This is only used for XML Document header (default value is 1.0).
//    public let version: Double
//    
//    /// This is only used for XML Document header (default value is "utf-8").
//    public let encoding: String
//    
//    /// This is only used for XML Document header (default value is "no").
//    public let standalone: String
//    
//    /// Root (the first child element) element of XML Document **(AEXMLError element if not exists)**.
//    public var root: AEXMLElement { return children.count == 1 ? children.first! : AEXMLElement(AEXMLElement.errorElementName, value: "XML Document must have root element.") }
//    
//    // MARK: Lifecycle
//    
//    /**
//        Designated initializer - Creates and returns XML Document object.
//    
//        :param: version Version value for XML Document header (defaults to 1.0).
//        :param: encoding Encoding value for XML Document header (defaults to "utf-8").
//        :param: standalone Standalone value for XML Document header (defaults to "no").
//        :param: root Root XML element for XML Document (defaults to `nil`).
//    
//        :returns: An initialized XML Document object.
//    */
//    public init(version: Double = 1.0, encoding: String = "utf-8", standalone: String = "no", root: AEXMLElement? = nil) {
//        // set document properties
//        self.version = version
//        self.encoding = encoding
//        self.standalone = standalone
//        
//        // init super with default name
//        super.init("AEXMLDocument")
//        
//        // document has no parent element
//        parent = nil
//        
//        // add root element to document (if any)
//        if let rootElement = root {
//            addChild(rootElement)
//        }
//    }
//    
//    /**
//        Convenience initializer - used for parsing XML data (by calling `readXMLData:` internally).
//    
//        :param: version Version value for XML Document header (defaults to 1.0).
//        :param: encoding Encoding value for XML Document header (defaults to "utf-8").
//        :param: standalone Standalone value for XML Document header (defaults to "no").
//        :param: xmlData XML data to parse.
//        :param: error If there is an error reading in the data, upon return contains an `NSError` object that describes the problem.
//    
//        :returns: An initialized XML Document object containing the parsed data. Returns `nil` if the data could not be parsed.
//    */
//    public convenience init?(version: Double = 1.0, encoding: String = "utf-8", standalone: String = "no", xmlData: NSData, inout error: NSError?) {
//        self.init(version: version, encoding: encoding, standalone: standalone)
//        if let parseError = readXMLData(xmlData) {
//            error = parseError
//            return nil
//        }
//    }
//    
//    // MARK: Read XML
//    
//    /**
//        Creates instance of `AEXMLParser` (private class which is simple wrapper around `NSXMLParser`) and starts parsing the given XML data.
//    
//        :param: data XML which should be parsed.
//    
//        :returns: `NSError` if parsing is not successfull, otherwise `nil`.
//    */
//    public func readXMLData(data: NSData) -> NSError? {
//        children.removeAll(keepCapacity: false)
//        let xmlParser = AEXMLParser(xmlDocument: self, xmlData: data)
//        return xmlParser.tryParsing() ?? nil
//    }
//    
//    // MARK: Override
//    
//    /// Override of `xmlString` property of `AEXMLElement` - it just inserts XML Document header at the beginning.
//    public override var xmlString: String {
//        var xml =  "<?xml version=\"\(version)\" encoding=\"\(encoding)\" standalone=\"\(standalone)\"?>\n"
//        for child in children {
//            xml += child.xmlString
//        }
//        return xml
//    }
//    
//}
//
//// MARK: -
//
//class AEXMLParser: NSObject, NSXMLParserDelegate {
//    
//    // MARK: Properties
//    
//    let xmlDocument: AEXMLDocument
//    let xmlData: NSData
//    
//    var currentParent: AEXMLElement?
//    var currentElement: AEXMLElement?
//    var currentValue = String()
//    var parseError: NSError?
//    
//    // MARK: Lifecycle
//    
//    init(xmlDocument: AEXMLDocument, xmlData: NSData) {
//        self.xmlDocument = xmlDocument
//        self.xmlData = xmlData
//        currentParent = xmlDocument
//        super.init()
//    }
//    
//    // MARK: XML Parse
//    
//    func tryParsing() -> NSError? {
//        var success = false
//        let parser = NSXMLParser(data: xmlData)
//        parser.delegate = self
//        success = parser.parse()
//        return success ? nil : parseError
//    }
//    
//    // MARK: NSXMLParserDelegate
//    
//    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
//        currentValue = String()
//        currentElement = currentParent?.addChild(name: elementName, attributes: attributeDict)
//        currentParent = currentElement
//    }
//    
//    func parser(parser: NSXMLParser, foundCharacters string: String) {
//        currentValue += string ?? String()
//        let newValue = currentValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
//        currentElement?.value = newValue == String() ? nil : newValue
//    }
//    
//    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
//        currentParent = currentParent?.parent
//        currentElement = nil
//    }
//    
//    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
//        self.parseError = parseError
//        print(parseError)
//    }
//    
//}
//
//// MARK: - Equatable
//
///**
//    Implementation of `Equatable` protocol for `AEXMLElement`.
//*/
//public func ==(lhs: AEXMLElement, rhs: AEXMLElement) -> Bool {
//    if lhs.name != rhs.name { return false }
//    if lhs.value != rhs.value { return false }
//    if lhs.parent != rhs.parent { return false }
//    if lhs.children != rhs.children { return false }
//    if lhs.attributes != rhs.attributes { return false }
//    return true
//}
//
///**
//    Implementation of `Equatable` protocol for `AEXMLElement`.
//*/
//public func !=(lhs: [NSObject: AnyObject], rhs: [NSObject: AnyObject]) -> Bool {
//    for (key, lhsValue) in lhs {
//        if let rhsValue: AnyObject = rhs[key] {
//            if !(lhsValue === rhsValue) { return true }
//        } else { return true }
//    }
//    return false
//}
