//
//  UIWindow.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

public extension UIWindow {
    class var mainWindow: UIWindow {
        return UIApplication.shared.windows.first!
    }
}
