
//
//  SignupViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 19/08/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import Alamofire
//import HotspotHelper
import DigitsKit
import CocoaLumberjack

//enum SignupViewControllerMode {
//    case Login, SignUp, Password
//}

protocol PasswordControllerDelegate: NSObjectProtocol {
    func passwordViewControllerSucceeded(_ controller: PasswordViewController)
}

class PasswordViewController: UIViewController, UITextFieldDelegate, UIViewControllerTransitioningDelegate {
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var labelError: UILabel!
    
    var emailUP: String?
    
    // MARK: - Propeties -
    weak var delegate: PasswordControllerDelegate?
//    var mode = SignupViewControllerMode.Login
    
    let deafaults = UserDefaults.standard
    
    //    var authByPhoneToken = NSUserDefaults.standardUserDefaults()
    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passField.delegate = self
        
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        
        self.preferredContentSize = CGSize(width: 300, height: 120)
        
        labelError.isHidden = true
        
        loginButton.backgroundColor = UIColor.gray
        loginButton.setTitle(QQLocalizedString("Sign In"), for: UIControlState())
        
    }
    
    func presentError(_ error: String) {
        
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func fieldEditingChanged(_ sender: AnyObject) {
        
        guard let password = passField.text else {
            loginButton.isEnabled = false
            loginButton.backgroundColor = UIColor.gray
            return
        }
        
        loginButton.isEnabled = password.characters.count >= 8
        loginButton.backgroundColor = loginButton.isEnabled ? UIColor.brandColor() : UIColor.gray
        
        labelError.isHidden = password.characters.count >= 8
        
        
    }
    
    @IBAction func registrationTapped(_ sender: AnyObject) {
        
        guard let email = emailUP, let password = passField.text else { return }
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        if isValidEmail(email) {
            
                LoginHelper.login(email: email, password: password) { success, errorString in
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    
                    if success {
                        self.delegate?.passwordViewControllerSucceeded(self)
                        GA.event("wifiSignIn_success")
                    } else {
                        self.presentError(errorString ?? QQLocalizedString("There was an error during your login. Try again later"))
                        GA.event("wifiSignIn_failure", label: errorString)
                    }
                }
                GA.event("wifiSignIn_tap")

        } else {
            self.presentError(QQLocalizedString("Please enter valid email"))
            CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
        }
        
    }
    
}
