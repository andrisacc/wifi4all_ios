////
////  DBHelper.swift
////  Pods
////
////  Created by Sergey Pronin on 4/9/16.
////
////
//
//import UIKit
////import SQLite
//import CocoaLumberjack
//
//class DBHelper: NSObject {
//    
//    class func createDatabaseConnection(name: String) -> Connection? {
//        let fileManager = NSFileManager.defaultManager()
//        
//        guard let libPath = fileManager.URLsForDirectory(.LibraryDirectory, inDomains: .UserDomainMask).first else { return nil }
//        guard let dbPath = libPath.URLByAppendingPathComponent("\(name).sqlite").path else { return nil }
//        
//        if !fileManager.fileExistsAtPath(dbPath) {
//            var path = NSBundle(forClass: HotspotCredentials.self).pathForResource(name, ofType: "sqlite")
//            if path == nil {
//                if let bundlePath = NSBundle(forClass: HotspotCredentials.self).pathForResource("HotspotHelper", ofType: "bundle"),
//                    let bundle = NSBundle(path: bundlePath) {
//                    
//                    path = bundle.pathForResource(name, ofType: "sqlite")
//                }
//            }
//            guard let readonlyPath = path else {
//                Logger.logMessage("no path")
////                DDLogDebug("no path")
//                ; return nil }
//            do {
//                try fileManager.copyItemAtPath(readonlyPath, toPath: dbPath)
//            } catch let error {
////                DDLogDebug("db copy error \(error)")
//                return nil
//            }
//        }
//        
//        do {
//            let db = try Connection(dbPath)
//            db.createFunction("regexp", deterministic: true) { args in
//                guard let value = args[0] as? String, let regex = args[1] as? String else { return nil }
//                return Int64((value =~ regex) == true ? 1 : 0)
//            }
//            return db
//        } catch let ex as Result {
//            switch ex {
//            case .Error(let message, _, _):
//                Logger.logMessage("db exception \(message)")
////                DDLogDebug("db exception \(message)")
//            }
//        } catch {
//            Logger.logMessage("unknown exception")
////            DDLogDebug("unknown exception")
//        }
//        return nil
//    }
//}
