//
//  HotspotDetailsTableViewCell.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 12.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class HotspotDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var imageHere: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surNameTextfield: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}
