//
//  EditProfileViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 20.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
//import HotspotHelper
import DigitsKit
import CocoaLumberjack
import Alamofire

class EditProfileViewController: UIViewController, ErrorHandling, SuccessHandling {
    
    // MARK: - Properties -
    
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func phoneNumberTapAction(_ sender: UITapGestureRecognizer) {
        changePhoneNumber ()
    }
    
    // MARK: - VC Load -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor.lightGray,
            NSFontAttributeName : CurrentFont(fontSize: 17.0).currentFont
            ] as [String : Any]
        nameTextField.font = CurrentFont(fontSize: 17.0).currentFont
        nameTextField.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("enter first name"), attributes:attributes)
        lastNameTextField.font = CurrentFont(fontSize: 17.0).currentFont
        lastNameTextField.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("enter last name"), attributes:attributes)
        emailTextField.font = CurrentFont(fontSize: 17.0).currentFont
        emailTextField.attributedPlaceholder = NSAttributedString(string: QQLocalizedString("enter last name"), attributes:attributes)
        
        _ = UserDefaults.standard.object(forKey: "user") as? Data
        // Do any additional setup after loading the view.
        
        let rightButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(EditProfileViewController.changeProfile))
        self.navigationItem.rightBarButtonItem = rightButton
        
        let leftButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(EditProfileViewController.dismissVC))
        
        self.navigationItem.leftBarButtonItem = leftButton
        
        self.title = QQLocalizedString("Edit profile")
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)], for: UIControlState())
        
        self.navigationController?.navigationBar.tintColor  = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        
        // Make a picture in a round
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2
        self.profileImage.clipsToBounds = true
        
        
        // Table View Delegates
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        drawCircle(view: profileImage, radius: 50, color: UIColor(hexString: "#FFFFFF"), lineWidth: 2.5)
        
        let data = UserDefaults.standard.object(forKey: "user") as? Data
        guard let _data = data else {return}
        let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
        guard let user = userUnwrapped else {return}
        let _user = JSON(user)
        
        if let name = _user["firstname"].string  {
            nameTextField.text = name
        }
        if let surname = _user["surname"].string  {
            lastNameTextField.text = surname
        }
        if let email = _user["email"].string  {
            emailTextField.text = email
        }
        
        tableView.reloadData()
        
        GA.screen("EditProfileViewController")
        GA.event("EditProfileViewController_saw")
    }
    
    
    // MARK: - Helpers -
    
    func changePhoneNumber() {
        
        GA.event("EditProfile_changePhone")
        
    }
    
    func changeProfile () {
        
        GA.event("EditProfile_change_tap")
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        let name = nameTextField.text
        let surname = lastNameTextField.text
        let email = emailTextField.text ?? ""
        
        if isValidEmail(email) || email == "" {
            
            APIRequestHelper.changingProfile(firstName: name, surName: surname, email: email, country: nil, completion: { (success, errorString) in
                
                if success {
                    
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    guard let error = errorString else {return}
                    CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
                    self.handleError(error: QQLocalizedString(error))
                }
            })
        } else {
            self.handleError(error: QQLocalizedString("Please enter valid email"))
            CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
        }
        
    }
    
    func dismissVC () {
        self.dismiss(animated: true, completion: nil)
    }

}

extension EditProfileViewController: UITableViewDelegate {
    
    //     Setting up the selection
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Here is the deselection of the row
        tableView.deselectRow(at: indexPath, animated: true)
        
        // Here put you code...
        
        switch indexPath.row {
            
        case 0:
            
            GA.event("profile_changePhone")
            changePhoneNumber()
            
            break
            
        default:
            break
        }
        
    }
}

extension EditProfileViewController: UITableViewDataSource {
    
    // MARK: - TABLE VIEW DATASOURCE REALIZATION -
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var numebrOfRows: Int!
        numebrOfRows = 1
        return numebrOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let data = UserDefaults.standard.object(forKey: "user") as? Data
        
        if (indexPath.row == 0) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            guard let _data = data else {return cell}
            let userUnwrapped = NSKeyedUnarchiver.unarchiveObject(with: _data)
            guard let user = userUnwrapped else {return cell}
            let _user = JSON(user)
            
            if let phone = _user["phone"].string  {
                cell.textLabel?.text = phone
                cell.textLabel?.textColor = UIColor.lightGray
                cell.textLabel?.font = CurrentFont(fontSize: 17.0).currentFont
                return cell
            } else {
                cell.textLabel?.text = QQLocalizedString("enter phone number")
                cell.textLabel?.textColor = UIColor.lightGray
                cell.textLabel?.font = CurrentFont(fontSize: 17.0).currentFont
                return cell
            }
            
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
    }
    
    // Setting up row heights
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height: CGFloat!
        height = 40.0
        return height
    }
    
}
