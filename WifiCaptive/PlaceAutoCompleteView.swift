//
//  PlaceAutoCompleteView.swift
//  WifiCaptive
//
//  Created by bars on 23.09.15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit

#if AITA
#else
//import HotspotHelper
#endif

class Address {
    let description: String
    let key: String
    let title: String
    var location: CLLocation?

    init(title: String, key: String, description: String) {
        self.description = description
        self.key = key
        self.title = title
    }
}

protocol PlaceAutoCompleteViewDelegate{
    func selectPlace(_ address: Address)
}

class PlaceAutoCompleteView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var view: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewItem: UIView!
    var delegate: PlaceAutoCompleteViewDelegate?

    var autocompleteData: [Address] = [Address]()
    var mainFrame: CGRect?

    override init(frame: CGRect) {
        super.init(frame: frame)
        mainFrame = frame
        xibSetup()

        tableView.delegate = self
        tableView.dataSource = self
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        isUserInteractionEnabled = true
    }

    func loadViewFromNib() -> UIView {

        let nib = UINib(nibName: "PlaceAutoCompleteView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    internal func new(_ place: String) {
        
        let query = place.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let urlStringQuery = query else { return }
        
        MapSession.dataTask(with: URLRequest(url: URL(string: BaseURL + "autocomplete/address?address=\(urlStringQuery)")!), completionHandler: { data, response, error in
                if let data = data {
                    var addresses = [Address]()
                    guard let json = JSON(data: data)["autocomplete"].array else { return }
                    
                    for record in json {
                        guard let key = record["key"].string,
                            let title = record["title"].string,
                            let description = record["description"].string
                            else { continue }
                        addresses.append(Address(title: title, key: key, description: description))
                    }
                    
                    DispatchQueue.main.async {
                        self.autocompleteData = addresses
                        self.tableView.reloadData()
                    }
                }
            })            
.resume()
    }
    
    func clear() {
        autocompleteData.removeAll()
        tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "tableCell")
        
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
        cell.textLabel?.textColor = UIColor(white: 0.3, alpha: 1)
        
        let address = autocompleteData[indexPath.row]
        if address.description.characters.count > 0 {
            cell.textLabel?.text = "\(address.title), \(address.description)"
        } else {
            cell.textLabel?.text = "\(address.title)"
        }

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteData.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let place = autocompleteData[indexPath.row]
        
        self.delegate?.selectPlace(place)
    }
}
