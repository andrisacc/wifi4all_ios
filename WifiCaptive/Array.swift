//
//  Array.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

public extension Array where Element: Hashable {
    
    func after(_ item: Element) -> (Element?, Index?) {
        if let index = self.index(of: item), index + 1 < self.count {
            return (self[index + 1], index + 1)
        }
        return (nil, nil)
    }
}
