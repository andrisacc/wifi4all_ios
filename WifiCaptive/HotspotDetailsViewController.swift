//
//  HotspotDetailsViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 18/08/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
import CoreLocation
//import HotspotHelper


class HotspotDetailsViewController: UIViewController, CLLocationManagerDelegate {

    // MARK: - IBOutlets -
    
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var getDirectionsOutlet: UIButton!
    @IBOutlet weak var buttonIgnore: UIButton!
    @IBOutlet weak var descriptionField: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func routeTapped(_ sender: AnyObject) {
        
        let coordinate = CLLocationCoordinate2DMake(self.point.latitude, self.point.longitude);
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let destination = MKMapItem(placemark: place)
        destination.name = point.name
        destination.openInMaps(launchOptions: nil)
        
        GA.event("wifiHotspot_route")
    }
    
    @IBAction func ignoreTapped(_ sender: AnyObject) {
        point.ignored ? point.allow() : point.ignore()
        buttonIgnore.setTitle(QQLocalizedString(point.ignored ? "Allow" : "Ignore"), for: UIControlState())
    }
    
    // MARK: - Properties -
    
    var addressValue: String?
    var point:WifiHotspot!
    
    // MARK: - VC Load -
    
    override func viewDidLoad() {
        
        // Add notification to show the profile controller
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
        
        nameField.font = CurrentFont(fontSize: 18.0).currentFont
        getDirectionsOutlet.titleLabel?.font = CurrentFont(fontSize: 14.0).currentFont
        
        // Table View Delegates
        tableView.dataSource = self
        tableView.delegate = self
        tableView.showsVerticalScrollIndicator = false
        
        
        nameField.text = point.ssid
        let accessRules = self.point.isUserHotspot ? QQLocalizedString("FREE") : QQLocalizedString("SUBSCRIPTION")
        descriptionField.text = String(format: QQLocalizedString("Description: %@"), point.desc) + "\n" + String(format: QQLocalizedString("Access: %@"), accessRules)
        mapView.addAnnotation(point)
        mapView.delegate = self
        centerMapOnLocation()
        updateDescription()

        buttonIgnore?.isHidden = true
        
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)]
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    
    // MARK: - Methods  -
    
    func centerMapOnLocation() {
        let regionRadius: CLLocationDistance = 1000
        var coordinateRegion = MKCoordinateRegionMakeWithDistance(point.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        coordinateRegion.span.latitudeDelta = 0.001
        coordinateRegion.span.longitudeDelta = 0.001
        mapView.setRegion(coordinateRegion, animated: true)

    }

    func updateDescription() {
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: point.coordinate.latitude, longitude: point.coordinate.longitude), completionHandler: {(placemarks, error) -> Void in
            guard let placemark = placemarks?.first else {
                print(error ?? "")
                return
            }
            print(placemark.addressDictionary!)
            guard let address = placemark.addressDictionary!["FormattedAddressLines"]  else {
                return
            }

            guard let addressFull = address as? [String] else {
                return
            }

            let addressFullAsString = addressFull.joined(separator: "\n")
            
//            let accessRules = self.point.isUserHotspot ? QQLocalizedString("FREE") : QQLocalizedString("PAID")
//            self.descriptionField.text = String(format: QQLocalizedString("Description: %@"), self.point.desc) + "\n" + String(format: QQLocalizedString("Access: %@"), accessRules) + "\n" + String(format: QQLocalizedString("Address: %@"), addressFullAsString)
            
            self.addressValue = addressFullAsString
            
            self.tableView.reloadData()
            
        })
    }

    func openProfileVC () {
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork(self)
        }
    }
    
}

extension HotspotDetailsViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView : MKAnnotationView?
        annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") as? MKPinAnnotationView
        if (annotationView == nil) {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        }
        annotationView!.image = UIImage(named: point.isUserHotspot ? "user_marker" : "signal_marker")
        return annotationView;
        
    }
    
}

extension HotspotDetailsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        
        if (indexPath.row == 0) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "desc", for: indexPath) as! HotspotDetailsTableViewCell
            //            cell.title.text = QQLocalizedString("DESCRIPTION")
            cell.title.font = CurrentFont(fontSize: 15.0).currentFont
            cell.value.font = CurrentFont(fontSize: 15.0).currentFont
            cell.value.text = self.point.desc
            
            return cell
            
            
        } else if (indexPath.row == 1)  {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "access", for: indexPath) as! HotspotDetailsTableViewCell
            //            cell.title.text = QQLocalizedString("ACCESS")
            cell.title.font = CurrentFont(fontSize: 15.0).currentFont
            cell.value.font = CurrentFont(fontSize: 15.0).currentFont
            cell.value.text = self.point.isUserHotspot ? QQLocalizedString("FREE") : QQLocalizedString("PAID")
            
            return cell
            
        }
        else if (indexPath.row == 2)  {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "address", for: indexPath) as! HotspotDetailsTableViewCell
            //            cell.title.text = QQLocalizedString("ADDRESS")
            cell.title.font = CurrentFont(fontSize: 15.0).currentFont
            cell.value.font = CurrentFont(fontSize: 15.0).currentFont
            cell.value.text = self.addressValue
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "desc", for: indexPath)
            return cell
        }
    }
    
}

extension HotspotDetailsViewController: UITableViewDelegate {
    
    // Setting up row heights
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0) {
            return 60.0
        } else if (indexPath.row == 1) {
            return 60.0
        } else if (indexPath.row == 2) {
            return 150.0
        } else {
            return 60.0
        }
    }
}


@IBDesignable class RoundedButton: UIButton {
    
    @IBInspectable var color: UIColor = UIColor.gray {
        didSet { setNeedsDisplay() }
    }

    @IBInspectable var radius: CGFloat = 4 {
        didSet { setNeedsDisplay() }
    }
    
    override func draw(_ rect: CGRect) {

        color.setFill()
        UIBezierPath(roundedRect: rect, cornerRadius: radius).fill()

        super.draw(rect)
    }

}
