//
//  Consts.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 10/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

// MARK: - PROD SERVER -

//var BaseURL = "https://www.wifiasyougo.com/api/" // Prod
//var BaseURLInsecured = "http://www.wifiasyougo.com/api/" // Production

// MARK: - TEST SERVER -

//public var BaseURL = "http://ec2-35-163-44-87.us-west-2.compute.amazonaws.com/api/" // Test
//public var BaseURLInsecured = "http://ec2-35-163-44-87.us-west-2.compute.amazonaws.com/api/" // Test

public var BaseURL = setBaseURL()
public var BaseURLInsecured = setBaseURLInsecured()

// MARK: - TEST SERVER SWAP -

//var BaseURL = "http://ec2-35-160-57-37.us-west-2.compute.amazonaws.com/api/" // Test Swap
//var BaseURLInsecured = "http://ec2-35-160-57-37.us-west-2.compute.amazonaws.com/api/" // Test Swap

public struct CurrentFont {
    
    let currentFont: UIFont!
    
    init(fontSize: CGFloat) {
        switch Locale.current.identifier.deviceLoc {
        case .RU:
            //             currentFont = UIFont.systemFontOfSize(fontSize)
            currentFont = UIFont(name: "OpenSans", size: fontSize)
            //            currentFont = UIFont(name: "ProximaNova", size: fontSize)
            break
        default:
            currentFont = UIFont(name: "OpenSans", size: fontSize)
            //            currentFont = UIFont(name: "Montserrat-Regular", size: fontSize)
            //            currentFont = UIFont(name: "ProximaNova", size: fontSize)
            break
        }
        
    }
}

public let AitaToken = "HotspotAitaToken"
public let DeviceToken = "DeviceToken"
public let ForceLogout = "ForceLogout"

// MARK: - Base URL -
public func setBaseURL() -> String {
    if isReleaseVersion() {
        return Settings.baseURL.value
    } else {
        return "http://ec2-35-163-44-87.us-west-2.compute.amazonaws.com/api/"
    }
}
public func setBaseURLInsecured() -> String {
    if isReleaseVersion() {
        return Settings.baseURL.value
    } else {
        return "http://ec2-35-163-44-87.us-west-2.compute.amazonaws.com/api/"
    }
}
