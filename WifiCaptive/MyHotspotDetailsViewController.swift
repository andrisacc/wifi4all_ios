//
//  MyHotspotDetailsViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 20.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class MyHotspotDetailsViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func getDirections(_ sender: UIButton) {
        
        guard let _point = point else {return}
        guard let lat = _point["latitude"].double else {return}
        guard let lon = _point["longitude"].double else {return}
        
        let coordinate = CLLocationCoordinate2DMake(lat, lon)
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let destination = MKMapItem(placemark: place)
        destination.name =  _point["name"].stringValue
        destination.openInMaps(launchOptions: nil)
        
        GA.event("wifiHotspot_route")
        
    }
    // MARK: - Properties -
    
    var point: JSON?

    
    // MARK: - VC Load -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Add notification to show the profile controller
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
        

        // Do any additional setup after loading the view.
        
//        nameField.text = point.ssid
//        let accessRules = self.point.isUserHotspot ? QQLocalizedString("FREE") : QQLocalizedString("SUBSCRIPTION")
//        descriptionField.text = String(format: QQLocalizedString("Description: %@"), point.desc) + "\n" + String(format: QQLocalizedString("Access: %@"), accessRules)
//        print("point", point)
        
        mapView.delegate = self
        guard let _point = point else {return}
        guard let lat = _point["latitude"].double else {return}
        guard let lon = _point["longitude"].double else {return}
        let coordinate = CLLocationCoordinate2DMake(lat, lon)
        // Drop a annotation
        let dropAnnotation = MKPointAnnotation()
        dropAnnotation.coordinate = coordinate
        dropAnnotation.title = _point["name"].stringValue
        mapView.addAnnotation(dropAnnotation)
        
        topLabel.font = CurrentFont(fontSize: 17.0).currentFont
        topLabel.text = _point["name"].stringValue
        centerMapOnLocation()
        
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.screen("wifiMyHotspotDetails")
         GA.event("wifiMyHotspotDetailsViewController_saw")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }
    
    func openProfileVC () {
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork(self)
        }
    }
    
    // MARK: - MapView -

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
                
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView : MKAnnotationView?
        
        guard let _point = point else {return annotationView}
        guard let paid = _point["paid"].bool else {return annotationView}
        
        annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") as? MKPinAnnotationView
        if (annotationView == nil) {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        }
        annotationView!.image = UIImage(named: paid ? "signal_marker" : "user_marker")
        return annotationView
        
    }
    
    func centerMapOnLocation() {
        
        guard let _point = point else {return}
        guard let lat = _point["latitude"].double else {return}
        guard let lon = _point["longitude"].double else {return}
        let coordinate = CLLocationCoordinate2DMake(lat, lon)
        let regionRadius: CLLocationDistance = 2000
        var coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        coordinateRegion.span.latitudeDelta = 0.001
        coordinateRegion.span.longitudeDelta = 0.001
        mapView.setRegion(coordinateRegion, animated: true)
        
    }

}
