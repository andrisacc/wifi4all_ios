//
//  MapView.swift
//  WifiCaptive
//
//  Created by plotkin on 04/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapView: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate  {
    @IBOutlet weak var mapOutlet: MKMapView!
    var locationManager: CLLocationManager!
    var annotations = [CustomPoint]()
    var clusteringController : KPClusteringController!
    var selectedAnnotaion = MKAnnotationView()

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
//        if (CLLocationManager.locationServicesEnabled())
//        {
//            locationManager = CLLocationManager()
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.requestAlwaysAuthorization()
//            locationManager.startUpdatingLocation()
//        }
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        centerMapOnLocation(initialLocation)
        let annotations = getMapAnnotations()
        mapOutlet.showsUserLocation = true
        //mapOutlet.setUserTrackingMode(MKUserTrackingMode.FollowWithHeading, animated: true)
        mapOutlet.delegate = self
        let algorithm : KPGridClusteringAlgorithm = KPGridClusteringAlgorithm()
        self.title = "Hotspots"
        
        algorithm.annotationSize = CGSizeMake(25, 50)
        algorithm.clusteringStrategy = KPGridClusteringAlgorithmStrategy.TwoPhase;
        clusteringController = KPClusteringController(mapView: self.mapOutlet, clusteringAlgorithm: algorithm)
        clusteringController.setAnnotations(annotations)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ListSegoe"){
            let viewController = segue.destinationViewController as! HotspotListViewController
            //var viewController = navigationController.topViewController as! HotspotListViewController
            viewController.title = "tot"
            viewController.annotations = self.annotations
            
        }
        if (segue.identifier == "HotspotDetails"){
            let viewController = segue.destinationViewController as! HotspotDetailsViewController
            viewController.title = "tot"
            viewController.point = (selectedAnnotaion.annotation as! KPAnnotation).annotations.first as! CustomPoint
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 1000
        var coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        coordinateRegion.span.latitudeDelta = 0.5
        coordinateRegion.span.longitudeDelta = 0.5
        mapOutlet.setRegion(coordinateRegion, animated: true)
        
    }
    
    func getMapAnnotations()-> [Mark] {
        var annotations:Array = [Mark]()
        for var i = 0; i < 50; i++
        {
            let lat = Random.within(21.286...21.29)
            let long = Random.within(-157.833...(-157.822))
            let annotation1 = CustomPoint(latitude: lat, longitude: long)
            annotation1.imageName = "flag.png"
            annotation1.title = String(i)
            self.annotations.append(annotation1)
            annotations.append(annotation1)
        }
        return annotations
    }
    
    func mapView(mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        print(mapView.region.span.longitudeDelta, appendNewline: false)
        print(mapView.region.span.latitudeDelta, appendNewline: false)
//        if (mapView.region.span.longitudeDelta <= 0.5 ){
//            mapView.removeAnnotations(mapView.annotations!)
//            let annotation = CustomPoint(latitude: lats/50, longitude: longs/50)
//            annotation.title = "LOL"
//            annotation.imageName = "1.png"
//            mapView.addAnnotation(annotation)
//        }
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView : MKAnnotationView?
        
        if let annotation = annotation as? KPAnnotation {
            let number = annotation.annotations.count
            annotation.title = String(number)
            
            if annotation.isCluster() {
                
                
                annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("cluster") as MKAnnotationView!
                
                if annotationView == nil {
                    annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "cluster")
                    
                    
                }
                
                annotationView!.image = UIImage(named:"cluster.png")
                if (annotationView!.subviews.count > 0) {
                    let l =  annotationView!.subviews[0] as? UILabel
                    l?.text = String(annotation.annotations.count)
                } else {
                    let label = UILabel(frame: CGRectMake(11, 10, 20, 15))
                    label.text = String(annotation.annotations.count)
                    label.textAlignment = NSTextAlignment.Center
                    label.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1)
                    label.font = UIFont(name: "Helvetica", size: 15)
                    annotationView?.addSubview(label)
                }
                
                
                
            }
                
            else {
                annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("pin") as? MKPinAnnotationView
                
                if (annotationView == nil) {
                    annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
                }
                annotationView!.image = UIImage(named:"hotspot.png")
                annotationView!.canShowCallout = true;
                annotationView!.rightCalloutAccessoryView = UIButton(type: UIButtonType.DetailDisclosure)
                //annotationView!.pinColor = .Red
            }
            
            
        }
        
        return annotationView;

    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        self.selectedAnnotaion = view
        self.performSegueWithIdentifier("HotspotDetails", sender: self)
        
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        clusteringController.refresh(true)
    }
    
    
}

class CustomPoint: Mark {
    var imageName: String!
}

struct Random {
    
    static func within(range: ClosedInterval<Double>) -> Double {
        return (range.end - range.start) * Double(Double(arc4random()) / Double(UInt32.max)) + range.start
    }
}