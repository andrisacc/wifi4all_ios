//
//  HotspotListViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 12/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import UIKit

class HotspotListViewController: UITableViewController {
    var annotations = [CustomPoint]()

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return annotations.count
        
    }
    
    
    override func viewDidLoad() {
        
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("HotspotCell", forIndexPath: indexPath)
        cell.textLabel?.text = annotations[indexPath.row].title
        return cell
    }
    

}
