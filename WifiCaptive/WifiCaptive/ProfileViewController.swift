//
//  ProfileViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 07/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import Foundation
import UIKit
import DigitsKit


class ProfileViewController: UIViewController, PTKViewDelegate {
    
    var id = ""
    var token = ""
    var secret = ""
    var defaults = NSUserDefaults.standardUserDefaults()
    //@IBOutlet weak var UserId: UILabel!
    
    var paymentView:PTKView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if (defaults.objectForKey("UserId") == nil) {
           // UserId.hidden = true
            let authenticateButton = DGTAuthenticateButton(authenticationCompletion: {
                (session: DGTSession!, error: NSError!) in
                if (error == nil){
                    self.id = session.userID
                    self.token = session.authToken
                    self.secret = session.authTokenSecret;
                    self.defaults.setObject(self.id, forKey: "UserId")
                    self.defaults.setObject(self.token, forKey: "AuthToken")
                    self.defaults.setObject(self.secret, forKey: "AuthSecret")
                }
            })
            authenticateButton.center = self.view.center
            //Digits.sharedInstance().logOut()
            
            self.view.addSubview(authenticateButton)
        }
        else {
            //UserId.hidden = false
            self.id = self.defaults.objectForKey("UserId") as! String
            //UserId.text = self.id
        }
        
        self.paymentView = PTKView(frame: CGRectMake(15, 20, 290, 55))
        self.paymentView.delegate = self
        //self.view.addSubview(self.paymentView)
        self.navigationItem.title = "Connect to WiFi"
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor()
        ]

        
    }
    func paymentView(paymentView: PTKView!, withCard card: PTKCard!, isValid valid: Bool) {
        var c = 5
    }

    
}