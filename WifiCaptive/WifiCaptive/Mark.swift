//
//  Mark.swift
//  WifiCaptive
//
//  Created by plotkin on 05/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import Foundation
import MapKit

class Mark: NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var latitude: Double
    var longitude:Double
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    override init() {
        latitude = 0
        longitude = 0
    }
}
