//
//  HotspotDetailsViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 18/08/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit

class HotspotDetailsViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var Description: UILabel!
    var point:CustomPoint = CustomPoint()
    
    override func viewDidLoad() {
        
        Description.text = point.title
        mapView.addAnnotation(point)
        mapView.delegate = self
        centerMapOnLocation()
    }
    
    func centerMapOnLocation() {
        let regionRadius: CLLocationDistance = 1000
        var coordinateRegion = MKCoordinateRegionMakeWithDistance(point.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        coordinateRegion.span.latitudeDelta = 0.001
        coordinateRegion.span.longitudeDelta = 0.001
        mapView.setRegion(coordinateRegion, animated: true)
        
    }
    
    
     func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView : MKAnnotationView?
        annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("pin") as? MKPinAnnotationView
        if (annotationView == nil) {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            }
        annotationView!.image = UIImage(named:"hotspot.png")
        return annotationView;
        
    }
    
    
}










@IBDesignable class RoundedButton: UIButton {
    @IBInspectable var color: UIColor = UIColor.grayColor() {
        didSet { setNeedsDisplay() }
    }
    
    @IBInspectable var radius: CGFloat = 10 {
        didSet { setNeedsDisplay() }
    }
    
    override func drawRect(rect: CGRect) {
        
        color.setFill()
        UIBezierPath(roundedRect: rect, cornerRadius: radius).fill()
        
        super.drawRect(rect)
    }
    
}