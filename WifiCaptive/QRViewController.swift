//
//  QRViewController.swift
//  Toka
//
//  Created by Victor Barskov on 21.10.16.
//  Copyright © 2016 Victor Barskov. All rights reserved.
//

import UIKit
import AVFoundation
//import HotspotHelper
import Alamofire

class QRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
//    @IBOutlet weak var messageLabel:UILabel!
    let currentUnixTime = Date().timeIntervalSince1970
    
    // MARK: - Properties -
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var qrDone = false
    var SSID = String()
    var password = String()
    var currentLocation: CLLocation?
    let locationManager = CLLocationManager()
    
    // Added to support different barcodes
    let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]
    var linkToGo: String?
    
    // MARK: - VCLoad -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "backCrossWhiteSmall"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(QRViewController.dismissVC))
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        self.navigationItem.title = NSLocalizedString("QR scanner", comment: "")
        
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.event("QRView_showed")
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            // Detect all the supported bar code
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture
            captureSession?.startRunning()
            
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    // MARK: - Helpers -
    
    func presentError(_ error: String) {
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK",
            style: UIAlertActionStyle.default,
            handler: {(alert: UIAlertAction!) in
                self.dismissVC()
                
            }))
        present(alert, animated: true, completion: nil)
    }
    
    func presentSimpleSuccess(_ success: String) {
        let alert = UIAlertController(title: QQLocalizedString("Success"), message: success, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK",
            style: UIAlertActionStyle.default,
            handler: {(alert: UIAlertAction!) in
                self.dismissVC()
                
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func presentSuccess(_ success: String) {
        
        if #available(iOS 10, *) {
            
            guard let popUpController =
                self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as? PopUpViewController else { return }
            
            popUpController.transitioningDelegate = TransitioningDelegate
            popUpController.mode = .ok
            //        popUpController.delegate = self
            
            self.present(popUpController, animated: true, completion: nil)
            
            //                            UIApplication.sharedApplication().openURL(<#T##url: NSURL##NSURL#>)
            //                            UIApplication.sharedApplication().open(url, options: [:],
            //                                completionHandler: {
            //                                    (success) in
            //                                    print("Open : \(success)")
            //                            })
            
            //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
        } else {
            
            let alert = UIAlertController(title: QQLocalizedString("Success"), message: success, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK",
                style: UIAlertActionStyle.default,
                handler: {(alert: UIAlertAction!) in
                    self.dismiss(animated: true, completion: { () -> Void   in
                        let settingsUrl = URL(string: "prefs:root=WIFI")
                        if let url = settingsUrl {
                            UIApplication.shared.openURL(url)
                        }
                    })
            }))
            present(alert, animated: true, completion: nil)
            
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            //            messageLabel.text = "No barcode/QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        // Here we use filter method to check if the type of metadataObj is supported
        // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
        // can be found in the array of supported bar codes.
        if supportedBarCodes.contains(metadataObj.type) {
            //        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil && !qrDone {
                
                let link = metadataObj.stringValue
                
                // Need to check if it link to WIFI network
                
                if isWiFi(link) {
                    
                    GA.event("QRView__implementation_success")
                    
                    var linkForVerification = String() {
                        
                        didSet {
                            
                            var SSID = [String]()
                            var password = [String]()
                            
                            var lettersArray = linkForVerification.characters.map { String($0) }
                            
                            if lettersArray.contains("S") {

                                for (index, element) in lettersArray.enumerated() {
                                    
                                    if element == "S" {
                                        if lettersArray[index + 1] == ":" {
                                            let pixindex = index + 1
                                            let row : [String] = Array(lettersArray[pixindex+1...lettersArray.count-1])
                                            for i in row {
                                                SSID.append(i)
                                                if i == ";" {break}
                                            }
                                        }
                                    }
                                }
                            }
                            
                            if lettersArray.contains("P") {

                                for (index, element) in lettersArray.enumerated() {
                                    
                                    if element == "P" {
                                        if lettersArray[index + 1] == ":" {
                                            let pixindex = index + 1
                                            let row : [String] = Array(lettersArray[pixindex+1...lettersArray.count-1])
                                            for i in row {
                                                password.append(i)
                                                if i == ";" {break}
                                            }
                                        }
                                    }
                                }
                            }
                            
                            self.SSID = SSID.joined(separator: "")
                            self.SSID = self.SSID.replacingOccurrences(of: ";", with: "")
                            self.password = password.joined(separator: "")
                            self.password = self.password.replacingOccurrences(of: ";", with: "")
                            
                            presentSuccess(QQLocalizedString("The code is valid! Start connection?"))
                            
                            print("SSID: \(self.SSID) with password: \(self.password)")
                        }
                    }
                    
                    linkForVerification = link!
                    
                    
//                    let id = "35757"
//                    let paramus: [String:AnyObject] = ["hotspot_password" : password]
//                    let name = SSID
//                    let SSIDregx = "^\(SSID)$"
//                    let params = JSON(paramus)
//                    let SSIDs = [SSID]
//                    let wisprEnabled = false
//                    let url = ""
//                    let paid = false
//                    var lat: Double
//                    var lon: Double
//                    let cl = shareCurrentLocationInBackGround()
//                    lat = cl.coordinate.latitude
//                    lon = cl.coordinate.longitude
                    
//                    GA.event("wifiAddNetwork_public_via_qr", label: "With QR Name: \(name), params: \(params)")
                    
//                    let conf = WifiConfiguration(id: id, name: name, SSIDregex: SSIDregx, params: params, SSIDs: SSIDs, wisprEnabled: wisprEnabled, url: url, paid: paid, lat: lat, lon: lon)
//                    WifiConfiguration.saveAll([conf])
                    
                    //            MBProgressHUD.showHUDAddedTo(UIWindow.mainWindow, animated: true)
//                    CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
                    
//                    var dict: [String: AnyObject] = [
//                        "name": SSID,
//                        "ssid": SSID,
//                        "public": true,
//                        "params": [
//                            "hotspot_password" : password
//                        ]
//                    ]
//                    
//                    
//                    
////                    GA.event("wifiAddNetwork_public", label: dict.description)
//                    
//                    if let location = currentLocation {
//                        
//                        dict["location"] = [
//                            "lat": location.coordinate.latitude,
//                            "lon": location.coordinate.longitude
//                        ]
//                        //                print("CurrentLocationTest", location.coordinate.latitude, location.coordinate.longitude)
//                    }
                    
                    
//                    let configus = WifiConfiguration(
                    
//                    let json = JSON(object)
//                    let config = json["configuration"]
//                    if config["id"].string != nil {
//                        
//                        WifiConfiguration.saveAll([WifiConfiguration(json: config)])
//                        
//                        //                            DDLogDebug("Saved network: \(json)")
//                    }
                    
//                    guard let token = NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String else { return }
                    
                    // Check if user allowed recieving of the coordinates
                    
                    
//                    Alamofire.request(.POST, BaseURL + "wifi/private", parameters: dict, encoding: .JSON, headers: ["WiFi-User-Token": token])
//                        
//                        .validate(statusCode: 200..<300)
//                        .responseJSON { response in
//                            
//                            switch response.result {
//                                
//                            case .Success(let object):
//                                
//                                GA.event("wifiAddNetwork_public_sucess")
//                                
//                                let json = JSON(object)
//                                let config = json["configuration"]
//                                if config["id"].string != nil {
//                                    
//                                    WifiConfiguration.saveAll([WifiConfiguration(json: config)])
//                                    
//                                    //                            DDLogDebug("Saved network: \(json)")
//                                }
//                                //                        MBProgressHUD.hideAllHUDsForView(UIWindow.mainWindow, animated: true)
//                                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
//                                self.dismissViewControllerAnimated(false) {}
//                                
//                                UIAlertView(title: QQLocalizedString("Success"), message: QQLocalizedString("Your hotspot has been successfully added. Thank you!"), delegate: nil, cancelButtonTitle: "OK").show()
//                                
//                            case .Failure(_):
//                                
//                                if let data = response.data, message = JSON(data: data)["error_message"].string {
//                                    
//                                    //print("ErorrOnAddFail:", JSON(data: data))
//                                    self.presentError(QQLocalizedString(message))
//                                    
//                                } else {
//                                    self.presentError(QQLocalizedString("There was an error adding new network. Try again later"))
//                                }
//                                
//                                GA.event("wifiAddNetwork_public_failure")
//                                
//                                //                        MBProgressHUD.hideAllHUDsForView(UIWindow.mainWindow, animated: true)
//                                CustomActivity.sharedInstance.hideActivityIndicator(UIWindow.mainWindow)
//                            }
//                    }
                    
                    qrDone = true
                    
                } else if canOpenURL(link){
                    
                    qrDone = true
                    if link == "https://appsto.re/ru/XFEk_.i" {
                        presentSimpleSuccess(QQLocalizedString("You are already in the app. Thank you!"))
                    }
                     GA.event("QRView__already_got_the_app", label: link)
                }
                else
                {
                    
                    qrDone = true
                    presentError(QQLocalizedString("Sorry. This is not QR WiFi code. Thank you!"))
//                    print("Wrong WiFi code!", link)
                    GA.event("QRView__implementation_fail", label: link)

                    }
                }
            }
        }
    
    
    
    func dismissVC () {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //    // MARK: - Navigation
    //
    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        
//        if segue.identifier == "web_view" {
//            if let queryvc = segue.destinationViewController as? WebViewController {
//                if let linkForward = linkToGo {
////                    queryvc.link = linkForward
//                }
//                
//            }
//        }
//        
//    }
}

extension QRViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last
        locationManager.stopUpdatingLocation()
    }
}
