//
//  CustomPopUpTransition.swift
//  WifiCaptive
//
//  Created by plotkin on 20/08/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import Foundation
import UIKit

let TransitionController = CustomTransitionController()
let TransitioningDelegate = CustomTransitioningDelegate()

class CustomTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {
    
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return TransitionController
//    }
//    
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return TransitionController
//    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TransitionController
    }
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TransitionController
    }
    
}

class CustomTransitionController: NSObject, UIViewControllerAnimatedTransitioning {
    
    var presentingController: UIViewController?
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let container = transitionContext.containerView
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!

        let reversed = container.viewWithTag(0xabc) != nil
        if reversed {
            let blurredView = container.viewWithTag(0xabc)
            let snapshotView = container.viewWithTag(0xabcd)
            
            UIView.animate(withDuration: 0.3,
                animations: {
                    fromVC.view.alpha = 0
                    blurredView!.alpha = 0
                },
                completion: { finished in
                    fromVC.view.removeFromSuperview()
                    blurredView!.removeFromSuperview()
                    snapshotView!.removeFromSuperview()
                    transitionContext.completeTransition(true)
                    self.presentingController = nil
                })
        } else {
            
            var snapshot = fromVC.view.snapshotImage()
            let snapshotView = UIImageView(image: snapshot)
            snapshotView.tag = 0xabcd
            
            let tintColor = UIColor(white: 0.11, alpha: 0.8)
            snapshot = snapshot?.applyBlur(withRadius: 5, tintColor:tintColor, saturationDeltaFactor:1.8, maskImage:nil)
            let blurredView = UIImageView(image: snapshot)
            blurredView.tag = 0xabc
            blurredView.isUserInteractionEnabled = true
            blurredView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomTransitionController.tapBack(_:))))
            
            snapshotView.frame = container.bounds
            blurredView.frame = container.bounds

            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            label.text = QQLocalizedString("X Close")
            label.font = CurrentFont(fontSize: 12.0).currentFont
            label.textAlignment = .center
            label.textColor = UIColor.white

            let size = toVC.preferredContentSize
            
//            print(size)
            if size.width > 0 && size.height > 0 {
                toVC.view.frame.size.height = toVC.preferredContentSize.height
                toVC.view.frame.size.width = transitionContext.finalFrame(for: toVC).size.width - 40
                toVC.view.center = container.center
                switch Int(UIScreen.main.bounds.height) {
                case 480:
                    label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 23)
                    toVC.view.frame.origin.y = 30
                case 568:
                    if (size.height > 200) {
                        label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 40)
                        toVC.view.frame.origin.y = 50
                    } else {
                        label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 70)
                        toVC.view.frame.origin.y = 80
                    }
                default:
                    if (size.height > 200) {
                        label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 55)
                        toVC.view.frame.origin.y = 65
                    } else {
                        label.center = CGPoint(x: transitionContext.finalFrame(for: toVC).size.width / 2, y: 140)
                        toVC.view.frame.origin.y = 150
                    }
                }
            } else {
                toVC.view.frame = transitionContext.finalFrame(for: toVC)
            }

            blurredView.addSubview(label)
            container.addSubview(snapshotView)
            container.addSubview(blurredView)
            container.addSubview(toVC.view)
            
            toVC.view.alpha = 0
            blurredView.alpha = 0
            
            self.presentingController = fromVC

            UIView.animate(withDuration: 0.3,
                animations: {
                    toVC.view.alpha = 1
                    blurredView.alpha = 1
                },
                completion: { finished in
                    transitionContext.completeTransition(true)
                })
        }
    }
    
    func tapBack(_ recognizer: UIGestureRecognizer) {
        if recognizer.state == .ended {
            presentingController?.dismiss(animated: true, completion: nil)
        }
    }
}
