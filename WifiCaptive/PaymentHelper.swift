//
//  PaymentHelper.swift
//  SqueekAir
//
//  Created by Sergey Pronin on 10/21/14.
//  Copyright (c) 2014 Empatika. All rights reserved.
//

import UIKit
import Stripe
import PassKit

let AitaMerchantID = "merchant.mobi.appintheair"

extension Subscription {
    var paymentRequest: PKPaymentRequest? {
        guard let request: PKPaymentRequest = Stripe.paymentRequest(withMerchantIdentifier: AitaMerchantID) else { return nil }
        request.currencyCode = self.currency
        request.paymentSummaryItems = [
            PKPaymentSummaryItem(label: self.planDescription, amount: NSDecimalNumber(value: self.decimalPrice as Double))
        ]
        return request
    }
}

class ApplePayHelper: NSObject {
    class var sharedInstance: ApplePayHelper! {
        struct Singleton {
            static let instance = ApplePayHelper()
        }
        return Singleton.instance
    }
    
    fileprivate var presentingController: UIViewController!
    fileprivate var subscription: Subscription!
    
    fileprivate var completion: ((Bool, String?) -> Void)!
    
    class func canPurchaseSubscription(_ subscription: Subscription) -> Bool {
        if let request = subscription.paymentRequest {
            return Stripe.canSubmitPaymentRequest(request)
        } else {
            return false
        }
    }
    
    func authorizePayment(_ subscription: Subscription, presentingController: UIViewController, completion: @escaping (Bool, String?) -> Void) {
            
        self.presentingController = presentingController
        self.subscription = subscription
        self.completion = completion
        
        guard let request = subscription.paymentRequest else { completion(false, nil); return }
        
        if Stripe.canSubmitPaymentRequest(request) {
            var controller: UIViewController!
                let paymentController = PKPaymentAuthorizationViewController(paymentRequest: request)
                paymentController.delegate = self
                controller = paymentController
            presentingController.present(controller, animated: true, completion: nil)
        } else {
            completion(false, QQLocalizedString("It is impossible to use your Apple Pay account"))
        }
    }
    
    fileprivate func handlePaymentAuthorization(_ payment: PKPayment, completionHandler: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        STPAPIClient.shared().createToken(with: payment) { token, error in
            if let token = token {
                self.subscription.subscribe(token) { success, errorString in
                    if success {
                        completionHandler(.success)
                        self.completion(true, errorString)
                    } else {
                        completionHandler(.failure)
                        self.completion(false, errorString)
                    }
                }
            } else {
                completionHandler(.failure)
                self.completion(false, nil)
            }
        }
    }
}


extension ApplePayHelper: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment,
        completion: (@escaping (PKPaymentAuthorizationStatus) -> Void)) {
            
            handlePaymentAuthorization(payment, completionHandler: completion)
            
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        self.presentingController.dismiss(animated: true, completion: nil)
        
        self.completion(false, nil)
    }
}

