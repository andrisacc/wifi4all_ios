//
//  TapIndicator.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 04.08.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class TapIndicator: UIView {

    // MARK - Variables
    
    lazy fileprivate var animationLayer : CALayer = {
        return CALayer()
    }()
    
    let tapLayer = TapOvalLayer()

    var isAnimating : Bool = false
    var hidesWhenStopped : Bool = true
    
    var parentFrame :CGRect = CGRect.zero
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        animationLayer.frame = frame
        self.layer.addSublayer(animationLayer)
        drawTapIndicator()
        pause(animationLayer)
        
        self.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func drawTapIndicator() {
        
        //        animationLayer.removeAllAnimations()
        animationLayer.sublayers = nil
        animationLayer.addSublayer(tapLayer)
        
        tapLayer.expand()
        Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(TapIndicator.wobbleOval),
                                               userInfo: nil, repeats: false)
    }
    
    func wobbleOval () {
        
        tapLayer.wobble()
        Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(TapIndicator.drawTapIndicator),
                                                                                              userInfo: nil, repeats: false)
    }
    
    func pause(_ layer : CALayer) {
        let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        
        layer.speed = 0.0
        layer.timeOffset = pausedTime
        
        isAnimating = false
    }
    
    func resume(_ layer : CALayer) {
        
        let pausedTime : CFTimeInterval = layer.timeOffset
        
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
        
        isAnimating = true
    }
    
    func startAnimating () {
        
        if isAnimating {
            return
        }
        
        if hidesWhenStopped {
            self.isHidden = false
        }
        resume(animationLayer)
    }
    
    func stopAnimating () {
        if hidesWhenStopped {
            self.isHidden = true
        }
        pause(animationLayer)
    }
}

class TapOvalLayer: CAShapeLayer {
    
    let animationDuration: CFTimeInterval = 0.3
    
    override init() {
        super.init()
        fillColor = UIColor.redBrandColor().cgColor
        path = ovalPathSmall.cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var ovalPathSquishVertical: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 202.5, y: 2.0, width: 50.0, height: 45.0))
    }
    
    var ovalPathSquishHorizontal: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 205, y: 2.0, width: 45.0, height: 45.0))
    }
    
    var ovalPathSmall: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 200.0, y: 2.0, width: 0.0, height: 0.0))
    }
    
    var ovalPathLarge: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 200, y: 2.0, width: 45, height: 45))
    }
    
    func expand() {
        let expandAnimation: CABasicAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = ovalPathSmall.cgPath
        expandAnimation.toValue = ovalPathLarge.cgPath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.isRemovedOnCompletion = false
        add(expandAnimation, forKey: nil)
    }
    
    func decrease() {
        let expandAnimation: CABasicAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = ovalPathLarge.cgPath
        expandAnimation.toValue = ovalPathSmall.cgPath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.isRemovedOnCompletion = false
        add(expandAnimation, forKey: nil)
    }
    
    
    func wobble() {
        
        let wobbleAnimation1: CABasicAnimation = CABasicAnimation(keyPath: "path")
        wobbleAnimation1.fromValue = ovalPathLarge.cgPath
        wobbleAnimation1.toValue = ovalPathSquishVertical.cgPath
        wobbleAnimation1.beginTime = 0.0
        wobbleAnimation1.duration = animationDuration
        
        let wobbleAnimation2: CABasicAnimation = CABasicAnimation(keyPath: "path")
        wobbleAnimation2.fromValue = ovalPathSquishVertical.cgPath
        wobbleAnimation2.toValue = ovalPathSquishHorizontal.cgPath
        wobbleAnimation2.beginTime = wobbleAnimation1.beginTime + wobbleAnimation1.duration
        wobbleAnimation2.duration = animationDuration
        
        let wobbleAnimation3: CABasicAnimation = CABasicAnimation(keyPath: "path")
        wobbleAnimation3.fromValue = ovalPathSquishHorizontal.cgPath
        wobbleAnimation3.toValue = ovalPathSquishVertical.cgPath
        wobbleAnimation3.beginTime = wobbleAnimation2.beginTime + wobbleAnimation2.duration
        wobbleAnimation3.duration = animationDuration
        
        let wobbleAnimation4: CABasicAnimation = CABasicAnimation(keyPath: "path")
        wobbleAnimation4.fromValue = ovalPathSquishVertical.cgPath
        wobbleAnimation4.toValue = ovalPathLarge.cgPath
        wobbleAnimation4.beginTime = wobbleAnimation3.beginTime + wobbleAnimation3.duration
        wobbleAnimation4.duration = animationDuration
        
        let wobbleAnimationGroup: CAAnimationGroup = CAAnimationGroup()
        wobbleAnimationGroup.animations = [wobbleAnimation1, wobbleAnimation2, wobbleAnimation3,
                                           wobbleAnimation4]
        wobbleAnimationGroup.duration = wobbleAnimation4.beginTime + wobbleAnimation4.duration
        wobbleAnimationGroup.repeatCount = 5
        
        add(wobbleAnimationGroup, forKey: nil)
    }
    
}

class SlideOvalLayer: CAShapeLayer {
    
    let animationDuration: CFTimeInterval = 0.3
    
    override init() {
        super.init()
        fillColor = UIColor.redBrandColor().cgColor
        path = ovalPathSmall.cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var finishPath: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 202.5, y: 2.0, width: 50.0, height: 45.0))
    }
    
    var startPath: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 205, y: 2.0, width: 45.0, height: 45.0))
    }
    
    var ovalPathSmall: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 200.0, y: 2.0, width: 0.0, height: 0.0))
    }
    
    var ovalPathLarge: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 200, y: 2.0, width: 45, height: 45))
    }
    
    func expand() {
        let expandAnimation: CABasicAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = ovalPathSmall.cgPath
        expandAnimation.toValue = ovalPathLarge.cgPath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.isRemovedOnCompletion = false
        add(expandAnimation, forKey: nil)
    }
    
    func decrease() {
        let expandAnimation: CABasicAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = ovalPathLarge.cgPath
        expandAnimation.toValue = ovalPathSmall.cgPath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.isRemovedOnCompletion = false
        add(expandAnimation, forKey: nil)
    }
    
    
    func startFinish() {
        
        let start: CABasicAnimation = CABasicAnimation(keyPath: "path")
        start.fromValue = ovalPathLarge.cgPath
        start.toValue = startPath.cgPath
        start.beginTime = 0.3
        start.duration = animationDuration
        
        let finish: CABasicAnimation = CABasicAnimation(keyPath: "path")
        finish.fromValue = startPath.cgPath
        finish.toValue = finishPath.cgPath
        finish.beginTime = start.beginTime + finish.duration
        finish.duration = 0.7
        
        
        let startFinishGroup: CAAnimationGroup = CAAnimationGroup()
        startFinishGroup.animations = [start, finish]
        startFinishGroup.duration = finish.beginTime + finish.duration
        finish.repeatCount = 0
        
        add(startFinishGroup, forKey: nil)
    }
    
}

open class TapActivity: UIView {
    
    open class var sharedInstance: TapActivity {
        struct Singleton {
            static let instance = TapActivity()
        }
        return Singleton.instance
    }
    
    let tapActivityView = TapIndicator()
    
    
    open func showTapIndicator(_ uiView: UIView) {
        
//        TapActivityView.frame = uiView.frame
//        TapActivityView.parentFrame = uiView.frame
        
//        let label = UILabel(frame: CGRectMake(10, 60, 60, 21))
//        label.center = CGPointMake(25, 25)
//        label.font = UIFont.brandFont()
//        label.textColor = UIColor.whiteBrandColor()
//        label.textAlignment = NSTextAlignment.Center
//        label.text = QQLocalizedString("Tap")
        
//        TapActivityView.addSubview(label)
        uiView.addSubview(tapActivityView)
        
        tapActivityView.startAnimating()
        
    }
    
    open func hideActivityIndicator(_ uiView: UIView) {
        
        DispatchQueue.main.async {
            
            self.tapActivityView.stopAnimating()
            self.tapActivityView.removeFromSuperview()
            
//            self.tapActivityView.hidden = true
        }
        
    }
}


extension UIColor {
    
    class func redBrandColor() -> UIColor {
        return UIColor(red: 216.0/255.0, green: 24.0/255.0, blue: 24.0/255.0, alpha: 0.4)
    }
}




