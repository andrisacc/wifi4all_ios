//
//  TapIndicator.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 04.08.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class SwipeMotivationAnimation: UIView {
    
    // MARK - Variables
    
    lazy fileprivate var animationLayer : CALayer = {
        return CALayer()
    }()
    
    let swipeLayer = SwipeOvalLayer()
    
    var isAnimating : Bool = false
    var hidesWhenStopped : Bool = true
    
    var parentFrame :CGRect = CGRect.zero
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        animationLayer.frame = frame
        self.layer.addSublayer(animationLayer)
        drawSwipeAnimation()
        pause(animationLayer)
        
        self.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func drawSwipeAnimation() {
        
        //        animationLayer.removeAllAnimations()
        animationLayer.sublayers = nil
        animationLayer.addSublayer(swipeLayer)
        
//        swipeLayer.expand()
        Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(SwipeMotivationAnimation.startFinish),
                                               userInfo: nil, repeats: false)
    }
    
    func startFinish () {
        
        swipeLayer.startFinish()
        Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(SwipeMotivationAnimation.drawSwipeAnimation),
                                               userInfo: nil, repeats: false)
    }
    
    func pause(_ layer : CALayer) {
        let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        
        layer.speed = 0.0
        layer.timeOffset = pausedTime
        
        isAnimating = false
    }
    
    func resume(_ layer : CALayer) {
        
        let pausedTime : CFTimeInterval = layer.timeOffset
        
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
        
        isAnimating = true
    }
    
    func startAnimating () {
        
        if isAnimating {
            return
        }
        
        if hidesWhenStopped {
            self.isHidden = false
        }
        resume(animationLayer)
    }
    
    func stopAnimating () {
        if hidesWhenStopped {
            self.isHidden = true
        }
        pause(animationLayer)
    }
}


class SwipeOvalLayer: CAShapeLayer {
    
    let animationDuration: CFTimeInterval = 0.3
    
    override init() {
        super.init()
        fillColor = UIColor.brandWhiteColor().cgColor
        path = ovalPathSmall.cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var finishPath: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 10.5, y: 2.0, width: 50.0, height: 45.0))
    }
    
    var startPath: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 205, y: 2.0, width: 45.0, height: 45.0))
    }
    
    var ovalPathSmall: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 200.0, y: 2.0, width: 0.0, height: 0.0))
    }
    
    var ovalPathLarge: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 200, y: 2.0, width: 45, height: 45))
    }
    
    func expand() {
        let expandAnimation: CABasicAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = ovalPathSmall.cgPath
        expandAnimation.toValue = ovalPathLarge.cgPath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.isRemovedOnCompletion = false
        add(expandAnimation, forKey: nil)
    }
    
    func decrease() {
        let expandAnimation: CABasicAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = ovalPathLarge.cgPath
        expandAnimation.toValue = ovalPathSmall.cgPath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.isRemovedOnCompletion = false
        add(expandAnimation, forKey: nil)
    }
    
    
    func startFinish() {
        
        let start: CABasicAnimation = CABasicAnimation(keyPath: "path")
        start.fromValue = ovalPathLarge.cgPath
        start.toValue = startPath.cgPath
        start.beginTime = 0.3
        start.duration = animationDuration
        
        let finish: CABasicAnimation = CABasicAnimation(keyPath: "path")
        finish.fromValue = startPath.cgPath
        finish.toValue = finishPath.cgPath
        finish.beginTime = start.beginTime + finish.duration
        finish.duration = 0.7
        
        
        let startFinishGroup: CAAnimationGroup = CAAnimationGroup()
        startFinishGroup.animations = [start, finish]
        startFinishGroup.duration = finish.beginTime + finish.duration
        finish.repeatCount = 0
        
        add(startFinishGroup, forKey: nil)
    }
    
}

open class swipeActivity: UIView {
    
    open class var sharedInstance: swipeActivity {
        struct Singleton {
            static let instance = swipeActivity()
        }
        return Singleton.instance
    }
    
    let swipeActivityView = SwipeMotivationAnimation()
    
    
    open func showSwipeActivity(_ uiView: UIView) {
        
        uiView.addSubview(swipeActivityView)
        swipeActivityView.startAnimating()
        
    }
    
    open func hideSwipeActivity(_ uiView: UIView) {
        
        DispatchQueue.main.async {
            
            self.swipeActivityView.stopAnimating()
            self.swipeActivityView.removeFromSuperview()
            
            //            self.tapActivityView.hidden = true
        }
        
    }
}


extension UIColor {
    
    class func brandWhiteColor() -> UIColor {
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.6)
    }
}




