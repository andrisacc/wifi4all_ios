//
//  Handlers.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation

protocol ErrorHandling {
    func handleError(error: String)
}

protocol SuccessHandling {
    func handleSuccess(alert: String)
}

extension ErrorHandling where Self: UIViewController {
    
    func handleError(error: String) {
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alert, animated: true, completion: {
            alert.view.shake()
        })
    }
    
    func handleErrorAndDismiss(error: String) {
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: QQLocalizedString("Ok"), style: .cancel) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        present(alert, animated: true, completion: {
            alert.view.shake()
        })
    }
}

extension SuccessHandling where Self: UIViewController {
    
    func handleSuccess(alert: String) {
        let alert = UIAlertController(title: QQLocalizedString("Success"), message: alert, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: QQLocalizedString("Ok"), style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
}
