//
//  AddPhoneViewController.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 19.09.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit
import DigitsKit
import CocoaLumberjack
//import HotspotHelper

class AddPhoneViewController: UIViewController {

//    let digits = Digits.sharedInstance()
//    let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var skipButtonOutlet: UIButton!
    @IBOutlet weak var addPhoneLabel: UILabel!
    @IBOutlet weak var addPhone: UIButton!
    @IBAction func addPhoneAction(_ sender: UIButton) {
        
        changePhone(viewController: self)
        
    }
    
    @IBAction func skipAction(_ sender: UIButton) {
        showMainView()
    }
    
    
    // MARK: - Propeties -
    
    
//    weak var delegate: PasswordControllerDelegate?
    //    var mode = SignupViewControllerMode.Login
    
    let deafaults = UserDefaults.standard
    
    //    var authByPhoneToken = NSUserDefaults.standardUserDefaults()
    
    
    // MARK: - VCLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add notification to show the profile controller
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
        skipButtonOutlet.isHidden = true
        
        skipButtonOutlet.titleLabel?.font = CurrentFont(fontSize: 15.0).currentFont
        addPhone.titleLabel?.font = CurrentFont(fontSize: 15.0).currentFont
        addPhoneLabel.font = CurrentFont(fontSize: 15.0).currentFont
        
        UserDefaults.standard.set(true, forKey: "connectShowed")
        
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        
        self.preferredContentSize = CGSize(width: 250, height: 400)
        
        addPhoneLabel.text = QQLocalizedString("From the next release the registration will be based on your mobile number. We need it to offer some exiting stuff for you. Please provide your number below")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    func presentError(_ error: String) {
        
        let alert = UIAlertController(title: QQLocalizedString("Error"), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }

    // MARK: - Helpers -
    
    func openProfileVC () {
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork(self)
        }
    }
    
    func showMainView() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "WifiMapViewController") as! WifiMapViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftCollectionViewController") as! LeftCollectionViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        UINavigationBar.appearance().tintColor = UIColor.brandColor()
        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        UIApplication.shared.keyWindow?.rootViewController  = slideMenuController
        
        nvc.navigationBar.tintColor = UIColor.white
        nvc.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isTranslucent = false
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore  {
            LoginHelper.startHotspotHelper()
        }
        
        GA.event("wifiDigitsPhoneChange_skip")
        
    }

}
