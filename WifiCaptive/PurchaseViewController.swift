//
//  ProfileViewController.swift
//  WifiCaptive
//
//  Created by plotkin on 07/08/15.
//  Copyright (c) 2015 plotkin. All rights reserved.
//

import Foundation
import UIKit
import Stripe
import CocoaLumberjack
//import HotspotHelper

//let pointPrices = [["period": "day","price": 1], ["period": "month", "price":5], ["period": "year", "price": 10]]
//
//class PointPrices: NSObject {
//    
//    let period: String
//    let price: Int
//    
//    init(prices: [String:NSObject]) {
//        
//        period = prices["period"]! as! String
//        price = prices["price"]! as! Int
//        
//    }
//}

class PurchaseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ErrorHandling, SuccessHandling {
    
    
    // MARK: - Properties -
    
    var subscriptions = [Subscription]()
    var noLeftBarItem = false
    var purchasingOption: Int?
    
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var paymentField: STPPaymentCardTextField!
    @IBOutlet weak var buttonPay: UIButton!
    @IBOutlet weak var buttonApplePay: UIButton!
    @IBOutlet weak var choseSubscriptionDescrLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
     
    @IBAction func clickBuy1(_ sender: AnyObject) {
        purchase(subscriptions[0])
        //        paymentView.hidden = false
        //        paymentField.becomeFirstResponder()
        //        purchasingOption = 0
        //        DDLogDebug("Buy1 clicked")
        let sub = subscriptions[0]
        GA.event("wifiPurchase_plan", label: sub.plan)
    }
    
    @IBAction func clickBuy2(_ sender: AnyObject) {
        purchase(subscriptions[1])
        
        //        paymentView.hidden = false
        //        paymentField.becomeFirstResponder()
        //        purchasingOption = 1
        //        DDLogDebug("Buy2 clicked")
        let sub = subscriptions[1]
        GA.event("wifiPurchase_plan", label: sub.plan)
    }
    
    @IBAction func clickPaymentCancel(_ sender: AnyObject) {
        paymentView.isHidden = true
        paymentField.clear()
        buttonPay.isEnabled = false
        //        DDLogDebug("Purchase cancelled")
        paymentField.resignFirstResponder()
        if let option = purchasingOption {
            let sub = subscriptions[option]
            GA.event("wifiPurchase_cancel", label: sub.plan)
        }
        purchasingOption = nil
    }
    
    @IBAction func clickApplePay(_ sender: AnyObject) {
        
        guard let option = purchasingOption else { return }
        let subscription = subscriptions[option]
        
        paymentField.resignFirstResponder()
        
        //        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        ApplePayHelper.sharedInstance.authorizePayment(subscription, presentingController: self) { success, errorString in
            self.processPaymentSuccess(success, errorString: errorString)
            if success {
                GA.event("wifiPayment_applePay_success", label: subscription.plan)
            } else {
                GA.event("wifiPayment_applePay_failure", label: errorString)
            }
        }
        
        GA.event("wifiPayment_applePay", label: subscription.plan)
    }
    
    @IBAction func clickBack(_ sender: AnyObject) {
        
        //        navigationController?.popViewControllerAnimated(true)
        
        // SlideMenu View Controller
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("WifiMapViewController") as! WifiMapViewController
        //        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftViewController") as! LeftViewController
        //
        //        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        //        leftViewController.mainViewController = nvc
        //        UINavigationBar.appearance().tintColor = UIColor.brandColor()
        //        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        //        UIApplication.sharedApplication().keyWindow?.rootViewController  = slideMenuController
        
        //        delegate?.changeViewController(LeftMenu.Main)
    }
    
    @IBAction func clickPay(_ sender: AnyObject) {
        
        guard let option = purchasingOption, let card = paymentField.card else { return }
        let subscription = subscriptions[option]
        
        paymentField.resignFirstResponder()
        
        //        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        CustomActivity.sharedInstance.showActivityIndicator(self.view, loadingLabelValue: QQLocalizedString("Loading..."))
        STPAPIClient.shared().createToken(with: card) { token, error in
            if let token = token {
                subscription.subscribe(token) { success, errorString in
                    self.processPaymentSuccess(success, errorString: errorString)
                    if success {
                        GA.event("wifiPayment_card_success", label: subscription.plan)
                    } else {
                        GA.event("wifiPayment_card_failure", label: errorString)
                    }
                }
            } else {
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                self.handleError(error: QQLocalizedString("Your hotspot has been successfully added. We added some points to your account, please check it!"))
                GA.event("wifiPayment_card_failure", label: "no_token")
            }
        }
        
        GA.event("wifiPayment_card", label: subscription.plan)
    }

    
    // MARK: - VCLoad -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Add notification to show the profile controller
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openProfileVC), name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)

        choseSubscriptionDescrLabel.font = CurrentFont(fontSize: 15.0).currentFont
        
        self.title = QQLocalizedString("Shop")
        
        paymentField.placeholderColor = UIColor.white.withAlphaComponent(0.8)
        paymentField.textColor = UIColor.white
        paymentField.borderColor = UIColor.white
        paymentField.textErrorColor = UIColor(red: 255.0/255.0, green: 201.0/255.0, blue: 174/255.0, alpha: 1)
        paymentField.delegate = self
        
        buttonPay.isEnabled = false
        paymentView.isHidden = true
                
        buttonApplePay.isHidden = !ApplePayHelper.canPurchaseSubscription(subscriptions[0])
        
        if noLeftBarItem {
            
            let leftButton = UIBarButtonItem(image: UIImage(named: "arrow_back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PurchaseViewController.dismissVC))
            self.navigationItem.leftBarButtonItem = leftButton
            
        }
        
        let navigationTitleFont = CurrentFont(fontSize: 18.0).currentFont
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: navigationTitleFont ?? UIFont.systemFont(ofSize: 18.0)]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if noLeftBarItem {
            self.setNavigationBarItem(true)
        } else {
            self.setNavigationBarItem(false)
        }
        
        self.navigationController?.navigationBar.tintColor  = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 56.0/255.0, green: 156.0/255.0, blue: 232.0/255.0, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        
        GA.screen("wifiPurchase")
        GA.event("wifiPurchase_saw")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showPaymentController"), object: nil)
    }
    
    
    
    // MARK: - TABLE VIEW DATASOURCE REALIZATION -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
//        let numberItems = subscriptions.count > 0 ? subscriptions.count : 3
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        
        if (indexPath.row == 0) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! PurchaseTableViewCell
            
            let subscriptionIndex = indexPath.row + 1
            
            if isWYGRealm() {
                
                if let userCurrentPointsCount = UserDefaults.standard.object(forKey: "user_points") as? String {
                    
                    if let currentIntPoints = Int(userCurrentPointsCount) {
                        
                        let price = subscriptions[subscriptionIndex].pointsPrice
                        
                        if price > currentIntPoints {
                            
                            cell.bigOneLabel.text = subscriptions[subscriptionIndex].number
                            cell.periodUpperCaseLabel.text = QQLocalizedString(subscriptions[subscriptionIndex].item.uppercased())
                            cell.ulimAccessLabel.text =  QQLocalizedString("UNLIMITED ACCESS")
                            cell.bigOneLabel.alpha = 0.3
                            cell.periodUpperCaseLabel.alpha = 0.3
                            cell.ulimAccessLabel.alpha = 0.6
                            //            button.setTitle(subscriptions[i].product?.priceString, forState: .Normal)
                            if let subscription = subscriptions[subscriptionIndex].product?.priceString {
                                cell.priceLabel.text = subscription
                                cell.priceLabel.alpha = 0.3
                                cell.pointsNumberLabel.text = "\( price)"
                                cell.pointsNumberLabel.alpha = 0.3
                            }
                            cell.pointsWordLabel.text = QQLocalizedString("points")
                            cell.pointsWordLabel.alpha = 0.3
                            
                        } else {
                            
                            cell.bigOneLabel.text = subscriptions[subscriptionIndex].number
                            cell.periodUpperCaseLabel.text = QQLocalizedString(subscriptions[subscriptionIndex].item.uppercased())
                            cell.ulimAccessLabel.text =  QQLocalizedString("UNLIMITED ACCESS")
                            cell.bigOneLabel.alpha = 0.3
                            cell.periodUpperCaseLabel.alpha = 0.3
                            cell.ulimAccessLabel.alpha = 0.6
                            if let subscription = subscriptions[subscriptionIndex].product?.priceString {
                                cell.priceLabel.text = subscription
                                cell.priceLabel.alpha = 0.3
                                cell.pointsNumberLabel.text = "\( price)"
                                cell.pointsNumberLabel.alpha = 0.3
                            }
                            cell.pointsWordLabel.text = QQLocalizedString("points")
                            cell.pointsWordLabel.alpha = 0.3
                        }
                        
                    }
                }
                
            } else {
                
                if let userCurrentPointsCount = UserDefaults.standard.object(forKey: "user_points") as? String {
                    
                    if let currentIntPoints = Int(userCurrentPointsCount) {
                        
                        let price = subscriptions[subscriptionIndex].pointsPrice
                        
                        print("CURRENT POINTS PRICE: ", price)
                        
                        if price > currentIntPoints {
                            
                            cell.bigOneLabel.text = subscriptions[subscriptionIndex].number
                            cell.periodUpperCaseLabel.text = QQLocalizedString(subscriptions[subscriptionIndex].item.uppercased())
                            cell.ulimAccessLabel.text =  QQLocalizedString("UNLIMITED ACCESS")
                            //            button.setTitle(subscriptions[i].product?.priceString, forState: .Normal)
                            if let subscription = subscriptions[subscriptionIndex].product?.priceString {
                                cell.priceLabel.text = subscription
                                cell.pointsNumberLabel.text = "\( price)"
                                cell.pointsNumberLabel.alpha = 0.3
                            }
                            cell.pointsWordLabel.text = QQLocalizedString("points")
                            cell.pointsWordLabel.alpha = 0.3
                            
                        } else {
                            
                            cell.bigOneLabel.text = subscriptions[subscriptionIndex].number
                            cell.periodUpperCaseLabel.text = QQLocalizedString(subscriptions[subscriptionIndex].item.uppercased())
                            cell.ulimAccessLabel.text =  QQLocalizedString("UNLIMITED ACCESS")
                            
                            if let _ = subscriptions[subscriptionIndex].product?.priceString {
                                //                                cell.priceLabel.text = subscription
                                cell.pointsNumberLabel.text = "\( price)"
                            }
                            cell.pointsWordLabel.text = QQLocalizedString("points")
                        }
                        
                    }
                }
                
            }
            
            return cell
            
        } else if (indexPath.row == 1) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! PurchaseTableViewCell
            
            let subscriptionIndex = indexPath.row + 1
            
            if isWYGRealm() {
                
                if let userCurrentPointsCount = UserDefaults.standard.object(forKey: "user_points") as? String {
                    
                    if let currentIntPoints = Int(userCurrentPointsCount) {
                        
                        let price = subscriptions[subscriptionIndex].pointsPrice
                        
                        if price > currentIntPoints {
                            
                            cell.bigOneLabel.text = subscriptions[subscriptionIndex].number
                            cell.periodUpperCaseLabel.text = QQLocalizedString(subscriptions[subscriptionIndex].item.uppercased())
                            cell.ulimAccessLabel.text =  QQLocalizedString("UNLIMITED ACCESS")
                            cell.bigOneLabel.alpha = 0.3
                            cell.periodUpperCaseLabel.alpha = 0.3
                            cell.ulimAccessLabel.alpha = 0.6
                            //            button.setTitle(subscriptions[i].product?.priceString, forState: .Normal)
                            if let subscription = subscriptions[subscriptionIndex].product?.priceString {
                                cell.priceLabel.text = subscription
                                cell.priceLabel.alpha = 0.3
                                cell.pointsNumberLabel.text = "\( price)"
                                cell.pointsNumberLabel.alpha = 0.3
                            }
                            cell.pointsWordLabel.text = QQLocalizedString("points")
                            cell.pointsWordLabel.alpha = 0.3
                            
                        } else {
                            
                            cell.bigOneLabel.text = subscriptions[subscriptionIndex].number
                            cell.periodUpperCaseLabel.text = QQLocalizedString(subscriptions[subscriptionIndex].item.uppercased())
                            cell.ulimAccessLabel.text =  QQLocalizedString("UNLIMITED ACCESS")
                            cell.bigOneLabel.alpha = 0.3
                            cell.periodUpperCaseLabel.alpha = 0.3
                            cell.ulimAccessLabel.alpha = 0.6
                            if let subscription = subscriptions[subscriptionIndex].product?.priceString {
                                cell.priceLabel.text = subscription
                                cell.priceLabel.alpha = 0.3
                                cell.pointsNumberLabel.text = "\( price)"
                                cell.pointsNumberLabel.alpha = 0.3
                            }
                            cell.pointsWordLabel.text = QQLocalizedString("points")
                            cell.pointsWordLabel.alpha = 0.3
                        }
                        
                    }
                }
                
            } else {
                
                if let userCurrentPointsCount = UserDefaults.standard.object(forKey: "user_points") as? String {
                    
                    if let currentIntPoints = Int(userCurrentPointsCount) {
                        
                        let price = subscriptions[subscriptionIndex].pointsPrice
                        
                        if price > currentIntPoints {
                            
                            cell.bigOneLabel.text = subscriptions[subscriptionIndex].number
                            cell.periodUpperCaseLabel.text = QQLocalizedString(subscriptions[subscriptionIndex].item.uppercased())
                            cell.ulimAccessLabel.text =  QQLocalizedString("UNLIMITED ACCESS")
                            //            button.setTitle(subscriptions[i].product?.priceString, forState: .Normal)
                            if let subscription = subscriptions[subscriptionIndex].product?.priceString {
                                cell.priceLabel.text = subscription
                                cell.pointsNumberLabel.text = "\( price)"
                                cell.pointsNumberLabel.alpha = 0.3
                            }
                            cell.pointsWordLabel.text = QQLocalizedString("points")
                            cell.pointsWordLabel.alpha = 0.3
                            
                        } else {
                            
                            cell.bigOneLabel.text = subscriptions[subscriptionIndex].number
                            cell.periodUpperCaseLabel.text = QQLocalizedString(subscriptions[subscriptionIndex].item.uppercased())
                            cell.ulimAccessLabel.text =  QQLocalizedString("UNLIMITED ACCESS")
                            if let _ = subscriptions[subscriptionIndex].product?.priceString {
                                //                                cell.priceLabel.text = subscription
                                cell.pointsNumberLabel.text = "\( price)"
                            }
                            //                        cell.priceLabel.text = "\( price)"
                            cell.pointsWordLabel.text = QQLocalizedString("points")
                        }
                        
                    }
                }
            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! PurchaseTableViewCell
            
            return cell
        }
        
    }
    
    // Setting up row heights
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isWYGRealm() {
            
            let alertController = UIAlertController (title: QQLocalizedString("Thank you"), message: QQLocalizedString("You have still valid subscription"), preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: QQLocalizedString("Ok"), style: .default) { (_) -> Void in
                GA.event("wifiPurchase_tap_toBuy_subscribed")
            }
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            whatMoney(indexPath.row)
        }
        
    }

    
    // MARK: - Helpers -
    
    func purchase(_ subscription: Subscription) {
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        BPStoreObserver.shared().buy(subscription.product) { success, error in
            
            if error != nil {
                //                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                //                DDLogDebug("Impossible to proceeed with this purchase")
                self.tableView.reloadData()
                GA.event("wifiPurchase_failure")
                self.handleError(error: QQLocalizedString("Impossible to proceeed with this purchase"))

            } else {
                //                DDLogDebug("Purchase success")
                GA.event("wifiPurchase_successs")
                self.tableView.allowsSelection = false
                self.tableView.reloadData()
                subscription.subscribe(completion: self.processPaymentSuccess)
            }
        }
    }
    
    func purchaseWithPoints(_ subscription: Subscription) {
        
        CustomActivity.sharedInstance.showActivityIndicator(UIWindow.mainWindow, loadingLabelValue: QQLocalizedString("Loading..."))
        
        subscription.subscribeWithPoints { (success, errorString) in
            
            if success {
                
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                self.handleSuccess(alert: QQLocalizedString("You have been successfully subscribed!"))
                self.tableView.reloadData()
                GA.event("wifiPurchase_withPoints_success")
                
            } else {
                CustomActivity.sharedInstance.hideActivityIndicator(self.view)
                self.tableView.reloadData()
                GA.event("wifiPurchase_forPoints_failure")
                self.handleError(error: QQLocalizedString("Impossible to proceeed with this purchase"))
            }
        }
    }
    
    func processPaymentSuccess(_ success: Bool, errorString: String?) {
        CustomActivity.sharedInstance.hideActivityIndicator(self.view)
        
        if success {
            self.handleSuccess(alert: QQLocalizedString("You have been successfully subscribed!"))
            
            navigationController?.popViewController(animated: true)
        } else {
            if let errorString = errorString {
                self.handleError(error: errorString)
            }
        }
    }
    
    func openProfileVC () {
        
        if UserDefaults.standard.object(forKey: AitaToken) == nil {
            guard let loginViewController =
                storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            
            navigationController?.pushViewController(loginViewController, animated: false)
            
        } else {
            addWiFiNetwork(self)
        }
    }
    
    func dismissVC () {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Deside how to buy 
    
    func whatMoney (_ subscriptionItem: Int) {
        
        if let userCurrentPointsCount = UserDefaults.standard.object(forKey: "user_points") as? String {
            
            if let currentIntPoints = Int(userCurrentPointsCount) {
                
                let price = subscriptions[subscriptionItem].pointsPrice
                
                if price > currentIntPoints {
                    
                    let alertController = UIAlertController (title: QQLocalizedString("Buy with in-app purchase?"), message: QQLocalizedString("You do not have enough points to purchase this item. You still can purchase this item with in-app purchase."), preferredStyle: .alert)
                    
                    
                    let moneyAction = UIAlertAction(title: QQLocalizedString("Buy"), style: .default) { (_) -> Void in
                        
                        self.purchase(self.subscriptions[subscriptionItem])
                        let sub = self.subscriptions[subscriptionItem]
                        GA.event("wifiPurchase_plan", label: sub.plan)
                        
                    }
                    alertController.addAction(moneyAction)
                    
                    let pointsAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default) { (_) -> Void in
                        
                    }
                    alertController.addAction(pointsAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    let alertController = UIAlertController (title: QQLocalizedString("Buy with points?"), message: nil, preferredStyle: .alert)
                    
                    let moneyAction = UIAlertAction(title: QQLocalizedString("Buy"), style: .default) { (_) -> Void in
                        self.purchaseWithPoints(self.subscriptions[subscriptionItem])
                    }
                    alertController.addAction(moneyAction)
                    
                    let cancelAction = UIAlertAction(title: QQLocalizedString("Cancel"), style: .default) { (_) -> Void in
                        
                    }
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
            }
 
        }

    }
}


//MARK: - STPPaymentCardTextFieldDelegate
extension PurchaseViewController: STPPaymentCardTextFieldDelegate {
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        buttonPay.isEnabled = textField.isValid
    }
}
