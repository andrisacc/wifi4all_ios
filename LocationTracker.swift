//
//  LocationTracker.swift
//  Pods
//
//  Created by Victor Barskov on 15.06.16.
//
//

import Foundation
import MapKit

public protocol LocationUpdateProtocol {
    func locationDidUpdateToLocation(location : CLLocation)
}

/// Notification on update of location. UserInfo contains CLLocation for key "location"
public let kLocationDidChangeNotification = "LocationDidChangeNotification"

public class UserLocationManager: NSObject, CLLocationManagerDelegate {
    
    public static let SharedManager = UserLocationManager()
    
    private var locationManager = CLLocationManager()
    
    public var currentLocation : CLLocation?
    
    public var delegate : LocationUpdateProtocol!
    
    private override init () {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
        locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    // MARK: - CLLocationManagerDelegate
    
   public  func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        currentLocation = newLocation
        let userInfo : NSDictionary = ["location" : currentLocation!]
        
         DispatchQueue.main.async { () -> Void in
            self.delegate.locationDidUpdateToLocation(location: self.currentLocation!)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kLocationDidChangeNotification), object: self, userInfo: userInfo as [NSObject : AnyObject])
//            kLocationDidChangeNotification, object: self, userInfo: userInfo as [NSObject : AnyObject]
        }
    }
    
}
