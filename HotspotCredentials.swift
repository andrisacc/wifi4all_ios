//
//  HotspotCredentials.swift
//  WifiHotspot
//
//  Created by Sergey Pronin on 8/21/15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

import UIKit
//import SQLite
import CocoaLumberjack


#if AITA

#else
import KeychainAccess
let HotspotKeychain = Keychain(service: "mobi.appintheair.wifi")
#endif

// MARK: - PROD SERVER -

//  var BaseURL = "https://www.wifiasyougo.com/api/" // Prod
//  var BaseURLInsecured = "http://www.wifiasyougo.com/api/" // Prod

// MARK: - TEST SERVER -

//var BaseURL = "http://ec2-35-163-44-87.us-west-2.compute.amazonaws.com/api/" // test
//var BaseURLInsecured = "http://ec2-35-163-44-87.us-west-2.compute.amazonaws.com/api/" // test

//var BaseURL = "http://ec2-35-160-57-37.us-west-2.compute.amazonaws.com/api/" // Test Swap
//var BaseURLInsecured = "http://ec2-35-160-57-37.us-west-2.compute.amazonaws.com/api/" // Test Swap

let Rules = DBHelper.createDatabaseConnection(name: "Rules")

public class HotspotCredentials: NSObject {
    
    class func hasValidCredentials() -> Bool {
        if let expiration = expiration {
//            DDLogDebug("Here is credentials: expiration_interval - \(expiration.timeIntervalSinceNow), expiration date - \(HotspotKeychain["expiration"]), username.count - \(username.characters.count), password.count - \(password.characters.count)")
            let nameCount = username.characters.count as Int
            let passCount = password.characters.count as Int
            return expiration.timeIntervalSinceNow > 0 && nameCount > 0 && passCount > 0
        } else {
            return false
        }
    }
    
    public class var username: String {
        get {
        #if AITA
            return NSUserDefaults.standardUserDefaults().objectForKey("hotspot_username") as? String ?? ""
        #else
            return HotspotKeychain["username"] ?? ""
            
        #endif
        }
        set {
            #if AITA
                NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "hotspot_username")
                NSUserDefaults.standardUserDefaults().synchronize()
            #else
                HotspotKeychain["username"] = newValue
            #endif
        }
    }
    
    public class var password: String {
        get {
            #if AITA
                return NSUserDefaults.standardUserDefaults().objectForKey("hotspot_password") as? String ?? ""
            #else
                 return HotspotKeychain["password"] ?? ""
            #endif
        }
        set {
            #if AITA
                NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "hotspot_password")
                NSUserDefaults.standardUserDefaults().synchronize()
            #else
                 HotspotKeychain["password"] = newValue
                
            #endif
        }
    }
    
    public class var expiration: NSDate? {
        get {
            #if AITA
                return NSUserDefaults.standardUserDefaults().objectForKey("hotspot_expiration") as? NSDate
            #else
                if let string = HotspotKeychain["expiration"], let expiration = Int(string) {
                    return NSDate(timeIntervalSince1970: TimeInterval(expiration))
                } else {
                    return nil
                }
                
            #endif
        }
        set {
            #if AITA
                NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "hotspot_expiration")
                NSUserDefaults.standardUserDefaults().synchronize()
            #else
                if let value = newValue {
                    HotspotKeychain["expiration"] = "\(Int(value.timeIntervalSince1970))"
                } else {
                    HotspotKeychain["expiration"] = nil
                }
                
            #endif
        }
    }
    
    public class var credentialsAcquired: Bool {
        return username.characters.count > 0 && password.characters.count > 0
    }
    
    public class func updateCredentials(jsonObj: JSON? = nil) {
        
        if let jsonObj = jsonObj {
            let json = JSON(jsonObj)
            storeCredentials(json: json)
//            DDLogDebug("credentials stored on signup/login \(json)")
            
        } else {
            
            guard let _ = UserDefaults.standard.object(forKey: AitaToken) as? String else {return}

//            DDLogDebug("Will send request wifi/credentials on login with this token: \(token)")
            
            let request = NSMutableURLRequest(url: NSURL(string: BaseURL + "wifi/credentials")! as URL)
            request.addAitaAuth()

            SharedSession.dataTask(with: request as URLRequest) { data, response, error in
                
                if let data = data {
                    let json = JSON(data: data)
                    
                    DispatchQueue.main.async {
                        storeCredentials(json: json)
//                        DDLogDebug("credentials stored on login \(json) with token \n \(token)")
                    }

                } else {
                    DDLogDebug("credentials error")
                }
            }.resume()
        }
    }
    
    class func storeCredentials(json: JSON) {
        if let username = json["credentials"]["username"].string,
            let password = json["credentials"]["password"].string,
            let expiration = json["credentials"]["expiration"].int, username.characters.count > 0 {
            DispatchQueue.main.async {
                HotspotCredentials.username = username
                HotspotCredentials.password = password
                HotspotCredentials.expiration = NSDate(timeIntervalSince1970: TimeInterval(expiration))
            }
        } else {
//            DDLogDebug("credentials bad data \(json.rawValue)")
        }
    }
    
    public class func test() {
        DDLogDebug("\(credentialsForSSIDRegex(regex: "cablewifi"))")
    }
    
    public class func credentialsForSSIDRegex(regex: String) -> (username: String, password: String) {
        
        var prefix: String?
        var suffix: String?
        do {
            if let results = try Rules?.prepare("SELECT ssid, prefix, suffix, active FROM HotspotRule WHERE regexp(ssid, ?)", regex) {
                for row in results {
                    guard let active = row[3] as? Int64,
                        let paramPrefix = row[1] as? String,
                        let paramSuffix = row[2] as? String else { continue }

                    if active == 1 {
                        prefix = paramPrefix
                        suffix = paramSuffix
                    }
                    
                    break
                }
            } else {
                Logger.flushMessages()
            }
        } catch { }
        var username = self.username
        if let prefix = prefix, prefix.characters.count > 0 {
            username = "\(prefix)/\(username)"
        }
        if let suffix = suffix, suffix.characters.count > 0 {
            username = "\(username)@\(suffix)"
        }
        return (username: username, password: password)
    }
    
    public class func updateRulesDB() {
        
        let lastID = (UserDefaults.standard.object(forKey: "rules-db-last-id") as AnyObject).integerValue ?? 3613
        
        let request = NSMutableURLRequest(url: NSURL(string: BaseURL + "wifi/rules?last_id=\(lastID)")! as URL)
        request.addAitaAuth()
        SharedSession.dataTask(with: request as URLRequest) { data, response, error in
            if let data = data {
                let json = JSON(data: data)
                
                guard let rules = json["rules"].array else { return }
                
                do {
                    for rule in rules {
                        guard let ssid = rule["ssid"].string, let active = rule["active"].bool else { continue }
                        let prefix = rule["prefix"].string
                        let suffix = rule["suffix"].string
                        let statement = try Rules?.prepare("INSERT INTO HotspotRule VALUES (?, ?, ?, ?)")
                        try statement?.run([ssid, prefix, suffix, active ? 1 : 0])
                    }
                    
                } catch let error {
                    DDLogDebug("rules db update error \(error)")
                    return
                }
                
                guard let updatedLastID = json["last_id"].int else { return }
                UserDefaults.standard.set(updatedLastID, forKey: "rules-db-last-id")
                UserDefaults.standard.synchronize()
            } else {
                DDLogDebug("rules error")
            }
        }.resume()
    }
}
