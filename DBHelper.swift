//
//  DBHelper.swift
//  Pods
//
//  Created by Sergey Pronin on 4/9/16.
//
//

import UIKit
import SQLite
import CocoaLumberjack

class DBHelper: NSObject {
    
    class func createDatabaseConnection(name: String) -> Connection? {
        
        let fileManager = FileManager.default

        guard let libPath = fileManager.urls(for: .libraryDirectory, in: .userDomainMask).first else { return nil }
//        guard let dbPath = Bundle.main.path(forResource: "\(name)", ofType: "sqlite") else { return nil }
        let dbPath = libPath.appendingPathComponent("\(name).sqlite").path
        
        if !fileManager.fileExists(atPath: dbPath) {
            
            var path = Bundle.main.path(forResource: name, ofType: "sqlite")
            
//            var path = Bundle(for: HotspotCredentials.self).path(forResource: name, ofType: "sqlite")
            
            if path == nil {
                
                if let bundlePath = Bundle(for: HotspotCredentials.self).path(forResource: "HotspotHelper", ofType: "bundle"),
                   let bundle = Bundle(path: bundlePath) {
                    
                    path = bundle.path(forResource: name, ofType: "sqlite")
//                    print("path2: ", path ?? "path2 is nil")
                }
            }
            
            guard let readonlyPath = path else { DDLogDebug("no path") ; return nil }
            do {
                try fileManager.copyItem(atPath: readonlyPath, toPath: dbPath)
            } catch _ {
//                DDLogDebug("db copy error \(error)")
                return nil
            }
        }
        
        do {
            let db = try Connection(dbPath)
            db.createFunction("regexp", deterministic: true) { args in
                guard let value = args[0] as? String, let regex = args[1] as? String else { return nil }
                return Int64((value =~ regex) == true ? 1 : 0)
            }
            return db
        } catch let ex as Result {
            switch ex {
            case .error(message: let message, code: _, statement: _):
                DDLogDebug("db exception \(message)")
            }
        } catch {
            DDLogDebug("unknown exception")
        }
        return nil
    }
}
