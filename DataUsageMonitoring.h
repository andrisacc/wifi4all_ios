//
//  DataUsageMonitoring.h
//  WifiCaptive
//
//  Created by Sergey Pronin on 8/23/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataUsageMonitoring : NSObject

+ (NSInteger)currentWifiDataUsage;
+ (NSString * __nonnull)deviceName;

@end
