//
//  Dictionary+URLEncode.swift
//  WifiHotspot
//
//  Created by Sergey Pronin on 8/12/15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

import UIKit

protocol URLEncodable {
    var URLEncodedValue: String? { get }
}

extension String: URLEncodable {
    var URLEncodedValue: String? {
        return addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
}

extension Int: URLEncodable {
    var URLEncodedValue: String? {
        return "\(self)"
    }
}

extension Double: URLEncodable {
    var URLEncodedValue: String? {
        return "\(self)"
    }
}

extension Dictionary where Value: URLEncodable {
    var URLEncodedString: String? {
        var params = [String]()
        for (k, v) in self {
            guard let value = v.URLEncodedValue else { return nil }
            params.append("\(k)=\(value)")
        }
        return params.joined(separator: "&")
    }
}
