//
//  CaptiveWebViewController.swift
//  WifiHotspot
//
//  Created by Sergey Pronin on 8/18/15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

import UIKit
import NetworkExtension
import PKHUD
import CocoaLumberjack

protocol CaptiveWebViewControllerDelegate: NSObjectProtocol {
    
    func captiveWebViewControllerDidSubmitForm(controller: CaptiveWebViewController,
        withConfiguration configuration: [String: AnyObject]?, success: Bool)
    
    func captiveWebViewControllerDidCancel(controller: CaptiveWebViewController)
    
    func captiveWebViewControllerDidFindSuccessPage(controller: CaptiveWebViewController)
}

//FIXME: internal
public class CaptiveWebViewController: UIViewController {
    
    //workaround for 8.0 and 9.0 compatibility
    private var _command: AnyObject!
    
    @available(iOS 9.0, *)
    var command: NEHotspotHelperCommand! {
        get { return _command as! NEHotspotHelperCommand }
        set { _command = newValue }
    }
    
    weak var delegate: CaptiveWebViewControllerDelegate?
    
    let webView = UIWebView()
    
    var wifiConfiguration: WifiConfiguration!
    var postInvoked = false
    var updatedConfig: [String: AnyObject]?
    var spinner = UIActivityIndicatorView()

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Authorization"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel,
            target: self, action: #selector(CaptiveWebViewController.clickCancel(sender:)))
        
        automaticallyAdjustsScrollViewInsets = false
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(webView)
        self.view.addConstraints(NSLayoutConstraint.constraints(
            // TODO: To be checked .alignAllFirstBaseLine...
            withVisualFormat: "V:[webView]-(0)-|", options: .alignAllFirstBaseline, metrics: nil, views: ["webView": webView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-(0)-[webView]-(0)-|", options: .alignAllFirstBaseline, metrics: nil, views: ["webView": webView]))

        webView.delegate = self
        
        self.view.addSubview(spinner)
        spinner.activityIndicatorViewStyle = .gray
        spinner.center = self.view.center
        
        spinner.hidesWhenStopped = true
        setupCredentialsView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        spinner.startAnimating()
        let url = URL(string: BaseURLInsecured + "invisible")! as URL
        let r = URLRequest(url: url)
        webView.loadRequest(r)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        Logger.flushMessages()
    }
    
    func setupCredentialsView() {
        let username: String, password: String
//        if let credentials = wifiConfiguration?["params"]["_credentials"].dictionary {
//            username = credentials["username"]!.string!
//            password = credentials["password"]!.string!
//        } else {
        if let regex = wifiConfiguration?.SSIDregex {
            let credentials = HotspotCredentials.credentialsForSSIDRegex(regex: regex)
            username = credentials.username
            password = credentials.password
        } else {
            (username, password) = ("username", "password")
        }
//        }
        let credentialsView = UIView()
        credentialsView.translatesAutoresizingMaskIntoConstraints = false
        credentialsView.backgroundColor = UIColor.white
        self.view.addSubview(credentialsView)
        self.view.addConstraint(NSLayoutConstraint(item: self.view, attribute: .top, relatedBy: .equal,
            toItem: credentialsView, attribute: .top, multiplier: 1, constant: -60))
        self.view.addConstraints(NSLayoutConstraint.constraints(
            // TODO: To be checked .alignAllFirstBaseLine...
            withVisualFormat: "V:[view(60)]", options: .alignAllFirstBaseline, metrics: nil, views: ["view": credentialsView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-(0)-[view]-(0)-|", options: .alignAllFirstBaseline, metrics: nil, views: ["view": credentialsView]))
        let labelUsername = UILabel()
        if #available(iOS 8.2, *) {
            labelUsername.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightLight)
        } else {
            labelUsername.font = UIFont(name: "HelveticaNeue-Light", size: 16)
        }
        labelUsername.textAlignment = .center
        labelUsername.isUserInteractionEnabled = true
        labelUsername.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CaptiveWebViewController.tapCredentials(recognizer:))))
        labelUsername.text = username
        labelUsername.translatesAutoresizingMaskIntoConstraints = false
        credentialsView.addSubview(labelUsername)
        // TODO: To be checked .alignAllFirstBaseLine...
        credentialsView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-(0)-[view(30)]", options: .alignAllFirstBaseline, metrics: nil, views: ["view": labelUsername]))
        credentialsView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-(0)-[view]-(0)-|", options: .alignAllFirstBaseline, metrics: nil, views: ["view": labelUsername]))
        
        let labelPassword = UILabel()
        if #available(iOS 8.2, *) {
            labelPassword.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightLight)
        } else {
            labelPassword.font = UIFont(name: "HelveticaNeue-Light", size: 16)
        }
        labelPassword.textAlignment = .center
        labelPassword.isUserInteractionEnabled = true
        labelPassword.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CaptiveWebViewController.tapCredentials(recognizer:))))
        labelPassword.text = password
        labelPassword.translatesAutoresizingMaskIntoConstraints = false
        credentialsView.addSubview(labelPassword)
        // TODO: To be checked .alignAllFirstBaseLine...
        credentialsView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:[view(30)]-(0)-|", options: .alignAllFirstBaseline, metrics: nil, views: ["view": labelPassword]))
        credentialsView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-(0)-[view]-(0)-|", options: .alignAllFirstBaseline, metrics: nil, views: ["view": labelPassword]))
        
        let separator = UIView()
        separator.backgroundColor = UIColor.lightGray
        separator.translatesAutoresizingMaskIntoConstraints = false
        credentialsView.addSubview(separator)
        // TODO: To be checked .alignAllFirstBaseLine...
        credentialsView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:[view(1)]-(0)-|", options: .alignAllFirstBaseline, metrics: nil, views: ["view": separator]))
        credentialsView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-(0)-[view]-(0)-|", options: .alignAllFirstBaseline, metrics: nil, views: ["view": separator]))
        
        self.view.addConstraint(NSLayoutConstraint(item: webView, attribute: .top, relatedBy: .equal,
            toItem: credentialsView, attribute: .bottom, multiplier: 1, constant: 0))
    }
    
    func checkConnectionReachability(completion: @escaping (Bool) -> Void) {
        let request = NSMutableURLRequest(url: URLGenerate204 as URL)
        if #available(iOS 9.0, *) {
            request.bind(to: command)
        }
        
        SharedSession.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
//                print(error.description)
                DDLogDebug(error.localizedDescription)
                completion(false)
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(false)
                return
            }
            
            if response.statusCode == 204 {
                completion(true)
            } else {
                completion(false)
            }
            
        }.resume()
    }
    
    func fixLinks() {
        let link = "https://service.thecloud.net"
        webView.stringByEvaluatingJavaScript(
            from: "var links = document.getElementsByTagName('a'); " +
            "for (var i=0; i < links.length; i++) { " +
                "var link = links[i]; var href = link.getAttribute('href'); " +
                "if (href.startsWith('/')) { " +
                    "link.setAttribute('href', '\(link)' + href); " +
                "} " +
            "}")
        
        webView.stringByEvaluatingJavaScript(
            from: "var imgs = document.getElementsByTagName('img'); " +
            "for (var i=0; i < imgs.length; i++) { " +
                "var img = imgs[i]; var src = img.getAttribute('src'); " +
                "if (src.startsWith('/')) { " +
                "img.setAttribute('src', '\(link)' + src); " +
                "} " +
            "}")
    }
    
    //MARK: Events
    func clickCancel(sender: AnyObject?) {
        delegate?.captiveWebViewControllerDidCancel(controller: self)
    }
    
    func tapCredentials(recognizer: UITapGestureRecognizer) {
        if recognizer.state == .ended {
            UIPasteboard.general.string = (recognizer.view as! UILabel).text!
            PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Copied!")
            PKHUD.sharedHUD.dimsBackground = false
            PKHUD.sharedHUD.show()
            PKHUD.sharedHUD.hide(afterDelay: 0.5)
        }
    }
}


//MARK: - UIWebViewDelegate
extension CaptiveWebViewController: UIWebViewDelegate {
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        Logger.logMessage(message: "captive shouldStart \(request.httpMethod!) \(request.url!)")
        DDLogDebug("captive shouldStart \(request.httpMethod!) \(request.url!)")
        
        if request.httpMethod == "POST" && request.httpBody != nil {
            updatedConfig = extractConfigurationFromRequest(request: request as NSURLRequest)! as [String : AnyObject]
            DDLogDebug("config gathered: \(String(describing: updatedConfig))")
            postInvoked = true
        }
        
        return true
    }
    
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        spinner.stopAnimating()
        if let html = webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML"),
           let url = webView.stringByEvaluatingJavaScript(from: "document.location.href") {
            Logger.logMessage(message: "captive finish url \(url)")
            DDLogDebug("captive finish url \(url)")
            
            Logger.logPage(html: html)
            Logger.flushMessages()
        }
        //fixLinks()
        if postInvoked {
            postInvoked = false
            delegate?.captiveWebViewControllerDidSubmitForm(controller: self, withConfiguration: updatedConfig, success: true)
//            Logger.logMessage(message: "captive post \(updatedConfig)")
            DDLogDebug("captive post \(String(describing: updatedConfig))")
            Logger.flushMessages()
        } else {
            let text = webView.stringByEvaluatingJavaScript(from: "document.body.innerText")
            if text?.range(of: "$invisible$") != nil {
                delegate?.captiveWebViewControllerDidFindSuccessPage(controller: self)
                Logger.logMessage(message: "captive success")
                DDLogDebug("captive success")
                
                Logger.flushMessages()
            }
        }
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        Logger.logMessage(message: "captive failedToLoad \(error?.description)")
        DDLogDebug("captive failedToLoad \(String(describing: error.localizedDescription))")
        
        spinner.stopAnimating()
        if postInvoked {
            postInvoked = false
            delegate?.captiveWebViewControllerDidSubmitForm(controller: self, withConfiguration: updatedConfig, success: false)
        }
    }
}


//MARK: - Extraction
extension CaptiveWebViewController {
    /**
    Exctracting wifi configutation from intercepted POST request from captive page.
    
    - parameter request:   intercepted HTTP POST request from captive authorization page
    
    - returns: wifi configuration with `url` and `params`
    */
    func extractConfigurationFromRequest(request: NSURLRequest) -> [String: Any]? {
        guard let url = request.url, let data = request.httpBody, let body = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }
        
        var formParams = [String]()
        var queryParams = [String]()
        var username: String?
        var password: String?
        
        //exctracting param names from form
        let paramNames = body.components(separatedBy: "&").map({ $0.components(separatedBy:"=").first! })

        //finding username and password param names
        for name in paramNames {
            if name.range(of: "username", options: .caseInsensitive) != nil {
                username = name
            } else if name.range(of: "password", options: .caseInsensitive) != nil {
                password = name
            } else {
                formParams.append(name)
            }
        }

        //exctracting param names from query
        if let query = url.query {
            queryParams = query.components(separatedBy: "&").map({ $0.components(separatedBy:"=").first! })
//            queryParams = query.componentsSeparatedByString("&").map({ $0.componentsSeparatedByString("=").first! })
        }

        if let username = username, let password = password {
            return [
                "url": url.absoluteString as AnyObject,
                "params": [
                    "username": username,
                    "password": password,
                    "extract": [
                        "form": formParams,
                        "query": queryParams
                    ]
                ]
            ]
        } else {
            return [
                "url": url.absoluteString as AnyObject,
                "params": [
                    "username": "",
                    "password": "",
                    "extract": [
                        "form": formParams,
                        "query": queryParams
                    ]
                ]
            ]

        }
    }
}
