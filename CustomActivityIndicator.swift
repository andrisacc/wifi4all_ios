//
//  HolderView.swift
//  SBLoader
//
//  Created by Victor Barskov on 24.07.16.
//  Copyright © 2016 plotkin. All rights reserved.
//

import UIKit

class CustomActivityIndicator: UIView {
    
    // MARK - Variables
    
    lazy fileprivate var animationLayer : CALayer = {
        return CALayer()
    }()
    
    let ovlLayer = OvalLayer()
    let firstWiFiLayer = FirstWiFiLayer()
    let secondWiFiLayer = SecondWiFiLayer()
    let thirdWiFiLayer = ThirdWiFiLayer()
    
    
    var isAnimating : Bool = false
    var hidesWhenStopped : Bool = true
    
    var parentFrame :CGRect = CGRect.zero
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        animationLayer.frame = frame
        self.layer.addSublayer(animationLayer)
        drawSpinner()
        pause(animationLayer)
        
        self.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func drawSpinner() {
        
        //        animationLayer.removeAllAnimations()
        animationLayer.sublayers = nil
        animationLayer.addSublayer(ovlLayer)
        ovlLayer.expand()
        Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(CustomActivityIndicator.firstWiFi),
                                               userInfo: nil, repeats: false)
    }
    
    func firstWiFi () {
        animationLayer.addSublayer(firstWiFiLayer)
        firstWiFiLayer.animate()
        Timer.scheduledTimer(timeInterval: 0.20, target: self, selector: #selector(CustomActivityIndicator.secondWiFi),
                                               userInfo: nil, repeats: false)
    }
    
    func secondWiFi () {
        animationLayer.addSublayer(secondWiFiLayer)
        secondWiFiLayer.animate()
        Timer.scheduledTimer(timeInterval: 0.15, target: self, selector: #selector(CustomActivityIndicator.thirdWiFi),
                                               userInfo: nil, repeats: false)
    }
    
    func thirdWiFi () {
        animationLayer.addSublayer(thirdWiFiLayer)
        thirdWiFiLayer.animate()
        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomActivityIndicator.drawSpinner),
                                               userInfo: nil, repeats: false)
    }
    
    func pause(_ layer : CALayer) {
        let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        
        layer.speed = 0.0
        layer.timeOffset = pausedTime
        
        isAnimating = false
    }
    
    func resume(_ layer : CALayer) {
        let pausedTime : CFTimeInterval = layer.timeOffset
        
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
        
        isAnimating = true
    }
    
    func startAnimating () {
        
        if isAnimating {
            return
        }
        
        if hidesWhenStopped {
            self.isHidden = false
        }
        resume(animationLayer)
    }
    
    func stopAnimating () {
        if hidesWhenStopped {
            self.isHidden = true
        }
        pause(animationLayer)
    }
    
}

class OvalLayer: CAShapeLayer {
    
    let animationDuration: CFTimeInterval = 0.3
    
    override init() {
        super.init()
        fillColor = UIColor.whiteBrandColor().cgColor
        path = ovalPathSmall.cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var ovalPathSmall: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 35.0, y: 35.0, width: 0.0, height: 0.0))
    }
    
    var ovalPathLarge: UIBezierPath {
        return UIBezierPath(ovalIn: CGRect(x: 30, y: 35, width: 10, height: 10))
    }
    
    
    func expand() {
        let expandAnimation: CABasicAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = ovalPathSmall.cgPath
        expandAnimation.toValue = ovalPathLarge.cgPath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.isRemovedOnCompletion = false
        add(expandAnimation, forKey: nil)
    }
    
}

class FirstWiFiLayer: CAShapeLayer {
    
    let animationDuration: CFTimeInterval = 0.01
    let desiredLineWidth: CGFloat = 5
    
    override init() {
        super.init()
        //fillColor = Colors.red.CGColor
        path = bezi.cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var bezi: UIBezierPath {
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 35.0, y: 40), radius: 10, startAngle: CGFloat(M_PI + M_PI/4), endAngle: CGFloat(Double.pi + Double.pi - Double.pi/4), clockwise: true)
        
        // Setup the CAShapeLayer with the path, colors, and line width
        self.path = circlePath.cgPath
        self.fillColor = UIColor.clear.cgColor
        self.strokeColor = UIColor.whiteBrandColor().cgColor
        self.lineWidth = desiredLineWidth
        
        // Don't draw the circle initially
        self.strokeEnd = 0.0
        
        return circlePath
    }
    
    
    func animate() {
        
        let strokeAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        strokeAnimation.fromValue = 0
        strokeAnimation.toValue = 1
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        strokeAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        strokeAnimation.duration = animationDuration
        
        self.strokeEnd = 1.0
        
        // Do the actual animation
        self.add(strokeAnimation, forKey: "animate")
        
    }
    
}

class SecondWiFiLayer: CAShapeLayer {
    
    let animationDuration: CFTimeInterval = 0.01
    let desiredLineWidth: CGFloat = 5
    
    override init() {
        super.init()
        path = bezi.cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var bezi: UIBezierPath {
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 35.0, y: 40), radius: 17.5, startAngle: CGFloat(M_PI + M_PI/4), endAngle: CGFloat(Double.pi + Double.pi - Double.pi/4), clockwise: true)
        
        // Setup the CAShapeLayer with the path, colors, and line width
        self.path = circlePath.cgPath
        self.fillColor = UIColor.clear.cgColor
        self.strokeColor = UIColor.whiteBrandColor().cgColor
        self.lineWidth = desiredLineWidth
        
        // Don't draw the circle initially
        self.strokeEnd = 0.0
        
        return circlePath
    }
    
    
    func animate() {
        
        let strokeAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        strokeAnimation.fromValue = 0
        strokeAnimation.toValue = 1
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        strokeAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        strokeAnimation.duration = animationDuration
        
        self.strokeEnd = 1.0
        
        // Do the actual animation
        self.add(strokeAnimation, forKey: "animate")
        
    }
    
}

class ThirdWiFiLayer: CAShapeLayer {
    
    let animationDuration: CFTimeInterval = 0.01
    let desiredLineWidth: CGFloat = 5
    
    override init() {
        super.init()
        //fillColor = Colors.red.CGColor
        path = bezi.cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var bezi: UIBezierPath {
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 35.0, y: 40), radius: 25.0, startAngle: CGFloat(Double.pi + Double.pi/4), endAngle: CGFloat(Double.pi + Double.pi - Double.pi/4), clockwise: true)
        
        // Setup the CAShapeLayer with the path, colors, and line width
        self.path = circlePath.cgPath
        self.fillColor = UIColor.clear.cgColor
        self.strokeColor = UIColor.whiteBrandColor().cgColor
        self.lineWidth = desiredLineWidth
        
        // Don't draw the circle initially
        self.strokeEnd = 0.0
        
        return circlePath
    }
    
    func animate() {
        
        let strokeAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        strokeAnimation.fromValue = 0
        strokeAnimation.toValue = 1
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        strokeAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        strokeAnimation.duration = animationDuration
        
        self.strokeEnd = 1.0
        strokeAnimation.isRemovedOnCompletion = true
        
        // Do the actual animation
        self.add(strokeAnimation, forKey: "animate")
    }
    
}

open class CustomActivity: UIView {
    
    open class var sharedInstance: CustomActivity {
        struct Singleton {
            static let instance = CustomActivity()
        }
        return Singleton.instance
    }
    
    fileprivate let container: UIView = UIView()
    fileprivate var loadingView: UIView = UIView()
    fileprivate var customActivityView = CustomActivityIndicator()
    fileprivate var label: UILabel = UILabel()
    
    open func showActivityIndicator(_ uiView: UIView, loadingLabelValue: String) {
        
        container.frame = uiView.frame
        container.center = uiView.center
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 70.0, height: 70.0)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.UIColorFromHex(0x333333, alpha: 0.95)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        label = UILabel(frame: CGRect(x: 10, y: 60, width: 60, height: 21))
        label.center = CGPoint(x: 35, y: 61)
        label.font = UIFont.brandFont()
        label.textColor = UIColor.whiteBrandColor()
        label.textAlignment = NSTextAlignment.center
        label.text = loadingLabelValue
        
        customActivityView.frame = uiView.frame
        customActivityView.parentFrame = uiView.frame
        
        loadingView.addSubview(label)
        loadingView.addSubview(customActivityView)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        
        customActivityView.startAnimating()
        
    }
    
    open func hideActivityIndicator(_ uiView: UIView) {
        
        DispatchQueue.main.async {
            
            self.customActivityView.stopAnimating()
            self.container.removeFromSuperview()
            
            self.loadingView.removeFromSuperview()
            self.customActivityView.isHidden = true
            
            self.customActivityView.removeFromSuperview()
            
            self.label.removeFromSuperview()
            
        }
    }
}

extension UIColor {
    
    class func whiteBrandColor() -> UIColor {
        return UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.9)
    }
        
    class func UIColorFromHex(_ rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}

extension UIFont {
    
    class func brandFont() -> UIFont {
        return UIFont.boldSystemFont(ofSize: 10.0)
    }
}



