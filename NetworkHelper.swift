//
//  NetworkHelper.swift
//  WifiHotspot
//
//  Created by Sergey Pronin on 8/4/15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

import Foundation
import UIKit
import NetworkExtension
import CoreLocation
import SystemConfiguration.CaptiveNetwork
import CocoaLumberjack

extension NSMutableURLRequest {
    func addAitaAuth() {
        if let aitaToken = UserDefaults.standard.object(forKey: AitaToken) as? String {
            self.setValue(aitaToken, forHTTPHeaderField: "WiFi-User-Token")
        } else if let userID = UserDefaults.standard.object(forKey: DeviceID) as? String {
            self.setValue(userID, forHTTPHeaderField: "Aita-User")
        }
    }
}

// Send current connection status to server

func reportCurrentSessionStatus(completion: ((Bool) -> Void)? = nil) {
    
    let defaults = UserDefaults.standard
    
    guard defaults.object(forKey: AitaToken) != nil || defaults.object(forKey: DeviceID) != nil else { completion?(false); return }
    guard let ssid = NetworkHelper.instance.currentSSID else { completion?(false); return }
    
    let request = NSMutableURLRequest(url: NSURL(string: BaseURL + "wifi/sessions")! as URL)
    request.addAitaAuth()
    request.httpMethod = "POST"
    let params = [
        "data_used": DataUsageMonitoring.currentWifiDataUsage().description,
        "ssid": ssid
    ]
//    if let location = NetworkHelper.instance.locationManager?.location {
//        params["lat"] = location.coordinate.latitude.description
//        params["lon"] = location.coordinate.longitude.description
//    }
    
    request.httpBody = params.URLEncodedString!.data(using: String.Encoding.utf8)
    NetworkHelperSession.dataTask(with: request as URLRequest) { data, response, error in
        completion?(error == nil)
        }.resume()
}

//public func shareCurrentLocationInBackGround () -> /*(lat: Double, lon: Double )*/CLLocation {
//    
//    var locationShare = CLLocation()
//    
//    if let location = NetworkHelper.instance.locationManager?.location {
//        locationShare = location
//    }
//    //return (locationShare.coordinate.latitude, locationShare.coordinate.longitude)
//    return locationShare
//    
//}

public var currentSSID: String? {
get {
    return NetworkHelper.instance.currentSSID
}
}

@available(iOS 9.0, *)
public var currentNetwork: NEHotspotNetwork? {
    return (NEHotspotHelper.supportedNetworkInterfaces() as? [NEHotspotNetwork])?.first
}

public func provideRemoteNotificationsToken(token: NSData) {
    //TODO: is it needed?
    //    var pushToken = token.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
    //    pushToken = pushToken.stringByReplacingOccurrencesOfString(" ", withString: "")
    //
    //    let defaults = NSUserDefaults.standardUserDefaults()
    //    defaults.setObject(pushToken, forKey: PushToken)
    //    defaults.synchronize()
    //
    //    guard let aitaToken = NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String else { return }
    //    guard let deviceID = NSUserDefaults.standardUserDefaults().objectForKey(DeviceID) as? String else { return }
    //    let request = NSMutableURLRequest(URL: NSURL(string: BaseURL + "user/device")!)
    //    request.HTTPMethod = "POST"
    //    request.setValue(pushToken, forHTTPHeaderField: "Aita-Apns-Token")
    //    request.setValue(aitaToken, forHTTPHeaderField: "Aita-Token")
    //    request.setValue(deviceID, forHTTPHeaderField: "Aita-User")
    //    request.setValue("wifi", forHTTPHeaderField: "Aita-App-Name")
    //    request.setValue(NSLocale.preferredLanguages().first ?? "en", forHTTPHeaderField: "Aita-Device-Language")
    //    request.setValue(DataUsageMonitoring.deviceName(), forHTTPHeaderField: "Aita-Device-Name")
    //
    //    NetworkHelperSession.dataTaskWithRequest(request).resume()
}

//public let AitaToken = "HotspotAitaToken"
public let PushToken = "HotspotPushToken"
public let DeviceID = "HotspotDeviceID"

let NetworkHelperSession = URLSession(configuration: URLSessionConfiguration.default)

//FIXME: internal
public class NetworkHelper: NSObject {
    
    //Notification for plushki on adding of free hotspot
    
    var plushkiNotificationMade = UserDefaults.standard.bool(forKey: "plushkiNotification")
    
    
    var currentLocation : CLLocation!
    var testCurrentLocation: CLLocation!
    
    //FIXME: internal
    public static let instance = NetworkHelper()
    var helperName: String?
    var captiveController: CaptiveWebViewController?
    
    //FIXME: internal
    public var shouldHandleWebViewRequests = false
    
    //workaround for 8.0 and 9.0 compatibility
    private var _command: AnyObject?
    
    @available(iOS 9.0, *)
    var lastCommand: NEHotspotHelperCommand? {
        get { return _command as? NEHotspotHelperCommand }
        set { _command = newValue }
    }
    
    var active = false
    var currentConfiguration: WifiConfiguration?
    
    //iOS 8
    private var reachabilityConnection: Reachability!
    private var reachabilityWifi: Reachability!
    var locationManager: CLLocationManager!
    private var timer: Timer!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private override init() {
        
        super.init()
        
        if #available(iOS 9.0, *) {
            
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.distanceFilter = 300
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            locationManager.pausesLocationUpdatesAutomatically = false
            if #available(iOS 9.0, *) {
                locationManager.allowsBackgroundLocationUpdates = true
            }
            if CLLocationManager.authorizationStatus() == .notDetermined {
//                locationManager.requestAlwaysAuthorization()
            }

        }
        
        requestNotificationsAccess()
    }
    
    var connectionHelper: HotspotConnectionHelper?
    
    @available(iOS 9.0, *)
    public var currentNetwork: NEHotspotNetwork? {
        return (NEHotspotHelper.supportedNetworkInterfaces() as? [NEHotspotNetwork])?.first
    }
    
    // Secured or not
    
    var secured: Bool {
        get {
            if #available(iOS 9.0, *) {
                let interfaces = NEHotspotHelper.supportedNetworkInterfaces() as! [NEHotspotNetwork]
                if let wifiNetwork = interfaces.first {
                    return wifiNetwork.isSecure
                } else {
                    return false
                }
            }
            else {
                return false
            }
        }
    }
    
    // Here we got SSID of the Network connected
    var currentSSID: String? {
        get {
            if #available(iOS 9.0, *) {
                let interfaces = NEHotspotHelper.supportedNetworkInterfaces() as! [NEHotspotNetwork]
                if let wifiNetwork = interfaces.first {
                    return wifiNetwork.ssid
                } else {
                    return nil
                }
            } else {
                guard CNCopySupportedInterfaces() != nil else { return nil }
                
                if let currentWiFi = CNCopyCurrentNetworkInfo("en0" as CFString) {
                    return (currentWiFi as NSDictionary)["SSID"] as? String
                } else {
                    return nil
                }
            }
        }
    }
    
    func shutdown() {
        if #available(iOS 9.0, *) {
            NEHotspotHelper.register(options: nil, queue: DispatchQueue.main) { cmd in
                switch cmd.commandType {
                case .filterScanList:
                    let response = cmd.createResponse(.success)
                    response.setNetworkList([])
                    response.deliver()
                default:
                    break
                }
            }
        } else {
            CNSetSupportedSSIDs([] as CFArray)
        }
    }
    
    func prepareCaptiveConfiguration() {
        let captiveNetworks = WifiConfiguration.allSSIDs().map({ $0 as CFTypeRef })
        let res = CNSetSupportedSSIDs(captiveNetworks as CFArray)
        //        DDLogDebug("\(captiveNetworks)")
        DDLogDebug("\(res)")
    }
    
    @available(iOS 9.0, *)
    
    func setupNetworkExtension() {
        
        //        DDLogDebug("NEHotspotHelper.supportedNetworkInterfaces(): \(NEHotspotHelper.supportedNetworkInterfaces())")
        
        // Here we realize that name of the heleper will be displayed properly
        var options: [String: String]?
        if let helperName = helperName {
            options = [kNEHotspotHelperOptionDisplayName: helperName]
        }
        
        // Here we call registerWithOptions to register the application as a HotspotHelper
        
        let res = NEHotspotHelper.register(options: options as [String : NSObject]?, queue:DispatchQueue.global(qos: .background)) { cmd in
            
            guard self.active else {
                let response = cmd.createResponse(.failure)
                response.deliver()
                return
                
            }
            
            switch cmd.commandType {
                
            case .filterScanList:
                
                guard let list = cmd.networkList else {
                    let response = cmd.createResponse(.failure)
                    response.deliver()
                    break
                }
                
                var matchedNetworks = [NEHotspotNetwork]()
                
                for network in list {
                    
                        if let config = WifiConfiguration.configurationForNetworkWithSSID(ssid: network.ssid) {

                            if !config.paid /*|| HotspotCredentials.hasValidCredentials()*/ {

                                network.setConfidence(.high)
                                DispatchQueue.main.sync {
                                    if let password = config.params["hotspot_password"].string {
                                    network.setPassword(password)
                                    }
                                }
                                matchedNetworks.append(network)
                            }
                            else if config.paid {
                                network.setConfidence(.high)
                                matchedNetworks.append(network)
                            }
                        } else {
                            network.setConfidence(.none) // It is better to set something here
                        }
                    
                }
                
                // Notification for motivation to add the hotspot which user connected to
                
                let currentNetSSID = self.currentSSID ?? ""
                if self.secured {
                    self.notifyOnConnectionWOAdd(firstAppearemce: true, currentSSID: currentNetSSID)
                }

                DDLogDebug("FilterScanList \(matchedNetworks.count) out of \(list.count)")
                
                let response = cmd.createResponse(.success)
                response.setNetworkList(matchedNetworks)
                response.deliver()
                
                DDLogDebug("HereWeOnFilterScanList")
                
            case .evaluate:
                
                DDLogDebug("evaluate \(String(describing: cmd.network))")
                
                guard let network = cmd.network, WifiConfiguration.configurationForNetworkWithSSID(ssid: network.ssid) != nil
                    else {cmd.createResponse(.failure).deliver(); break }
                
                self.currentConfiguration = nil
                
                if let config = WifiConfiguration.configurationForNetworkWithSSID(ssid: network.ssid) {
                    if !config.paid /*|| HotspotCredentials.hasValidCredentials()*/ {
                        network.setConfidence(.high)
                        DispatchQueue.main.sync {
                            if let password = config.params["hotspot_password"].string {
                                network.setPassword(password)
                            }
                        } 
                    }
                    else if config.paid {
                        network.setConfidence(.high)
                    }
                } else {
                    network.setConfidence(.none)
                }
                
                let response = cmd.createResponse(.success)
                response.setNetwork(network)
                response.deliver()
                
                DDLogDebug("HereWeOnEvaluate")
                
            case .authenticate:
                
                DDLogDebug("authenticate \(cmd)")
                
                guard let network = cmd.network, let config = WifiConfiguration.configurationForNetworkWithSSID(ssid: network.ssid)
                        else {cmd.createResponse(.failure).deliver(); break }
                
                if config.paid && !HotspotCredentials.hasValidCredentials() {
                    cmd.createResponse(.failure).deliver()
                    return
                }
                
                self.lastCommand = nil
                self.dismissCaptiveUI()
                
                self.currentConfiguration = config
                
                if config.params["hotspot_password"].string != nil {
                    
                    DDLogDebug("hotspot_password network, deliver success")
                    cmd.createResponse(.success).deliver()
                    
//                    DDLogDebug("Config on Auth: \(config.params, config.id)")
                    
//                    let currentCoords = shareCurrentLocationInBackGround()
//                    self.connectionHelper = HotspotConnectionHelper(command: cmd, configuration: config)
//                    self.connectionHelper!.checkConnectionReachability { success in
//                        
//                        if success {
//                            
//                            DDLogDebug("Authenticate Success")
//                            AccessLogger.sharedInstance.successAccess(pointId: config.id, latitude: currentCoords.coordinate.latitude, longitude: currentCoords.coordinate.longitude)
//                            
//                        } else {
//                            
//                            DDLogDebug("Authenticate NO connection reachablity...")
//                            
//                            let point = failedHotspot(pointID: config.id, latitude: currentCoords.coordinate.latitude, longitude: currentCoords.coordinate.longitude)
//                            
//                            DDLogDebug("Point: \(point.point)")
//                            
//                            if let points = NSUserDefaults.standardUserDefaults().objectForKey("failedHotspots") as? NSData {
//                                let pointz = NSKeyedUnarchiver.unarchiveObjectWithData(points) as! NSMutableArray
//                                pointz.addObject(point)
//                                let data = NSKeyedArchiver.archivedDataWithRootObject(pointz)
//                                NSUserDefaults.standardUserDefaults().setObject(data, forKey: "failedHotspots")
//                            } else {
//                                let points = NSMutableArray()
//                                points.addObject(point)
//                                let data = NSKeyedArchiver.archivedDataWithRootObject(points)
//                                NSUserDefaults.standardUserDefaults().setObject(data, forKey: "failedHotspots")
//                            }
//                        }
//                    }
                    
                } else if HotspotCredentials.credentialsAcquired {
                    
                    DDLogDebug("gonna connect")
                    self.connectionHelper = HotspotConnectionHelper(command: cmd, configuration: config)
                    self.connectionHelper!.connect()
                    
                } else {
                    
                    DDLogDebug("no credentials")
                    self.sendNeedsPaymentNotification()
                    cmd.createResponse(.failure).deliver()
                }
                
                DDLogDebug("HereWeOnAuthenticate")
                
            case .presentUI:
                
                guard let network = cmd.network, let config = WifiConfiguration.configurationForNetworkWithSSID(ssid: network.ssid)
                    else { cmd.createResponse(.failure).deliver(); break }
                
//                Logger.logMessage("present ui for config \(config.params.rawValue)")
                  DDLogDebug("present ui for config \(config.params.rawValue)")
                
                self.lastCommand = cmd
                self.presentCaptiveUI(configuration: config)
                
                DDLogDebug("HereWeOnPresentUI")
                
            case .maintain:
                
                guard let network = cmd.network, let config = WifiConfiguration.configurationForNetworkWithSSID(ssid: network.ssid)
                    else { cmd.createResponse(.failure).deliver(); break }
                self.connectionHelper = HotspotConnectionHelper(command: cmd, configuration: config)
                self.connectionHelper!.checkConnectionReachability { success in
                    if success {
                        cmd.createResponse(.success).deliver()
                    } else {
                        cmd.createResponse(.authenticationRequired).deliver()
                    }
                }
                DDLogDebug("HereWeOnMaintain")
                
            default:
                self.currentConfiguration = nil
                DDLogDebug("default \(cmd)")
                break
            }
        }
        DDLogDebug("res \(res)")
    }
    
    
    func checkConnectionStatus() {
        
        guard let ssid = currentSSID, let config = WifiConfiguration.configurationForNetworkWithSSID(ssid: ssid) else { return }
        
        connectionHelper = HotspotConnectionHelper(configuration: config)
        connectionHelper!.connect()
        
    }
    
    func presentCaptiveUI(configuration: WifiConfiguration) {
        guard let window = UIApplication.shared.windows.first else { return }
        
        shouldHandleWebViewRequests = true
        captiveController = CaptiveWebViewController()
        if #available(iOS 9, *) {
            captiveController!.command = lastCommand
        }
        captiveController!.wifiConfiguration = configuration
        captiveController!.delegate = self
        
        window.rootViewController = UINavigationController(rootViewController: self.captiveController!)
        
        //        window.rootViewController?.presentViewController(,
        //            animated: false, completion: nil)
    }
    
    func dismissCaptiveUI() {
        shouldHandleWebViewRequests = false
        captiveController?.navigationController?.presentingViewController?.dismiss(animated: false, completion: nil)
        captiveController = nil
    }
    
    func updateBackgroundTask() {
        var task: UIBackgroundTaskIdentifier?
        task = UIApplication.shared.beginBackgroundTask {
            if let task = task {
                UIApplication.shared.endBackgroundTask(task)
            }
        }
    }
    
    //MARK: User Notifications
    private func requestNotificationsAccess() {
        let settings = UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
    }
    
    private func sendNeedsPaymentNotification() {
        
        if let _ = UserDefaults.standard.object(forKey: AitaToken) as? String {
        }
        
        DispatchQueue.main.async {
            let notification = UILocalNotification()
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            notification.userInfo = [
                HotspotHelperPaymentNeededNotification: true,
            ]
            notification.alertBody = NSLocalizedString("You need to have active subscription in order to connect.", comment: "")
            if #available(iOS 8.2, *) {
                notification.alertTitle = NSLocalizedString("Subscribe", comment: "")
            }
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
    
    private func sendLocalNotification(title: String, body: String) {
        
        DispatchQueue.main.async {
            let notification = UILocalNotification()
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            notification.userInfo = [
                HotspotHelperPaymentNeededNotification: true,
            ]
            notification.alertBody = body
            if #available(iOS 8.2, *) {
                notification.alertTitle = title
            }
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
    
    // MARK: NSNotificationCenter
    func reachabilityChangedNotification(notification: NSNotification) {
        
        let networkReachability = notification.object as! Reachability;
        let remoteHostStatus = networkReachability.currentReachabilityStatus
        
        switch remoteHostStatus {
        case .notReachable:
            DDLogDebug("Not Reachable")
            dismissCaptiveUI()
        case .reachableViaWiFi:
            DDLogDebug("Reachable Wifi")
            checkConnectionStatus()
        default:
            DDLogDebug("Reachable")
        }
    }
    
    @available(iOS 9.0, *)
    func powerModeChangedNotification() {
        SQLQueue.maxConcurrentOperationCount = ProcessInfo.processInfo.isLowPowerModeEnabled ? 2 : 10
    }
    
    //MARK: Setup
    public class func sendLogsIfAny() {
        Logger.sendLogs()
    }
    
    public class func setServerURL(url: String) {
        BaseURL = url
        BaseURLInsecured = url.replacingOccurrences(of:"https", with: "http")
    }
    
    public class func setupHotspotsMonitoring(token: String? = nil, userID: String? = nil, helperName: String? = nil) {
        
//        DDLogDebug("Here is token before I try to save it: \(NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String)")
        
        let defaults = UserDefaults.standard
        if let token = token {
            DDLogDebug("I'm here now starting to save the token")
            defaults.set(token, forKey: AitaToken)
        } else if let userID = userID {
            defaults.set(userID, forKey: DeviceID)
        }
//        else {
//            return
//        }
        
        defaults.synchronize()
        
//        DDLogDebug("Here is saved token after login: \(NSUserDefaults.standardUserDefaults().objectForKey(AitaToken) as? String)")
        
        NetworkHelper.instance.active = true
        NetworkHelper.instance.helperName = helperName
        if #available(iOS 9.0, *) {
            NetworkHelper.instance.setupNetworkExtension()
        } else {
            NetworkHelper.instance.checkConnectionStatus()
        }
        
        // HotspotCredentials.updateCredentials()
        HotspotCredentials.updateRulesDB()
        
        WifiConfiguration.fetchWifiConfiguration()
        
        URLProtocol.registerClass(CaptiveURLProtocol.self)
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    public class func shutdownHotspotsMonitoring(helperName: String? = nil) {
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: AitaToken)
        defaults.removeObject(forKey: DeviceID)
        
        defaults.synchronize()
        
        HotspotCredentials.username = ""
        HotspotCredentials.password = ""
        
        // Clean up keychain
        
        HotspotKeychain["username"] = nil
        HotspotKeychain["password"] = nil
        HotspotKeychain["expiration"] = nil
        
        //        DDLogDebug("HotspotKeychain[username]: \(HotspotKeychain["username"])")
        //        DDLogDebug("HotspotKeychain[password]: \(HotspotKeychain["password"])")
        //        DDLogDebug("HotspotKeychain[expiration]: \(HotspotKeychain["password"])")
        
        
        NetworkHelper.instance.active = false
        NetworkHelper.instance.helperName = helperName
        if #available(iOS 9.0, *) {
            NetworkHelper.instance.setupNetworkExtension()
        } else {
            NetworkHelper.instance.checkConnectionStatus()
        }
    }
    
    public class func processLocalNotification(notification: UILocalNotification) -> Bool {
        
        guard let userInfo = notification.userInfo else { return false }
        
        if userInfo[HotspotHelperPresentUINotification] as? Bool == true {
            if #available(iOS 9.0, *) { }
            else {
                NetworkHelper.instance.presentCaptiveUI(configuration: WifiConfiguration(json: JSON(userInfo[HotspotHelperConfigurationKey]!)))
            }
            return true
        } else {
            return false
        }
    }
    
    public class func handleRemoteNotification(userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        reportCurrentSessionStatus { completionHandler( $0 ? .newData : .failed ) }
    }
    
    
    private func notifyOnConnectionWOAdd (firstAppearemce: Bool, currentSSID: String) {
        
        if !self.plushkiNotificationMade {
        
            self.plushkiNotificationMade = firstAppearemce
            
            let timeInterval = NSDate().timeIntervalSince1970
            UserDefaults.standard.set(timeInterval, forKey: "PlushkaNotificationStamp")
            UserDefaults.standard.set(true, forKey: "plushkiNotification")
            
            if let _ = WifiConfiguration.configurationForNetworkWithSSID(ssid: currentSSID) {
                // Do nothing here
            } else {
                if self.currentSSID == "" {
                    self.sendLocalNotification(title: NSLocalizedString("Hey!", comment: ""), body: NSLocalizedString("Connect to the secured WiFi, add it and get access to > 500 000 hotspots in the world", comment: ""))
                } else  {
                    self.sendLocalNotification(title: NSLocalizedString("Please add the network", comment: ""), body: NSLocalizedString("Please share your network with others to make the world a bit better", comment: ""))
                }
            }
            
        } else {
            
            let currentUnixTime = NSDate().timeIntervalSince1970
            guard let lastTimeStamp = UserDefaults.standard.object(forKey: "PlushkaNotificationStamp") as? TimeInterval else {return}
            let timePasseSinceLastNPlushka = currentUnixTime - lastTimeStamp
            
            if  timePasseSinceLastNPlushka > 604800 {
                
                let timeInterval = NSDate().timeIntervalSince1970
                
                if let _ = WifiConfiguration.configurationForNetworkWithSSID(ssid: currentSSID) {
                    // Do nothing here
                } else {
                    if self.currentSSID == "" {
                        // Do nothing here
                        self.sendLocalNotification(title: NSLocalizedString("Hey!", comment: ""), body: NSLocalizedString("Connect to the secured WiFi, add it and get access to > 500 000 hotspots in the world", comment: ""))
                        UserDefaults.standard.set(timeInterval, forKey: "PlushkaNotificationStamp")
                    } else  {
                        self.sendLocalNotification(title: NSLocalizedString("Please add the network", comment: ""), body: NSLocalizedString("Please share your network with others to make the world a bit better", comment: ""))
                        UserDefaults.standard.set(timeInterval, forKey: "PlushkaNotificationStamp")
                    }
                }
            }
        }
    }
}


//MARK: - CLLocationManagerDelegate
extension NetworkHelper: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
                if status == .authorizedAlways {
                    locationManager.startUpdatingLocation()
                }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let _ = locations.last else { return }
        
        //        DDLogDebug("location updated \(location.coordinate.latitude),\(location.coordinate.longitude)")
        locationManager.stopUpdatingLocation()
    }
    
    
//    public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let location = locations.last else { return }
//        
//        //        DDLogDebug("location updated \(location.coordinate.latitude),\(location.coordinate.longitude)")
//        locationManager.stopUpdatingLocation()
//    }
}


//MARK: - CaptiveWebViewControllerDelegate
extension NetworkHelper: CaptiveWebViewControllerDelegate {
    
    func captiveWebViewControllerDidSubmitForm(controller: CaptiveWebViewController, withConfiguration configuration: [String : AnyObject]?, success: Bool) {
        
        var config = configuration
        
        shouldHandleWebViewRequests = false
        if #available(iOS 9.0, *) {
            lastCommand = nil
        }
        
        connectionHelper!.checkConnectionReachability { reachable in
            guard let window = UIApplication.shared.windows.first else { return }
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            window.rootViewController = storyboard.instantiateInitialViewController()
            
            let message: String
            
            if reachable {
                message = NSLocalizedString("You were successfully authenticated in the network", comment: "")
                if #available(iOS 9.0, *) {
                    controller.command.createResponse(.success).deliver()
                } else {
                    CNMarkPortalOnline("en0" as CFString)
                }
                if config != nil {
                    config!["id"] = self.connectionHelper!.config.id as AnyObject
                    WifiConfiguration.updateConfiguration(configuration: config!)
                }
                
                reportCurrentSessionStatus()
                
            } else {
                message = NSLocalizedString("It was impossible to authorize you in the network", comment: "")
                if #available(iOS 9.0, *) {
                    controller.command.createResponse(.failure).deliver()
                } else {
                    CNMarkPortalOffline("en0" as CFString)
                }
            }
            
            let alertController = UIAlertController(title: NSLocalizedString("Information", comment: ""), message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            window.rootViewController?.present(
                alertController, animated: true, completion: nil)
        }
    }
    
    func captiveWebViewControllerDidCancel(controller: CaptiveWebViewController) {
        shouldHandleWebViewRequests = false
        if #available(iOS 9.0, *) {
            lastCommand = nil
        }
        
        if #available(iOS 9.0, *) {
            controller.command.createResponse(.failure).deliver()
        } else {
            CNMarkPortalOffline("en0" as CFString)
        }
        
        guard let window = UIApplication.shared.windows.first else { return }
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        window.rootViewController = storyboard.instantiateInitialViewController()
    }
    
    func captiveWebViewControllerDidFindSuccessPage(controller: CaptiveWebViewController) {
        captiveWebViewControllerDidSubmitForm(controller: controller, withConfiguration: nil, success: true)
    }
    
    
}
//
//extension NetworkHelper: LocationUpdateProtocol {
//
//    // MARK: - LocationUpdateProtocol
//    public func locationDidUpdateToLocation(location: CLLocation) {
//        testCurrentLocation = location
//        print("Latitude : \(self.currentLocation.coordinate.latitude)")
//        print("Longitude : \(self.currentLocation.coordinate.longitude)")
//    }

//}

extension NetworkHelper {
    
    // MARK: - Notifications
    
    func deliverStatusNotification(message: String) {
        
        DispatchQueue.main.async {
            let notification = UILocalNotification()
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            notification.alertBody = message
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
}
