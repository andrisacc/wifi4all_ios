//
//  Logger.swift
//  WifiCaptive
//
//  Created by Sergey Pronin on 9/30/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit

public class Logger: NSObject {
    static let shared = Logger()
    
    static let logsSession: URLSession = {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 90
        config.timeoutIntervalForResource = 90
        return URLSession(configuration: config)
    }()
    
    static let logsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("logs")
    
    private var messages = [String]()
    
    private override init() {
        super.init()
        
        do {
            try FileManager.default.createDirectory(at: Logger.logsDirectoryURL, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print(error)
        }
    }
    
    class var timestamp: TimeInterval {
        return NSDate().timeIntervalSince1970
    }
    
    var analytics: ((_ event: String, _ label: String?) -> Void)?
    
    public class func logEvent(event: String, label: String? = nil) {
        shared.analytics?(event, label)
    }
    
    public class func setLoggerAnalytics(analytics: @escaping (_ event: String, _ label: String?) -> Void) {
        shared.analytics = analytics
    }
    
    public class func logMessage(message: String) {
        shared.messages.append("[\(timestamp)]: \(message)")
        print(message)
        shared.analytics?("wifi_log", message)
    }
    
    class func flushMessages() {
        let fileURL = logsDirectoryURL.appendingPathComponent("\(timestamp).log")
        
        if shared.messages.count == 0 {
            return
        }
        
        do {
            try shared.messages.joined(separator: "\n").write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
            shared.messages.removeAll()
        } catch let error as NSError {
            print(error.description)
        }
    }
    
    public class func logPage(html: String) {
        print(html)
        let fileURL = logsDirectoryURL.appendingPathComponent("\(timestamp)html.log")
        
        do { try html.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8) }
        catch { }
    }
    
    public class func sendLogs(completion: ((Bool, String) -> Void)? = nil) {
        flushMessages()
        
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: logsDirectoryURL,
                includingPropertiesForKeys: nil, options: []).filter({ $0.pathExtension == "log" })
            
            if fileURLs.count == 0 {
                completion?(false, "no logs")
                return
            }
            
            let request = NSMutableURLRequest(url: NSURL(string: "http://iappintheair.appspot.com/report_hotspots")! as URL)
            request.httpMethod = "POST"
            let boundary = "appintheair123456"
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.addAitaAuth()
            
            let body = NSMutableData()
            body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"count\"\r\n\r\n\(fileURLs.count)".data(using: String.Encoding.utf8)!)
            
            for (i, fileURL) in fileURLs.enumerated() {
                body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"html\(i)\"; filename=\"\(i).html\"\r\n"
                    .data(using: String.Encoding.utf8)!)
                body.append("Content-Type: text/html\r\n\r\n".data(using: String.Encoding.utf8)!)
                do {
                    let data = try Data(contentsOf: fileURL as URL)
                    body.append(data)
                } catch {
                    print("Unable to load data: \(error)")
                }
//                if let data = NSData(contentsOfURL: fileURL) {
//                    body.appendData(data)
//                }
            }
            body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            logsSession.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                if error == nil {
                    do {
                        for url in fileURLs {
                            try FileManager.default.removeItem(at: url)
                        }
                    } catch { }
                }
                
                DispatchQueue.main.async {
                    completion?(error == nil, "\(String(describing: error?.localizedDescription))")
                }
                
            }).resume()
        } catch let error as NSError {
            completion?(false, error.description)
        } catch { }
    }

}
